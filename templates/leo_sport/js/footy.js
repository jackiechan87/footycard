jQuery(document).ready(function () {

  jQuery('input[name=category]').change(function(){
    var categoryId = this.value,
        view = this.getAttribute('cateview'),
        formAction = 'index.php?option=com_virtuemart&view='+view+'&virtuemart_category_id='+ categoryId;

    jQuery("form#submit-change-category").attr("action", formAction);
    jQuery('form#submit-change-category').submit();
  });

  if(jQuery('#name_field').length){
    jQuery('#name_field').parents('tr').hide();
    jQuery('#first_name_field').blur(function() {
       var valueFirstName = jQuery('#first_name_field').val();
      jQuery('#name_field').val(valueFirstName);
    });
  }

  //event click event
  jQuery('a.mod_events_daylink').live('click',function(){

    var ele = this,
        href = ele.getAttribute('href');
    jQuery.ajax({
      url:href,
      success:function(result){
        var divparent = jQuery(ele).closest('div');
          divparent.find('ul.ev_ul').remove();
          divparent.append(result);
      }
    });

    return false;
  });

  //event click event
  jQuery('a.mod_events_link').live('click',function(){
    return false;
  });
});