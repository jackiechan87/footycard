<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.5
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<div class="reset<?php echo $this->pageclass_sfx?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
	<h1><span><span>
		<?php //echo $this->escape($this->params->get('page_heading')); ?>
		<?php echo JText::_('Forgot Your Password?'); ?>
        </span></span>
	</h1>
	<?php endif; ?>

	<form id="user-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.request'); ?>" method="post" class="form-validate">

		<?php foreach ($this->form->getFieldsets() as $fieldset): ?>
		<p><?php echo JText::_($fieldset->label); ?></p>
			<span class="title-group-register"><?php echo JText::_('Your E-Mail Address');?></span>
			<div class="center-group">
				<fieldset>
					<dl>
					<?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field): ?>
						<dt class="reset-password-label"><?php echo $field->label; ?></dt>
						<dd class="reset-password-input"><?php echo $field->input; ?></dd>
					<?php endforeach; ?>
					</dl>
				</fieldset>
			</div>
		<?php endforeach; ?>
		<div class="group-button">
			<div class="reset-button">
				<button class="button" type="reset" onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_users&view=login'); ?>'" ><?php echo JText::_('Back'); ?></button>
				<button type="submit" class="validate"><?php echo JText::_('Continue'); ?></button>
				<?php echo JHtml::_('form.token'); ?>
			</div>
		</div>
	</form>
</div>
