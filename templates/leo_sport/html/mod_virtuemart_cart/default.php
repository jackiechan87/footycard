<?php // no direct access
defined('_JEXEC') or die('Restricted access');

//dump ($cart,'mod cart');
// Ajax is displayed in vm_cart_products
// ALL THE DISPLAY IS Done by Ajax using "hiddencontainer" ?>

<!-- Virtuemart 2 Ajax Card -->
<?php
if ((isset($_SESSION['checkList']) && !empty($_SESSION['checkList']))
	|| (isset($_SESSION['tradeList']) && !empty($_SESSION['tradeList']))) {
	$hidden = ' hidden';
}

//check if count data product is 1
$products = $data->products;
$count = 0;
if(count($products) == 1 && $products[0]['product_sku'] === 'trading-fee')
{
	$hidden = ' hidden';
}

	?>
<div class="vmCartModule <?php echo $params->get('moduleclass_sfx'); ?>" id="vmCartModule">
<?php

if ($show_product_list) {
	?>
	<div id="hiddencontainer" style=" display: none; ">
		<div class="container">
			<?php if ($show_price) { ?>
			  <div class="prices" style="float: right;"></div>
			<?php } ?>
			<div class="product_row">
				<span class="quantity"></span>&nbsp;x&nbsp;<span class="product_name"></span>
			</div>

			<div class="product_attributes"></div>
		</div>
	</div>
	<div class="vm_cart_products <?php echo $hidden;?>">
		<div class="container">
		<?php foreach ($data->products as $product)
		{
				$cls = '';

				 if($product['product_sku'] === 'trading-fee'){
						$cls = ' trading-fee';
						$count = 1;
				}?>
				<div class="product_row <?php echo $cls;?>">
					<span class="quantity"><?php echo  $product['quantity'] ?></span>&nbsp;x&nbsp;<span class="product_name"><?php echo  $product['product_name'] ?></span>

				<?php
					if ($show_price) { ?>
					<div class="prices" style="float: right;"><?php echo  $product['prices'] ?></div>
				<?php } ?>
				</div>
				<?php if ( !empty($product['product_attributes']) ) { ?>
					<div class="product_attributes"><?php echo  $product['product_attributes'] ?></div>

				<?php }

		}
		?>
		</div>
	</div>
<?php } ?>


	<?php
	if($hidden !== ' hidden'){
		echo '<div class="total_products">'.($data->totalProduct - $count).' '.JText::_('products').'</div>';
		?>
		<div class="total" style="float: right;">
			<?php if ($data->totalProduct) echo  $data->billTotal; ?>
		</div>
		<div class="show_cart">
			<?php if ($data->totalProduct) echo  $data->cart_show; ?>
		</div>
	<?php
	}
	else{
		echo '<div class="total_products">'.JText::_('Cart Empty').'</div>';
		echo '<div class="total" style="float: right;"></div><div class="show_cart"></div>';
	}
	?>


<div style="clear:both;"></div>

<noscript>
<?php echo JText::_('MOD_VIRTUEMART_CART_AJAX_CART_PLZ_JAVASCRIPT') ?>
</noscript>

</div>

