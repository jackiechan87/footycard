<?php // no direct access
defined('_JEXEC') or die('Restricted access');

//dump ($cart,'mod cart');
// Ajax is displayed in vm_cart_products
// ALL THE DISPLAY IS Done by Ajax using "hiddencontainer" ?>

<!-- Virtuemart 2 Ajax Card -->
<div class="<?php echo $params->get('moduleclass_sfx'); ?>" id="vmCartModule">
<?php
$checkList = empty($_SESSION['checkList']) ? array() : $_SESSION['checkList'];
$tradeList = empty($_SESSION['tradeList']) ? array() : $_SESSION['tradeList'];
$checkListQuantity = empty($_SESSION['checkListQuantity']) ? array() : $_SESSION['checkListQuantity'];
$tradeListQuantity = empty($_SESSION['tradeListQuantity']) ? array() : $_SESSION['tradeListQuantity'];
$productModel = VmModel::getModel('Product');
    
if (!empty($checkList) || !empty($tradeList)) {
    ?>
    <div class="vm_cart_products">
        <div class="container">
            <?php foreach ($tradeList as $teamId => $teamData) {
                foreach ($teamData['chosenCards'] as $playerId => $cards) {
                    foreach ($cards as $categoryName => $cardId) {
                        $cardInfo = $productModel->getProduct($cardId); ?>
                        <div class="prices tradingItem" style="float: right;"
                             title="<?php echo JText::_('COM_VIRTUEMART_I_WANT') ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        <div class="product_row">
                                        <span class="quantity"><?php echo (isset($tradeListQuantity[$cardId])
                                                && $tradeListQuantity[$cardId] > 0) ? $tradeListQuantity[$cardId] : 1; ?></span>&nbsp;x&nbsp;<span
                                class="product_name"><?php echo $cardInfo->product_name; ?></span>
                        </div>
                    <?php }
                }
            }

            foreach ($checkList as $teamId => $teamData) {
                foreach ($teamData['chosenCards'] as $playerId => $cards) {
                    foreach ($cards as $categoryName => $cardId) {
                        $cardInfo = $productModel->getProduct($cardId); ?>
                        <div class="prices checkingItem" style="float: right;"
                             title="<?php echo JText::_('COM_VIRTUEMART_I_WANT_TO_TRADE') ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        <div class="product_row">
                                        <span class="quantity"><?php echo (isset($tradeListQuantity[$cardId])
                                                && $tradeListQuantity[$cardId] > 0) ? $tradeListQuantity[$cardId] : 1; ?></span>&nbsp;x&nbsp;<span
                                class="product_name"><?php echo $cardInfo->product_name; ?></span>
                        </div>
                    <?php }
                }
            }
            ?>
        </div>
    </div>
    <!--
<div class="total" style="float: right;">
	<?php //if ($data->totalProduct) echo  $data->billTotal; ?>
</div>
<div class="total_products"><?php // echo  $data->totalProductTxt ?></div> -->
    <div class="show_cart">
        <a style="float:right;"
           href="<?php echo JRoute::_("index.php?option=com_virtuemart&view=cart&Itemid=697", $useXHTML, $useSSL); ?>"><?php echo JText::_('COM_VIRTUEMART_CART_SHOW'); ?></a>
    </div>
<?php
    echo '<div class="total_products cartempty hidden">'.JText::_('Cart empty').'</div><div class="show_cart"></div>';
}
else
{
    echo '<div class="total_products cartempty">'.JText::_('Cart empty').'</div><div class="show_cart"></div>';
}
    
?>

<div style="clear:both;"></div>

<noscript>
<?php echo JText::_('MOD_VIRTUEMART_CART_AJAX_CART_PLZ_JAVASCRIPT') ?>
</noscript>
</div>

