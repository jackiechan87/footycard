
<?php if( $row->title ) :?>
<div class="lof-description">	
     <div class="lof-title">
		<a href="<?php echo $row->link;?>" target="<?php echo $openTarget ;?>" title="<?php echo $row->title; ?>"><?php echo $row->title; ?></a>
	</div>
   
    <?php if( $row->description   && $row->description !="..." ): ?>
    <p>
	<?php echo $row->description;?>
    </p>
    <?php endif; ?>
    <?php if($show_addcart == 1):?>
    <div class="lofaddcart-nav">
        <?php
            echo lofaddtocart::addtocart($row);
        ?>
    </div>
    <?php endif;?>
</div>

<?php endif; ?>
<?php 

echo $row->mainImage;	
$prices = $row->prices;

$percent = (intval($prices['discountAmount']) / intval($prices['basePriceWithTax']))*100;

?>
 <?php if ($show_price): ?>
    <div class="lof-price">
    	<div class="lof-priceDiscount"><?php echo $percent.'% <br />Off ';?></div>
        <div class="mask"><span>Kettler</span> <br />Outrigger Rowing Machine</div>
        <div class="lof-basePrice">Base price  <?php echo $row->basePriceWithTax;?></div>
    </div>
   	<?php endif; ?>