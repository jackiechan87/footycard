<?php
/**
*
* Show the products in a category
*
* @package	VirtueMart
* @subpackage
* @author RolandD
* @author Max Milbers
* @todo add pagination
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: default.php 5120 2011-12-18 18:29:26Z electrocity $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
JHTML::_( 'behavior.modal' );
/* javascript for list Slide
  Only here for the order list
  can be changed by the template maker
*/
$js = "
jQuery(document).ready(function () {
	jQuery('.orderlistcontainer').hover(
		function() { jQuery(this).find('.orderlist').stop().show()},
		function() { jQuery(this).find('.orderlist').stop().hide()}
	)
});
";

$document = JFactory::getDocument();
$document->addScriptDeclaration($js);

$vmModel = new VirtueMartModelCategory;

//get category parent of category current
$categoryParent = $vmModel->getCategoryParentByChild($this->category->virtuemart_category_id);
$catagories = $vmModel->getChildCategoryList($categoryParent->virtuemart_vendor_id,$categoryParent->category_parent_id);
?>

<div class="category_description">
	<?php echo $this->category->category_description ; ?>
</div>
<?php
/* Show child categories */

if ( VmConfig::get('showCategory',1) ) {
	if ($this->category->haschildren) {

		// Category and Columns Counter
		$iCol = 1;
		$iCategory = 1;

		// Calculating Categories Per Row
		$categories_per_row = VmConfig::get ( 'categories_per_row', 3 );
		$category_cellwidth = ' width'.floor ( 100 / $categories_per_row );

		// Separator
		$verticalseparator = " vertical-separator";
		?>
<div class="category-view">
	<?php // Start the Output
		if(!empty($this->category->children)){
		foreach ( $this->category->children as $category ) {

			// Show the horizontal seperator
			if ($iCol == 1 && $iCategory > $categories_per_row) : ?>
	<?php endif; ?>
	<?php
				// this is an indicator wether a row needs to be opened or not
				if ($iCol == 1) :
			?>
	<div class="row">
		<?php endif; ?>
		<?php
			// Show the vertical seperator
			if ($iCategory == $categories_per_row or $iCategory % $categories_per_row == 0) {
				$show_vertical_separator = ' ';
			} else {
				$show_vertical_separator = $verticalseparator;
			}

			// Category Link
			$caturl = JRoute::_ ( 'index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $category->virtuemart_category_id );

				// Show Category ?>
		<div class="category floatleft<?php echo $category_cellwidth . $show_vertical_separator ?>">
			<div class="spacer">
				<h2 class="catSub">
					<a href="<?php echo $caturl ?>" title="<?php echo $category->category_name ?>">
					<?php echo $category->category_name ?>


					</a>
				</h2>
				<?php //if ($category->ids) {
								echo $category->images[0]->displayMediaThumb("",false);
							//} ?>
			</div>
		</div>
		<?php
			$iCategory ++;

		// Do we need to close the current row now?
		if ($iCol == $categories_per_row) { ?>
		<div class="clear"></div>
	</div>
	<?php
			$iCol = 1;
		} else {
			$iCol ++;
		}
	}
	}
	// Do we need a final closing row tag?
	if ($iCol != 1) { ?>
	<div class="clear"></div>
</div>
<?php } ?>
</div>
<?php }
}?>

<div class="div-list-category">
	<?php
	if(count($catagories) > 0) {
		$div = '';
		if($categoryParent->file_url) {
			$div = 'class="list_category_child"';
			?>
			<div class="logo_category_parent">
				<img src="<?php echo JURI::root().$categoryParent->file_url?>">
			</div>
		<?php }?>
		<div <?php echo $div;?> >
			<form action="" id="submit-change-category" method="POST">
				<?php

				foreach ($catagories as $k => $c) {
					$checked = '';
					$childs = VirtueMartModelCategory::getChildCategoryList($categoryParent->virtuemart_vendor_id,$c->virtuemart_category_id);
					if($k != (count($catagories)-1) || empty($childs))
					{
						if($c->virtuemart_category_id === $this->category->virtuemart_category_id) {
							$checked = 'checked';
						}
						?>
						<div class="div-categories">
							<input type="radio" name="category" cateview="category" value="<?php echo $c->virtuemart_category_id?>" <?php echo $checked; ?> ><span><?php echo $c->category_name;?></span>
						</div>
					<?php
					}
					else {
						//$childs = VirtueMartModelCategory::getChildCategoryList(1,$c->virtuemart_category_id);
						if(count($childs) > 0) {
							foreach ($childs as $child) {
							?>
								<div class="div-categories">
									<input type="radio" name="category" cateview="categorychecklist" value="<?php echo $child->virtuemart_category_id?>" <?php echo $checked; ?> ><span><?php echo $child->category_name;?></span>
								</div>
						<?php
							}
						}
					}
				}
				?>
			</form>
		</div>
	<?php }?>
</div>

<?php
// Show child categories
if (!empty($this->products)) {
	if (!empty($this->keyword)) {
		?>
<h3><?php echo $this->keyword; ?></h3>
<?php
	}
	?>
<?php // Category and Columns Counter
$iBrowseCol = 1;
$iBrowseProduct = 1;

// Calculating Products Per Row
$BrowseProducts_per_row = $this->perRow;
$Browsecellwidth = ' width'.floor ( 100 / $BrowseProducts_per_row );

// Separator
$verticalseparator = " vertical-separator"
?>

<div id="footy_special_cart">
<div class="browse-view">
<?php
	if ($this->category->category_name) {
		$nameCate = $this->category->category_name;
		if (!empty($categoryParent->category_name))
		{
			$nameCate .= " - ". $categoryParent->category_name;
		}
?>
<h2><span><span><?php echo $nameCate; ?></span></span></h2>
<?php } ?>

<!--Disable form search-->

<!--	<form action="--><?php //echo JRoute::_('index.php?option=com_virtuemart&view=category&limitstart=0&virtuemart_category_id='.$this->category->virtuemart_category_id ); ?><!--" method="get">-->
<!--		--><?php //if ($this->search) { ?>
<!--		<!--BEGIN Search Box -->
<!--		<div class="virtuemart_search">-->
<!--			--><?php //echo $this->searchcustom ?>
<!--			<br />-->
<!--			--><?php //echo $this->searchcustomvalues ?>
<!--			<input style="height:16px;vertical-align :middle;" name="keyword" class="inputbox" type="text" size="20" value="--><?php //echo $this->keyword ?><!--" />-->
<!--			<input type="submit" value="--><?php //echo JText::_('COM_VIRTUEMART_SEARCH') ?><!--" class="button" onclick="this.form.keyword.focus();"/>-->
<!--		</div>-->
<!--		<input type="hidden" name="search" value="true" />-->
<!--		<input type="hidden" name="view" value="category" />-->
<!---->
<!--		<!-- End Search Box -->
<!--		--><?php //} ?>
<!--		<div class="orderby-displaynumber">-->
<!--			--><?php //echo $this->orderByList['orderby']; ?>
<!--			<div class="display-number">--><?php //echo $this->vmPagination->getResultsCounter();?><!-- --><?php //echo $this->vmPagination->getLimitBox(); ?><!--</div>-->
<!--		</div>-->
<!--	</form>-->


	<?php // Start the Output
foreach ( $this->products as $product ) {

	// Show the horizontal seperator
	if ($iBrowseCol == 1 && $iBrowseProduct > $BrowseProducts_per_row) { ?>
	<?php }

	// this is an indicator wether a row needs to be opened or not
	if ($iBrowseCol == 1) { ?>
	<div class="row">
		<?php }

	// Show the vertical seperator
	if ($iBrowseProduct == $BrowseProducts_per_row or $iBrowseProduct % $BrowseProducts_per_row == 0) {
		$show_vertical_separator = ' ';
	} else {
		$show_vertical_separator = $verticalseparator;
	}

		// Show Products ?>
		<div class="product floatleft<?php echo $Browsecellwidth . $show_vertical_separator ?>">
			<div class="spacer">
				<div class="div-product">
						<?php /** @todo make image popup */
							echo $product->images[0]->displayMediaThumb('class="browseProductImage" border="0" title="'.$product->product_name.'" ',true,'class="modal"');
						?>

                        <!-- The "Average Customer Rating" Part -->
						<?php if ($this->showRating) { ?>
					<!--	<span class="contentpagetitle"><?php echo JText::_('COM_VIRTUEMART_CUSTOMER_RATING') ?>:</span>
						<br />-->
						<?php
						// $img_url = JURI::root().VmConfig::get('assets_general_path').'/reviews/'.$product->votes->rating.'.gif';
						// echo JHTML::image($img_url, $product->votes->rating.' '.JText::_('COM_VIRTUEMART_REVIEW_STARS'));
						// echo JText::_('COM_VIRTUEMART_TOTAL_VOTES').": ". $product->votes->allvotes; ?>
						<?php } ?>

				</div>
				<div>
					<h3 class="catProductTitle"><?php echo JHTML::link($product->link, $product->product_name); ?></h3>
					<?php // Product Short Description
						if(!empty($product->product_s_desc)) { ?>
					<p class="product_s_desc">
						<?php echo shopFunctionsF::limitStringByWord($product->product_s_desc, 30, '...') ?>
					</p>
					<?php } ?>

                    <?php
						if (!VmConfig::get('use_as_catalog') and !(VmConfig::get('stockhandle','none')=='none') && (VmConfig::get ( 'display_stock', 1 )) ){?>
<!-- 						if (!VmConfig::get('use_as_catalog') and !(VmConfig::get('stockhandle','none')=='none')){?> -->
						<div class="stockLavel">
							<span class="vmicon vm2-<?php echo $product->stock->stock_level ?>" title="<?php echo $product->stock->stock_tip ?>"></span>
							<span class="stock-level"><?php echo JText::_('COM_VIRTUEMART_STOCK_LEVEL_DISPLAY_TITLE_TIP') ?></span>
						</div>
						<?php }?>

					<div class="catProductPrice" id="productPrice<?php echo $product->virtuemart_product_id ?>">
						<?php
					if ($this->show_prices == '1') {
						if( $product->product_unit && VmConfig::get('vm_price_show_packaging_pricelabel')) {
							echo "<strong>". JText::_('COM_VIRTUEMART_CART_PRICE_PER_UNIT').' ('.$product->product_unit."):</strong>";
						}


						echo $this->currency->createPriceDiv('salesPrice','',$product->prices);
						//echo $this->currency->createPriceDiv('discountAmount','COM_VIRTUEMART_PRODUCT_DISCOUNT_AMOUNT',$product->prices);
						//echo $this->currency->createPriceDiv('taxAmount','COM_VIRTUEMART_PRODUCT_TAX_AMOUNT',$product->prices);

					} ?>
					</div>

  <div class="cat-cart">
 <form method="post" class="product js-recalculate" action="index.php" >

	<div class="addtocart-bar">

<?php // Display the quantity box  ?>
						<!-- <label for="quantity<?php echo $this->product->virtuemart_product_id; ?>" class="quantity_box"><?php echo JText::_('COM_VIRTUEMART_CART_QUANTITY'); ?>: </label> -->
	    <span class="quantity-box">
		<input type="text" class="quantity-input js-recalculate" name="quantity[]" value="<?php if (isset($this->product->min_order_level) && (int) $this->product->min_order_level > 0) {
    echo $this->product->min_order_level;
} else {
    echo '1';
} ?>" />
	    </span>
	    <span class="quantity-controls js-recalculate">
		<input type="button" class="quantity-controls quantity-plus" />
		<input type="button" class="quantity-controls quantity-minus" />
	    </span>
	    <?php // Display the quantity box END ?>

	    <?php
	    // Add the button
	    $button_lbl = JText::_('COM_VIRTUEMART_CART_ADD_TO');
	    $button_cls = 'addtocart-button'; //$button_cls = 'addtocart_button';
	    $button_name = 'addtocart'; //$button_cls = 'addtocart_button';
	    // Display the add to cart button
	    $stockhandle = VmConfig::get('stockhandle', 'none');
	    if (($stockhandle == 'disableit' or $stockhandle == 'disableadd') and ($product->product_in_stock - $product->product_ordered) < 1) {
//                var_dump($product);die();
		$button_lbl = JText::_('COM_VIRTUEMART_CART_OUT_OF_STOCK');
		$button_cls = 'notify-button addtocart-button';
		$button_name = 'notifycustomer';
	    }
	    //vmdebug('$stockhandle '.$stockhandle.' and stock '.$this->product->product_in_stock.' ordered '.$this->product->product_ordered);
	    ?>

		<?php if($button_cls == 'notify-button addtocart-button'){
			?>
			<span name="<?php echo $button_name ?>"  class="<?php echo $button_cls ?>" title="<?php echo $button_lbl ?>" > <?php echo $button_lbl ?></span>
		<?php
		}
		else{
			?>
		<span class="addtocart-button">
			<input type="submit" name="<?php echo $button_name ?>"  class="<?php echo $button_cls ?>" value="<?php echo $button_lbl ?>" title="<?php echo $button_lbl ?>" />
			</span>
		<?php
		}
		?>


	    <div class="clear"></div>
	</div>

	<?php // Display the add to cart button END  ?>
	<input type="hidden" class="pname" value="<?php echo $product->product_name ?>" />
	<input type="hidden" name="option" value="com_virtuemart" />
	<input type="hidden" name="view" value="cart" />
	 <input type="hidden" name="quantity" value="1" />
	<noscript><input type="hidden" name="task" value="add" /></noscript>
	<input type="hidden" name="virtuemart_product_id" value="<?php echo $product->virtuemart_product_id ?>" />
<?php /** @todo Handle the manufacturer view */ ?>
	<input type="hidden" name="virtuemart_manufacturer_id" value="<?php echo $product->virtuemart_manufacturer_id ?>" />
	<input type="hidden" name="virtuemart_category_id" value="<?php echo $product->virtuemart_category_id ?>" />
    </form>
    </div>
					<?php // Product Details Button
				//	echo JHTML::link($product->link, JText::_('COM_VIRTUEMART_PRODUCT_DETAILS'), array('title' => $product->product_name,'class' => 'catProductDetails'));
					?>
				</div>
			</div>
		</div>
		<?php
	$iBrowseProduct ++;

	// Do we need to close the current row now?
	if ($iBrowseCol == $BrowseProducts_per_row) { ?>
	</div>
	<?php
		$iBrowseCol = 1;
	} else {
		$iBrowseCol ++;
	}
}
// Do we need a final closing row tag?
if ($iBrowseCol != 1) { ?>
	<div class="clear"></div>
</div>
<?php
}
?>
<div class="pagination">
	<?php echo $this->vmPagination->getPagesLinks(); ?>
</div>
</div>
</div>
<?php } else { ?>
	<div class="browse-view">
	<?php if ($this->category->category_name) {
	    $nameCate = $this->category->category_name;
		if (!empty($categoryParent->category_name))
		{
			$nameCate .= " - ". $categoryParent->category_name;
		}
		?>
        <h2><span><span><?php echo $nameCate; ?></span></span></h2>
		<div class="row">
			<?php echo JText::_('No Product')?>
		</div>
<?php
	}
}
?>