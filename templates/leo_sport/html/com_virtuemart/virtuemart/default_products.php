<?php defined('_JEXEC') or die('Restricted access'); 


// Separator
$verticalseparator = " vertical-separator";

foreach ($this->products as $type => $productList ) {
// Calculating Products Per Row
$products_per_row = VmConfig::get ( $type.'_products_per_row', 5 ) ;
$cellwidth = ' width'.floor ( 100 / $products_per_row );

// Category and Columns Counter
$col = 1;
$nb = 1;

$productTitle = JText::_('COM_VIRTUEMART_'.$type.'_PRODUCT')

?>

<div class="<?php echo $type ?>-view">
	<h2><span><span><?php echo $productTitle ?></span></span></h2>
	<?php // Start the Output
foreach ( $productList as $product ) {

	// Show the horizontal seperator
	if ($col == 1 && $nb > $products_per_row) { ?>
	<!--<div class="horizontal-separator"></div>-->
	<?php }

	// this is an indicator wether a row needs to be opened or not
	if ($col == 1) { ?>
	<div class="row">
		<?php }

	// Show the vertical seperator
	if ($nb == $products_per_row or $nb % $products_per_row == 0) {
		$show_vertical_separator = ' ';
	} else {
		$show_vertical_separator = $verticalseparator;
	}

		// Show Products ?>
		<div class="product floatleft<?php echo $cellwidth . $show_vertical_separator ?>">
			<div class="spacer">
				<?php // Product Image
					if ($product->images) {
						echo JHTML::_ ( 'link', JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id ), $product->images[0]->displayMediaThumb( 'class="featuredProductImage" border="0"',true,'class="modal"' ) );
					}
					?>
				<h3>
					<?php // Product Name
					echo JHTML::link ( JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id ), $product->product_name, array ('title' => $product->product_name ) ); ?>
				</h3>
				<?php // Product Short Description
						if(!empty($product->product_s_desc)) { ?>
				<p class="product_s_desc">
					<?php echo shopFunctionsF::limitStringByWord($product->product_s_desc, 40, '...') ?>
				</p>
				<?php } ?>
				<div class="catProductPrice">
					<?php
					if (VmConfig::get ( 'show_prices' ) == '1') {
					//				if( $featProduct->product_unit && VmConfig::get('vm_price_show_packaging_pricelabel')) {
					//						echo "<strong>". JText::_('COM_VIRTUEMART_CART_PRICE_PER_UNIT').' ('.$featProduct->product_unit."):</strong>";
					//					} else echo "<strong>". JText::_('COM_VIRTUEMART_CART_PRICE'). ": </strong>";

					echo $this->currency->createPriceDiv( 'salesPrice', '', $product->prices );				
					echo $this->currency->createPriceDiv( 'taxAmount', 'COM_VIRTUEMART_PRODUCT_TAX_AMOUNT', $product->prices );
					echo $this->currency->createPriceDiv( 'discountAmount', 'COM_VIRTUEMART_PRODUCT_DISCOUNT_AMOUNT', $product->prices );
					} ?>
				</div>
                
                <div class="cat-cart">                 
 <form method="post" class="product js-recalculate" action="index.php" >
	
	<div class="addtocart-bar">

<?php // Display the quantity box  ?>
						<!-- <label for="quantity<?php echo $this->product->virtuemart_product_id; ?>" class="quantity_box"><?php echo JText::_('COM_VIRTUEMART_CART_QUANTITY'); ?>: </label> -->
	    <span class="quantity-box">
		<input type="text" class="quantity-input js-recalculate" name="quantity[]" value="<?php if (isset($this->product->min_order_level) && (int) $this->product->min_order_level > 0) {
    echo $this->product->min_order_level;
} else {
    echo '1';
} ?>" />
	    </span>
	    <span class="quantity-controls js-recalculate">
		<input type="button" class="quantity-controls quantity-plus" />
		<input type="button" class="quantity-controls quantity-minus" />
	    </span>
	    <?php // Display the quantity box END ?>

	    <?php
	    // Add the button
	    $button_lbl = JText::_('COM_VIRTUEMART_CART_ADD_TO');
	    $button_cls = 'addtocart-button'; //$button_cls = 'addtocart_button';
	    $button_name = 'addtocart'; //$button_cls = 'addtocart_button';
	    // Display the add to cart button
	    $stockhandle = VmConfig::get('stockhandle', 'none');
	    if (($stockhandle == 'disableit' or $stockhandle == 'disableadd') and ($this->product->product_in_stock - $this->product->product_ordered) < 1) {
		$button_lbl = JText::_('COM_VIRTUEMART_CART_NOTIFY');
		$button_cls = 'notify-button addtocart-button';
		$button_name = 'notifycustomer';
	    }
	    //vmdebug('$stockhandle '.$stockhandle.' and stock '.$this->product->product_in_stock.' ordered '.$this->product->product_ordered);
	    ?>
	    <span class="addtocart-button">
			<?php if($button_cls == 'notify-button addtocart-button'){
				?>
				<span name="<?php echo $button_name ?>"  class="<?php echo $button_cls ?>" title="<?php echo $button_lbl ?>" />
		<?php
			}
			else{
				?>
				<input type="submit" name="<?php echo $button_name ?>"  class="<?php echo $button_cls ?>" value="<?php echo $button_lbl ?>" title="<?php echo $button_lbl ?>" />
				<?php
			}
			?>

	    </span>

	    <div class="clear"></div>
	</div>

	<?php // Display the add to cart button END  ?>
	<input type="hidden" class="pname" value="<?php echo $product->product_name ?>" />
	<input type="hidden" name="option" value="com_virtuemart" />
	<input type="hidden" name="view" value="cart" />
	 <input type="hidden" name="quantity" value="1" />
	<noscript><input type="hidden" name="task" value="add" /></noscript>
	<input type="hidden" name="virtuemart_product_id" value="<?php echo $product->virtuemart_product_id ?>" />
<?php /** @todo Handle the manufacturer view */ ?>
	<input type="hidden" name="virtuemart_manufacturer_id" value="<?php echo $product->virtuemart_manufacturer_id ?>" />
	<input type="hidden" name="virtuemart_category_id" value="<?php echo $product->virtuemart_category_id ?>" />
    </form>
    </div>
				<?php // Product Details Button
					//echo JHTML::link ( JRoute::_ ( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id ), JText::_ ( 'COM_VIRTUEMART_PRODUCT_DETAILS' ), array ('title' => $product->product_name, 'class' => 'catProductDetails' ) );
					?>
			</div>
		</div>
		<?php
	$nb ++;

	// Do we need to close the current row now?
	if ($col == $products_per_row) { ?>
	</div>
	<?php
		$col = 1;
	} else {
		$col ++;
	}
}
// Do we need a final closing row tag?
if ($col != 1) { ?>
	<div class="clear"></div>
</div>
<?php
}
?>
</div>
<?php } 



