<?php
// no direct access
defined('_JEXEC') or die('Restricted access');
include_once (dirname(__FILE__) . DS . 'vars.php');
JHtml::_('behavior.framework', true);
jimport('joomla.application.component.view');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">

    <head>
        <jdoc:include type="head" />
        <?php JHTML::_('behavior.mootools'); ?>
        <link rel="stylesheet" href="<?php echo $tmpTools->baseurl(); ?>templates/system/css/system.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $tmpTools->baseurl(); ?>templates/system/css/general.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $tmpTools->templateurl(); ?>/css/template.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo $tmpTools->templateurl(); ?>/css/style.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $tmpTools->templateurl(); ?>/css/fonts.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $tmpTools->templateurl(); ?>/css/news.css" type="text/css" />
        <!-- <link rel="stylesheet" href="<?php echo $tmpTools->templateurl(); ?>/css/typo.css" type="text/css" /> -->
        <!-- insert javascript here -->
        <!-- Menu head -->
        <?php if ($jamenu) $jamenu->genMenuHead(); ?>

    </head>
    <body id="bd" class="fs<?php echo $tmpTools->getParam(JA_TOOL_FONT); ?>">
        <div id="wrapper">
            <!-- HEADER -->
            <div id="header-wrapper">
                <div id="ja-header" class="wrap">
                    <div class="header-inner">    
                        <?php if ($this->countModules('language')) { ?>
                            <div id="language">
                                <jdoc:include type="modules" name="language" style="raw" />
                            </div>	
                        <?php } ?>

                        <?php if ($this->countModules('text-header')) { ?>
                            <div id="text-header">
                                <jdoc:include type="modules" name="text-header" style="raw" />
                            </div>	
                        <?php } ?>

                        <?php if ($this->countModules('link-header')) { ?>
                            <div id="link-header">
                                <jdoc:include type="modules" name="link-header" style="raw" />
                            </div>	
                        <?php } ?>

                        <?php if ($this->countModules('social-header')) { ?>
                            <div id="social-header">
                                <jdoc:include type="modules" name="social-header" style="raw" />
                            </div>	
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!-- //HEADER -->

            <!-- MAIN NAVIGATION -->
			<div id="ja-mainnav-wrapper">
				<div id="ja-mainnav">
					<div class="logo">
						<?php if ($this->countModules('logo')) { ?>
							<jdoc:include type="modules" name="logo" style="raw" />
						<?php } ?>
					</div>
					<div class="nav">
						<?php echo $hornav; ?>
					</div>    
				</div>
			</div>
            <!-- //MAIN NAVIGATION -->
            <?php if ($this->countModules('ja-slideshow')) { ?>
                <!-- TOP SPOTLIGHT -->
                <div id="ja-topsl" class="wrap">
                    <div class="inner clearfix">
                        <div id="after-header">
                            <div id="ja-slideshow">
                                <jdoc:include type="modules" name="ja-slideshow" style="raw" />
                            </div>
                            
                            <div class="form-tour">
                                <jdoc:include type="modules" name="form-tour" style="jamodule" />
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //TOP SPOTLIGHT -->
            <?php } ?>

            <?php if ($this->countModules('top-main')) { ?>
                <div id="top-main-wrapper">
                    <div id="top-main">
                        <jdoc:include type="modules" name="top-main" style="jamodule" />
                    </div>
                </div>
            <?php } ?>

            <?php if ($this->countModules('top-main-2')) { ?>
                <div id="top-main-wrapper-2">
                    <div id="top-main-2">
                        <jdoc:include type="modules" name="top-main-2" style="jamodule" />
                    </div>
                </div>
            <?php } ?>

            <div id="ja-container<?php echo $divid; ?>" class="wrap clearfix">
                <div class="main">
                    <div class="inner clearfix">
                        <!-- CONTENT -->  

                        <div id="ja-mainbody">
                            <!-- BOTTOM SPOTLIGHT-->
                            <div id="ja-botsl">
                                <div class="inner clearfix">
									<?php if ($this->countModules('button-bottom-1 or button-bottom-2')) { ?>
                                        <div class="button-bottom">
                                            <div class="button-bottom-inner">
                                                <?php if ($this->countModules('button-bottom-1')) { ?>
                                                    <div class="button-bottom-1">
                                                        <jdoc:include type="modules" name="button-bottom-1" style="raw" />
                                                    </div>
                                                <?php } ?>

                                                <?php if ($this->countModules('button-bottom-2')) { ?>
                                                    <div class="button-bottom-2">
                                                        <jdoc:include type="modules" name="button-bottom-2" style="raw" />
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
									
                                    <?php if ($this->countModules('user5')) { ?>
                                        <div class="user5">
                                            <jdoc:include type="modules" name="user5" style="jamodule" />
                                        </div>
                                    <?php } ?>

                                    <?php if ($this->countModules('user6')) { ?>
                                        <div class="user6">
                                            <jdoc:include type="modules" name="user6" style="jamodule" />
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                            <!-- //BOTTOM SPOTLIGHT 2 -->

                            <div id="ja-pathway">
                                <jdoc:include type="module" name="breadcrumbs" />
                            </div>

                            <jdoc:include type="message" />

                            <div id="ja-current-content" class="clearfix">
                                <jdoc:include type="component" />
                            </div>

                            <?php if ($this->countModules('ja-news')) { ?>
                                <!-- JA NEWS -->
                                <div id="ja-news">
                                    <jdoc:include type="modules" name="ja-news" style="jamodule" />
                                </div>
                                <!-- //JA NEWS -->
                            <?php } ?>

                        </div>
                        <!-- //CONTENT -->

                        <?php if ($ja_left || $ja_right) { ?>
                            <!-- RIGHT COLUMN -->
                            <div id="ja-colwrap">
                                <div class="ja-innerpad">

                                    <?php if ($hasSubnav) : ?>
                                        <div id="ja-subnav" class="moduletable_menu">
                                            <h3 class="clearfix"></h3>
                                            <div class="ja-boxct-wrap"><div class="ja-box-ct">
                                                    <?php if ($jamenu) $jamenu->genMenu(1, 1); ?>
                                                </div></div>
                                        </div>
                                    <?php endif; ?>

                                    <jdoc:include type="modules" name="left" style="jamodule" />
                                    <jdoc:include type="modules" name="right" style="jamodule" />
                                </div>
                            </div>
                            <!-- //RIGHT COLUMN -->
                        <?php } ?>

                    </div>
                </div>
            </div>

            <?php if ($this->countModules('footer-slider')) { ?>
                <div id="footer-slider-wrapper">
                    <div id="footer-slider">
                        <jdoc:include type="modules" name="footer-slider" style="raw" />
                    </div>
                </div>
            <?php } ?>

            <?php
            $spotlight = array('user1', 'user2', 'user8', 'user9');
            $botsl = $tmpTools->calSpotlight($spotlight, $tmpTools->isOP() ? 100 : 100);
            if ($botsl) {
                ?>
                <!-- BOTTOM SPOTLIGHT-->
                <div id="ja-botsl1" class="wrap">
                    <div class="main"><div class="inner clearfix">

                            <?php if ($this->countModules('user1')) { ?>
                                <div class="ja-box<?php echo $botsl['user1']['class']; ?>" style="width: <?php echo $botsl['user1']['width']; ?>;">
                                    <jdoc:include type="modules" name="user1" style="jamodule" />
                                </div>
                            <?php } ?>

                            <?php if ($this->countModules('user2')) { ?>
                                <div class="ja-box<?php echo $botsl['user2']['class']; ?>" style="width: <?php echo $botsl['user2']['width']; ?>;">
                                    <jdoc:include type="modules" name="user2" style="jamodule" />
                                </div>
                            <?php } ?>

                            <?php if ($this->countModules('user8')) { ?>
                                <div class="ja-box<?php echo $botsl['user8']['class']; ?>" style="width: <?php echo $botsl['user8']['width']; ?>;">
                                    <jdoc:include type="modules" name="user8" style="jamodule" />
                                </div>
                            <?php } ?>

                            <?php if ($this->countModules('user9')) { ?>
                                <div class="ja-box<?php echo $botsl['user9']['class']; ?>" style="width: <?php echo $botsl['user9']['width']; ?>;">
                                    <jdoc:include type="modules" name="user9" style="jamodule" />
                                </div>
                            <?php } ?>

                        </div></div></div>
                <!-- //BOTTOM SPOTLIGHT 2 -->
            <?php } ?>

            <!-- FOOTER -->
            <div id="ja-footer" class="wrap">
                <div class="main">
                    <div class="inner clearfix">
                        <jdoc:include type="modules" name="user3" />
                    </div>
                    <div class="inner clearfix">
                        <jdoc:include type="modules" name="footer" />
                    </div>
                </div>
            </div>
            <!-- //FOOTER -->
        </div>	
        <jdoc:include type="modules" name="debug" />
        </div>
    </body>

</html>