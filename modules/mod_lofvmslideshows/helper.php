<?php 
/**
 * $ModDesc
 * 
 * @version		$Id: helper.php $Revision
 * @package		modules
 * @subpackage	$Subpackage
 * @copyright	Copyright (C) May 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>. All rights reserved.
 * @website 	htt://landofcoder.com
 * @license		GNU General Public License version 2
 */
// no direct access
defined('_JEXEC') or die;
// Load the model framework
jimport( 'joomla.application.component.model');
require_once JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php';
if( !defined('PhpThumbFactoryLoaded') ) {
	require_once dirname(__FILE__).DS.'libs'.DS.'phpthumb'.DS.'ThumbLib.inc.php';
	define('PhpThumbFactoryLoaded',1);
}
if( !class_exists('LofSlideShowGroupBase') ){
    require_once( dirname(__FILE__).DS.'libs'.DS.'group_base.php' );
}
if(  !defined("JPATH_VM_ADMINISTRATOR") ){
	define( "JPATH_VM_ADMINISTRATOR", JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'  );
}
JFactory::getLanguage()->load('com_virtuemart');
if (!class_exists( 'calculationHelper' )) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'calculationh.php');
if (!class_exists( 'CurrencyDisplay' )) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'currencydisplay.php');
if (!class_exists( 'VirtueMartModelVendor' )) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'models'.DS.'vendor.php');
if (!class_exists( 'VmImage' )) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'image.php');
if (!class_exists( 'shopFunctionsF' )) require(JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'shopfunctionsf.php');
if (!class_exists( 'calculationHelper' )) require(JPATH_COMPONENT_SITE.DS.'helpers'.DS.'cart.php');
abstract class modLofSlideShowHelper {
	
	/**
	 * get list articles
	 */
	public static function getList( $params, $moduleName ){
		if ( $params->get('enable_cache','0') ) { 
			$cache =& JFactory::getCache( $moduleName );
			$cache->setCaching( true );
			$cache->setLifeTime( $params->get( 'cache_time', 15 ) * 60 );	
			return $cache->get( array( 'modLofSlideShowHelper' , 'getGroupObject' ), array( $params ) ); 
		} else {
			return self::getGroupObject( $params );
		}	
	}
	
	/**
	 * get list articles
	 */
	public static function getGroupObject( $params ){
		$group = $params->get( 'group', 'virtuemart' );
		$file = dirname(__FILE__).DS.'libs'.DS.'groups'.DS.strtolower($group).DS.strtolower($group).'.php';	
		if( file_exists($file) ){
			require_once($file);
			$className = 'LofSlideShowGroup'.ucfirst($group);
			if( class_exists($className) ){
				$object = new $className( $group );	
				$object->setCurrentPath(  dirname(__FILE__).DS.'libs'.DS.'groups'.DS.strtolower($group).DS );							
			}
		}
		if( $object ){
			return $object->getListByParameters( $params );	
		} else {
			return array();
		}
	}
        
	/**
	 * load css - javascript file.
	 * 
	 * @param JParameter $params;
	 * @param JModule $module
	 * @return void.
	 */
	public static function loadMediaFiles( $params, $module, $theme='' ){

		global $mainframe;
        $mainframe = JFactory::getApplication();
		$template = self::getTemplate();
		//load style of module
		if(file_exists(JPATH_BASE.DS.'templates/'.$template.'/css/'.$module->module.'.css')){
			JHTML::stylesheet(  $module->module.'.css','templates/'.$template.'/css/' );		
		}			
		// load style of theme follow the setting
               
		if( $theme && $theme != -1 ){
			$tPath = JPATH_BASE.DS.'templates'.DS.$template.DS.'css'.DS.$module->module.'_'.$theme.'.css';
			if( file_exists($tPath) ){
				JHTML::stylesheet( $module->module.'_'.$theme.'.css','templates/'.$template.'/css/');
			} else {
				JHTML::stylesheet('style.css','modules/'.$module->module.'/tmpl/'.$theme.'/assets/');	
			}
		} else {
           JHTML::stylesheet( 'style.css','modules/'.$module->module.'/assets/' );
		}
		$app = JFactory::getApplication();
        JHTML::script( 'script_12.js','modules/'.$module->module.'/assets/');
	}
	
	/**
	 * get name of current template
	 */
	public static function getTemplate(){
		global $mainframe;
		return $mainframe->getTemplate();
	}
}
?>