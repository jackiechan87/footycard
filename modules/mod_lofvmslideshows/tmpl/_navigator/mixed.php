<div class="lof-navigator-wrapper clearfix">
    <!-- NAVIGATOR -->
      <div class="lof-navigator-outer">
            <ul class="lof-navigator">
            <?php foreach( $list as $row ):?>
                <li style="width:<?php echo $navWidth;?>px; height:<?php echo $navHeight;?>px"><div> 
                    <img style="margin:<?php echo $thumbnailMargin;?>" src="<?php echo $row->thumbnailURL;?>" title="<?php echo $row->title?>" />
					<span class="lof-title"><?php echo $row->title; ?></span>
                 </div></li>
             <?php endforeach; ?> 		
            </ul>
      </div>
 	<!-- END NAVIGATOR //-->
</div>    