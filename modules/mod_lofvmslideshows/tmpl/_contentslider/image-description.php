<?php 

echo $row->mainImage;	?>
<?php if( $row->title ) :?>
<div class="lof-description">	
     <div class="lof-title">
		<a href="<?php echo $row->link;?>" target="<?php echo $openTarget ;?>" title="<?php echo $row->title; ?>"><?php echo $row->title; ?></a>
	</div>
    <?php if ($show_price): ?>
    <div class="lof-price">
		<span><?php echo $row->salesPrice;;?></span>
        <span class="price-discount"><?php echo $row->salesPriceWithDiscount;?></span>
    </div>
   	<?php endif; ?>
    <?php if( $row->description   && $row->description !="..." ): ?>
    <p>
	<?php echo $row->description;?>
    </p>
    <?php endif; ?>
    <?php if($show_addcart == 1):?>
    <div class="lofaddcart-nav">
        <?php
            echo lofaddtocart::addtocart($row);
        ?>
    </div>
    <?php endif;?>
</div>
<?php endif; ?>