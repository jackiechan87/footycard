<?php
/*------------------------------------------------------------------------
 # com_lofshowcase - Lof Lofshowcase Component
 # ------------------------------------------------------------------------
 # author    LandOfCoder
 # copyright Copyright (C) 2011 landofcoder.com. All Rights Reserved.
 # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Websites: http://www.landofcoder.com
 # Technical Support:  Forum - http://www.landofcoder.com/forum.html
-------------------------------------------------------------------------*/
defined('_JEXEC') or die();

 
class JFormFieldModuleTheme extends JFormField
{
	protected $type 		= 'ModuleTheme';

	protected function getInput() {
		$groups = array();
		 
	 
			$options = array();
			$extension =   $this->element->getAttribute( "module" );  
			$appDir =  JPATH_SITE. DS . "modules" . DS .  $extension.DS."tmpl";
 
            $exclude = (  $this->element->getAttribute("exclude"));
   
			if(  file_exists($appDir) ) {
				$atemplates = JFolder::folders( $appDir ) ;
                				// Create the group for the template
				$groups['app']		    = array();
				$groups['app']['id']   = "";
				$groups['app']['text'] = JText::sprintf('MODULE_LOCAL_THEMES'  );
				$groups['app']['items'] = array();
				
				foreach( $atemplates as $template ){
					
                    // Check to see if the file is in the exclude mask.
    				if ($exclude) {  
    					if (preg_match(chr(1).$exclude.chr(1), $template)) {
    						continue;
    					}
    				}
                    

                    $groups['app']['items'][] = JHtml::_('select.option', $template, $template);	
				}
			}
	//		echo '<pre>'.print_r($groups,1); die;
			$template = JFactory::getApplication(1)->getTemplate();
			// Get the database object and a new query object.
			$db		= JFactory::getDBO();
			$query	= $db->getQuery(true);
			$clientId = 0;
			$query->select('*');
			$query->from('#__template_styles');
			$query->where( "client_id=0 AND home=1" );
	 
			 
			// Set the query and load the templates.
			$db->setQuery($query);
			$template = $db->loadObject(); 
			if( isset($template) ) {
				$ttemplate = JPATH_ROOT.DS."templates".DS.$template->template.DS."html".DS.$extension.DS;
				if(  file_exists($ttemplate)){
					$ttemplates = JFolder::folders( $ttemplate ) ;		
					// Create the group for the template
					$groups['template']		    = array();
					$groups['template']['id']   = "";
					$groups['template']['text'] = JText::sprintf('THEMES_IN_CURRENT_TEMPLATE', $template->template);
					$groups['template']['items'] = array();
								
					foreach( $ttemplates as $template ){
						$groups['template']['items'][] = JHtml::_('select.option', $template, $template);	
					}
				}
			}
			$attr = $this->element['size'] ? ' size="'.(int) $this->element['size'].'"' : '';
	 		// Compute the current selected values
			$selected = array($this->value);
		//	echo '<pre>'.print_r($groups,1); die;
			return JHtml::_('select.groupedlist', $groups, $this->name, array('id'=>$this->id, 'group.id'=>'id', 'list.attr'=>$attr, 'list.select'=>$selected));
 
	}
}
?>
 