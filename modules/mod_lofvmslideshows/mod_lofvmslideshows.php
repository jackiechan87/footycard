<?php
/**
 * $ModDesc
 * 
 * @version		$Id: helper.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) Dec 2009 IceTheme.com.All rights reserved.
 * @license		GNU General Public License version 2
 * -------------------------------------
 * Based on Module Libs From LandOfCoder
 * @copyright (C) May 2010 LandOfCoder.com <@emai:landofcoder@gmail.com, @site: http://landofcoder.com>. 
 */
 
// no direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__).DS.'helper.php';
require_once dirname(__FILE__).DS.'libs'.DS.'lofaddtocart.php';
$list = modLofSlideShowHelper::getList( $params, $module->module );

$group = $params->get( 'group','virtuemart' );
$tmp = $params->get( 'module_height', 'auto' );
$moduleHeight   =  ( $tmp=='auto' ) ? 'auto' : (int)$tmp.'px';
$tmp = $params->get( 'module_width', 'auto' );
$moduleWidth    =  ( $tmp=='auto') ? 'auto': (int)$tmp.'px';
$themeClass 			= $params->get( 'theme' , '');
$openTarget 	= $params->get( 'open_target', '_parent' );
$class 			= !$params->get( 'navigator_pos', 0 ) ? '':'lof-'.$params->get( 'navigator_pos', 0 );
$theme		    =  $params->get( 'theme', '' ); 
$show_price		    =  $params->get( 'show_price', '1' ); 
$show_addcart		    =  $params->get( 'show_addcart', '1' ); 
$target = '';
$navigatorLayout = (($params->get( 'navigator_layout', 'default' )=='custom_layout')?'_custom_navigator':'_navigator').".php";
$navigatorPattern = $params->get( 'custom_layout', '%IMG %TITLE %DATE' );
$navHeight = (int)$params->get('navitem_height', 100);
$navWidth = (int)$params->get('navitem_width', 290);
$thumbnailWidth = (int)$params->get('thumbnail_width', 60);
$thumbnailHeight = (int)$params->get('thumbnail_height', 60);
$thumbnailMargin=$params->get('thumbnail_margin','10px 10px');

if(empty($list)) return false;
$currency = CurrencyDisplay::getInstance( );

modLofSlideShowHelper::loadMediaFiles( $params, $module, $theme );
$css3Class = $params->get('enable_css3','1') ? ' lof-css3':'';

$moreInfor = trim($params->get("showinformation","#CAT | #DATE"));
if( !empty($theme) ){
	$layout = trim($theme).DS.'default';
	require( JModuleHelper::getLayoutPath($module->module, $layout ) );
} else {
	require( JModuleHelper::getLayoutPath($module->module) );
}


?>
<script type="text/javascript">
//window.addEvent("domready", function(){

	var _lofmain =  $('lofslideshow<?php echo $module->id; ?>'); 
	var object = new LofSlideshow( _lofmain,
								  { 
									  fxObject:{
										transition:<?php echo $params->get( 'effect', '' );?>,  
										duration:<?php echo (int)$params->get('duration', '700')?>
									  },
									  startItem:<?php echo (int)$params->get('start_item',0);?>,
									  interval:<?php echo (int)$params->get('interval', '3000'); ?>,
									  direction :'<?php echo $params->get('layout_style','opacity');?>', 
									  navItemHeight:<?php echo $params->get('navitem_height', 100) ?>,
									  navItemWidth:<?php echo $params->get('navitem_width', 290) ?>,
									  navItemsDisplay:<?php echo $params->get('max_items_display', 3) ?>,
									  navPos:'<?php echo $params->get( 'navigator_pos', 0 ); ?>'
								  } );
	<?php if( $params->get('display_button', '') ): ?>
		object.registerButtonsControl( 'click', {next:_lofmain.getElement('.lof-next'),
												 previous:_lofmain.getElement('.lof-previous')} );
	<?php endif; ?>
		object.start( <?php echo $params->get('auto_start',1)?>, _lofmain.getElement('.preload') );
	
// });		
</script>