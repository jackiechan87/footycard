<?php
/*------------------------------------------------------------------------
# mod_webnhanh247_youtube - Youtube Module by Webnhanh247.com
# ------------------------------------------------------------------------
# author    Webnhanh247 http://www.webnhanh247.com
# Copyright (C) 2014 Webnhanh247.com. All Rights Reserved.
# Websites: http://www.webnhanh247.com
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<?php if ($youtube_id) { ?>
<div class="w_youtube">
	<iframe id="youtube<?php echo $uniqid ?>" width="250" height="140" src="http://www.youtube.com/embed/<?php echo $youtube_id ?>?wmode=transparent" frameborder="0"></iframe>
</div>
<?php } else { ?>
	<p>Please enter youtube id.</p>
<?php } ?>