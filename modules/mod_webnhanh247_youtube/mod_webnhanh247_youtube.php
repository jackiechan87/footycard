<?php
/*------------------------------------------------------------------------
# mod_webnhanh247_youtube - Youtube Module by Webnhanh247.com
# ------------------------------------------------------------------------
# author    Webnhanh247 http://www.webnhanh247.com
# Copyright (C) 2014 Webnhanh247.com. All Rights Reserved.
# Websites: http://www.webnhanh247.com
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');
//Parameters
$doc 					= JFactory::getDocument();
$uniqid 				= $module->id;
$youtube_id				= $params->get ('youtube_id');

require(JModuleHelper::getLayoutPath('mod_webnhanh247_youtube'));