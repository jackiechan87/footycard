<?php
if (!class_exists( 'VmConfig' )) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'config.php');
VmConfig::loadConfig();
class leoaddtocart {
	function addtocart($product) {
            if (!VmConfig::get('use_as_catalog',0)) { ?>
                <div class="addtocart-area">

		<form method="post" class="product" action="index.php">
                    <?php
                    // Product custom_fields
                    if (!empty($product->customfieldsCart)) {  ?>
                        <div class="product-fields">
                        <?php foreach ($product->customfieldsCart as $field) { ?>

                            <div style="display:inline-block;" class="product-field product-field-type-<?php echo $field->field_type ?>">
                            <span class="product-fields-title" ><b><?php echo $field->custom_title ?></b></span>
                            <?php echo JHTML::tooltip($field->custom_tip, $field->custom_title, 'tooltip.png'); ?>
                            <span class="product-field-display"><?php echo $field->display ?></span>
                            <span class="product-field-desc"><?php echo $field->custom_field_desc ?></span>
                            </div>

                        <?php } ?>
                        </div>
                    <?php } ?>

                    <div class="addtocart-bar">

			<?php
                        // Display the quantity box
                        ?>
			<span class="quantity-box">
			<input type="text" class="quantity-input" name="quantity[]" value="1" />
			</span>
			<!-- <span class="quantity-controls">
			<input type="button" class="quantity-controls quantity-plus" />
			<input type="button" class="quantity-controls quantity-minus" />
			</span>-->


			<?php
                        // Add the button
			$button_lbl = JText::_('COM_VIRTUEMART_CART_ADD_TO');
			$button_cls = '';
			$stockhandle = VmConfig::get('stockhandle','none');
			if(($stockhandle=='disableit' or $stockhandle=='disableadd') and ($product->product_in_stock - $product->product_ordered)<1){
				$button_lbl = JText::_('COM_VIRTUEMART_CART_NOTIFY');
				$button_cls = 'notify-button';
				$button_name = 'notifycustomer';
			}
			?>
			<?php // Display the add to cart button ?>
			<span class="addtocart-button">
				<input type="submit" name="addtocart"  class="addtocart-button" value="<?php echo $button_lbl ?>" title="<?php echo $button_lbl ?>" />
			</span>

                        <div class="clear"></div>
                    </div>

                    <input type="hidden" class="pname" value="<?php echo $product->product_name ?>"/>
                    <input type="hidden" name="option" value="com_virtuemart" />
                    <input type="hidden" name="view" value="cart" />
                    <noscript><input type="hidden" name="task" value="add" /></noscript>
                    <input type="hidden" name="virtuemart_product_id[]" value="<?php echo $product->virtuemart_product_id ?>" />
                    <input type="hidden" name="virtuemart_category_id[]" value="<?php echo $product->virtuemart_category_id ?>" />
                </form>
		<div class="clear"></div>
            </div>
        <?php }
     }
}

?>