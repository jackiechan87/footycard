<?php // no direct access
defined('_JEXEC') or die('Restricted access');
//JHTML::stylesheet ( 'menucss.css', 'modules/mod_virtuemart_category/css/', false );

/* ID for jQuery dropdown */
$ID = str_replace('.', '_', substr(microtime(true), -8, 8));
$js="jQuery(document).ready(function() {
		jQuery('#VMmenu".$ID." li.VmClose ul').hide();
		jQuery('#VMmenu".$ID." li .VmArrowdown').click(
		function() {

			if (jQuery(this).parent().next('ul').is(':hidden')) {
				jQuery('#VMmenu".$ID." ul:visible').delay(500).slideUp(500,'linear').parents('li').addClass('VmClose').removeClass('VmOpen');
				jQuery(this).parent().next('ul').slideDown(500,'linear');
				jQuery(this).parents('li').addClass('VmOpen').removeClass('VmClose');
			}
		});
	});" ;

		$document = JFactory::getDocument();
		$document->addScriptDeclaration($js);
?>

<div id="category-slider">
	<?php
		$db1 = JFactory::getDbo();
		foreach ($categories as $category) {
			$query1 = $db1->getQuery(true);
			$redirectID = $category->virtuemart_category_id;
			$query1->select(array('id'))
				->from($db1->quoteName('#__virtuemart_categories', 'a'))
				->from($db1->quoteName('#__virtuemart_category_categories', 'b'))
				->where($db1->quoteName('b.category_parent_id')."=".$category->virtuemart_category_id)
				->where($db1->quoteName('b.id')."= a.virtuemart_category_id")
				->order('a.ordering');

			$db1->setQuery($query1);
			$categoryChirls = $db1->loadResult();
			if($categoryChirls){
				$redirectID = $categoryChirls;
			}
			$caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$redirectID);
	?>

	<div class="category-slider-image">
		<a href="<?php echo $caturl;?>">
			<img src="<?php echo $category->images[0]->file_url;?>" title="<?php echo $category->category_name;?>" style="opacity: 1;">
		</a>

	</div>
<?php }?>
</div>
