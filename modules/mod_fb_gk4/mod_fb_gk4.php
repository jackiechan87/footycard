<?php
/**
* Main file
* @package Facebook GK4
* @Copyright (C) 2009-2011 Gavick.com
* @ All rights reserved
* @ Joomla! is Free Software
* @ Released under GNU/GPL License : http://www.gnu.org/copyleft/gpl.html
* @version $Revision: 4.0.0 $
**/
defined('_JEXEC') or die('Restricted access');
/**	Loading helper class **/
require_once (dirname(__FILE__).DS.'helper.php');
$helper = new FB_GK4_Helper($params, $module);
$helper->renderLayout();
?>