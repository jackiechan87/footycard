<?php
/**
* Layout file FB GK4 
* @package FB GK4
* @Copyright (C) 2009-2011 Gavick.com
* @ All rights reserved
* @ Joomla! is Free Software
* @ Released under GNU/GPL License : http://www.gnu.org/copyleft/gpl.html
* @version $Revision: 4.0.0 $
**/
// access restriction
defined('_JEXEC') or die('Restricted access');
?>
<?php if($this->config['code_type'] == 'iframe') : ?>
	<iframe src="http://www.facebook.com/plugins/activity.php?site=<?php echo urlencode($this->config['site']); ?>&amp;width=<?php echo $this->config['width']; ?>&amp;height=<?php echo $this->config['height']; ?>&amp;header=<?php echo $this->config['header']; ?>&amp;colorscheme=<?php echo $this->config['colorscheme']; ?>&amp;linktarget=<?php echo $this->config['link_target']; ?>&amp;font=<?php echo $this->config['font']; ?>&amp;border_color=<?php echo $this->config['border_color']; ?>&amp;recommendations=<?php echo $this->config['recommendations']; ?>" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:<?php echo $this->config['width']; ?>px; height:<?php echo $this->config['height']; ?>px;" allowTransparency="true"></iframe>
<?php elseif($this->config['code_type'] == 'XFBML')  : ?>
	<?php echo '<fb:activity site="'.$this->config['site'].'" width="'.$this->config['width'].'" height="'.$this->config['height'].'" header="'.$this->config['header'].'" font="'.$this->config['font'].'" border_color="" linktarget="_top"  recommendations="'.$this->config['recommedations'].'" linktarget="'.$this->config['link_target'].'"></fb:activity>'; ?>
<?php else : ?>
    <?php echo '<div class="fb-activity" data-width="'.$this->config['width'].'" data-height="'.$this->config['height'].'" data-header="'.$this->config['header'].'" data-linktarget="'.$this->config['link_target'].'" data-recommendations="'.$this->config['recommedations'].'"></div>'; ?>
<?php endif; ?>