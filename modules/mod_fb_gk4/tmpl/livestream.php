<?php
/**
* Layout file FB GK4 
* @package FB GK4
* @Copyright (C) 2009-2011 Gavick.com
* @ All rights reserved
* @ Joomla! is Free Software
* @ Released under GNU/GPL License : http://www.gnu.org/copyleft/gpl.html
* @version $Revision: 4.0.0 $
**/


defined('_JEXEC') or die('Restricted access');

if(($this->config['code_type'] == 'iframe') || ($this->config['code_type'] == 'XFBML') ) {
    echo '<fb:live-stream event_app_id="'.$this->config['app_id'].'" width="'.$this->config['livestream_width'].'" height="'.$this->config['livestream_height'].'" xid="'.$this->config['livestream_xid'].'" via_url="'.$this->config['livestream_url'].'" always_post_to_friends="'.$this->config['livestream_friends'].'"></fb:live-stream>';
} else {
    echo '<div class="fb-live-stream" data-event-app-id="'.$this->config['app_id'].'" data-width="'.$this->config['livestream_width'].'" data-height="'.$this->config['livestream_height'].'" data-xid="'.$this->config['livestream_xid'].'" data-via-url="'.$this->config['livestream_url'].'" data-always-post-to-friends="'.$this->config['livestream_friends'].'"></div>';
}

/* eof */