<?php
/**
* Layout file FB GK4 
* @package FB GK4
* @Copyright (C) 2009-2011 Gavick.com
* @ All rights reserved
* @ Joomla! is Free Software
* @ Released under GNU/GPL License : http://www.gnu.org/copyleft/gpl.html
* @version $Revision: 4.0.0 $
**/
// access restriction
defined('_JEXEC') or die('Restricted access');
?>
<?php if($this->config['code_type'] == 'iframe') : ?>
<iframe src="http://www.facebook.com/plugins/facepile.php?href=<?php echo urlencode($this->config['site']); ?>&amp;width=<?php echo $this->config['facepile_width']; ?>&amp;max_rows=<?php echo $this->config['facepile_num_rows']; ?>&amp;size=<?php echo $this->config['facepile_size']; ?>&amp;colorscheme=<?php echo $this->config['facepile_colorscheme']; ?>" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:<?php echo $this->config['facepile_width'] ?>px;" allowTransparency="true"></iframe>
<?php elseif($this->config['code_type'] == 'XFBML') : ?>
<?php echo '<div id="fb-root"></div><fb:facepile href="'.$this->config['site'].'" width="'.$this->config['facepile_width'].'" max_rows="'.$this->config['facepile_num_rows'].'" size="'.$this->config['facepile_size'].'" colorscheme="'.$this->config['facepile_colorscheme'].'"></fb:facepile>'; ?>
<?php else : ?>
<?php echo '<div class="fb-facepile" data-href="'.$this->config['site'].'" data-width="'.$this->config['facepile_width'].'" data-max-rows="'.$this->config['facepile_num_rows'].'" data-size="'.$this->config['facepile_size'].'" data-colorscheme="'.$this->config['facepile_colorscheme'].'"></div>'; ?>
<?php endif; ?>