<?php


/**
* Layout file FB GK4 
* @package FB GK4
* @Copyright (C) 2009-2011 Gavick.com
* @ All rights reserved
* @ Joomla! is Free Software
* @ Released under GNU/GPL License : http://www.gnu.org/copyleft/gpl.html
* @version $Revision: 4.0.0 $
**/



defined('_JEXEC') or die('Restricted access');



?>


<?php if($this->config['only_number_url'] == 'true') : ?>

<iframe src="http://www.facebook.com/plugins/comments.php?href=<?php echo urlencode($this->config['site']); ?>&permalink=1" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:130px; height:16px;" allowTransparency="true" ></iframe> 

<?php elseif($this->config['only_number'] == 'true') : ?>

<?php echo '<fb:comments-count href="'. $this->config['site'] .'"></fb:comments-count> '.$this->config['only_number_add']; ?>

<?php else : ?>
        
    <?php if( ($this->config['code_type'] == 'XFBML') || ($this->config['code_type'] == 'iframe')) : ?>   
	   <?php echo '<fb:comments href="'.$this->config['site'].'" num_posts="'.$this->config['number_comments'].'" width="'.$this->config['width_comments'].'" colorscheme="'.$this->config['comments_colorscheme'].'"></fb:comments>'; ?>
    <?php else : ?>
        <?php echo '<div class="fb-comments" data-href="'.$this->config['site'].'" data-num-posts="'.$this->config['number_comments'].'" data-width="'.$this->config['width_comments'].'" data-colorscheme="'.$this->config['comments_colorscheme'].'"></div>'; ?>
    <?php endif; ?>
<?php endif; ?>