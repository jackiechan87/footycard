<?php
/**
* Layout file FB GK4 
* @package FB GK4
* @Copyright (C) 2009-2011 Gavick.com
* @ All rights reserved
* @ Joomla! is Free Software
* @ Released under GNU/GPL License : http://www.gnu.org/copyleft/gpl.html
* @version $Revision: 4.0.0 $
**/
// access restriction
defined('_JEXEC') or die('Restricted access');
?>
<?php if($this->config['code_type'] == 'iframe') : ?>
	<iframe src="http://www.facebook.com/plugins/likebox.php?href=<?php echo urlencode($this->config['site']); ?>&amp;width=<?php echo $this->config['likebox_width']; ?>&amp;colorscheme=<?php echo $this->config['likebox_colorscheme']; ?>&amp;show_faces=<?php echo $this->config['likebox_faces']; ?>&amp;stream=<?php echo $this->config['likebox_stream'] ?>&amp;header=<?php echo $this->config['likebox_header']; ?>&amp;height=<?php echo $this->config['likebox_height']; ?>" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:<?php echo $this->config['likebox_width'] ?>px; height:<?php echo $this->config['likebox_height']; ?>px;" allowTransparency="true"></iframe>
<?php elseif($this->config['code_type'] == 'XFBML') : ?>
	<?php echo '<fb:like-box href="'.$this->config['site'].'" width="'.$this->config['likebox_width'].'" height="'.$this->config['likebox_height'].'" colorscheme="'.$this->config['likebox_colorscheme'].'" show_faces="'.$this->config['likebox_faces'].'" stream="'.$this->config['likebox_stream'].'" header="'.$this->config['likebox_header'].'"></fb:like-box>'; ?>
<?php else : ?>
<?php echo '<div class="fb-like-box" data-href="'.$this->config['site'].'" data-width="'.$this->config['likebox_width'].'" data-height="'.$this->config['likebox_height'].'" data-colorscheme="'.$this->config['likebox_colorscheme'].'" data-show-faces="'.$this->config['likebox_faces'].'" data-stream="'.$this->config['likebox_stream'].'" data-header="'.$this->config['likebox_header'].'"></div>'; ?>
<?php endif; ?>