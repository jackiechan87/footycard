<?php
/**
* Layout file FB GK4 
* @package FB GK4
* @Copyright (C) 2009-2011 Gavick.com
* @ All rights reserved
* @ Joomla! is Free Software
* @ Released under GNU/GPL License : http://www.gnu.org/copyleft/gpl.html
* @version $Revision: 4.0.0 $
**/
defined('_JEXEC') or die('Restricted access');
if($this->config['code_type'] == 'iframe') {
	echo '<iframe src="http://www.facebook.com/plugins/recommendations.php?site='.urlencode($this->config['site']).'&amp;width='.$this->config['rec_width'].'&amp;height='.$this->config['rec_height'].'&amp;header='.$this->config['rec_header'].'&amp;colorscheme='.$this->config['rec_colorscheme'].'&amp;font='.$this->config['rec_font'].'&amp;linktarget='.$this->config['rec_link_target'].'&amp;border_color="'.$this->config['rec_border_color'].'scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:'.$this->config['rec_width'].'px; height:'.$this->config['rec_height'].'px;" allowTransparency="true"></iframe>';
} elseif($this->config['code_type'] == 'XFBML') {
	echo '<fb:recommendations site="'.$this->config['site'].'" width="'.$this->config['rec_width'].'" height="'.$this->config['rec_height'].'" linktarget="'.$this->config['rec_link_target'].'" header="'.$this->config['rec_header'].'" colorscheme="'.$this->config['rec_colorscheme']. '" font="'.$this->config['rec_font'].'" border_color="'.$this->config['rec_border_color'].'"></fb:recommendations>';
} else {
    echo '<div class="fb-recommendations" data-site="'.$this->config['site'].'" data-width="'.$this->config['rec_width'].'" data-height="'.$this->config['rec_height'].'" data-linktarget="'.$this->config['rec_link_target'].'" header="'.$this->config['rec_header'].'" data-colorscheme="'.$this->config['rec_colorscheme']. '" data-font="'.$this->config['rec_font'].'" data-border-color="'.$this->config['rec_border_color'].'"></div>';
}
/* eof */