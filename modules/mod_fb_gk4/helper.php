<?php
/**
* Helper file
* @package FB GK4
* @Copyright (C) 2009-2011 Gavick.com
* @ All rights reserved
* @ Joomla! is Free Software
* @ Released under GNU/GPL License : http://www.gnu.org/copyleft/gpl.html
* @version $Revision: 4.0.0 $
**/
// access restriction
defined('_JEXEC') or die('Restricted access');
// Main class
class FB_GK4_Helper {
	var $config = array(); // configuration array
	function __construct(&$params, &$module) {
        // Basic settings
		$this->module_id = ($params->get('automatic_module_id',0) == 1) ? 'gkFacebook_'.$module->id : $params->get('module_unique_id',0);
        $this->config['code_type'] = $params->get('code_type', 'iframe');
        $this->config['language'] = $params->get('language', 'en_US');
        $this->config['plugin'] = $params->get('data_source','');
        $this->config['comments_admin_id'] = $params->get('comments_admin_id','');
        $this->config['app_id'] = $params->get('app_id', '');
        if($params->get('auto_url', '') == 'true'){
            $this->config['site'] = JURI::current();
        } else {
            $this->config['site'] = $params->get('site', 'www.gavick.com');
        }
        if($this->config['plugin'] == 'like_box') {
        	$this->config['site'] = $params->get('site', 'www.gavick.com');
        }
        // Activity Feed
        $this->config['width'] = $params->get('width','300');
        $this->config['height'] = $params->get('height','300');
        $this->config['header'] = $params->get('header','true');
        $this->config['colorscheme'] = $params->get('colorscheme','light');
        $this->config['font'] = $params->get('font', '');
        $this->config['border_color'] = $params->get('border_color', '#ffffff');
        $this->config['recommedations'] = $params->get('recommendations', 'true');
        $this->config['link_target'] = $params->get('link_target', '_blank');
        // Comments
        $this->config['only_number_url'] = $params->get('only_number_url', 'false');
        $this->config['number_comments'] = $params->get('number_comments', '2');
        $this->config['width_comments'] = $params->get('width_comments', '500');
        $this->config['only_number'] = $params->get('only_number', 'false');
        $this->config['only_number_add'] = $params->get('only_number_add', '');
        $this->config['only_number_url'] = $params->get('only_number_url', 'false');
        $this->config['comments_admin_id'] = $params->get('comments_admin_id', '');
        $this->config['comments_colorscheme'] = $params->get('comments_colorscheme', 'light');
        // Facepile
        $this->config['facepile_width'] = $params->get('facepile_width', '200');
        $this->config['facepile_num_rows'] = $params->get('facepile_num_rows', '1');
        $this->config['facepile_colorscheme'] = $params->get('facepile_colorscheme', 'light');
        $this->config['facepile_size'] = $params->get('facepile_size', 'small');
        //likebox
        $this->config['likebox_width'] = $params->get('likebox_width', '200');
        $this->config['likebox_height'] = $params->get('likebox_height', '200');
        $this->config['likebox_header'] = $params->get('likebox_header', 'true');
        $this->config['likebox_colorscheme'] = $params->get('likebox_colorscheme', 'light');
        $this->config['likebox_faces'] = $params->get('likebox_faces', 'true');
        $this->config['likebox_stream'] = $params->get('likebox_stream', 'true');
        //livestream
        $this->config['livestream_width'] = $params->get('livestream_width', '200');
        $this->config['livestream_height'] = $params->get('livestream_height', '400');
        $this->config['livestream_xid'] = $params->get('livestream_xid', '');
        $this->config['livestream_url'] = $params->get('livestream_url', '');
        $this->config['livestream_width'] = $params->get('livestream_width', '200');
        $this->config['livestream_friends'] = $params->get('livestream_friends', 'true');
        //recommendations
        $this->config['rec_width'] = $params->get('rec_width', '200');
        $this->config['rec_height'] = $params->get('rec_height', '400');
        $this->config['rec_header'] = $params->get('rec_header', 'true');
        $this->config['rec_colorscheme'] = $params->get('rec_colorscheme', 'light');
        $this->config['rec_font'] = $params->get('rec_font', '');
        $this->config['rec_border_color'] = $params->get('rec_border_color', '');
        $this->config['rec_link_target'] = $params->get('rec_link_target', '_blank');
	}
	// RENDERING LAYOUT
	function renderLayout() {	
		/** GENERATING FINAL XHTML CODE START **/
		// create instances of basic Joomla! classes
		$document = JFactory::getDocument();
		// init $headData variable
		$headData = false;	
		// getting module head section datas
		unset($headData);
		$headData = $document->getHeadData();
		// generate keys of script section
		$headData_keys = array_keys($headData["scripts"]);
		// set variable for false
		$script_founded = false;
		// searching phrase facebook in scripts paths
		if(array_search('connect.facebook.net'.$this->config['language'].'/all.js#xfbml=1', $headData_keys) > 0) {
			$script_founded = true;
		}
		// if facebook script file doesn't exists in document head section
        if(!$script_founded && $this->config['app_id'] == ''){ 
		    $document->addScript('http://connect.facebook.net/'.$this->config['language'].'/all.js#xfbml=1');
     	} else if(!$script_founded && $this->config['app_id'] != '' && $this->config['plugin'] == 'live_stream'){ 
		    echo '<div id="fb-root"></div><script type="text/javascript" src="http://connect.facebook.net/'.$this->config['language'].'/all.js#appId='.$this->config['app_id'].'&amp;xfbml=1"></script>';
     	} else {
     	  	$document->addScript('http://connect.facebook.net/'.$this->config['language'].'/all.js#xfbml=1');
     	}
        
      
        
		// select proper layout to load
        switch($this->config['plugin']) {
            case 'activity_feed' : require(JModuleHelper::getLayoutPath('mod_fb_gk4', 'activity_feed')); break;
           	case 'comments'      : {
                    if($this->config['comments_admin_id'] != ''){
                    	// add custom meta tags for comments administratrion
                    	$document->addCustomTag("<meta property=\"fb:admins\" content=\"".$this->config['comments_admin_id']."\"/>");
           			}
            		if($this->config['app_id'] != ''){
            			$document->addCustomTag("<meta property=\"fb:app_id\" content=\"".$this->config['app_id']."\"/>");
            		}
	            require(JModuleHelper::getLayoutPath('mod_fb_gk4', 'comments')); 
            	break;
            }
            case 'facepile'      :  require(JModuleHelper::getLayoutPath('mod_fb_gk4', 'facepile')); break;
            case 'like_box'      :  require(JModuleHelper::getLayoutPath('mod_fb_gk4', 'likebox')); break;
            case 'live_stream'    :  require(JModuleHelper::getLayoutPath('mod_fb_gk4', 'livestream')); break;
            case 'recommendations': require(JModuleHelper::getLayoutPath('mod_fb_gk4', 'recommendations')); break;
            default              :  break;
        }
	}
}
/* eof */