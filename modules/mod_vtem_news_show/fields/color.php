<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/
// no direct access
defined('_JEXEC') or die();
jimport('joomla.html.html');
jimport('joomla.form.formfield');
JHTML::_('behavior.mootools');
class JFormFieldColor extends JFormField
{
    public $type = 'color';
    public function getInput(){   
	      return "<input name='" . $this->name . "' data-hex='true' type='color' value='" . $this->value . "' class='inputcolor' />";
    }
}