<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/
// no direct access
defined('_JEXEC') or die('Restricted accessd');
require_once (dirname(__FILE__).DS.'helper.php');
$content_source = $params->get('content_source');
$newsshow = $params->get('newsshow_count', 3);
$theme = $params->get('theme', 'style1');
$panel_width = $params->get('panel_width', 630);
$panel_height = $params->get('panel_height', 250);
$frame_width = $params->get('frame_width', 100);
$frame_height = $params->get('frame_height', 45);
$frame_opacity = $params->get('frame_opacity', 0.8);
$start_at_index = $params->get('start_at_index', 1);
$next_prev = $params->get('next_prev', 1);
$back_forward = $params->get('back_forward', 1);
$transition_speed = $params->get('transition_speed', 3000);
$animationTime = $params->get('animationTime', 400);
$autostart = $params->get('autostart', 0);
$effect = $params->get('effect', 'slide-hori');
$keyboard_move = $params->get('keyboard_move', 1);
$background_color = $params->get('background_color', '#333');
$color_active = $params->get('color_active', '#f80');
$border = $params->get('border', '1px solid #ddd');
$overlay_opacity = $params->get('overlay_opacity', 0.6);
$overlay_text_color = $params->get('overlay_text_color', '#fff');
$overlay_height = $params->get('overlay_height', '100px');
$linkedimage = $params->get('linkedimage', 0);
$showcaption = $params->get('showcaption', 0);
$linktarget = $params->get('linktarget');
$jquery = $params->get('jquery', 1);
$css = $params->get('css');
$imagePath 	= modVtemnewsshowHelper::cleanDir($params->get( 'imagePath', 'images/sampledata/fruitshop' ));
$sortCriteria = $params->get( 'sortCriteria', 0);
$sortOrder = $params->get( 'sortOrder', 'asc');
$sortOrderManual = $params->get( 'sortOrderManual', '');

if($content_source == "mods"){
	if (trim($sortOrderManual) != "")
		$images = explode(",", $sortOrderManual);
	else
		$images = modVtemnewsshowHelper::imageList($imagePath, $sortCriteria, $sortOrder);
}else{	
	$list = modVtemnewsshowHelper::getLists($params);
}
$module_id = 'newsshowid'.$module->id;

$doc =& JFactory::getDocument();
if($jquery ==1){
   $jqueryload = 'if(!window.jQuery){
   var script = document.createElement("script");
   script.type = "text/javascript";
   script.src = "'.JURI::root(true).'/modules/mod_vtem_news_show/styles/jquery-1.7.2.min.js";
   document.getElementsByTagName("head")[0].appendChild(script);}';
   $doc->addScriptDeclaration($jqueryload);
}elseif($jquery ==2){
   $doc->addScript(JURI::root(true).'/modules/mod_vtem_news_show/styles/jquery-1.7.2.min.js');
}
$doc->addStyleSheet(JURI::root(true).'/modules/mod_vtem_news_show/styles/styles.css');
$customstyle='#vtem'.$module_id.'-newsshow{width:'.$panel_width.';}
#vtem'.$module_id.'-newsshow .ad-image-wrapper{height:'.$panel_height.';}
#vtem'.$module_id.'-newsshow .ad-nav .vt_thumb,
#vtem'.$module_id.'-newsshow .ad-image-wrapper .ad-image{background-color:'.$background_color.'; border-color:'.$border.';}
#vtem'.$module_id.'-newsshow .ad-nav .ad-active .vt_thumb{border-color:'.$color_active.';}
#vtem'.$module_id.'-newsshow .ad-image-wrapper .ad-image-description{background-color:'.$background_color.'; color:'.$overlay_text_color.';}
#vtem'.$module_id.'-newsshow .ad-image-wrapper .ad-image-description a,
#vtem'.$module_id.'-newsshow .ad-image-wrapper .ad-image-description .vtem-newsshow-readon{color:'.$overlay_text_color.' !important;}
#vtem'.$module_id.'-newsshow .ad-thumb-list .vt_thumb{width:'.$frame_width.'px;height:'.$frame_height.'px;}
#vtem'.$module_id.'-newsshow.vtem-newsshow-style1 .ad-back,#vtem'.$module_id.'-newsshow.vtem-newsshow-style1 .ad-forward{height:'.$frame_height.'px;}
#vtem'.$module_id.'-newsshow.vtem-newsshow-style2 .ad-back,#vtem'.$module_id.'-newsshow.vtem-newsshow-style2 .ad-forward,
#vtem'.$module_id.'-newsshow.vtem-newsshow-style4 .ad-back,#vtem'.$module_id.'-newsshow.vtem-newsshow-style4 .ad-forward,
#vtem'.$module_id.'-newsshow.vtem-newsshow-style5 .ad-back,#vtem'.$module_id.'-newsshow.vtem-newsshow-style5 .ad-forward{height:'.$frame_height.'px;}
#vtem'.$module_id.'-newsshow.vtem-newsshow-style3 .ad-nav{background-color:'.$background_color.';border-color:'.$border.';}
#vtem'.$module_id.'-newsshow.vtem-newsshow-style3 .ad-back,#vtem'.$module_id.'-newsshow.vtem-newsshow-style3 .ad-forward{height:'.$frame_height.'px;}';
if(!$back_forward){
$customstyle.='#vtem'.$module_id.'-newsshow.vtem_news_show .ad-nav{width:'.($panel_width + 18).'px;padding:0;}';
}
$customstyle.=$css;
$doc->addStyleDeclaration($customstyle);
require(JModuleHelper::getLayoutPath('mod_vtem_news_show'));