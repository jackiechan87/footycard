/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/

if ($defined(window.jQuery) && $type(jQuery.noConflict)=='function') {
    jQuery.noConflict();
}

jQuery(document).ready( function(){  
	 jQuery('#newsshow_BASIC-options').parent().addClass("vt_basic");
     jQuery('#newsshow_JOOMLA-options').parent().addClass("vt_joomla");
	 jQuery('#newsshow_K2-options').parent().addClass("vt_k2");
	 jQuery('#newsshow_MODULE-options').parent().addClass("vt_mods");
	 jQuery('.inputcolor,#jform_params_border,#jform_params_rounded').parent().addClass("activecolor");
/*	 
	 jQuery('#jform_params_style').each(function() {
		currentValue = jQuery(this).val();
		jQuery(this).find("option").each(function(index,Element) {		
		    if(jQuery(Element).val() == currentValue){		    	
		    	jQuery('.activecolor').slideDown();
		    }else{		    	
		    	jQuery('.activecolor').hide();
		    }
		});
	});	
	 jQuery('#jform_params_style').change(function() {
		currentValue = jQuery(this).val();
		jQuery(this).find("option").each(function(index,Element) {		
		    if(jQuery(Element).val() == currentValue){		    	
		    	jQuery('.activecolor').slideDown();
		    }else{		    	
		    	jQuery('.activecolor').hide();
		    }
		});
	});	
*/	 
    //On Click
    jQuery('.panel .title').click(function(){
        if( jQuery(this).next().is(':hidden') ) { //If immediate next container is closed...
            jQuery('.panel').removeClass('active'); //Remove all "active" state and slide up the immediate next container
        }
        else{
            jQuery('.panel').removeClass('active');
        }
        return false; //Prevent the browser jump to the link anchor
    });
	    
    //Joomla, k2 and modules  newsshow hide and show effect depend on content source.
    function showJoomla(){
        jQuery('.vt_joomla').addClass('active').slideDown();
        jQuery('.vt_k2').removeClass('active').hide();
        jQuery('.vt_mods').removeClass('active').hide();
    }
    function showK2(){
        jQuery('.vt_joomla').removeClass('active').hide();
        jQuery('.vt_mods').removeClass('active').hide();
        jQuery('.vt_k2').addClass('active').slideDown();
    }
    function showMods(){
        jQuery('.vt_joomla').removeClass('active').hide();
        jQuery('.vt_k2').removeClass('active').hide();
        jQuery('.vt_mods').addClass('active').slideDown();
    }
    
    //determine which settings is enable in content source and show related container
    switch(jQuery('#jform_params_content_source').val()){
        case 'joomla':
            showJoomla();
            break;
        case 'k2':
            showK2();
            break;
        case 'mods':
            showMods();
            break;
    }
    
    //change newsshow realtime 
    jQuery('#jform_params_content_source').change(function(){
        switch(jQuery('#jform_params_content_source').val()){
        case 'joomla':
            showJoomla();
            break;
        case 'k2':
            showK2();
            break;
        case 'mods':
            showMods();
            break;
    }
    });
});