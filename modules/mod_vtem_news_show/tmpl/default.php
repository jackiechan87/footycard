<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/
?>
<script type="text/javascript" src="<?php echo JURI::root(true)?>/modules/mod_vtem_news_show/styles/jquery.ad-gallery.js"></script>
<script type="text/javascript">
var vtemnewsshow = jQuery.noConflict();
(function($){
	$(document).ready(function(){ 
		<?php
		if($content_source == "mods"){
		     if($showcaption == 1){
				foreach($images as $key => $img):
					  $vttitles = explode(";",$params->get('imagetitle'));
					  $vttitle = (isset($vttitles[$key])) ? $vttitles[$key] : '';
					  $vtcontents = explode(";",$params->get('imagecontent'));
					  $vtcontent = (isset($vtcontents[$key])) ? $vtcontents[$key] : '';
						  echo "$('#vtem".$key." img').data('ad-desc', '<h4 class=\"vtem_news_show_title\">".trim($vttitle)."</h4><div>".trim($vtcontent)."</div>');";
				endforeach;
			}
		}else{
			for($i=0; $i<count($list); $i++):
				if($list[$i]->introtext != NULL){
		echo "$('#vtem".$i." img').data('ad-desc', '".trim(str_replace("'","\'",preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "",$list[$i]->introtext)))."');";
				}
			endfor;
		}
		?>
		$('#vtem<?php echo $module_id;?>-newsshow').adGallery({
                     loader_image: '<?php echo JURI::root();?>modules/mod_vtem_news_show/images/loading.gif',
					 update_window_hash: false,
                     start_at_index: <?php echo $start_at_index-1;?>,
                     <?php 
					   if($theme =="style1") echo 'bottompos: 50,';
					   if($theme =="style4" || $theme =="style3") echo 'bottompos: 20,';
					 ?>
                     thumb_opacity: <?php echo $frame_opacity;?>,
                     animation_speed: <?php echo $animationTime;?>,
                     width: '<?php echo $panel_width;?>',
                     height: '<?php echo $panel_height;?>',
                     display_next_and_prev: <?php echo $next_prev;?>,
                     display_back_and_forward: <?php echo $back_forward;?>,
                     slideshow: {
                       autostart: <?php echo $autostart;?>,
                       speed: <?php echo $transition_speed;?>
                     },
                     effect: '<?php echo $effect;?>', // or 'slide-vert', 'fade', or 'resize', 'none'
                     enable_keyboard_move: <?php echo $keyboard_move;?>,
					 link_target: '_self'
		});
	});
})(jQuery);
</script>
<div id="vtem<?php echo $module_id;?>-newsshow" class="vtem-newsshow-<?php echo $theme;?> vtem_news_show clearfix newsshow<?php echo $params->get('moduleclass_sfx');?>">
  <div class="ad-image-wrapper"></div>
      <div class="ad-nav">
        <div class="ad-thumbs">
          <ul class="ad-thumb-list">
	    <?php
		if($content_source == "mods"){
				foreach($images as $key => $img):
				   $vtlinks = explode(";",$params->get('urls'));
				   $vtlink = (isset($vtlinks[$key])) ? $vtlinks[$key] : '';
				   if($linkedimage == 1){
				     $link = 'longdesc="'.trim($vtlink).'"';
				   }else{
				   $link = '';
				   }
						  echo '<li id="vtem'.$key.'">
						     <a href="'.JURI::root().$imagePath.$img.'"><img class="vt_thumb" '.$link.' src="'.JURI::root().$imagePath.$img.'" alt="" /></a>';
						  echo '</li>';
				endforeach;
		}else{
			for($i=0; $i<count($list); $i++):
				if($list[$i]->introtext != NULL){
					echo "<li id='vtem".$i."'>".$list[$i]->image."</li>\n";
				}
			endfor;
		}
		?>
	    </ul>
	</div>
  </div>
</div>