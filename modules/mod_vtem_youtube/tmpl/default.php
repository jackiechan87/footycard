<?php
/*------------------------------------------------------------------------
# mod_vtem_youtube - VTEM Youtube Module
# ------------------------------------------------------------------------
# author Nguyen Van Tuyen
# copyright Copyright (C) 2011 VTEM.NET. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.vtem.net
# Technical Support: Forum - http://vtem.net/en/forum.html
-------------------------------------------------------------------------*/
// no direct access
defined('_JEXEC') or die('Restricted access');
$slideID 				= $params->get( 'slideID','vtemfisheyemenu1' );
$width 			= $params->get('width', 450);
$height		= $params->get('height', 350);
$displaytype		= $params->get('displaytype', 'single' );
$videoid 		= $params->get('videoid', 'vj7yWqVy7s4');
$feedid 		= $params->get('feedid', 'machinima');
$background		= $params->get('background', '#eee');
$textcolor		= $params->get('textcolor', '#333');
$activecolor		= $params->get('activecolor', '#026b92');
$showthumbnails		= $params->get('showthumbnails', 1 ) ? 'true' : 'false';
$showdescription				= $params->get( 'showdescription', 0 ) ? 'true' : 'false';
$related				= $params->get( 'related', 0 ) ? 'true' : 'false';
$thumbpos				= $params->get( 'thumbpos', 'right' );
$maxresults		= $params->get('maxresults', 10);
$script 				= $params->get( 'script', 1 );
$css 				= $params->get( 'css' );

if($showthumbnails == 'true'){$thumbwidth = 280;}else{$thumbwidth = 0;}
if($thumbpos == "right"){$thumbpos1='left';}else{$thumbpos1='right';}
if($displaytype == "single"){$videoid1=$videoid;}else{$videoid1=$feedid;}
$doc =& JFactory::getDocument();
if($displaytype != "single"){
$style1 = '#'.$slideID.'{background:'.$background.';position:relative;color:'.$textcolor.';width:'.($width+$thumbwidth).'px}'
         .'#'.$slideID.':after,.vtemyoutube_thumb:after {content: ".";display: block;height: 0;clear: both;visibility: hidden;zoom:1;}.vtemyoutube_thumb{display: inline-block;}'
         .'#'.$slideID.' .videothumbnails{width:'.$thumbwidth.'px;display:block;float:'.$thumbpos.';overflow:hidden;overflow-y:auto;height:'.$height.'px;}'
		 .'#'.$slideID.' .videothumbnails div{display:block;margin:5px 5px 0 5px;background-image:url('.JURI::root().'modules/mod_vtem_youtube/assets/bg.png);padding:5px;cursor:pointer;text-align:'.$thumbpos1.'}'
		 .'#'.$slideID.' .videothumbnails div.vtemyoutube_active{background-color:'.$activecolor.'}'
		 .'#'.$slideID.' .videothumbnails div img{display:block;margin-'.$thumbpos.':5px;float:'.$thumbpos1.';width:80px;}'
		 .'#'.$slideID.' .videoholder{display:block;float:left;}';
}else{$style1 = "";}
$style = $style1.$css;
$doc->addStyleDeclaration( $style );
if ($script == 1){
echo "<script src='".JURI::root()."modules/mod_vtem_youtube/assets/jquery-1.4.2-min.js' type='text/javascript'></script>";
}
?>
<script type="text/javascript" src="<?php echo JURI::root();?>modules/mod_vtem_youtube/assets/jquery.simpletube.js"></script>
<script type="text/javascript" src="<?php echo JURI::root();?>modules/mod_vtem_youtube/assets/jquery.swfobject.1-1-1.min.js"></script>
<script type="text/javascript">
var vtemyoutube = jQuery.noConflict();
(function($) {
$(document).ready(function(){
    $('#<?php echo $slideID;?>').simpletube({
	        feedid: '<?php echo $videoid1;?>', // feed url, only required parameters
			maxresults: <?php echo $maxresults;?>, // number of results to show, 0 for no limit
			defaultvideo: 'none', // default video to display initially, uses the YouTube video id parameter
			displaytype: '<?php echo $displaytype;?>', // playlist, single, user
			activeclass: 'vtemyoutube_active', // css class applied to the thumbnail div when it is the current video playing
			videowidth: <?php echo $width;?>, // width of the displayed video
			videoheight: <?php echo $height;?>, // height of the displayed video
			showthumbnails: <?php echo $showthumbnails;?>, // show or hide thumbnails
			showdescription: <?php echo $showdescription;?>, // show or hide video description
			<?php if($displaytype == "single"){echo 'related:'.$related.',';}?>
			thumbpos:'before' // arrange the thumbnails before or after video		
	});
});
})(jQuery);
</script>
<div id="<?php echo $slideID;?>"></div>
