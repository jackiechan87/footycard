<?php // no direct access
defined('_JEXEC') or die('Restricted access');
//JHTML::stylesheet ( 'menucss.css', 'modules/mod_virtuemart_category/css/', false );

/* ID for jQuery dropdown */
$ID = str_replace('.', '_', substr(microtime(true), -8, 8));
$js="jQuery(document).ready(function() {
		jQuery('#VMmenu".$ID." li.VmClose ul').hide();
		jQuery('#VMmenu".$ID." li .VmArrowdown').click(
		function() {

			if (jQuery(this).parent().next('ul').is(':hidden')) {
				jQuery('#VMmenu".$ID." ul:visible').delay(500).slideUp(500,'linear').parents('li').addClass('VmClose').removeClass('VmOpen');
				jQuery(this).parent().next('ul').slideDown(500,'linear');
				jQuery(this).parents('li').addClass('VmOpen').removeClass('VmClose');
			}
		});
	});" ;

		$document = JFactory::getDocument();
		$document->addScriptDeclaration($js);
?>
<table class="VMmenu<?php echo $class_sfx ?>" id="<?php echo "VMmenu".$ID ?>" border="1">
	<?php
		$i = 1;

		foreach ($categories as $category) {
//			print_r($category->images[0]->file_url);exit('ccc');
			$class = ($i%2 == 1) ? 'menu-category-first' : 'menu-category-second' ;
	?>
		<tr class="<?php echo $class;?>">
			<td><?php echo '<div class="category-number">'.$i.'</div>';?></td>
			<td>
				<?php
					$caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$category->virtuemart_category_id);
					$cattext = $category->category_name;
				?>
				<div class="thumbnail-club"><img src="<?php echo $category->images[0]->file_url;?>" /></div>
				<div class="div-club-name">
					<span><?php echo $cattext;?></span>
					<?php //echo JHTML::link($caturl, $cattext);?>
				</div>
			</td>
			<td>0</td>
		</tr>
	<?php
		$i++;
		}
	?>
</table>
<!--<ul class="VMmenu--><?php //echo $class_sfx ?><!--" id="--><?php //echo "VMmenu".$ID ?><!--" >-->
<?php //foreach ($categories as $category) {
//		 $active_menu = 'class="VmClose"';
//		$caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$category->virtuemart_category_id);
//		$cattext = $category->category_name;
//		//if ($active_category_id == $category->virtuemart_category_id) $active_menu = 'class="active"';
//		if (in_array( $category->virtuemart_category_id, $parentCategories)) $active_menu = 'class="VmOpen"';
//
//		?>

<!--<li --><?php //echo $active_menu ?><!-->
<!--	<div >-->
<!--		--><?php //echo JHTML::link($caturl, $cattext);
//		if ($category->childs) {
//			?>
<!--			<span class="VmArrowdown"> </span>-->
<!--			--><?php
//		}
//		?>
<!--	</div>-->
<?php //if ($category->childs) { ?>
<!--<ul class="menu--><?php //echo $class_sfx; ?><!--">-->
<?php
//		foreach ($category->childs as $child) {
//
//		$caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$child->virtuemart_category_id);
//		$cattext = $child->category_name;
//		?>

<!--<li>-->
<!--	<div >--><?php //echo JHTML::link($caturl, $cattext); ?><!--</div>-->
<!--</li>-->
<?php	//	} ?>
<!--</ul>-->
<?php //	} ?>
<!--</li>-->
<?php
//	} ?>
<!--</ul>-->
