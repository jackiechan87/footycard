<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.model');

JModel::addIncludePath(JPATH_SITE . '/components/com_travelbook/models', 'TravelbookModel');

/**
 * @package		Joomla.Site
 * @subpackage	mod_travelbook
 * @since		1.5
 */
class modTravelbookHelper
{
    
    public static function getSearchModel() {
		$model = JModel::getInstance('search', 'TravelbookModel', array('ignore_request' => true));
		return $model;
    }

    /**
	 * Display the search button as an image.
	 *
	 * @param	string	$button_text	The alt text for the button.
	 *
	 * @return	string	The HTML for the image.
	 * @since	1.5
	 */
	public static function getSearchImage($button_text)
	{
		$img = JHtml::_('image', 'mod_travelbook/'.JText::_('MOD_TRAVELBOOK_SEARCH').'.png', $button_text, NULL, true, true);
		return $img;
	}
}
