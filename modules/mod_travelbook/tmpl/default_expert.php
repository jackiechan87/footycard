<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$output = null;
$output .= $searchForm->getLabel('departure', 'advanced');
$output .= $searchForm->getInput('departure', 'advanced');

$output .= $searchForm->getLabel('arrival', 'advanced');
$output .= $searchForm->getInput('arrival', 'advanced');

$output .= $searchForm->getLabel('duration-from', 'advanced');
$output .= $searchForm->getInput('duration-from', 'advanced');

$output .= $searchForm->getLabel('duration-to', 'advanced');
$output .= $searchForm->getInput('duration-to', 'advanced');

$output .= $searchForm->getLabel('category', 'advanced');
$output .= $searchForm->getInput('category', 'advanced');

$output .= $searchForm->getLabel('destination', 'advanced');
$output .= $searchForm->getInput('destination', 'advanced');

if ($button) {
    if ($imagebutton) {
        $button = '<input type="image" value="'.$button_text.'" class="button'.$moduleclass_sfx.'" src="'.$img.'" onclick="this.form.searchword.focus();"/>';
    } else {
        $button = '<input type="submit" value="'.$button_text.'" class="button'.$moduleclass_sfx.'" onclick="this.form.searchword.focus();"/>';
    }
}

switch ($button_pos) {
    case 'top' :
        $button = $button.'<br />';
        $output = $button.$output;
        break;
    case 'bottom' :
        $button = '<br />'.$button;
        $output = $output.$button;
        break;
    case 'right' :
        $output = $output.$button;
        break;
    case 'left' :
    default :
        $output = $button.$output;
        break;
}

echo $output;