<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$DOM = null;

$searchForm->setFieldAttribute('keyword', 'label', $label, 'keyword');
$searchForm->setFieldAttribute('keyword', 'description', $desc, 'keyword');
$searchForm->setFieldAttribute('keyword', 'size', $size, 'keyword');
$searchForm->setFieldAttribute('keyword', 'class', $class, 'keyword');
$searchForm->setFieldAttribute('keyword', 'type', 'texttitle', 'keyword');
$searchForm->setFieldAttribute('keyword', 'title', $title, 'keyword');
$searchForm->setFieldAttribute('keyword', 'id', 'module_keyword', 'keyword');

$DOM .= $showLabel ? $searchForm->getLabel('keyword', 'keyword') : '';
$DOM .= $searchForm->getInput('keyword', 'keyword', $originalKeyword['keyword']);

$ordering = isset($originalKeyword['ordering']) ? $originalKeyword['ordering'] : 'rate';
$searchForm->setFieldAttribute('ordering', 'id', 'module_ordering', 'keyword');
$searchForm->setFieldAttribute('ordering', 'type', 'hidden', 'keyword');
$DOM .= $searchForm->getInput('ordering', 'keyword', $ordering);

echo $DOM;