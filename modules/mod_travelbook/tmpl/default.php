<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die; 

JHtml::_('behavior.tooltip'); ?>

<?php $type = $params->get('type', '_advanced'); ?>

<form action="<?php echo JRoute::_('index.php');?>" method="post" class="<?php echo $type; ?>">
	<div class="travelbook search<?php echo $moduleclass_sfx ?>">

	<?php
        require JModuleHelper::getLayoutPath('mod_travelbook', $params->get('layout', 'default').'_'.$type); 
	?>

	<?php 
	    $output = null;
	    if ($button) {
            if ($imagebutton) {
                $output .= '<input type="image" value="'.$button_text.'" class="button '.$moduleclass_sfx.'" src="'.$img.'" onclick="this.form.searchword.focus();"/>';
            } else {
                $output .= '<input type="submit" value="'.$button_text.'" class="button '.$moduleclass_sfx.'" onclick="this.form.searchword.focus();"/>';
            }
        }
        $output .= '<div class="clr"></div>';

        echo $output;
	?>

    	<input type="hidden" name="searchtype" value="<?php echo $type; ?>" />
    	<input type="hidden" name="task" value="search" />
    	<input type="hidden" name="limit" value="<?php echo $pagination->limit; ?>" />
    	<input type="hidden" name="option" value="com_travelbook" />
    	<input type="hidden" name="Itemid" value="<?php echo $mitemid; ?>" />
	</div>
	<div class="clr"></div>
</form>