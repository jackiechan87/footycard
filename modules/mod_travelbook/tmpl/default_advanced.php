<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$DOM = null;

$originalCategory = isset($originalCriterias['category']) ? $originalCriterias['category'] : 0;
$searchForm->setFieldAttribute('category', 'id', 'module_category', 'advanced');
$DOM .= $searchForm->getLabel('category', 'advanced');
$DOM .= $searchForm->getInput('category', 'advanced', $originalCategory);

$originalDestination = isset($originalCriterias['destination']) ? $originalCriterias['destination'] : 0;
$searchForm->setFieldAttribute('destination', 'id', 'module_destination', 'advanced');
$DOM .= $searchForm->getLabel('destination', 'advanced');
$DOM .= $searchForm->getInput('destination', 'advanced', $originalDestination);

$originalActivity = isset($originalCriterias['activity']) ? $originalCriterias['activity'] : 0;
$searchForm->setFieldAttribute('activity', 'id', 'module_activity', 'advanced');
$DOM .= $searchForm->getLabel('activity', 'advanced');
$DOM .= $searchForm->getInput('activity', 'advanced', $originalActivity);

$originalDeparture = isset($originalCriterias['departure']) ? $originalCriterias['departure'] : '';
$searchForm->setFieldAttribute('departure', 'id', 'module_departure', 'advanced');
$DOM .= $searchForm->getLabel('departure', 'advanced');
$DOM .= $searchForm->getInput('departure', 'advanced', $originalDeparture);

$originalArrival = isset($originalCriterias['arrival']) ? $originalCriterias['arrival'] : '';
$searchForm->setFieldAttribute('arrival', 'id', 'module_arrival', 'advanced');
$DOM .= $searchForm->getLabel('arrival', 'advanced');
$DOM .= $searchForm->getInput('arrival', 'advanced', $originalArrival);

$originalDurationFrom = isset($originalCriterias['duration-from']) ? $originalCriterias['duration-from'] : '';
$searchForm->setFieldAttribute('duration-from', 'id', 'module_duration_from', 'advanced');
$DOM .= $searchForm->getLabel('duration-from', 'advanced');
$DOM .= $searchForm->getInput('duration-from', 'advanced', $originalDurationFrom);
$DOM .= '<span>'.JText::_('COM_TRAVELBOOK_SEARCH_TO').'</span>';
$originalDurationTo = isset($originalCriterias['duration-to']) ? $originalCriterias['duration-to'] : '';
$searchForm->setFieldAttribute('duration-to', 'id', 'module_duration_to', 'advanced');
$DOM .= $searchForm->getInput('duration-to', 'advanced', $originalDurationTo);
$DOM .= '<span>'.JText::_('COM_TRAVELBOOK_SEARCH_DAYS').'</span>';

$originalBudgetFrom = isset($originalCriterias['budget-from']) ? $originalCriterias['budget-from'] : '';
$searchForm->setFieldAttribute('budget-from', 'id', 'module_budget_from', 'advanced');
$DOM .= $searchForm->getLabel('budget-from', 'advanced');
$DOM .= $searchForm->getInput('budget-from', 'advanced', $originalBudgetFrom);
$DOM .= '<span>'.JText::_('COM_TRAVELBOOK_SEARCH_TO').'</span>';
$originalBudgetTo = isset($originalCriterias['budget-to']) ? $originalCriterias['budget-to'] : '';
$searchForm->setFieldAttribute('budget-to', 'id', 'module_budget_to', 'advanced');
$DOM .= $searchForm->getInput('budget-to', 'advanced', $originalBudgetTo);
$DOM .= '<span>'.JText::_('COM_TRAVELBOOK_SEARCH_EURO').'</span>';

$ordering = isset($originalCriterias['ordering']) ? $originalCriterias['ordering'] : 'relevance';
$searchForm->setFieldAttribute('ordering', 'id', 'module_ordering', 'advanced');
$searchForm->setFieldAttribute('ordering', 'type', 'hidden', 'advanced');
$DOM .= $searchForm->getInput('ordering', 'advanced', $ordering);

echo $DOM;