<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

JHtml::addIncludePath(JPATH_PLATFORM . '/joomla/html/html');
JHtml::addIncludePath(JPATH_SITE.'/components/com_travelbook/models/fields');
JHtml::addIncludePath(JPATH_SITE . '/components/com_travelbook/helpers/html');

$doc = JFactory::getDocument();

$lang = JFactory::getLanguage();

JHtml::stylesheet('mod_travelbook/layout.css', false, true);
JHtml::stylesheet('mod_travelbook/custom.css', false, true);
$doc->addScript(JURI::root().'media/mod_travelbook/js/mod_travelbook.config.js');

$upper_limit = $lang->getUpperLimitSearchWord();

$button = $params->get('button', '');
$imagebutton = $params->get('imagebutton', '');
//$button_pos = $params->get('button_pos', 'left');
$button_text = htmlspecialchars($params->get('button_text', JText::_('MOD_TRAVELBOOK_SEARCHBUTTON_TEXT')));

$maxlength = $upper_limit;
$searchwordText = htmlspecialchars($params->get('text', JText::_('MOD_TRAVELBOOK_SEARCHBOX_TEXT')));
$destinationText = htmlspecialchars($params->get('destination-text', JText::_('MOD_TRAVELBOOK_DESTINATION_TEXT')));

$showLabel = $params->get('showlabel', true);
$showTitle = $params->get('showtitle', true);

$label = htmlspecialchars($params->get('label', JText::_('COM_TRAVELBOOK_SEARCH_KEYWORD_LABEL')));
$desc = htmlspecialchars($params->get('desc', JText::_('COM_TRAVELBOOK_SEARCH_KEYWORD_DESC')));
$size = intval($params->get('width', 20));
$title = $showTitle ? htmlspecialchars($params->get('title', JText::_('COM_TRAVELBOOK_SEARCH_TITLE'))) : '';
$class= "inputbox left overtext";


$set_Itemid = intval($params->get('set_itemid', 0));
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

if ($imagebutton) {
    $img = modTravelbookHelper::getSearchImage($button_text);
}

$originalKeyword = JRequest::getVar('keyword', array('keyword' => '', 'ordering' => 'rate'), 'get', 'array');
$originalCriterias = JRequest::getVar('advanced', '', 'get', 'array');

$model = modTravelbookHelper::getSearchModel();

$pagination = $model->getPagination();

$searchForm = $model->getForm(array(), false);

$mitemid = $set_Itemid > 0 ? $set_Itemid : JRequest::getInt('Itemid');
require JModuleHelper::getLayoutPath('mod_travelbook', $params->get('layout', 'default'));