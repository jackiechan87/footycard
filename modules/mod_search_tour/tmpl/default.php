<?php
// no direct access
defined('_JEXEC') or die('Restricted access');
JHTML::stylesheet('style.css', 'modules/mod_search_tour/css/');
JHTML::script('jquery-1.7.2.min', 'modules/mod_search_tour/js/');
?>

<div id="search_tour<?php echo $params->get('moduleclass_sfx'); ?>">
    <div class="tabbed_area">
        <ul class="tabs">
            <li><a href="#" title="content_1" class="tab active">PARTICULIER</a></li>
            <li><a href="#" title="content_2" class="tab">ENTREPRISE</a></li>
        </ul>

        <div id="content_1" class="content">
            <p>
                <label class="lb1">Pays d’origine</label>
            </p>
            <p>
                <select id="tour1" name="tour1">
                    <option value="1">Le pays</option>
                    <option value="2">Conseil</option>
                    <option value="3">Destinations</option>
                </select>
            </p>    
            <p>  
                <label class="lb2">Destination</label>
            </p>
            <p>
                <select id="tour2" name="tour2">
                    <option value="1">Le pays</option>
                    <option value="2">Conseil</option>
                    <option value="3">Destinations</option>
                </select>  
            </p>  
            <p>  
                <label class="lb3">Durée du séjour</label>
            </p>
            <p>
                <select id="tour3" name="tour3">
                    <option value="1">Le pays</option>
					<option value="2">Conseil</option>
                    <option value="3">Destinations</option>
                </select>  
            </p>
            <p>
                <input type="submit" name="envoyez" class="envoyez" value="DEVIS GRATUIT"/>
            </p>
        </div>
        <div id="content_2" class="content">
            <p>
                <label class="lb1">Pays d’origine</label>
            </p>
            <p>
                <select id="tour1" name="tour1">
                    <option value="1">Le pays</option>
                    <option value="2">Conseil</option>
                    <option value="3">Destinations</option>
                </select>
            </p>    
            <p>  
                <label class="lb2">Destination</label>
            </p>
            <p>
                <select id="tour2" name="tour2">
                    <option value="1">Le pays</option>
                    <option value="2">Conseil</option>
                    <option value="3">Destinations</option>
                </select>  
            </p>  
            <p>  
                <label class="lb3">Durée du séjour</label>
            </p>
            <p>
                <select id="tour3" name="tour3">
                    <option value="1">Le pays</option>
					<option value="2">Conseil</option>
                    <option value="3">Destinations</option>
                </select>  
            </p>
            <p>
                <input type="submit" name="envoyez" class="envoyez" value="DEVIS GRATUIT"/>
            </p>
        </div>

    </div>
</div>

<script>
	$(function(){
        $("a.tab").click(function() {
            $(".active").removeClass("active");
            $(this).addClass("active");
            $(".content").slideUp();
            var content_show = $(this).attr("title");
            $("#" + content_show).slideDown();
        });
    });
</script>