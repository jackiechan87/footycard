<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/
// no direct access
defined('_JEXEC') or die;
require_once (JPath::clean(JPATH_SITE . '/components/com_content/models/articles.php'));
require_once (JPath::clean(JPATH_SITE . '/components/com_content/helpers/route.php'));
require_once (JPath::clean(JPATH_SITE . '/libraries/joomla/html/html/content.php'));
class modVtemSlidesHelper
{
	public static function getList(&$params){
	global $mainframe;
		
        $db         =& JFactory::getDBO();
        $user       =& JFactory::getUser();
        $userId     =  (int) $user->get('id');
        $aid        =  $user->get('aid', 0);
        $nullDate   =  $db->getNullDate();
        $date       =& JFactory::getDate();
        $now        =  $date->toMySQL();
		$app = JFactory::getApplication();
		$user_id = $params->get('user_id');
		
        $content_source = $params->get('content_source','joomla');
        //joomla specific
        if($content_source == 'joomla'){
				// Get an instance of the generic articles model
				$articles = JModel::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
				// Set application parameters in model
				$appParams = $app->getParams();
				$articles->setState('params', $appParams);
				// Set the filters based on the module params
				$articles->setState('list.start', 0);
				$articles->setState('list.limit', (int) $params->get('count', 3));
				$articles->setState('filter.published', 1);
				// Access filter
				$access = !JComponentHelper::getParams('com_content')->get('show_noauth');
				$authorised = JAccess::getAuthorisedViewLevels(JFactory::getUser()->get('id'));
				$articles->setState('filter.access', $access);
				$catids = $params->get('catid');
				$articles->setState('filter.category_id.include', (bool) $params->get('category_filtering_type', 1));
				// Category filter
				if ($catids) {
				 if ($params->get('show_child_category_articles', 0) && (int) $params->get('levels', 0) > 0) {
					// Get an instance of the generic categories model
					$categories = JModel::getInstance('Categories', 'ContentModel', array('ignore_request' => true));
					$categories->setState('params', $appParams);
					$levels = $params->get('levels', 1) ? $params->get('levels', 1) : 9999;
					$categories->setState('filter.get_children', $levels);
					$categories->setState('filter.published', 1);
					$categories->setState('filter.access', $access);
					$additional_catids = array();
	
					foreach($catids as $catid)
					{
						$categories->setState('filter.parentId', $catid);
						$recursive = true;
						$items = $categories->getItems($recursive);
	
						if ($items)
						{
							foreach($items as $category)
							{
								$condition = (($category->level - $categories->getParent()->level) <= $levels);
								if ($condition) {
									$additional_catids[] = $category->id;
								}
	
							}
						}
					}
	
					$catids = array_unique(array_merge($catids, $additional_catids));
				 }
	
				$articles->setState('filter.category_id', $catids);
			  }
			// Ordering
			$articles->setState('list.ordering', $params->get('article_ordering', 'a.ordering'));
			$articles->setState('list.direction', $params->get('article_ordering_direction', 'ASC'));
			// New Parameters
			$articles->setState('filter.featured', $params->get('show_front', 'show'));
			$articles->setState('filter.author_id', $params->get('created_by', ""));
			$articles->setState('filter.author_id.include', $params->get('author_filtering_type', 1));
			$article_filtering_type = $params->get('article_filtering_type', 1);
			$article_ids = $params->get('article_ids', '');
			if($article_filtering_type == 1){
			  if ($article_ids) {
				$article_ids = explode(",", $article_ids);
				$articles->setState('filter.article_id', $article_ids);
				$articles->setState('filter.article_id.include', true); 
			  }
			}else{
				$article_ids = explode(",", $article_ids);
				$articles->setState('filter.article_id', $article_ids);
				$articles->setState('filter.article_id.include', false); 
			}
			$items = $articles->getItems();
	        // Display options
			$show_date = $params->get('show_date', 0);
			$show_date_field = $params->get('show_date_field', 'created');
			$show_date_format = $params->get('show_date_format', 'Y-m-d H:i:s');
			$show_category = $params->get('show_category', 0);
			$show_hits = $params->get('show_hits', 0);
			$show_author = $params->get('show_author', 0);
			$show_introtext = $params->get('show_introtext', 0);
			$introtext_limit = $params->get('introtext_limit', 100);
			$image_poisition = $params->get('image_poisition', 0);
			$link_titles = $params->get('link_titles', 1);
			$image_type = $params->get('image_type', 'image_article');
	
			// Find current Article ID if on an article page
			$option = JRequest::getCmd('option');
			$view = JRequest::getCmd('view');
	
			if ($option === 'com_content' && $view === 'article') {
				$active_article_id = JRequest::getInt('id');
			}
			else {
				$active_article_id = 0;
			}
			// Prepare data for display using display options
			$i = 0;
			$lists = array();
			foreach ($items as &$item){
					$item->slug = $item->id.':'.$item->alias;
					$item->catslug = $item->catid ? $item->catid .':'.$item->category_alias : $item->catid;
					if ($access || in_array($item->access, $authorised)){ // We know that user has the privilege to view the article
						$lists[$i]->link = JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catslug));
					}else {
						// Angie Fixed Routing
						$menu	= $app->getMenu();
						$menuitems	= $menu->getItems('link', 'index.php?option=com_users&view=login');
						if(isset($menuitems[0])) {
							$Itemid = $menuitems[0]->id;
						} else if (JRequest::getInt('Itemid') > 0) { //use Itemid from requesting page only if there is no existing menu
							$Itemid = JRequest::getInt('Itemid');
						}
		
						$lists[$i]->link = JRoute::_('index.php?option=com_users&view=login&Itemid='.$Itemid);
					}
					$item->displayImage = '';
					$images = json_decode($item->images);
					if($image_type == 'image_full' && (isset($images) && $images->image_fulltext!= '')) {
						$item->displayImage = $images->image_fulltext;
					}elseif($image_type == 'imgae_intro' && (isset($images) && $images->image_intro!='')) {
						$item->displayImage = $images->image_intro;
					}elseif($image_type == 'image_article'){
						if(preg_match('/\<img.*src=.*?\>/', $item->introtext)){
							$imgStartPos = JString::strpos($item->introtext, 'src="');
							if($imgStartPos)  $imgEndPos = JString::strpos($item->introtext, '"', $imgStartPos + 5);	
							if($imgStartPos > 0) $item->displayImage = JString::substr($item->introtext, ($imgStartPos + 5), ($imgEndPos - ($imgStartPos + 5)));
						}
					}
					if($item->displayImage){
					 $lists[$i]->introtext ='<a class="vtem_slides_a'.$image_poisition.'" href="'.$lists[$i]->link.'"><img class="vtem_slides_img'.$image_poisition.'" src="'.JURI::root().$item->displayImage.'" alt="vtem slides" /></a>';
					}else{
					 $lists[$i]->introtext ='';
					}
					if($link_titles){
					  $lists[$i]->introtext .= '<h4 class="vtemslides-title"><a href="'.$lists[$i]->link.'">'.$item->title.'&nbsp;'.($show_hits ? '('.$item->hits.')' : '').'</a></h4>';
					}else{
					  $lists[$i]->introtext .= '<h4 class="vtemslides-title">'.$item->title.'&nbsp;'.($show_hits ? '('.$item->hits.')' : '').'</h4>';
					}
					$lists[$i]->introtext .= '<div class="vtemsilde_infor">';
					if ($item->catid) {
						$item->displayCategoryLink .= JRoute::_(ContentHelperRoute::getCategoryRoute($item->catid));
						$lists[$i]->introtext .= $show_category ? '<a href="'.$item->displayCategoryLink.'">'.$item->category_title.'</a>' : '';
			        }else {
				        $lists[$i]->introtext .= $show_category ? $item->category_title : '';
			        }
			        $lists[$i]->introtext .= $show_author ? '<span class="vtemsildeauthor">'.$item->author.'</span>' : '';
					if ($show_date) {
				      $lists[$i]->introtext .= '<span class="vtemsildedate">'.JHTML::_('date', $item->$show_date_field, $show_date_format).'</span>';
			        }
				    $lists[$i]->introtext .='</div>';
					if ($show_introtext) {
						$item->introtext = JHtml::_('content.prepare', $item->introtext);
						$item->introtext = preg_replace( '/<img[^>]*>/', '', $item->introtext );
						if($params->get('introtext_limit') == 0){
						  $lists[$i]->introtext .= $item->introtext;
						}else{
						  $item->introtext = self::_cleanIntrotext($item->introtext);
						  $lists[$i]->introtext .= $show_introtext ? self::truncate($item->introtext, $introtext_limit) : '';
						}
					}
			
				    ob_start();
                    if ($params->get('show_readmore')) :
						echo '<div class="vtem-slides-readon-wrap"><a class="vtem-slides-readon" href="'.$lists[$i]->link.'"><span>';
							if ($readmore = $params->get('readmore')) :
							  echo $readmore;
							else :
							  echo JText::sprintf('Read more...');
							endif;
						echo '</span></a></div>';
			        endif;
                  $readmore_html = ob_get_clean();
                  $lists[$i]->introtext .= $readmore_html;
				  $i++;
			}
			return $lists;
        }
		if($content_source == 'k2'){
			   // start K2 specific
				require_once(JPATH_SITE . DS . 'components' . DS . 'com_k2' . DS . 'models' . DS . 'itemlist.php');
				require_once(JPATH_SITE . DS . 'components' . DS . 'com_k2' . DS . 'helpers' . DS . 'route.php');
				require_once(JPATH_SITE . DS . 'components' . DS . 'com_k2' . DS . 'helpers' . DS . 'utilities.php');
	
				//Initialize Variables
				$k2_category = $params->get('k2_category', array());
				$k2_children = $params->get('k2_children', 0);
				$k2_ordering = $params->get('k2_ordering', 'a.title');
				$k2_featured = $params->get('k2_featured', 1);
				$k2_image_size = $params->get('k2_image_size', 'M');
				$query = "SELECT a.*, c.name AS categoryname,c.id AS categoryid, c.alias AS categoryalias, c.params AS categoryparams";
				$query .= " FROM #__k2_items as a LEFT JOIN #__k2_categories c ON c.id = a.catid";
				$query .= " WHERE a.published = 1 AND a.access IN(" . implode(',', $user->authorisedLevels()) . ") AND a.trash = 0 AND c.published = 1 AND c.access IN(" . implode(',', $user->authorisedLevels()) . ")  AND c.trash = 0";
				//User Filter
				switch ($user_id){
					case 'by_me':
						$query .= ' AND (a.created_by = ' . (int)$userId . ' OR a.modified_by = ' . (int)$userId . ')';
						break;
					case 'not_me':
						$query .= ' AND (a.created_by <> ' . (int)$userId . ' AND a.modified_by <> ' . (int)$userId . ')';
						break;
				}
	
				$query .= " AND ( a.publish_up = " . $db->Quote($nullDate) . " OR a.publish_up <= " . $db->Quote($now) . " )";
				$query .= " AND ( a.publish_down = " . $db->Quote($nullDate) . " OR a.publish_down >= " . $db->Quote($now) . " )";
	
				if (!is_null($k2_category)) {
					if (is_array($k2_category)) {
						if ($k2_children) {
							require_once (JPATH_SITE . DS . 'components' . DS . 'com_k2' . DS . 'models' . DS . 'itemlist.php');
							$categories = K2ModelItemlist::getCategoryTree($k2_category);
							$sql = @implode(',', $categories);
							$query .= " AND a.catid IN ({$sql})";
	
						} else {
							JArrayHelper::toInteger($k2_category);
							$query .= " AND a.catid IN(" . implode(',', $k2_category) . ")";
						}
	
					} else {
						if ($k2_children) {
							require_once (JPATH_SITE . DS . 'components' . DS . 'com_k2' . DS . 'models' . DS . 'itemlist.php');
							$categories = K2ModelItemlist::getCategoryTree($k2_category);
							$sql = @implode(',', $categories);
							$query .= " AND a.catid IN ({$sql})";
						} else {
							$query .= " AND a.catid=" . (int)$k2_category;
						}
	
					}
				}
	
				if ($k2_featured == '0')
					$query .= " AND a.featured != 1";
	
				if ($k2_featured == '2')
					$query .= " AND a.featured = 1";
	
				if ($app->getLanguageFilter()) {
					$languageTag = JFactory::getLanguage()->getTag();
					$query .= " AND c.language IN (" . $db->Quote($languageTag) . ", " . $db->Quote('*') . ") AND a.language IN (" . $db->Quote($languageTag) . ", " . $db->Quote('*') . ")";
				}
	
				// ordering
				$ordering = $params->get('itemsOrdering');
				switch ($ordering) {
					case 'date' :
						$orderby = 'a.created ASC';
						break;
					case 'rdate' :
						$orderby = 'a.created DESC';
						break;
					case 'alpha' :
						$orderby = 'a.title';
						break;
					case 'ralpha' :
						$orderby = 'a.title DESC';
						break;
					case 'order':
						if ($k2_featured == '2')
							$orderby = 'a.featured_ordering';
						else
							$orderby = 'a.ordering';
						break;
					case 'random' :
						$orderby = 'RAND()';
						break;
					default :
						$orderby = 'a.id DESC';
						break;
				}
	
				$query .= " ORDER BY " . $orderby;
				$db->setQuery($query, 0, $params->get('count', 3));
				$items = $db->loadObjectList();
				
				$k2image_poisition = $params->get('k2image_poisition', 0);
				$i = 0;
				$lists = array();
				foreach ($items as $item)
				{
				  $lists[$i]->id = $item->id;
				  $link = JRoute::_(K2HelperRoute::getItemRoute($item->id, $item->catid));
				  $readmore_register = false;
                  $images = self::getK2Images($item->id, $k2_image_size);
				  $lists[$i]->link = $link;
                  $lists[$i]->readmore_register = $readmore_register;
				  $lists[$i]->title = htmlspecialchars($item->title);
				  if ($images) {
                     $lists[$i]->image = '<img class="vtem_slides_img'.$k2image_poisition.'" src="'.$images->image.'" alt="'.$lists[$i]->title.'"/>';
                  } else {
                   $lists[$i]->image = 'Image not found for the item';
				  }
				  $lists[$i]->introtext = '<a class="vtem_slides_a'.$k2image_poisition.'" href="'.$lists[$i]->link.'">'.$lists[$i]->image.'</a>';
				  $lists[$i]->introtext .= '<h4 class="vtemslides-title"><a href="'.$lists[$i]->link.'">'.$lists[$i]->title.'</a></h4>';
				  if ($params->get('itemIntroText')) {
						if ($params->get('itemIntroTextWordLimit')) {
							$lists[$i]->introtext .= K2HelperUtilities::wordLimit($item->introtext, $params->get('itemIntroTextWordLimit'));
						} else {
							$lists[$i]->introtext .= $item->introtext;
						}
				  }
                  ob_start();
				  if ($params->get('show_k2readmore')) :
					  echo '<a href="'.$lists[$i]->link.'" class="vtem-slides-readon"><span>';
						  if ($lists[$i]->readmore_register) :
						  	echo JText::_('Register to read more...');
						  elseif ($readmore = $params->get('k2readmore')) :
						  	echo $readmore;
						  else :
						  	echo JText::sprintf('Read more...');
					  	  endif; 
					  echo '</span></a>';
				 endif;

                  $readmore_html = ob_get_clean();
                  $lists[$i]->introtext .= $readmore_html;
				  $i++;
				}
				return $lists;
        }else{
				if (!class_exists( 'VmConfig' )) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'config.php');
				VmConfig::loadConfig();
				// Load the language file of com_virtuemart.
				JFactory::getLanguage()->load('com_virtuemart');
				if (!class_exists( 'calculationHelper' )) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'calculationh.php');
				if (!class_exists( 'CurrencyDisplay' )) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'currencydisplay.php');
				if (!class_exists( 'VirtueMartModelVendor' )) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'models'.DS.'vendor.php');
				if (!class_exists( 'VmImage' )) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'image.php');
				if (!class_exists( 'shopFunctionsF' )) require(JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'shopfunctionsf.php');
				if (!class_exists( 'calculationHelper' )) require(JPATH_COMPONENT_SITE.DS.'helpers'.DS.'cart.php');
				if (!class_exists( 'VirtueMartModelProduct' )){
				   JLoader::import( 'product', JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart' . DS . 'models' );
				}
        }
    }
	
  	public static function _cleanIntrotext($introtext)
	{
		$introtext = str_replace('<p>', ' ', $introtext);
		$introtext = str_replace('</p>', ' ', $introtext);
		$introtext = strip_tags($introtext, '');

		$introtext = trim($introtext);

		return $introtext;
	}

	/**
	* This is a better truncate implementation than what we
	* currently have available in the library. In particular,
	* on index.php/Banners/Banners/site-map.html JHtml's truncate
	* method would only return "Article...". This implementation
	* was taken directly from the Stack Overflow thread referenced
	* below. It was then modified to return a string rather than
	* print out the output and made to use the relevant JString
	* methods.
	*
	* @link http://stackoverflow.com/questions/1193500/php-truncate-html-ignoring-tags
	* @param mixed $html
	* @param mixed $maxLength
	*/
	public static function truncate($html, $maxLength = 0)
	{
	    $printedLength = 0;
	    $position = 0;
	    $tags = array();

	    $output = '';

	    if (empty($html)) {
			return $output;
	    }

	    while ($printedLength < $maxLength && preg_match('{</?([a-z]+)[^>]*>|&#?[a-zA-Z0-9]+;}', $html, $match, PREG_OFFSET_CAPTURE, $position))
	    {
	        list($tag, $tagPosition) = $match[0];

	        // Print text leading up to the tag.
			$str = JString::substr($html, $position, $tagPosition - $position);
	        if ($printedLength + JString::strlen($str) > $maxLength) {
	            $output .= JString::substr($str, 0, $maxLength - $printedLength);
	            $printedLength = $maxLength;
	            break;
	        }

	        $output .= $str;
	        $lastCharacterIsOpenBracket = (JString::substr($output, -1, 1) === '<');

	        if ($lastCharacterIsOpenBracket) {
				$output = JString::substr($output, 0, JString::strlen($output) - 1);
	        }

	        $printedLength += JString::strlen($str);

	        if ($tag[0] == '&') {
	            // Handle the entity.
	            $output .= $tag;
	            $printedLength++;
	        }
	        else {
	            // Handle the tag.
	            $tagName = $match[1][0];

	            if ($tag[1] == '/') {
	                // This is a closing tag.
	                $openingTag = array_pop($tags);

	                $output .= $tag;
	            }
	            else if ($tag[JString::strlen($tag) - 2] == '/') {
	                // Self-closing tag.
	                $output .= $tag;
	            }
	            else {
	                // Opening tag.
	                $output .= $tag;
	                $tags[] = $tagName;
	            }
	        }

	        // Continue after the tag.
	        if ($lastCharacterIsOpenBracket) {
				$position = ($tagPosition - 1) + JString::strlen($tag);
			}
			else {
				$position = $tagPosition + JString::strlen($tag);
			}

	    }

	    // Print any remaining text.
	    if ($printedLength < $maxLength && $position < JString::strlen($html)) {
			$output .= JString::substr($html, $position, $maxLength - $printedLength);
	    }

	    // Close any open tags.
	    while (!empty($tags))
	    {
			$output .= sprintf('</%s>', array_pop($tags));
	    }

	    $length = JString::strlen($output);
	    $lastChar = JString::substr($output, ($length - 1), 1);
	    $characterNumber = ord($lastChar);

	    if ($characterNumber === 194) {
			$output = JString::substr($output, 0, JString::strlen($output) - 1);
	    }

		$output = JString::rtrim($output);

	    return $output.'&hellip;';
	}
	
	
    public static function getK2Images($id, $image_size){
        if (file_exists(JPATH_SITE . DS . 'media' . DS . 'k2' . DS . 'items' . DS . 'cache' . DS . md5("Image" . $id) . '_' . $image_size . '.jpg')) {
            $image_path = 'media/k2/items/cache/' . md5("Image" . $id) . '_' . $image_size . '.jpg';
            $images->image = JURI::Root(true) . '/' . $image_path;
            return $images;
        }
    }
	
}
