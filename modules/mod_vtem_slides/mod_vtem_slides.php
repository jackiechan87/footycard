<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/
// no direct access
defined('_JEXEC') or die;
$content_source = $params->get('content_source','joomla');
$width = ($params->get('width') == NULL OR $params->get('width') == 0 OR $params->get('width') == 'auto') ? 'auto' : $params->get('width');
$height = ($params->get('height') == NULL OR $params->get('height') == 0 OR $params->get('height') == 'auto') ? 'auto' : $params->get('height');
$theme = $params->get('style', 'style1');
$transition_type = $params->get('transition_type', 'fade');
$auto_play = $params->get('auto_play', 0) ? $autoplay_delay = $params->get('autoplay_delay', 4000) : $autoplay_delay = 0;
$pauseonhover = $params->get('pauseonhover', 1);
$pause_text = $params->get('pause_text', '[Pause]');
$image_poisition = $params->get('image_poisition', 'left');
$k2image_poisition = $params->get('k2image_poisition', 'left');
$image_size = $params->get('image_size', 0);
$image_width = $params->get('image_width', '200px');
$image_height = $params->get('image_height', '150px');
$prevnext = $params->get('prevnext', 1);
$navigetion = $params->get('navigetion', 1);
$custombackground = $params->get('custombackground','#333');
$customcolor = $params->get('customcolor','#ccc');
$custombackgroundactive = $params->get('custombackgroundactive','#c60');
$customcoloractive = $params->get('customcoloractive','#fff');
$border = $params->get('border','3px solid #555');
$rounded = $params->get('rounded','3px');
$jquery = $params->get('jquery', 1);
$css = $params->get('css');

require_once (dirname(__FILE__).DS.'helper.php');
$list = modVtemSlidesHelper::getList($params);
$module_id = 'slidesid'.$module->id;

if($content_source == 'mods'){
////////////////VITUEMART///////////////
$max_items =  $params->get('count', 3);
$category_id = 		$params->get( 'virtuemart_category_id', null ); // Display products from this category only
$filter_category = 	(bool)$params->get( 'filter_category', 0 ); // Filter the category
$show_price = 		(bool)$params->get( 'productprice', 1 ); // Display the Product Price?
$Product_group = 	$params->get( 'product_group', 'featured'); // Display a footerText
$productname = 	$params->get( 'productname', 1);
$productdesc = 	$params->get( 'productdesc', 1);
$productdetail = 	$params->get( 'productdetail', 0);
$productdetailtext = 	$params->get( 'productdetailtext' );

$mainframe = Jfactory::getApplication();
$virtuemart_currency_id = $mainframe->getUserStateFromRequest( "virtuemart_currency_id", 'virtuemart_currency_id',JRequest::getInt('virtuemart_currency_id',0) );

$vendorId = JRequest::getInt('vendorid', 1);
if ($filter_category ) $filter_category = TRUE;
$productModel = VmModel::getModel('Product');
$list = $productModel->getProductListing($Product_group, $max_items, $show_price, true, false,$filter_category, $category_id);
$productModel->addImages($list);
$totalProd = count( $list );
if(empty($list)) return false;
$currency = CurrencyDisplay::getInstance( );
///////////////END////////////////////
}

$doc =& JFactory::getDocument();
if($jquery ==1){
   $jqueryload = 'if(!window.jQuery){
   var script = document.createElement("script");
   script.type = "text/javascript";
   script.src = "'.JURI::root(true).'/modules/mod_vtem_slides/styles/jquery-1.7.2.min.js";
   document.getElementsByTagName("head")[0].appendChild(script);}';
   $doc->addScriptDeclaration($jqueryload);
}elseif($jquery ==2){
   $doc->addScript(JURI::root(true).'/modules/mod_vtem_slides/styles/jquery-1.7.2.min.js');
}
$doc->addStyleSheet(JURI::root(true).'/modules/mod_vtem_slides/styles/styles.css');
$customstyle='#'.$module_id.' .vtem_slides_img'.$image_poisition.', .vtemslides-item img{float:'.$image_poisition.' !important;';
if($image_size && $content_source == "joomla"){$customstyle .= 'width:'.$image_width.'; height:'.$image_height.';';}
$customstyle .='display:block;}
#'.$module_id.' .vtem_slides_img'.$k2image_poisition.'{float:'.$k2image_poisition.' !important;}
#'.$module_id.'.vtemslides-custom .vtemslidesnav li.active-slide a,.vtemslides-custom .vtemslidesnav li a:hover,.vtemslides-custom .vtemslidesnav li a:focus,.vtemslides-custom .vtemslides-next:hover,.vtemslides-custom .vtemslides-prev:hover{color:'.$customcoloractive.' !important;}
#'.$module_id.'.vtemslides-custom .vtemslidesnav li a,.vtemslides-custom .vtemslides-title,.vtemslides-custom .vtemslides-title a{color:'.$customcolor.' !important;}
#'.$module_id.'.vtemslides-custom .vtem-slides-wrapper-inside{border:'.$border.';-moz-border-radius:'.$rounded.';-webkit-border-radius:'.$rounded.';border-radius:'.$rounded.';background-color:'.$custombackground.' !important;color:'.$customcolor.' !important;}'
.$css;
$doc->addStyleDeclaration($customstyle);
require JModuleHelper::getLayoutPath('mod_vtem_slides', $params->get('layout', 'default'));