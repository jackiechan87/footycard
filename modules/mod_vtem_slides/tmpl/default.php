<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/
// no direct access
defined('_JEXEC') or die;
?>
<script src="<?php echo JURI::root();?>modules/mod_vtem_slides/styles/jquery.cycle.all.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
var vtemslides = jQuery.noConflict();
(function($) {
	$(function() {
		$('#vtem<?php echo $module_id;?>-slides').cycle({ 
			activePagerClass: 'active-slide',
			fx:     '<?php echo $transition_type;?>', 
			speed:  'fast', 
			prev: '#vtem<?php echo $module_id;?>-slidesprev',
			next: '#vtem<?php echo $module_id;?>-slidesnext', 
			pause: <?php echo $pauseonhover;?>,
			timeout: <?php echo $autoplay_delay;?>, 
			pager:  '#<?php echo $module_id;?> .vtemslidesnav', 
			width: '<?php echo $width;?>',
			height: '<?php echo $height;?>',
			pagerAnchorBuilder: function(idx, slide) { 
				return '#<?php echo $module_id;?> .vtemslidesnav li:eq(' + idx + ') a'; 
			}     
		}); 
		<?php if($pauseonhover == 1){?>
		$('#vtem<?php echo $module_id;?>-slides').hover(
			function() { $('#vtem<?php echo $module_id;?>_pause').fadeIn(); },
			function() { $('#vtem<?php echo $module_id;?>_pause').fadeOut(); }); 
		 <?php }?>
		<?php if($params->get('style') == 'style1' || $params->get('style') == 'style1black') :?>
		$('#cycle_pause').click(function() { 
			$('#vtem<?php echo $module_id;?>-slides').cycle('pause');
			$("#cycle_resume").addClass("vtem_active");
			$("#cycle_pause").removeClass("vtem_active"); 
		});
		$('#cycle_resume').click(function() { 
			$('#vtem<?php echo $module_id;?>-slides').cycle('resume');
			$("#cycle_pause").addClass("vtem_active");
			$("#cycle_resume").removeClass("vtem_active"); 
		});
		<?php endif;?>
	});
})(jQuery);
</script>
<div id="<?php echo $module_id;?>" class="vtem-slides-wrapper clearfix vtemslides-<?php echo $params->get('style','style1');?> module<?php echo $params->get('moduleclass_sfx'); ?>">
   <div class="vtem-slides-wrapper-inside"><div class="vtem-slides-inside clearfix">
        <ul id="vtem<?php echo $module_id;?>-slides" class="vtem-slides clearfix slides-nextprev<?php echo $prevnext;?>">
		 <?php
		       if($params->get('content_source') == 'mods'){
			        require(dirname(__FILE__).DS.'vmdefault.php');
			   }else{
					for($i=0; $i<count($list); $i++){
						if($list[$i]->introtext != NULL){
							echo "<li class='vtemslides-panel'><div class='vtemslides-item clearfix'>\n";
								echo $list[$i]->introtext;?>
							<?php echo "</div></li>\n";
						}
					}
			   }
                ?>
  
     </ul>
	 <?php if($auto_play) :?>
	  <span id="vtem<?php echo $module_id;?>_pause" class="vtemslide-pause"><span><?php echo $pause_text;?></span></span>
	 <?php endif;?>
	<?php require(dirname(__FILE__).DS.'slidesnav.php');?>
  </div></div>
</div>
