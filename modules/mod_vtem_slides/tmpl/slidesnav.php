<?php 
if($theme == "style1"  || $theme == "style1black") :
  if($navigetion == 1 || $prevnext == 1) :
?>
	<table class="navigation"><tr>
	  <td align="left" width="60%">
		<?php if($navigetion == 1) :?> 
			<ul class="vtemslidesnav">
				<?php foreach ($list as $key => $item) : ?>
					 <li><a href="#slide-<?php echo $key; ?>"><?php echo $key+1; ?></a></li>
				<?php endforeach; ?>
			</ul>
		<?php endif;?> 
	  </td>
	  <td align="right">
		<?php if($prevnext == 1) :?> 
			<span class="control">
				<a id="vtem<?php echo $module_id;?>-slidesprev" class="vtemslides-next">Next</a>
				<?php if($params->get('auto_play') == 1){
				echo '<a id="cycle_pause" class="vtem_active vtem_pause">Pause</a><a id="cycle_resume" class="vtem_resume">Resume</a>';
				}?>
				<a id="vtem<?php echo $module_id;?>-slidesnext" class="vtemslides-prev">Prev</a>
			 </span>
		 <?php endif;?> 
	  </td>
	</tr></table>
<?php
  endif;
endif;
?>




<?php 
if($theme == "style2" || $theme == "style2black") :
  if($navigetion == 1 || $prevnext == 1) :
?>
	<table class="navigation"><tr>
		 <td width="60" align="left">
		 <?php if($prevnext == 1) :?> 
			 <a id="vtem<?php echo $module_id;?>-slidesprev" class="vtemslides-next">Next</a>
			 <a id="vtem<?php echo $module_id;?>-slidesnext" class="vtemslides-prev">Prev</a>
		 <?php endif;?> 
		 </td>
		 <td align="right">
		   <?php if($navigetion == 1) :?> 
			<ul class="vtemslidesnav">
				<?php foreach ($list as $key => $item) : ?>
					 <li><a href="#slide-<?php echo $key; ?>"></a></li>
				<?php endforeach; ?>
			</ul>
		   <?php endif;?>
		 </td>
	</tr></table>
	<span class="style2coner"></span>
<?php 
  endif;
endif;
?>




<?php 
if($theme == "style3" || $theme == "style4" || $theme == "style3black" || $theme == "style4black") :
  if($navigetion == 1 || $prevnext == 1) :
?>
	<table class="navigation"><tr>
	  <td class="tdprvenext">
	   <?php if($prevnext==1) :?> 
	   <a id="vtem<?php echo $module_id;?>-slidesprev" class="vtemslides-next">Next</a>
	   <?php endif;?> 
	  </td>
	  <td>
		<?php if($navigetion == 1) :?> 
		 <table style="margin:0 auto !important; float:none !important;"><tr><td>
			<ul class="vtemslidesnav">
				<?php foreach ($list as $key => $item) : ?>
					 <li><a href="#slide-<?php echo $key; ?>"></a></li>
				<?php endforeach; ?>
			</ul>
		 </td></tr></table>
		<?php endif;?>
	  </td>
	  <td class="tdprvenext">
		<?php if($prevnext==1) :?> 
		 <a id="vtem<?php echo $module_id;?>-slidesnext" class="vtemslides-prev">Prev</a>
		 <?php endif;?> 
	  </td>
	</tr></table>
<?php 
  endif;
endif;
?>



<?php 
if($theme == "style5" || $theme == "style5black") :
  if($navigetion == 1 || $prevnext == 1) :
?>
	<?php if($prevnext==1) :?> 
		<a id="vtem<?php echo $module_id;?>-slidesprev" class="vtemslides-next">Next</a>
		<a id="vtem<?php echo $module_id;?>-slidesnext" class="vtemslides-prev">Prev</a>
	<?php endif;?>
	<?php if($navigetion == 1) :?> 
		<div style="bottom:-28px;display:block;left:0;padding:8px 0;position:absolute;width:100%;z-index:100;">
			<table class="navigation" align="center"><tr><td>
			  <div class="nav_warp1"><div class="nav_warp2"><div class="nav_warp3">
				<ul class="vtemslidesnav">
					<?php foreach ($list as $key => $item) : ?>
						 <li><a href="#slide-<?php echo $key; ?>"></a></li>
					<?php endforeach; ?>
				</ul>
			  </div></div></div>
			</td></tr></table>
		</div>
	<?php endif;?>
<?php 
  endif;
endif;
?>


<?php 
if($theme == "custom") :
  if($navigetion == 1 || $prevnext == 1) :
?>
  <div class="vtemslidecustom-nav">
	<?php if($prevnext==1) :?> 
		<a id="vtem<?php echo $module_id;?>-slidesprev" class="vtemslides-next">< Prev</a>
	<?php endif;?>
	<?php if($navigetion == 1) :?> 
		<ul class="vtemslidesnav">
			<?php foreach ($list as $key => $item) : ?>
				 <li><a href="#slide-<?php echo $key; ?>"><?php echo $key+1; ?></a></li>
			<?php endforeach; ?>
		</ul>
	<?php endif;?>
	<?php if($prevnext==1) :?> 
		<a id="vtem<?php echo $module_id;?>-slidesnext" class="vtemslides-prev">Next ></a>
	<?php endif;?>
   <div style="clear:both"></div></div>
<?php 
  endif;
endif;
?>