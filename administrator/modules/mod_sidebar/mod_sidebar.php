<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	mod_sidebar
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

if (!class_exists('JSideBar')) 
{
	JLoader::register('JSideBar', JPATH_ADMINISTRATOR. '/components/com_travelbook/helpers/html/sidebar.php');
}

require_once dirname(__FILE__).'/helper.php';

$doc = JFactory::getDocument();
$doc->addStyleSheet(JURI::root().'media/mod_sidebar/css/mod_sidebar.css');

if ($list = modSidebarHelper::getItems()) {
	$groups = modSidebarHelper::getGroups();
	require JModuleHelper::getLayoutPath('mod_sidebar', $params->get('layout', 'default'));
}
