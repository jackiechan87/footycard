<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	mod_sidebar
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

/**
 * @package		Travelbook.Administrator
 * @subpackage	mod_sidebar
 * @since		2.1
 */
abstract class modSidebarHelper
{
	/**
	 * Get the member items of the sidebar.
	 *
	 * @return	mixed	An arry of menu items, or false on error.
	 */
	public static function getItems()
	{
		// Initialise variables.
		$list = array();
		if (class_exists('JSidebar')) {
			$menu = JSidebar::getInstance('sidebar');
			$list = $menu->getItems();
		}
		
		if (!is_array($list) || !count($list)) {
			return false;
		}

		return $list;
	}
	
	public static function getGroups()
	{
		// Initialise variables.
		$menu = JSideBar::getInstance('sidebar');
	
		$groups = $menu->getGroups();
		
		if (!is_array($groups) || !count($groups)) {
			return false;
		}

		return $groups;
	}
}
