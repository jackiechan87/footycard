<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	mod_sidebar
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$hide = JRequest::getInt('hidesidebar');
?>

<?php echo JHtml::_('tabs.start', 'sidebar-slider', array('useCookie'=>1)); ?>

<?php foreach ($groups as $group) : ?>
	<?php echo JHtml::_('tabs.panel', JTEXT::_($group), JApplication::stringURLSafe($group).'-options'); ?>
	<ul id="sidebar">
		<?php foreach ($list as $item) : ?>
			<?php if ($item[0] === $group) : ?>
			<li>
			<?php
				if ($hide) :
					if (isset ($item[3]) && $item[3] == 1) :
						?><span class="nolink active"><?php echo $item[1]; ?></span><?php
					else :
						?><span class="nolink"><?php echo $item[1]; ?></span><?php
					endif;
				else :
					if(strlen($item[2])) :
						if (isset ($item[3]) && $item[3] == 1) :
							?><a class="active" href="<?php echo JFilterOutput::ampReplace($item[2]); ?>"><?php echo $item[1]; ?></a><?php
						else :
							?><a href="<?php echo JFilterOutput::ampReplace($item[2]); ?>"><?php echo $item[1]; ?></a><?php
						endif;
					else :
						?><?php echo $item[1]; ?><?php
					endif;
				endif;
			?>
			</li>
			<?php endif; ?>
		<?php endforeach; ?>
	</ul>
<?php endforeach; ?>

<?php echo JHtml::_('tabs.end'); ?>
