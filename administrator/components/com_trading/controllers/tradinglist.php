<?php
/*------------------------------------------------------------------------
# tradinglist.php - Manage Trading Component
# ------------------------------------------------------------------------
# author    Khuong Tran
# copyright Copyright (C) 2015. All Rights Reserved
# license   www.chienluocweb.com - Tran Hoai Khuong
# website   chienluocweb.com
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controllerform library
jimport('joomla.application.component.controllerform');

/**
 * Trading Controller Trading List
 */
class TradingControllertradinglist extends JControllerForm
{
	public function __construct($config = array())
	{
		$this->view_list = 'trading'; // safeguard for setting the return view listing to the main view.
		parent::__construct($config);
	}

	/**
	 * Function that allows child controller access to model data
	 * after the data has been saved.
	 * 
	 * @param   JModel  &$model     The data model object.
	 * @param   array   $validData  The validated data.
	 * 
	 * @return  void
	 * 
	 * @since   11.1
	 */
	protected function postSaveHook(JModel &$model, $validData = array())
	{
		// Get a handle to the Joomla! application object
		$application = JFactory::getApplication();

		$model->save($data);

	}

}
?>