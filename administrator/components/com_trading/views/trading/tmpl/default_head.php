<?php
/*------------------------------------------------------------------------
# default_head.php - Manage Trading Component
# ------------------------------------------------------------------------
# author    Khuong Tran
# copyright Copyright (C) 2015. All Rights Reserved
# license   www.chienluocweb.com - Tran Hoai Khuong
# website   chienluocweb.com
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

?>
<tr>
	<th width="5">
		<?php echo JText::_('ID'); ?>
	</th>
	<th width="20">
		<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->items); ?>);" />
	</th>
	<th>
		<?php echo JText::_('Trading_code'); ?>
	</th>
	<th>
		<?php echo JText::_('User_id'); ?>
	</th>
	<th>
		<?php echo JText::_('Created-date'); ?>
	</th>
	<th>
		<?php echo JText::_('Status'); ?>
	</th>
	<th>
		<?php echo JText::_('Accepted-date'); ?>
	</th>
</tr>