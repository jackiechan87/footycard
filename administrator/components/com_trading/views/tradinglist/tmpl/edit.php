<?php
/*------------------------------------------------------------------------
# edit.php - Manage Trading Component
# ------------------------------------------------------------------------
# author    Khuong Tran
# copyright Copyright (C) 2015. All Rights Reserved
# license   www.chienluocweb.com - Tran Hoai Khuong
# website   chienluocweb.com
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
JLoader::import('config', JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart' . DS . 'helpers');
JLoader::import('product', JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart' . DS . 'models');
JLoader::import('user', JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart' . DS . 'models');
 
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
$params = $this->form->getFieldsets('params');
$productModel = new VirtueMartModelProduct();

?>
<form action="<?php echo JRoute::_('index.php?option=com_trading&view=tradinglist&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="contact-form" class="form-validate">
	<div class="width-100 fltlft">
		<fieldset class="adminform">
			<legend><?php echo JText::_( 'Details' ); ?></legend>
                        
			<div class="adminformlist">
                            <?php 
                            $this->checkList = json_decode($this->form->getValue('tradewhat'), true);
                            $this->tradeList = json_decode($this->form->getValue('tradefor'), true);
                            $this->checkListQuantity = json_decode($this->form->getValue('customfield1'), true);
                            $this->tradeListQuantity = json_decode($this->form->getValue('customfield1'), true);
                            $field = $this->form->getField('userid');
                            $userid = $field->value;
                            $modelVMUser = new VirtueMartModelUser();
                            $modelVMUser->setId($userid);
                            $user = $modelVMUser->getUser();
                            $keys = array_keys($user->userInfo);
                            $userInfo = $user->userInfo[$keys[0]];
//                            echo "<pre>";
//                            print_r($userInfo);
//                            die();
                            $data = array(
                                'userId' => array('displayName' => 'User ID', 'value' => $userid),
                                'name' => array('displayName' => 'User Name', 'value' => $userInfo->name),
                                'address' => array('displayName' => 'Address', 'value' => $userInfo->address_1 . ' - ' . $userInfo->city),
                                'telephone' => array('displayName' => 'Phone number', 'value' => $userInfo->Telephone),
                                'email' => array('displayName' => 'Email', 'value' => $userInfo->email)
                            );
                            foreach ($data as $field) { ?>
                                    
                            <div>
                                <label id="jform_userid-lbl" for="jform_userid" class=" required"><?php echo JText::_($field['displayName']); ?></label>
                                <?php echo $field['value']; ?>
                            </div>
                            <div class="clr"></div>
                                
                                <?php } 
                                $fields = array('tradingcode', 'createddate');
				foreach($fields as $fieldName){ 
                                    $field = $this->form->getField($fieldName); ?>
					<div>
						<?php echo $field->label; echo $field->value;?>
					</div>
					<div class="clr"></div>
				<?php }; 
                            ?>
                                        
                            <!-- start content -->
                            <?php
					$arrayTradeSummary = array();
                                        foreach ($this->tradeList as $val) {
                                            foreach ($val['chosenCards'] as $playerId => $cards) {
                                                foreach ($cards as $key => $cardId) {
                                                    $arrayTradeSummary[] = array(
                                                        'cart' => $key,
                                                        'cartId' => $cardId,
                                                    );
                                                }
                                            }
                                        }
                                        $arrayCheckListSummary = array();
                                        foreach ($this->checkList as $val) {
                                            foreach ($val['chosenCards'] as $playerId => $cards) {
                                                foreach ($cards as $key => $cardId) {
                                                    $arrayCheckListSummary[] = array(
                                                        'cart' => $key,
                                                        'cartId' => $cardId,
                                                    );
                                                }
                                            }
                                        }
                                        
                            ?>
                            <div id="page-trade">

	<!-- trade summary-->
	<div class="floatleft trade-summary">
		<div class="trade-header">
			<h1><?php echo JText::_('Trade Summary');?></h1>
		</div>
		<div class="trade-border-content">
			<div class="trade-content">
				<table>
					<tr class="row-first-right">
						<td width="4%"></td>
						<td width="10%" align="center"></td>
						<td width="28%" class="trade-summary-title-for"><?php echo JText::_('Trade for?')?></td>
						<td width="5%"></td>
						<td width="5%"></td>
						<td width="10%" align="center"></td>
						<td width="28%" class="trade-summary-title-what"><?php echo JText::_('Trade what?')?></td>
						<td width="5%"></td>
						<td width="5%"></td>
					</tr>
					<?php
						$totalTradeSummary = count($arrayTradeSummary);
						$totalCheckListSummary = count($arrayCheckListSummary);

						$i = (($totalTradeSummary > $totalCheckListSummary) || ($totalTradeSummary == $totalCheckListSummary)) ? $totalTradeSummary : $totalCheckListSummary;
						if($i > 0)
						{
							for($j = 1; $j <= $i; $j++)
							{

								$class = ($j%2 == 1) ? 'menu-category-first' : 'menu-category-second' ;
						?>
						<tr class="<?php echo $class;?>">
							<td><div class="category-number"><?php echo $j;?></div></td>
							<?php
							if (isset($arrayTradeSummary[$j-1]))
							{
								$player = $productModel->getProduct($arrayTradeSummary[$j-1]['cartId']);
								$productModel->addImages($player);
							?>
								<td><?php echo '<div class="thumbnail-club">'.$player->images[0]->displayMediaThumb('class="browseProductImage" border="0" title="' . $player->product_name . '" ', true, 'class="modal"').'</div>';?></td>
								<td>
									<?php

										echo '<div class="div-club-name"><span>'.$player->product_name.'</span></div>';
									?>
								</td>
								<td>
									<?php
										echo $arrayTradeSummary[$j-1]['cart'];
									?>
								</td>
								<td>
									<?php
										if(isset($this->tradeListQuantity[$arrayTradeSummary[$j-1]['cartId']])) {
											echo $this->tradeListQuantity[$arrayTradeSummary[$j-1]['cartId']];
										}
										else
										{
											echo 1;
										}
									?>
								</td>
							<?php
							}
							else
							{
								echo "<td></td><td></td><td></td><td></td>";
							}

							if (isset($arrayCheckListSummary[$j-1])) {
								$player = $productModel->getProduct($arrayCheckListSummary[$j-1]['cartId']);
								$productModel->addImages($player);
							?>
								<td><?php echo '<div class="thumbnail-club">'.$player->images[0]->displayMediaThumb('class="browseProductImage" border="0" title="' . $player->product_name . '" ', true, 'class="modal"').'</div>';?></td>
								<td>
									<?php
										echo '<div class="div-club-name"><span>'.$player->product_name.'</span></div>';
									?>
								</td>
								<td>
									<?php
										echo $arrayCheckListSummary[$j-1]['cart'];
									?>
								</td>
								<td>
									<?php
									if(isset($this->checkListQuantity[$arrayCheckListSummary[$j-1]['cartId']])) {
										echo $this->checkListQuantity[$arrayCheckListSummary[$j-1]['cartId']];
									}
									else
									{
										echo 1;
									}
									?>
								</td>
							<?php
							}
							else
							{
								echo "<td></td><td></td><td></td><td></td>";
							}
							?>

						</tr>
					<?php
							}
						}
					?>
				</table>
			</div>
		</div>
	</div>
</div>
                            <!-- end content -->
			</div>
		</fieldset>
	</div>
	<div>
		<input type="hidden" name="task" value="tradinglist.edit" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
