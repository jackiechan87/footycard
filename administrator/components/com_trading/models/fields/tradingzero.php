<?php
/*------------------------------------------------------------------------
# tradingzero.php - Manage Trading Component
# ------------------------------------------------------------------------
# author    Khuong Tran
# copyright Copyright (C) 2015. All Rights Reserved
# license   www.chienluocweb.com - Tran Hoai Khuong
# website   chienluocweb.com
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the list field type
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

/**
 * tradingcode Form Field class for the Trading component
 */
class JFormFieldtradingzero extends JFormFieldList
{
	/**
	 * The tradingcode field type.
	 *
	 * @var		string
	 */
	protected $type = 'tradingzero';

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return	array		An array of JHtml options.
	 */
	protected function getOptions()
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('#__trading_tradinglist.id as id, #__trading_tradinglist.tradingcode as tradingcode');
		$query->from('#__trading_tradinglist');
		$db->setQuery((string)$query);
		$items = $db->loadObjectList();
		$options = array();
		if($items){
			foreach($items as $item){
				$options[] = JHtml::_('select.option', $item->id, ucwords($item->tradingcode));
			};
		};
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
?>