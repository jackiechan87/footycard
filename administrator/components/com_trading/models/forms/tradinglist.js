/**
 *	Trading List : Validate
 *	Filename : tradinglist.js
 *
 *	Author : Khuong Tran
 *	Component : Manage Trading
 *
 *	Copyright : Copyright (C) 2015. All Rights Reserved
 *	License : www.chienluocweb.com - Tran Hoai Khuong
 *
 **/
window.addEvent('domready', function() {
	document.formvalidator.setHandler('tradingcode',
		function (value) {
			regex=/^[^_]+$/;
			return regex.test(value);
	});
	document.formvalidator.setHandler('userid',
		function (value) {
			regex=/^[^_]+$/;
			return regex.test(value);
	});
});