<?php
/*------------------------------------------------------------------------
# controller.php - Manage Trading Component
# ------------------------------------------------------------------------
# author    Khuong Tran
# copyright Copyright (C) 2015. All Rights Reserved
# license   www.chienluocweb.com - Tran Hoai Khuong
# website   chienluocweb.com
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

/**
 * General Controller of Trading component
 */
class TradingController extends JController
{
	/**
	 * display task
	 *
	 * @return void
	 */
	function display($cachable = false, $urlparams = false)
	{
		// set default view if not set
		JRequest::setVar('view', JRequest::getCmd('view', 'Trading'));

		// call parent behavior
		parent::display($cachable);

		// set view
		$view = strtolower(JRequest::getVar('view'));

		// Set the submenu
		TradingHelper::addSubmenu($view);
	}
}
?>