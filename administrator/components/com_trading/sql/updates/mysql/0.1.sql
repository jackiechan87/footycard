CREATE TABLE IF NOT EXISTS `#__trading_tradinglist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tradingcode` varchar(256) NOT NULL,
  `userid` varchar(256) NOT NULL,
  `createddate` DATE NOT NULL,
  `status` varchar(256) NOT NULL,
  `accepteddate` DATE NOT NULL,
  `tradewhat` TEXT NOT NULL DEFAULT '',
  `tradefor` TEXT NOT NULL DEFAULT '',
  `customfield1` varchar(256) NOT NULL,
  `customfield2` varchar(256) NOT NULL,
  `customfield3` varchar(256) NOT NULL,
  `checked_out` int(11) NOT NULL,
  `checked_out_time` DATETIME NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;