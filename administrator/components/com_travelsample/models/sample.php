<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_travelsample
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/travelsample.php';

/**
 * Item Model for an Sample.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_travelsample
 * @since		1.6
 */
class TravelsampleModelSample extends JModelAdmin
{
	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since	1.6
	 */
	protected $text_prefix = 'COM_TRAVELSAMPLE';

	/**
	 * Batch copy items to a new category or current.
	 *
	 * @param   integer  $value     The new category.
	 * @param   array    $pks       An array of row IDs.
	 * @param   array    $contexts  An array of item contexts.
	 *
	 * @return  mixed  An array of new IDs on success, boolean false on failure.
	 *
	 * @since	11.1
	 */
	protected function batchCopy($value, $pks, $contexts)
	{
		$table = $this->getTable();
		$i = 0;

		// Check that the user has create permission for the component
		$extension = JFactory::getApplication()->input->get('option', '');
		$user = JFactory::getUser();
		if (!$user->authorise('core.create', $extension))
		{
			$this->setError(JText::_('JLIB_APPLICATION_ERROR_BATCH_CANNOT_CREATE'));
			return false;
		}

		// Parent exists so we let's proceed
		while (!empty($pks))
		{
			// Pop the first ID off the stack
			$pk = array_shift($pks);

			$table->reset();

			// Check that the row actually exists
			if (!$table->load($pk))
			{
				if ($error = $table->getError())
				{
					// Fatal error
					$this->setError($error);
					return false;
				}
				else
				{
					// Not fatal error
					$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_BATCH_MOVE_ROW_NOT_FOUND', $pk));
					continue;
				}
			}

			// Alter the title & alias
			$table->title = $this->incrementTitle($table->title);

			// Reset the ID because we are making a copy
			$table->id = 0;

			// Check the row.
			if (!$table->check())
			{
				$this->setError($table->getError());
				return false;
			}

			// Store the row.
			if (!$table->store())
			{
				$this->setError($table->getError());
				return false;
			}

			// Get the new item ID
			$newId = $table->get('id');

			// Add the new ID to the array
			$newIds[$i]	= $newId;
			$i++;
		}

		// Clean the cache
		$this->cleanCache();

		return $newIds;
	}

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param	object	$record	A record object.
	 *
	 * @return	boolean	True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since	1.6
	 */
	protected function canDelete($record)
	{
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return ;
			}
			$user = JFactory::getUser();
			return $user->authorise('core.delete', 'com_travelsample.sample.'.(int) $record->id);
		}
	}

	/**
	 * Method to test whether a record can have its state edited.
	 *
	 * @param	object	$record	A record object.
	 *
	 * @return	boolean	True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since	1.6
	 */
	protected function canEditState($record)
	{
		$user = JFactory::getUser();

		// Check for existing sample.
		if (!empty($record->id)) {
			return $user->authorise('core.edit.state', 'com_travelsample.sample.'.(int) $record->id);
		}
// 		// New sample, so check against the category.
// 		elseif (!empty($record->catid)) {
// 			return $user->authorise('core.edit.state', 'com_travelsample.category.'.(int) $record->catid);
// 		}
		// Default to component settings if neither sample nor category known.
		else {
			return parent::canEditState('com_travelsample');
		}
	}

	/**
	 * Prepare and sanitise the table data prior to saving.
	 *
	 * @param	JTable	A JTable object.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function prepareTable(&$table)
	{
		// Set the publish date to now
		$db = $this->getDbo();
		if($table->state == 1 && intval($table->publish_up) == 0) {
			$table->publish_up = JFactory::getDate()->toSql();
		}

		// Increment the content version number.
		$table->version++;

		// Reorder the samples within the category so the new sample is first
		if (empty($table->id)) {
			$table->reorder('state >= 0');
		}
	}

	/**
	 * Returns a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 *
	 * @return	JTable	A database object
	*/
	public function getTable($type = 'Sample', $prefix = 'TravelsampleTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk)) {
			// Convert the params field to an array.
			$registry = new JRegistry;
			$registry->loadString($item->attribs);
			$item->attribs = $registry->toArray();

			$item->fields = $item->introtext;
		}

		return $item;
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		Data for the form.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 *
	 * @return	mixed	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_travelsample.sample', 'sample', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		$jinput = JFactory::getApplication()->input;

		$id = $jinput->get('id', 0);

		// Determine correct permissions to check.
		if ($this->getState('sample.id')) {
			$id = $this->getState('sample.id');
		}

		$user = JFactory::getUser();

		// Check for existing sample.
		// Modify the form based on Edit State access controls.
		if ($id != 0 && (!$user->authorise('core.edit.state', 'com_travelsample.sample.' . (int) $id))
		|| ($id == 0 && !$user->authorise('core.edit.state', 'com_travelsample'))
		) {
			// Disable fields for display.
			$form->setFieldAttribute('ordering', 'disabled', 'true');
			$form->setFieldAttribute('state', 'disabled', 'true');

			// Disable fields while saving.
			// The controller has already verified this is an sample you can edit.
			$form->setFieldAttribute('ordering', 'filter', 'unset');
			$form->setFieldAttribute('state', 'filter', 'unset');

		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_travelsample.edit.sample.data', array());

		if (empty($data)) {
			$data = $this->getItem();
		}

		return $data;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param	array	The form data.
	 *
	 * @return	boolean	True on success.
	 * @since	1.6
	 */
	public function save($data)
	{
		// Alter the title for save as copy
		if (JRequest::getVar('task') == 'save2copy') {
			list($title, $alias) = $this->generateNewTitle(0, '', $data['title']);
			$data['title'] = $title;
		}

		return parent::save($data);
	}

	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param	object	A record object.
	 *
	 * @return	array	An array of conditions to add to add to ordering queries.
	 * @since	1.6
	 */
	protected function getReorderConditions($table)
	{
		$condition = array();
		$condition[] = '';
		return $condition;
	}

	/**
	 * Custom clean the cache of com_travelsample and content modules
	 *
	 * @since	1.6
	 */
	protected function cleanCache($group = null, $client_id = 0)
	{
		parent::cleanCache('com_travelsample');
	}

	/**
	 * Method to change the title
	 *
	 * @param   string   $title        The title.
	 *
	 * @return	string  Contains the modified title.
	 *
	 * @since	11.1
	 */
	protected function incrementTitle($title)
	{
		// Alter the title & alias
		$table = $this->getTable();
		while ($table->load(array('title' => $title)))
		{
			$title = JString::increment($title);
		}

		return $title;
	}

	
	/**
	 * 
	 * @param array $pks
	 */
	public function import($pks) {
		// Sanitize the ids.
		$pks = (array) $pks;
		JArrayHelper::toInteger($pks);
		
		if (empty($pks)) {
			$this->setError(JText::_('COM_TRAVELSAMPLE_NO_ITEM_SELECTED'));
			return false;
		}
		
		foreach ($pks as $pk) {
			if ($item = $this->getItem($pk)) {
 				$sample = new SimpleXMLElement($item->introtext);
 				$records = $sample->xpath('/sample/record');
 				$count = count($records);
// 				$result = $this->getInsertQuery($records);
				$this->test($records);
			}
		}
		
		return $count;
	}
	
	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	protected function getInsertQuery($records)	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();

		$query->insert('#__tb_samples')->columns('catid, title')->values(array('88,\'title_test\'', '89,\'title_test2\''));
		
		$db->setQuery($query);
		$result = $db->loadObjectList();
		
		return true;
	}
	
	
	/**
	 * 
	 * @param unknown_type $records
	 */
	private function test($records) {
		$sampleModel = $this->getModel('sample');
		
	}
	
	/**
	 * 
	 * @param unknown_type $pks
	 */
	public function remove($pks)
	{
		// Sanitize the ids.
		$pks = (array) $pks;
		JArrayHelper::toInteger($pks);
		
		$count = 0;
		
		if (empty($pks)) {
			$this->setError(JText::_('COM_TRAVELSAMPLE_NO_ITEM_SELECTED'));
			return false;
		}
		
		foreach ($pks as $pk) {
			$count = $count+3;
		}
		
		return $count;
	}
}
