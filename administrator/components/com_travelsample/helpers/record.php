<?php
/**
 * @version		$Id$
 * @package		Travelsample.Administrator
 * @subpackage	com_travelsample
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

/**
 * JRecord Class.
 *
 * @package		Travelsample.Administrator
 * @subpackage	com_travelsample
 * @since		2.0
 */
class SampleRecord
{

	public $fields = array();
	protected $model;
	protected $prefix;
	
	/**
	 * Method to instantiate the sample field object.
	 *
	 * @param   object  $form  The form to attach to the form field object.
	 *
	 * @since   11.1
	 */
	public function __construct($record, $model, $prefix) {
		$this->model = $model;
		$this->prefix = $prefix;
		
		foreach ($record->field as $field) {
			$this->fields[] = new SampleField($field, $this->model, $this->prefix);
		}
	}

	/**
	 * 
	 */
	public function getData() {
		$data = array();
		
		foreach ($this->fields as $field) {
 			$data[$field->name] = $field->getValue();
		}

		return $data;
	}
	
	/**
	 * 
	 */
	private function getValue($field) {
		if ($field->rule == 'sql') {
			$value = '0';
		}
		else {
			$value = $field->value;
		}
		
		return $value;
	}
}
