<?php
/**
 * @version		$Id$
 * @package		Travelsample.Administrator
 * @subpackage	com_travelsample
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

//JLoader::register('JSidebarHelper', dirname(__FILE__) . '/sidebar.php');

/**
 * Travelsample component helper.
 *
 * @package		Travelsample.Administrator
 * @subpackage	com_travelsample
 * @since		2.0
 */
class TravelsampleHelper
{
	public static $extension = 'com_travelsample';

	/**
	 * Configure the Sidebar.
	 *
	 * @param	string	$vName	The name of the active view.
	 *
	 * @return	void
	 * @since	1.6
	 */
	public static function addSubmenu($vName)
	{
		$doc = JFactory::getDocument();
		$doc->addStyleSheet(JURI::root().'media/com_travelsample/administrator/css/com_travelsample.css');
	  
		JSubMenuHelper::addEntry(
		JText::_('COM_TRAVELSAMPLE_SUBMENU_SAMPLES'),
			'index.php?option=com_travelsample&view=samples',
			$vName == 'samples'
		);
	}
	
	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param	int		The category ID.
	 * @param	int		The sample ID.
	 *
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions()
	{
		$user = JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_travelsample';

		$actions = array(
			'core.admin', 
			'core.manage', 
			'core.create', 
			'core.edit', 
			'core.edit.own', 
			'core.edit.state', 
			'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}

}
