<?php
/**
 * @version		$Id$
 * @package		Travelsample.Administrator
 * @subpackage	com_travelsample
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

/**
 * JSample Class.
 *
 * @package		Travelsample.Administrator
 * @subpackage	com_travelsample
 * @since		2.0
 */
class SampleSample
{

	protected $count = 0;
	public $records = array();
	protected $model;
	protected $prefix;
	
	/**
	 * Method to instantiate the sample field object.
	 *
	 * @param   object  $form  The form to attach to the form field object.
	 *
	 * @since   11.1
	 */
	public function __construct($form = null) {
		$sample = new SimpleXMLElement($form);
		$form = new JForm('sample');
		$form->load($sample);

		$model = $form->getFieldAttribute('id', 'types', false);

		$records = $sample->xpath('/form/fields');
		$this->model = (string) $sample->model;
		$this->prefix = (string) $sample->prefix;
		$this->count = (int) $sample->count;
		
		foreach ($records as $record) {
			$this->records[] = new SampleRecord($record, $this->model, $this->prefix);
		}
	}
	
	public function getCount() {
		return $this->count;
	}
	
	public function getModel() {
		return $this->model;
	}
	
	public function getPrefix() {
		return $this->prefix;
	}
	
}
