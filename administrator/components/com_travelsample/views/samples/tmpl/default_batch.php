<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_travelsample
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$published = $this->state->get('filter.published');
?>
<fieldset class="batch">
	<legend><?php echo JText::_('COM_TRAVELSAMPLE_SAMPLE_BATCH_OPTIONS');?></legend>
	<p><?php echo JText::_('COM_TRAVELSAMPLE_SAMPLE_BATCH_TIP'); ?></p>
	<?php echo JHtml::_('batch.access');?>

	<?php if ($published >= 0) : ?>
		<input type="hidden" name="batch[category_id]" class="inputbox" id="batch-category-id" value="1">
		<input type="hidden" name="batch[move_copy]" id="batch[move_copy]c" value="c">
		<?php //echo JHtml::_('batch.item', 'com_travelsample');?>
	<?php endif; ?>

	<button type="submit" onclick="Joomla.submitbutton('sample.batch');">
		<?php echo JText::_('JGLOBAL_BATCH_PROCESS'); ?>
	</button>
	<button type="button" onclick="document.id('batch-category-id').value='';document.id('batch-access').value='';document.id('batch-language-id').value=''">
		<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>
	</button>
</fieldset>
