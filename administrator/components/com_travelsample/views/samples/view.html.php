<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_travelsample
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of samples.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_travelsample
 * @since		1.6
 */
class TravelsampleViewSamples extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return	void
	 */
	public function display($tpl = null)
	{
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->state = $this->get('State');
		$this->authors = $this->get('Authors');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();

		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		$canDo = TravelsampleHelper::getActions();
		$user = JFactory::getUser();
		JToolBarHelper::title(JText::_('COM_TRAVELSAMPLE_SAMPLES_TITLE'), 'sample.png');

		// Built the actions for new and existing records.
		if ($canDo->get('core.create')) {
			JToolBarHelper::custom('samples.import', 'import', 'import_over', JText::_('COM_TRAVELSAMPLE_IMPORT_SAMPLE'), false, true);
		}
		if ($canDo->get('core.delete')) {
			JToolBarHelper::custom('samples.remove', 'tbdelete', 'tbdelete_over', JText::_('COM_TRAVELSAMPLE_DELETE_SAMPLE'), false, true);
		}
		JToolBarHelper::divider();
		
		if ($canDo->get('core.create')) {
			JToolBarHelper::addNew('sample.add');
		}

		if (($canDo->get('core.edit')) || ($canDo->get('core.edit.own'))) {
			JToolBarHelper::editList('sample.edit');
		}

		if ($canDo->get('core.edit.state')) {
			JToolBarHelper::divider();
			JToolBarHelper::publish('samples.publish', 'JTOOLBAR_PUBLISH', true);
			JToolBarHelper::unpublish('samples.unpublish', 'JTOOLBAR_UNPUBLISH', true);
			JToolBarHelper::divider();
			JToolBarHelper::checkin('samples.checkin');
		}

		if ($this->state->get('filter.published') == -2 && $canDo->get('core.delete')) {
			JToolBarHelper::deleteList('', 'samples.delete', 'JTOOLBAR_EMPTY_TRASH');
			JToolBarHelper::divider();
		}
		elseif ($canDo->get('core.edit.state')) {
			JToolBarHelper::trash('samples.trash');
			JToolBarHelper::divider();
		}

		if ($canDo->get('core.admin')) {
			JToolBarHelper::preferences('com_travelsample');
			JToolBarHelper::divider();
		}

//		JToolBarHelper::help('JHELP_CONTENT_SAMPLE_MANAGER');
	}
}
