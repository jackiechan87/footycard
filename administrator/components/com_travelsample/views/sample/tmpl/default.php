<?php
/**
 * @version		$Id$
 * @package		Travelsample.Administrator
 * @subpackage	com_travelsample
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die; ?>

<form action="<?php echo JRoute::_('index.php?option=com_travelsample&view=samples');?>" method="post" name="adminForm" id="adminForm">

	<div>SAMPLE</div>

	<div>
		<input type="hidden" name="task" value="" /> 
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
