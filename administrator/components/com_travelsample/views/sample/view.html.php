<?php
/**
 * @package		Travelsample.Administrator
 * @subpackage	com_travelsample
 *
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of samples.
 *
 * @package		Travelsample.Administrator
 * @subpackage	com_travelsample
 * @since		2.0
 */
class TravelsampleViewSample extends JViewLegacy
{
	protected $form;
	protected $item;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		if ($this->getLayout() == 'pagebreak') {
			// TODO: This is really dogy - should change this one day.
			$eName		= JRequest::getVar('e_name');
			$eName		= preg_replace( '#[^A-Z0-9\-\_\[\]]#i', '', $eName );
			$document	= JFactory::getDocument();
			$document->setTitle(JText::_('COM_TRAVELSAMPLE_PAGEBREAK_DOC_TITLE'));
			$this->assignRef('eName', $eName);
			parent::display($tpl);
			return;
		}

		// Initialiase variables.
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');
		$this->state = $this->get('State');
		$this->canDo = TravelsampleHelper::getActions();

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}
	
	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		JRequest::setVar('hidemainmenu', true);
		$user = JFactory::getUser();
		$userId = $user->get('id');
		$isNew = ($this->item->id == 0);
		$checkedOut = !($this->item->checked_out == 0 || $this->item->checked_out == $userId);
		$canDo = TravelsampleHelper::getActions();
		JToolBarHelper::title(JText::_('COM_TRAVELSAMPLE_PAGE_'.($checkedOut ? 'VIEW_SAMPLE' : ($isNew ? 'ADD_SAMPLE' : 'EDIT_SAMPLE'))), 'sample-add.png');

		// For new records, check the create permission.
		if ($isNew && (count($user->getAuthorisedCategories('com_content', 'core.create')) > 0)) {
			JToolBarHelper::apply('sample.apply');
			JToolBarHelper::save('sample.save');
			JToolBarHelper::save2new('sample.save2new');
			JToolBarHelper::cancel('sample.cancel');
		}
		else {
			// Can't save the record if it's checked out.
			if (!$checkedOut) {
				// Since it's an existing record, check the edit permission, or fall back to edit own if the owner.
				if ($canDo->get('core.edit') || ($canDo->get('core.edit.own') && $this->item->created_by == $userId)) {
					JToolBarHelper::apply('sample.apply');
					JToolBarHelper::save('sample.save');

					// We can save this record, but check the create permission to see if we can return to make a new one.
					if ($canDo->get('core.create')) {
						JToolBarHelper::save2new('sample.save2new');
					}
				}
			}

			// If checked out, we can still save
			if ($canDo->get('core.create')) {
				JToolBarHelper::save2copy('sample.save2copy');
			}

			JToolBarHelper::cancel('sample.cancel', 'JTOOLBAR_CLOSE');
		}

//		JToolBarHelper::divider();
//		JToolBarHelper::help('JHELP_TRAVELSAMPLE_SAMPLE_MANAGER_EDIT');
	}
}
