<?php
/**
 * @package		Travelsample.Administrator
 * @subpackage	com_travelsample
 *
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_travelsample')) {
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

// Register helper class
JLoader::register('TravelsampleHelper', dirname(__FILE__) . '/helpers/travelsample.php');
JLoader::register('TravelsampleVersion', dirname(__FILE__) . '/helpers/version.php');

$controller = JControllerLegacy::getInstance('Travelsample');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
