<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_content
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

JLoader::register('SampleSample', dirname(__FILE__) . '/../helpers/sample.php');
JLoader::register('SampleRecord', dirname(__FILE__) . '/../helpers/record.php');
JLoader::register('SampleField', dirname(__FILE__) . '/../helpers/field.php');

/**
 * Samples list controller class.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_content
 * @since	1.6
 */
class TravelsampleControllerSamples extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param	array	$config	An optional associative array of configuration settings.

	 * @return	TravelsampleControllerSamples
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	/**
	 * Method to import the samples.
	 *
	 * @return	void
	 * @since	1.6
	 */
	function import()
	{
		// Check for request forgeries
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$user = JFactory::getUser();
		$ids = JRequest::getVar('cid', array(), '', 'array');

		if (empty($ids)) {
			JError::raiseWarning(500, JText::_('JERROR_NO_ITEMS_SELECTED'));
		}
		else {
			// Get the model.
			$model = $this->getModel();
			$recordCount = 0;
			foreach ($ids as $id) {
				if ($item = $model->getItem($id)) {
					if ($item->state) {
						$sample = new SampleSample($item->introtext);
						
						$this->addModelPath('components/com_' . strtolower($sample->getPrefix()) . '/models', $sample->getPrefix() . 'Model');
						$sampleModel = $this->getModel($sample->getModel(), $sample->getPrefix() . 'Model');
	
						$count = $sample->getCount();
						for ($i = 1; $i <= $count; $i++) {
							foreach ($sample->records as $record) {
								$data = array();
								$data = $record->getData();
								if (array_key_exists ($item->sample_field, $data)) {
									$registry = new JRegistry;
									$registry->loadString($data[$item->sample_field]);
									$data[$item->sample_field] = $registry->toArray();
									$data[$item->sample_field]['sample'] = '1';
									$registry->loadArray($data[$item->sample_field]);
									$data[$item->sample_field] = (string) $registry;
								} else {
									$data[$item->sample_field] = '{"sample":"1"}';
								}
								$sampleModel->setState($sampleModel->getName() . '.id', 0);
								if (!$sampleModel->save($data)) {
									$this->setMessage(JText::_($sampleModel->getError()));
									$this->setRedirect('index.php?option=com_travelsample&view=samples');
									return false;
								}
								$recordCount++;
							}
						}
					}
				}
			}
		}	

		$this->setMessage(JText::plural($this->text_prefix . '_SAMPLE_N_ITEMS_IMPORTED', $recordCount));
		$this->setRedirect('index.php?option=com_travelsample&view=samples');
	}

	/**
	 * Method to import the samples.
	 *
	 * @return	void
	 * @since	1.6
	 */
	function remove()
	{
		// Check for request forgeries
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$user = JFactory::getUser();
		$ids = JRequest::getVar('cid', array(), '', 'array');

		if (empty($ids)) {
			JError::raiseWarning(500, JText::_('JERROR_NO_ITEMS_SELECTED'));
		}
		else {
			// Get the model.
			$model = $this->getModel();
			$recordCount = 0;
			foreach ($ids as $id) {
				if ($item = $model->getItem($id)) {
					$sample = new SampleSample($item->introtext);
					// Add the model
					$this->addModelPath('components/com_' . strtolower($sample->getPrefix()) . '/models', $sample->getPrefix() . 'Model');
					$sampleModel = $this->getModel($sample->getModel(), $sample->getPrefix() . 'Model');
					// Now get all ids of records having {"sample" : "1"} in params or attribs.
					$db = JFactory::getDBO();
					$db->setQuery('SELECT id FROM ' . $item->table . ' WHERE ' . $item->sample_field . ' LIKE "{%sample%1%}";');
					$results = $db->loadColumn();

					foreach ($results as $key => $result) {
						$sampleModel->publish($result, -2);
					}
					$recordCount = $recordCount + count($results);
					if (!$sampleModel->delete($results)) {
						$this->setMessage(JText::_($sampleModel->getError()));
					}
				}		
			}
		}

		$this->setMessage(JText::plural($this->text_prefix . '_SAMPLE_N_ITEMS_REMOVED', $recordCount));
		$this->setRedirect('index.php?option=com_travelsample&view=samples');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param	string	$name	The name of the model.
	 * @param	string	$prefix	The prefix for the PHP class name.
	 *
	 * @return	JModel
	 * @since	1.6
	 */
	public function getModel($name = 'Sample', $prefix= '', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}
}
