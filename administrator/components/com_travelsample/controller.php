<?php
/**
 * @package		Travelsample.Administrator
 * @subpackage	com_travelsample
 *
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Component Controller
 *
 * @package		Travelsample.Administrator
 * @subpackage	com_travelsample
 */
class TravelsampleController extends JControllerLegacy
{
	/**
	 * @var		string	The default view.
	 * @since	2.0
	 */
	protected $default_view = 'samples';

	/**
	 * Method to display a view.
	 *
	 * @param	boolean			If true, the view output will be cached
	 * @param	array			An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		// Load the submenu.
		TravelsampleHelper::addSubmenu(JRequest::getCmd('view', 'samples'));
					
		$view = JRequest::getCmd('view', 'samples');
		$layout = JRequest::getCmd('layout', 'samples');
		$id = JRequest::getInt('id');

		// Check for sample edit form.
		if ($view == 'sample' && $layout == 'edit' && !$this->checkEditId('com_travelsample.edit.sample', $id)) {
			// Somehow the person just went to the form - we don't allow that.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_travelsample&view=samples', false));

			return false;
		}

		parent::display();

		$version = new TravelsampleVersion();
		echo $version->renderVersion();
		
		return $this;
		
	}
}
