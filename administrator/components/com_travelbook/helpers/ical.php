<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('demopage.ical.ical.loader');
jimport('demopage.ical.jcaldate.timezone');

require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/travelbook.php';

abstract class TravelbookHelperIcal
{

	/**
	 * static method to display an array of events in .ics format
	 *
	 * @param array $events
	 */
	public static function toIcal($events, $dates) {

		$params = JComponentHelper::getParams( 'com_travelbook' );
		$company = $params->get('company');
		
		// instantiate new calendar
		$config = array('unique_id' => str_replace('/administrator', '', trim( JURI::base(), '/')));
		$ical = new vcalendar($config);

		// better for outlook 2003 : set method = publish
		$ical->setMethod('PUBLISH');

		// set some X-properties
		$ical->setProperty("X-WR-CALNAME", JText::sprintf('COM_TRAVELBOOK_ICAL_CALNAME', $company));
		$ical->setProperty("X-WR-CALDESC", JText::sprintf('COM_TRAVELBOOK_ICAL_CALDESC', $company));
		$ical->setProperty("X-WR-TIMEZONE", (string) JCalTimeZone::joomla());


		// add timezones
		$vtimezone = & $ical->newComponent( 'VTIMEZONE' );
		// set timezone properties

		$config = new JConfig();
		$tzid = $config->offset;
		$vtimezone->setProperty('tzid', $tzid);

		if (!empty($events)) {
			foreach ($events as $event) {
				// prepare vevent object with elements from current event
				$item = new vevent();
				// add title
				$item->setProperty('summary', $event->title);

				if ($url = JRoute::_(TravelbookHelperRoute::getTourRoute($event->slug, $event->catslug))) {
					$item->setProperty('url', $url);
				}

				$location = null;
				$destinations = json_decode($event->destination);
				foreach ($destinations as $destination) {
					$dest = JCategories::getInstance('travelbook.destination')->get($destination);
					$location .= $dest->title.', ';
				}
				$location = substr($location, 0, strlen($location)-2);

				// add location
				$item->setProperty('location', $location);

				// add contact
//				$item->setProperty('contact', trim($event->application_name).', '.trim($event->application_phone).', '.trim(strtolower($event->application_email)));

				// add description
				$filter = JFilterInput::getInstance();

				$item->setProperty('description', $filter->clean($event->introtext, 'html'));

				// start and end date
				foreach ($dates as $date) {

					// set Start Date
					$year = JHtml::_('date', $date->departure, 'Y', true);
					$month = JHtml::_('date', $date->departure, 'm', true);
					$day = JHtml::_('date', $date->departure, 'd', true);
					$item->setProperty('dtstart', $year, $month, $day, 0, 0, 0, $tzid);
					// set End Date
					$year = JHtml::_('date', $date->arrival, 'Y', true);
					$month = JHtml::_('date', $date->arrival, 'm', true);
					$day = JHtml::_('date', $date->arrival, 'd', true);
					$item->setProperty('dtend', $year, $month, $day, 0, 0, 0, $tzid);

					// set UID
					$item->setProperty('uid', $event->id);

					// add item to ical
					$ical->addComponent($item);
				}
			}
		}

		$ical->returnCalendar();
		jexit();
	}
}