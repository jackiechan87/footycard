<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

/**
 * JRecord Class.
 *
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @since		2.0
 */
class SampleField
{

	protected $model;
	protected $prefix;
	
	/**
	 * Method to instantiate the sample field object.
	 *
	 * @param   object  $form  The form to attach to the form field object.
	 *
	 * @since   11.1
	 */
	public function __construct($field, $model, $prefix) {
		$this->model = $model;
		$this->prefix = $prefix;
		
		$this->setup($field);
	}

	public function setup(&$element)
	{
		// Make sure there is a valid JFormField XML element.
		if (!($element instanceof SimpleXMLElement) || (string) $element->getName() != 'field')
		{
			return false;
		}
	
		// Get the attributes from the form field element.
		$this->name = (string) $element['name'];
		$this->value = (string) $element['value'];
		$this->type = (string) $element['type'];
		$this->rule = (string) $element['rule'];
		$this->translate = (string) $element['translate']=='true' ? true : false;
		
		
	}
	
	/**
	 * 
	 */
	public function getValue() {
		
		switch ($this->rule) {
			case 'sql':
				$value = $this->getValueFromSql();
				break;
			case 'increment':
				$value = $this->translate ? JText::_($this->value) : $this->value;
				$value = $this->increment($value, $this->name);
				break;
			default:
				$value = $this->translate ? JText::_($this->value) : $this->value;
				break;
		}

		switch ($this->type) {
			case 'array':
				$registry = new JRegistry;
				$registry->loadArray($value);
				$value = (string) $registry;
				break;
			case 'int':
				$value = (int) $value;
				break;
			case 'date':
				$value = $this->calculateDate($value,  $this->name);
				break;
				case 'string':
			default:
				$value = (string) $value;
				break;
			}
				
		
		return $value;
	}

	/**
	 * 
	 */
	protected function getValueFromSql() {
		$query = (string) $this->value;

		// Get the database object.
		$db = JFactory::getDBO();

		// Set the query and get the result list.
		$db->setQuery($query);
		
		$result = $this->type=='array' ? $db->loadColumn() : $db->loadResult();
		return $result;
	}

	/**
	 * Returns a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 *
	 * @return	JTable	A database object
	*/
	public function getTable($type, $prefix, $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to change the title
	 *
	 * @param   string   $title        The title.
	 *
	 * @return	string  Contains the modified title.
	 *
	 * @since	11.1
	 */
	protected function increment($value, $name)
	{
		// Alter the value
		$table = $this->getTable($this->model, $prefix = $this->prefix . 'Table');
		while ($table->load(array($name => $value)))
		{
			$value = JString::increment($value);
		}
	
		return $value;
	}

	/**
	 * 
	 */
	protected function calculateDate($value, $name) {
		$value = new JDate('now');
		// Alter the date
		$table = $this->getTable($this->model, $prefix = $this->prefix . 'Table');
		while ($table->load(array($name => $value->toFormat('%Y-%m-%d'))))
		{
			$value = $value->add(new DateInterval('P1D'));
		}
		
		return $value->toFormat('%Y-%m-%d');
		
	}
	
}

