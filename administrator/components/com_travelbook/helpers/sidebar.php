<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

JLoader::register('JSidebar', dirname(__FILE__) . '/html/sidebar.php');

/**
 * Utility class for the submenu.
 *
 * @package		Joomla.Administrator
 */
abstract class JSidebarHelper
{
	/**
	 * Method to add a menu item to sidemenu.
	 *
	 * @param	string	$name	Name of the menu item.
	 * @param	string	$link	URL of the menu item.
	 * @param	bool	True if the item is active, false otherwise.
	 */
	public static function addEntry($name, $group = '', $link = '', $active = false)
	{
		$menu = JSideBar::getInstance('sidebar');
		$menu->appendButton($group, $name, $link, $active);
		$menu->appendGroup($group);
	}
}

