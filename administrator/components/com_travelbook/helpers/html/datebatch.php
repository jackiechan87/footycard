<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Extended Utility class for batch processing widgets.
 *
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @since       2.0
 */
abstract class JHtmlDateBatch
{
	/**
	 * Displays a batch widget for moving or copying items.
	 *
	 * @return  string  The necessary HTML for the widget.
	 *
	 * @since   2.0
	 */
	public static function tour()
	{
		// Create the copy/move options.
//		$options = array(JHtml::_('select.option', 'c', JText::_('JLIB_HTML_BATCH_COPY')),
//		JHtml::_('select.option', 'm', JText::_('JLIB_HTML_BATCH_MOVE')));

		$lines = array();
	
		$lines[] = '<label id="date-batch-choose-action-lbl" for="date-batch-choose-action">';
		$lines[] = JText::_('COM_TRAVELBOOK_HTML_BATCH_TOUR_LABEL');
		$lines[] = '</label>';
		$lines[] = '<select name="batch[tour_id]" class="inputbox" id="date-batch-tour-id">';
		$lines[] = '<option value="">' . JText::_('JHTML_BATCH_TOUR_NOCHANGE') . '</option>'; 
		$lines[] = JHtml::_('select.options', JHtml::_('tour.options', 0));
		$lines[] = '</select>'; 
		
		return implode("\n", $lines);
	}
}
