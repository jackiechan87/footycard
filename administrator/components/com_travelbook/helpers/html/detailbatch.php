<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Extended Utility class for batch processing widgets.
 *
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @since       2.0
 */
abstract class JHtmlDetailBatch
{
	/**
	 * Displays a batch widget for moving or copying items.
	 *
	 * @return  string  The necessary HTML for the widget.
	 *
	 * @since   2.0
	 */
	public static function tour()
	{
		// Create the batch selector to change select the category by which to move or copy.
		$lines = array();
		$lines[] = '<label id="detail-batch-choose-action-lbl" for="detail-batch-choose-action">';
		$lines[] = JText::_('COM_TRAVELBOOK_HTML_BATCH_TOUR_LABEL');
		$lines[] = '</label>';
		$lines[] = '<select name="batch[tour_id]" class="inputbox" id="detail-batch-tour-id">';
		$lines[] = '<option value="">' . JText::_('JHTML_BATCH_TOUR_NOCHANGE') . '</option>';
		$lines[] = JHtml::_('select.options', JHtml::_('tour.options', 0));
		$lines[] = '</select>';

		return implode("\n", $lines);
	}
}
