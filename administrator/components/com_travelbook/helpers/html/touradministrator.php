<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

/**
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 */
abstract class JHtmlTourAdministrator
{
	/**
	 * @param	int $value	The state value
	 * @param	int $i
	 */
	static function featured($value = 0, $i, $canChange = true)
	{
		// Array of image, task, title, action
		$states	= array(
		0 => array('disabled.png', 'tours.featured', 'COM_TRAVELBOOK_UNFEATURED', 'COM_TRAVELBOOK_TOUR_TOGGLE_TO_FEATURE'),
		1 => array('featured.png', 'tours.unfeatured', 'COM_TRAVELBOOK_FEATURED', 'COM_TRAVELBOOK_TOUR_TOGGLE_TO_UNFEATURE'),
		);
		$state = JArrayHelper::getValue($states, (int) $value, $states[1]);
		$html = JHtml::_('image', 'admin/'.$state[0], JText::_($state[2]), NULL, true);
		if ($canChange) {
			$html = '<a href="#" onclick="return listItemTask(\'cb'.$i.'\',\''.$state[1].'\')" title="'.JText::_($state[3]).'">'
			. $html.'</a>';
		}

		return $html;
	}
}
