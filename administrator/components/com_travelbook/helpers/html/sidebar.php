<?php
/**
 * @package     Joomla.Platform
 * @subpackage  HTML
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('_JEXEC') or die;

//Register the session storage class with the loader
//JLoader::register('JButton', dirname(__FILE__) . '/toolbar/button.php');

/**
 * SideBar handler
 *
 * @package     Joomla.Platform
 * @subpackage  HTML
 * @since       11.1
 */
class JSideBar extends JObject
{
	/**
	 * Sidebar name
	 *
	 * @var    string
	 */
	protected $_name = array();

	/**
	 * Sidebar array
	 *
	 * @var    array
	 */
	protected $_bar = array();

	/**
	 * Groups array
	 *
	 * @var    array
	 */
	protected $_groups = array();

	/**
	 * Loaded buttons
	 *
	 * @var    array
	 */
	protected $_buttons = array();

	/**
	 * Directories, where button types can be stored.
	 *
	 * @var    array
	 */
	protected $_buttonPath = array();

	/**
	 * Constructor
	 *
	 * @param   string  $name  The sidebar name.
	 *
	 * @since   11.1
	 */
	public function __construct($name = 'sidebar')
	{
		$this->_name = $name;

		// Set base path to find buttons.
		$this->_buttonPath[] = dirname(__FILE__) . '/sidebar/button';

	}

	/**
	 * Stores the singleton instances of various sidebar.
	 *
	 * @var JSidebar
	 * @since 11.3
	 */
	protected static $instances = array();

	/**
	 * Returns the global JSideBar object, only creating it if it
	 * doesn't already exist.
	 *
	 * @param   string  $name  The name of the sidebar.
	 *
	 * @return  JSideBar  The JSideBar object.
	 *
	 * @since   11.1
	 */
	public static function getInstance($name = 'sidebar')
	{
		if (empty(self::$instances[$name]))
		{
			self::$instances[$name] = new JSideBar($name);
		}

		return self::$instances[$name];
	}

	/**
	 * Set a value
	 *
	 * @return  string  The set value.
	 *
	 * @since   11.1
	 */
	public function appendButton()
	{
		// Push button onto the end of the sidebar array.
		$btn = func_get_args();
		array_push($this->_bar, $btn);
		return true;
	}

	/**
	 * Set a value
	 *
	 * @return  string  The set value.
	 *
	 * @since   11.1
	 */
	public function appendGroup($group = 'group')
	{
		// Push group onto the end of the group array.
		if (!in_array($group, $this->_groups)) {
			array_push($this->_groups, $group);
		}
		return true;
	}

	/**
	 * Get the list of sidebar links.
	 *
	 * @return  array
	 *
	 * @since   11.1
	 */
	public function getItems()
	{
		return $this->_bar;
	}

	/**
	 * Get the list of groups.
	 *
	 * @return  array
	 *
	 * @since   11.1
	 */
	public function getGroups()
	{
		return $this->_groups;
	}

	/**
	 * Get the name of the sidebar.
	 *
	 * @return  string
	 *
	 * @since   11.1
	 */
	public function getName()
	{
		return $this->_name;
	}

	/**
	 * Get a value.
	 *
	 * @return  string
	 *
	 * @since   11.1
	 */
	public function prependButton()
	{
		// Insert button into the front of the sidebar array.
		$btn = func_get_args();
		array_unshift($this->_bar, $btn);
		return true;
	}

	/**
	 * Render a tool bar.
	 *
	 * @return  string  HTML for the sidebar.
	 *
	 * @since   11.1
	 */
	public function render()
	{
		$html = array();

		// Start sidebar div.
		$html[] = '<div class="sidebar-list" id="' . $this->_name . '">';
		$html[] = '<ul>';

		// Render each button in the sidebar.
		foreach ($this->_bar as $button)
		{
			$html[] = $this->renderButton($button);
		}

		// End sidebar div.
		$html[] = '</ul>';
		$html[] = '<div class="clr"></div>';
		$html[] = '</div>';

		return implode("\n", $html);
	}

	/**
	 * Render a button.
	 *
	 * @param   object  &$node  A sidebar node.
	 *
	 * @return  string
	 *
	 * @since   11.1
	 */
	public function renderButton(&$node)
	{
		// Get the button type.
		$type = $node[0];

		$button = $this->loadButtonType($type);

		// Check for error.
		if ($button === false)
		{
			return JText::sprintf('JLIB_HTML_BUTTON_NOT_DEFINED', $type);
		}
		return $button->render($node);
	}

	/**
	 * Loads a button type.
	 *
	 * @param   string   $type  Button Type
	 * @param   boolean  $new   False by default
	 *
	 * @return  object
	 *
	 * @since   11.1
	 */
	public function loadButtonType($type, $new = false)
	{
		$signature = md5($type);
		if (isset($this->_buttons[$signature]) && $new === false)
		{
			return $this->_buttons[$signature];
		}

		if (!class_exists('JButton'))
		{
			JError::raiseWarning('SOME_ERROR_CODE', JText::_('JLIB_HTML_BUTTON_BASE_CLASS'));
			return false;
		}

		$buttonClass = 'JButton' . $type;
		if (!class_exists($buttonClass))
		{
			if (isset($this->_buttonPath))
			{
				$dirs = $this->_buttonPath;
			}
			else
			{
				$dirs = array();
			}

			$file = JFilterInput::getInstance()->clean(str_replace('_', DIRECTORY_SEPARATOR, strtolower($type)) . '.php', 'path');

			jimport('joomla.filesystem.path');
			if ($buttonFile = JPath::find($dirs, $file))
			{
				include_once $buttonFile;
			}
			else
			{
				JError::raiseWarning('SOME_ERROR_CODE', JText::sprintf('JLIB_HTML_BUTTON_NO_LOAD', $buttonClass, $buttonFile));
				return false;
			}
		}

		if (!class_exists($buttonClass))
		{
			//return	JError::raiseError('SOME_ERROR_CODE', "Module file $buttonFile does not contain class $buttonClass.");
			return false;
		}
		$this->_buttons[$signature] = new $buttonClass($this);

		return $this->_buttons[$signature];
	}

	/**
	 * Add a directory where JSideBar should search for button types in LIFO order.
	 *
	 * You may either pass a string or an array of directories.
	 *
	 * JSidebar will be searching for an element type in the same order you
	 * added them. If the parameter type cannot be found in the custom folders,
	 * it will look in libraries/joomla/html/sidebar/button.
	 *
	 * @param   mixed  $path  Directory or directories to search.
	 *
	 * @return  void
	 *
	 * @since   11.1
	 * @see JSidebar
	 */
	public function addButtonPath($path)
	{
		// Just force path to array.
		settype($path, 'array');

		// Loop through the path directories.
		foreach ($path as $dir)
		{
			// No surrounding spaces allowed!
			$dir = trim($dir);

			// Add trailing separators as needed.
			if (substr($dir, -1) != DIRECTORY_SEPARATOR)
			{
				// Directory
				$dir .= DIRECTORY_SEPARATOR;
			}

			// Add to the top of the search dirs.
			array_unshift($this->_buttonPath, $dir);
		}

	}
}
