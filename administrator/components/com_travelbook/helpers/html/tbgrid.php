<?php
/**
 * @package     Joomla.Platform
 * @subpackage  HTML
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('JPATH_PLATFORM') or die;

/**
 * Utility class for creating HTML Grids
 *
 * @package     Joomla.Platform
 * @subpackage  HTML
 * @since       11.1
 */
abstract class JHtmlTBGrid extends JHtmlJGrid
{
	/**
	 * Returns a extra type on a grid
	 *
	 * @param   integer       $value         The type value.
	 * @param   integer       $i             The row index
	 * @param   string|array  $prefix        An optional task prefix or an array of options
	 * @param   boolean       $enabled       An optional setting for access control on the action.
	 * @param   string        $checkbox      An optional prefix for checkboxes.
	 * @param   string        $publish_up    An optional start publishing date.
	 * @param   string        $publish_down  An optional finish publishing date.
	 *
	 * @return  string  The Html code
	 *
	 * @see     JHtmlJGrid::state
	 * @since   11.1
	 */
	public static function type($value)
	{
		$states = array(0 => 'TBINCLUDED', 1 => 'TBEXCLUDED', 2 => 'TBOPTIONAL', 3 => 'TBFACULTATIV');

		$text = JArrayHelper::getValue($states, (int) $value, $states[0]);

		return JText::_($text);
		
	}

	/**
	 * Returns an array of standard type filter options.
	 *
	 * @param   array  $config  An array of configuration options.
	 *                          This array can contain a list of key/value pairs where values are boolean
	 *                          and keys can be taken from 'included', 'excluded', 'archived', 'trash', 'all'.
	 *                          These pairs determine which values are displayed.
	 *
	 * @return  string  The HTML code for the select tag
	 *
	 * @since   11.1
	 */
	public static function typeOptions($config = array())
	{
		// Build the active state filter options.
		$options = array();
		if (!array_key_exists('included', $config) || $config['included'])
		{
			$options[] = JHtml::_('select.option', '0', strip_tags(JTEXT::_('TBINCLUDED')));
		}
		if (!array_key_exists('excluded', $config) || $config['excluded'])
		{
			$options[] = JHtml::_('select.option', '1', strip_tags(JTEXT::_('TBEXCLUDED')));
		}
		if (!array_key_exists('optional', $config) || $config['optional'])
		{
			$options[] = JHtml::_('select.option', '2', strip_tags(JTEXT::_('TBOPTIONAL')));
		}
		if (!array_key_exists('facultativ', $config) || $config['facultativ'])
		{
			$options[] = JHtml::_('select.option', '3', strip_tags(JTEXT::_('TBFACULTATIV')));
		}
		if (!array_key_exists('all', $config) || $config['all'])
		{
			$options[] = JHtml::_('select.option', '*', 'JALL');
		}
		return $options;
	}

}
