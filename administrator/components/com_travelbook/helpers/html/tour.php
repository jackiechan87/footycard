<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Utility class for tours
 *
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @since       2.0
 */
abstract class JHtmlTour
{
	/**
	 * Cached array of the tour items.
	 *
	 * @var    array
	 * @since  11.1
	 */
	protected static $items = array();

	/**
	 * Returns an array of tours for the given extension.
	 *
	 * @param   string  $extension  The extension option e.g. com_something.
	 * @param   array   $config     An array of configuration options. By default, only
	 *                              published and unpublished categories are returned.
	 *
	 * @return  array
	 *
	 * @since   11.1
	 */
	public static function options($catid, $config = array('filter.published' => array(0, 1)))
	{
		$hash = md5($catid . '.' . serialize($config));

		if (!isset(self::$items[$hash]))
		{
			$config = (array) $config;
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);

			$query->select('a.id, a.title');
			$query->from('#__tb_tours AS a');

			// Filter on catid.
			if ($catid) {
				$query->where('catid = ' . $db->quote($catid));
			}

			// Filter on the published state
			if (isset($config['filter.published']))
			{
				if (is_numeric($config['filter.published']))
				{
					$query->where('a.state = ' . (int) $config['filter.published']);
				}
				elseif (is_array($config['filter.published']))
				{
					JArrayHelper::toInteger($config['filter.published']);
					$query->where('a.state IN (' . implode(',', $config['filter.published']) . ')');
				}
			}

			// Filter on dates
			if (isset($config['filter.dates']))
			{
				if ($config['filter.dates'])
				{
					$query->select('COUNT(d.id) AS date_count');
					$query->join('LEFT', $db->quoteName('#__tb_dates').' AS d ON d.TID = a.id');
					$query->group('a.id');
					$query->having('date_count > 0');
				}
			}

			$query->order('a.ordering');

			$db->setQuery($query);
			$items = $db->loadObjectList();

			// Assemble the list options.
			self::$items[$hash] = array();

			foreach ($items as &$item)
			{
				self::$items[$hash][] = JHtml::_('select.option', $item->id, $item->title);
			}
		}

		return self::$items[$hash];
	}

	/**
	 * Returns an array of categories for the given extension.
	 *
	 * @param   string  $extension  The extension option.
	 * @param   array   $config     An array of configuration options. By default, only published and unpublished categories are returned.
	 *
	 * @return  array   Categories for the extension
	 *
	 * @since   11.1
	 */
	public static function tours($catid, $config = array('filter.published' => array(0, 1)))
	{
		$hash = md5($catid . '.' . serialize($config));

		if (!isset(self::$items[$hash]))
		{
			$config = (array) $config;
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);

			$query->select('a.id, a.title');
			$query->from('#__tb_tours AS a');
			//			$query->where('a.parent_id > 0');

			// Filter on catid.
			if ($catid) {
				$query->where('catid = ' . $db->quote($catid));
			}

			// Filter on the published state
			if (isset($config['filter.published']))
			{
				if (is_numeric($config['filter.published']))
				{
					$query->where('a.state = ' . (int) $config['filter.published']);
				}
				elseif (is_array($config['filter.published']))
				{
					JArrayHelper::toInteger($config['filter.published']);
					$query->where('a.state IN (' . implode(',', $config['filter.published']) . ')');
				}
			}

			$query->order('a.ordering');

			$db->setQuery($query);
			$items = $db->loadObjectList();

			// Assemble the list options.
			self::$items[$hash] = array();

			foreach ($items as &$item)
			{
				//				$repeat = ($item->level - 1 >= 0) ? $item->level - 1 : 0;
				//				$item->title = str_repeat('- ', $repeat) . $item->title;
				self::$items[$hash][] = JHtml::_('select.option', $item->id, $item->title);
			}
			// Special "Add to root" option:
			self::$items[$hash][] = JHtml::_('select.option', '1', JText::_('JLIB_HTML_ADD_TO_ROOT'));
		}

		return self::$items[$hash];
	}
}
