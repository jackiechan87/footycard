<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

class TravelbookVersion {
	var $_version = '2.12';
	var $_versionid = 'buonarroti';
	var $_date = '2013-03-06';
	var $_status = 'Stable';
	var $_copyyears = '2009-2013';

	public function getVersion() {
		return $this->_version . ' (' . $this->_versionid .')';
	}

	public function getCopyright() {
		return '&copy; ' . $this->_copyyears;
	}

	public function getRevision() {
		return '' . $this->_revision . ' (' . $this->_date . ')';
	}
	
	public function renderVersion() 
	{
		$output = array();
		$output[] = "<div class='smaller-grey'>";
		$output[] = "TRAVELbook " . $this->getVersion() . " - Joomla!<sup>&reg;</sup> on Tour. " . $this->getCopyright();
		$output[] = "<a href='http://www.demo-page.de' target='_blank' class='smaller-grey'>";
		$output[] = "<img src='http://www.demo-page.de/templates/jyaml/favicon.ico' height='10' width='10' alt='Suchmaschinenoptimierung und Marketing mit Demo Page' /> ";
		$output[] = "Demo Page";
		$output[] = "</a>, all rights reserved.";
		$output[] = "</div>";
		
		return implode($output);
	}
}