<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_travelbook
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of booking records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_travelbook
 */
class TravelbookModelBookings extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'a.id',
				'title', 'a.title',
				'alias', 'a.alias',
				'checked_out', 'a.checked_out',
				'checked_out_time', 'a.checked_out_time',
				'state', 'a.state',
				'access', 'a.access', 'access_level',
				'created', 'a.created',
				'departure', 'd.departure', 
				'tour', 't.title', 
				'client', 'c.firstname',
				'created_by', 'a.created_by',
				'ordering', 'a.ordering'
				);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication();
		$session = JFactory::getSession();

		// Adjust the context to support modal layouts.
		if ($layout = JRequest::getVar('layout')) {
			$this->context .= '.'.$layout;
		}

		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$access = $this->getUserStateFromRequest($this->context.'.filter.access', 'filter_access', 0, 'int');
		$this->setState('filter.access', $access);

		$authorId = $app->getUserStateFromRequest($this->context.'.filter.author_id', 'filter_author_id');
		$this->setState('filter.author_id', $authorId);

		$published = $this->getUserStateFromRequest($this->context.'.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);

		$tourId = $this->getUserStateFromRequest($this->context.'.filter.tour_id', 'filter_tour_id');
		$this->setState('filter.tour_id', $tourId);

		$dateId = $this->getUserStateFromRequest($this->context.'.filter.date_id', 'filter_date_id');
		$this->setState('filter.date_id', $dateId);

		$clientId = $this->getUserStateFromRequest($this->context.'.filter.client_id', 'filter_client_id');
		$this->setState('filter.client_id', $clientId);

		// List state information.
		parent::populateState('a.created', 'desc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param	string		$id	A prefix for the store id.
	 *
	 * @return	string		A store id.
	 * @since	1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id	.= ':'.$this->getState('filter.search');
		$id	.= ':'.$this->getState('filter.access');
		$id	.= ':'.$this->getState('filter.published');
		$id	.= ':'.$this->getState('filter.tour_id');
		$id	.= ':'.$this->getState('filter.date_id');
		$id	.= ':'.$this->getState('filter.client_id');
		$id	.= ':'.$this->getState('filter.author_id');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();

		// Select the required fields from the table.
		$query->select(
		$this->getState(
				'list.select',
				'a.id, a.title, a.alias, a.checked_out, a.checked_out_time' .
				', a.state, a.access, a.created, a.created_by, a.ordering' .
				',a.info'
				)
				);
				$query->from('#__tb_bookings AS a');

				// Join over the clients.
				$query->select('c.id AS client_id, c.address, c.firstname AS client, c.lastname, c.birthdate, c.street, c.street_number, c.postcode, c.city, c.country' .
				',c.mobile, c.phone, c.fax, c.email, c.mode_mail, c.remark');
				$query->join('LEFT', '#__tb_clients AS c ON c.id=a.CID');

				// Join over the tourdates.
				$query->select('d.id AS date_id, d.departure AS departure, d.arrival AS arrival, d.duration AS duration');
				$query->join('LEFT', '#__tb_dates AS d ON d.id=a.DID');
				// Join over the tours.
				$query->select('t.id AS tour_id, t.title AS tour');
				$query->join('LEFT', '#__tb_tours AS t ON t.id=d.TID');

				// Join over the users for the checked out user.
				$query->select('uc.name AS editor');
				$query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');

				// Join over the asset groups.
				$query->select('ag.title AS access_level');
				$query->join('LEFT', '#__viewlevels AS ag ON ag.id = a.access');

				// Join over the users for the author.
				$query->select('ua.name AS author_name');
				$query->join('LEFT', '#__users AS ua ON ua.id = a.created_by');

				// Filter by access level.
				if ($access = $this->getState('filter.access')) {
					$query->where('a.access = ' . (int) $access);
				}

				// Implement View Level Access
				if (!$user->authorise('core.admin'))
				{
					$groups	= implode(',', $user->getAuthorisedViewLevels());
					$query->where('a.access IN ('.$groups.')');
				}

				// Filter by published state
				$published = $this->getState('filter.published');
				if (is_numeric($published)) {
					$query->where('a.state = ' . (int) $published);
				}
				elseif ($published === '') {
					$query->where('(a.state = 0 OR a.state = 1)');
				}

					// Filter by tour
				$tourId = $this->getState('filter.tour_id');
				if (is_numeric($tourId)) {
					$type = $this->getState('filter.tour_id.include', true) ? '=' : '<>';
					$query->where('t.id' . $type . (int) $tourId);
				}
					// Filter by date
				$dateId = $this->getState('filter.date_id');
				if (is_numeric($dateId)) {
					$type = $this->getState('filter.date_id.include', true) ? '=' : '<>';
					$query->where('d.id' . $type . (int) $dateId);
				}
					// Filter by client
				$clientId = $this->getState('filter.client_id');
				if (is_numeric($clientId)) {
					$type = $this->getState('filter.client_id.include', true) ? '=' : '<>';
					$query->where('c.id' . $type . (int) $clientId);
				}
				
				// Filter by author
				$authorId = $this->getState('filter.author_id');
				if (is_numeric($authorId)) {
					$type = $this->getState('filter.author_id.include', true) ? '= ' : '<>';
					$query->where('a.created_by '.$type.(int) $authorId);
				}

				// Filter by search in title.
				$search = $this->getState('filter.search');
				if (!empty($search)) {
					if (stripos($search, 'id:') === 0) {
						$query->where('a.id = '.(int) substr($search, 3));
					}
					elseif (stripos($search, 'author:') === 0) {
						$search = $db->Quote('%'.$db->escape(substr($search, 7), true).'%');
						$query->where('(ua.name LIKE '.$search.' OR ua.username LIKE '.$search.')');
					}
					else {
						$search = $db->Quote('%'.$db->escape($search, true).'%');
						$query->where('(a.title LIKE '.$search.' OR a.alias LIKE '.$search.')');
					}
				}

				// GROUP BY
				$query->group('a.id, a.title, a.alias, a.checked_out, a.checked_out_time, a.state, a.access, a.created, a.created_by, a.ordering, a.info');
				$query->group('client_id, c.address, client, c.lastname, c.birthdate, c.street, c.street_number, c.postcode, c.city, c.country, c.mobile, c.phone, c.fax, c.email, c.mode_mail, c.remark');
				$query->group('date_id, departure, arrival, duration');
				$query->group('tour_id, tour');
				$query->group('editor, access_level, author_name');
				
				// Add the list ordering clause.
				$orderDirn = $this->state->get('list.direction', 'asc');
				$orderCol = $this->state->get('list.ordering', 'a.title');
				$orderCol = ($orderCol=='a.ordering' || $orderCol=='category_title') ? 'c.title ' . $orderDirn . ', a.ordering' : $orderCol;
				$orderCol = $orderCol=='access_level' ? 'ag.title' : $orderCol;
				$orderCol = $orderCol=='language' ? 'l.title' : $orderCol;
				
				// ORDER BY
				$query->order($db->escape($orderCol.' '.$orderDirn));

//				echo nl2br(str_replace('#__', $db->getPrefix(), $query));

				return $query;
	}

	/**
	 * Build a list of authors
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	public function getAuthors() {
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Construct the query
		$query->select('u.id AS value, u.name AS text');
		$query->from('#__users AS u');
		$query->join('INNER', '#__tb_bookings AS c ON c.created_by = u.id');
		$query->group('u.id, u.name');
		$query->order('u.name');

		// Setup the query
		$db->setQuery($query->__toString());

		// Return the result
		return $db->loadObjectList();
	}

	/**
	 * Method to get a list of bookings.
	 * Overridden to add a check for access levels.
	 *
	 * @return	mixed	An array of data items on success, false on failure.
	 * @since	1.6.1
	 */
	public function getItems()
	{
		$items = parent::getItems();
		$app = JFactory::getApplication();
		if ($app->isSite()) {
			$user	= JFactory::getUser();
			$groups	= $user->getAuthorisedViewLevels();

			for ($x = 0, $count = count($items); $x < $count; $x++) {
				//Check the access level. Remove bookings the user shouldn't see
				if (!in_array($items[$x]->access, $groups)) {
					unset($items[$x]);
				}
			}
		}
		return $items;
	}
}
