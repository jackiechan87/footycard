<?php
/**
 * TRAVELbook - JOOMLA! on Tour
 *
 * @version		$Id: bookings.php 100 2011-05-30 10:53:58Z hoepe999 $
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Methods supporting a list of tour records.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_travelbook
 * @since		1.6
 */
class TravelbookModelTourExtra extends JModelAdmin
{

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param	object	A record object.
	 * @return	boolean	True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since	1.6
	 */
	protected function canDelete($record)
	{
		if (!empty($record->id)) {
			$user = JFactory::getUser();
			return $user->authorise('core.delete', 'com_travelbook.tour.'.(int) $record->id);
		}
	}

	/**
	 * Method to test whether a record can have its state changed.
	 *
	 * @param	object	A record object.
	 * @return	boolean	True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since	1.6
	 */
	protected function canEditState($record)
	{
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_travelbook.category.'.(int) $record->catid);
		}
		else {
			return parent::canEditState($record);
		}
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_travelbook.tourextra', 'tourextra', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		// Determine correct permissions to check.
		if ($this->getState('tourextra.id')) {
			// Existing record. Can only edit in selected categories.
			$form->setFieldAttribute('catid', 'action', 'core.edit');
		} else {
			// New record. Can only create in selected categories.
			$form->setFieldAttribute('catid', 'action', 'core.create');
		}

		// Modify the form based on access controls.
		if (!$this->canEditState((object) $data)) {
			// Disable fields for display.
			$form->setFieldAttribute('ordering', 'disabled', 'true');
			$form->setFieldAttribute('state', 'disabled', 'true');
			$form->setFieldAttribute('publish_up', 'disabled', 'true');
			$form->setFieldAttribute('publish_down', 'disabled', 'true');

			// Disable fields while saving.
			// The controller has already verified this is a record you can edit.
			$form->setFieldAttribute('ordering', 'filter', 'unset');
			$form->setFieldAttribute('state', 'filter', 'unset');
			$form->setFieldAttribute('publish_up', 'filter', 'true');
			$form->setFieldAttribute('publish_down', 'filter', 'true');
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_travelbook.edit.tour.data', array());

		if (empty($data)) {
			$data = $this->getItem();

			// Prime some default values.
			if ($this->getState('tour.id') == 0) {
				$app = JFactory::getApplication();
				$data->set('catid', JRequest::getInt('catid', $app->getUserState('com_travelbook.tours.filter.category_id')));
			}
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk)) {
		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since	1.6
	 */
	protected function prepareTable(&$table)
	{
		jimport('joomla.filter.output');
		$date = JFactory::getDate();
		$user = JFactory::getUser();

		// Set the publish date to now
		if($table->state == 1 && intval($table->publish_up) == 0) {
			$table->publish_up = JFactory::getDate()->toMySQL();
		}

		// Increment the tour version number.
		$table->version++;
		
		// Reorder the tours within the category so the new tour is first
	//	$table->reorder('catid = '.(int) $table->catid.' AND state >= 0');
	}

	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param	object	A record object.
	 * @return	array	An array of conditions to add to add to ordering queries.
	 * @since	1.6
	 */
	protected function getReorderConditions($table)
	{
		$condition = array();
		$condition[] = 'catid = ' . (int) $table->catid . ' AND type = ' . (int) $table->type;
		return $condition;
	}
	
	/**
	 * Method to link extras with the tour.
	 *
	 * @param   array  $cids the checkbox-Ids.
	 *
	 * @return  boolean  True on success, False on error.
	 * @since   11.1
	 */
	public function link($cids)
	{
		$data = JRequest::getVar('jform', array(), 'post', 'array');
		foreach ($cids as $cid) {
			$extra['id_2'] = $data['id'][$cid];
			$extra['catid'] = $data['catid'][$cid];
			$extra['type'] = $data['type'][$cid];
			$extra['rate'] = $data['rate'][$cid];
			$extra['ordering'] = $data['ordering'][$cid];
			$extra['id_1'] = $data['tour_id'];
			$this->setState($this->getName().'.id', 0);
			parent::save($extra);
		}

		return true;
	}

	/**
	 * Method to link extras with the tour.
	 *
	 * @param   array  $cids the checkbox-Ids.
	 *
	 * @return  boolean  True on success, False on error.
	 * @since   11.1
	 */
	public function unlink($cids)
	{
		return parent::delete($cids);
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'TourExtra', $prefix = 'TravelbookTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

}