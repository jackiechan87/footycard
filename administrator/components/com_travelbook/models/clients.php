<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_travelbook
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of client records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_travelbook
 */
class TravelbookModelClients extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'a.id',
				'title', 'a.title',
				'alias', 'a.alias',
				'checked_out', 'a.checked_out',
				'checked_out_time', 'a.checked_out_time',
				'catid', 'a.catid', 'category_title',
				'state', 'a.state',
				'access', 'a.access', 'access_level',
				'created', 'a.created',
				'created_by', 'a.created_by',
				'ordering', 'a.ordering'
				);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication();
		$session = JFactory::getSession();

		// Adjust the context to support modal layouts.
		if ($layout = JRequest::getVar('layout')) {
			$this->context .= '.'.$layout;
		}

		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$access = $this->getUserStateFromRequest($this->context.'.filter.access', 'filter_access', 0, 'int');
		$this->setState('filter.access', $access);

		$authorId = $app->getUserStateFromRequest($this->context.'.filter.author_id', 'filter_author_id');
		$this->setState('filter.author_id', $authorId);

		$published = $this->getUserStateFromRequest($this->context.'.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);

		$categoryId = $this->getUserStateFromRequest($this->context.'.filter.category_id', 'filter_category_id');
		$this->setState('filter.category_id', $categoryId);

		$level = $this->getUserStateFromRequest($this->context.'.filter.level', 'filter_level', 0, 'int');
		$this->setState('filter.level', $level);

		// List state information.
		parent::populateState('a.title', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param	string		$id	A prefix for the store id.
	 *
	 * @return	string		A store id.
	 * @since	1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id	.= ':'.$this->getState('filter.search');
		$id	.= ':'.$this->getState('filter.access');
		$id	.= ':'.$this->getState('filter.published');
		$id	.= ':'.$this->getState('filter.category_id');
		$id	.= ':'.$this->getState('filter.author_id');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
		$user	= JFactory::getUser();

		// Select the required fields from the table.
		$query->select(
		$this->getState(
				'list.select',
				'a.id, a.title, a.alias, a.checked_out, a.checked_out_time, a.catid' .
				', a.state, a.access, a.created, a.created_by, a.ordering'
				)
				);
				$query->from('#__tb_clients AS a');

				// Join over the users for the checked out user.
				$query->select('uc.name AS editor');
				$query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');

				// Join over the asset groups.
				$query->select('ag.title AS access_level');
				$query->join('LEFT', '#__viewlevels AS ag ON ag.id = a.access');

				// Join over the categories.
				$query->select('c.title AS category_title');
				$query->join('LEFT', '#__categories AS c ON c.id = a.catid');

				// Join over the users for the author.
				$query->select('ua.name AS author_name');
				$query->join('LEFT', '#__users AS ua ON ua.id = a.created_by');

				// Filter by access level.
				if ($access = $this->getState('filter.access')) {
					$query->where('a.access = ' . (int) $access);
				}

				// Implement View Level Access
				if (!$user->authorise('core.admin'))
				{
					$groups	= implode(',', $user->getAuthorisedViewLevels());
					$query->where('a.access IN ('.$groups.')');
				}

				// Filter by published state
				$published = $this->getState('filter.published');
				if (is_numeric($published)) {
					$query->where('a.state = ' . (int) $published);
				}
				elseif ($published === '') {
					$query->where('(a.state = 0 OR a.state = 1)');
				}

				// Filter by a single or group of categories.
				$baselevel = 1;
				$categoryId = $this->getState('filter.category_id');
				if (is_numeric($categoryId)) {
					$cat_tbl = JTable::getInstance('Category', 'JTable');
					$cat_tbl->load($categoryId);
					$rgt = $cat_tbl->rgt;
					$lft = $cat_tbl->lft;
					$baselevel = (int) $cat_tbl->level;
					$query->where('c.lft >= '.(int) $lft);
					$query->where('c.rgt <= '.(int) $rgt);
				}
				elseif (is_array($categoryId)) {
					JArrayHelper::toInteger($categoryId);
					$categoryId = implode(',', $categoryId);
					$query->where('a.catid IN ('.$categoryId.')');
				}

				// Filter on the level.
				if ($level = $this->getState('filter.level')) {
					$query->where('c.level <= '.((int) $level + (int) $baselevel - 1));
				}

				// Filter by author
				$authorId = $this->getState('filter.author_id');
				if (is_numeric($authorId)) {
					$type = $this->getState('filter.author_id.include', true) ? '= ' : '<>';
					$query->where('a.created_by '.$type.(int) $authorId);
				}

				// Filter by search in title.
				$search = $this->getState('filter.search');
				if (!empty($search)) {
					if (stripos($search, 'id:') === 0) {
						$query->where('a.id = '.(int) substr($search, 3));
					}
					elseif (stripos($search, 'author:') === 0) {
						$search = $db->Quote('%'.$db->escape(substr($search, 7), true).'%');
						$query->where('(ua.name LIKE '.$search.' OR ua.username LIKE '.$search.')');
					}
					else {
						$search = $db->Quote('%'.$db->escape($search, true).'%');
						$query->where('(a.title LIKE '.$search.' OR a.alias LIKE '.$search.')');
					}
				}

				// GROUP BY
				$query->group('a.id, a.title, a.alias, a.checked_out, a.checked_out_time, a.catid, a.state, a.access, a.created, a.created_by, a.ordering');
				$query->group('editor, access_level, category_title, author_name');
				
				// Add the list ordering clause.
				$orderCol = $this->state->get('list.ordering', 'a.title');
				$orderDirn = $this->state->get('list.direction', 'asc');
				$orderCol = ($orderCol=='a.ordering' || $orderCol=='category_title') ? 'c.title ' . $orderDirn . ', a.ordering' : $orderCol;
				$orderCol = $orderCol=='access_level' ? 'ag.title' : $orderCol;
				$orderCol = $orderCol=='language' ? 'l.title' : $orderCol;
				
				// ORDER BY
				$query->order($db->escape($orderCol.' '.$orderDirn));
				
//				echo nl2br(str_replace('#__', $db->getPrefix(), $query));
				
				return $query;
	}

	/**
	 * Build a list of authors
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	public function getAuthors() {
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Construct the query
		$query->select('u.id AS value, u.name AS text');
		$query->from('#__users AS u');
		$query->join('INNER', '#__tb_clients AS c ON c.created_by = u.id');
		$query->group('u.id, u.name');
		$query->order('u.name');

		// Setup the query
		$db->setQuery($query->__toString());

		// Return the result
		return $db->loadObjectList();
	}

	/**
	 * Method to get a list of clients.
	 * Overridden to add a check for access levels.
	 *
	 * @return	mixed	An array of data items on success, false on failure.
	 * @since	1.6.1
	 */
	public function getItems()
	{
		$items	= parent::getItems();
		$app	= JFactory::getApplication();
		if ($app->isSite()) {
			$user	= JFactory::getUser();
			$groups	= $user->getAuthorisedViewLevels();

			for ($x = 0, $count = count($items); $x < $count; $x++) {
				//Check the access level. Remove clients the user shouldn't see
				if (!in_array($items[$x]->access, $groups)) {
					unset($items[$x]);
				}
			}
		}
		return $items;
	}
}
