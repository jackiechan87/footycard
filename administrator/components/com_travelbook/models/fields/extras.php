<?php
/**
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 *
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JFormHelper::loadFieldClass('list');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @since		2.2
 */
class JFormFieldExtras extends JFormField
{
	/**
	 * A unsorted list of booked extras
	 *
	 * @var		string
	 * @since	2.2
	 */
	public $type = 'Extras';

	/**
	 * Method to get the field input markup for Extras Lists.
	 * 
	 * @return  string  The field input markup.
	 */
	protected function getInput()
	{
		JHtml::_('behavior.tooltip');

		// Get the available extras.
		$extras = $this->getExtras();

		// Prepare output
		$html = array();
		$html[] = '<ul id="extras">';

		// Start a row for extra.
		foreach ($extras as $extra)
		{
			if (in_array($extra->value, $this->value)) 
			{
				$html[] = '<li>';
				$html[] = '<a href="index.php?option=com_travelbook&view=extra&layout=edit&id=' . $extra->value . '" target="_blank">' . $extra->text . '</a>';
				$html[] = '</li>';
			}
		}

		$html[] = '</ul>';

		return implode("\n", $html);
	}

	/**
	 * Get a list of the user groups.
	 *
	 * @return  array
	 *
	 * @since   11.1
	 */
	protected function getExtras()
	{
		// Initialise variables.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('a.id AS value, a.title AS text, a.rate, a.pricing_plan')
			->from('#__tb_extras AS a')
			->leftJoin($db->quoteName('#__tb_tours_extras') . ' AS b ON b.id_1 = a.id')
			->group('a.id, a.title, b.id_1')
			->order('a.ordering ASC');
		$db->setQuery($query);
		$options = $db->loadObjectList();

		return $options;
	}
}
