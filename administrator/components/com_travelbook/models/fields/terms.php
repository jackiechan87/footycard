<?php
/**
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 *
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JFormHelper::loadFieldClass('list');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @since		2.2
 */
class JFormFieldTerms extends JFormField
{
	/**
	 * A unsorted list of checked terms
	 *
	 * @var		string
	 * @since	2.2
	 */
	public $type = 'Terms';

	/**
	 * Method to get the field input markup for Extras Lists.
	 * 
	 * @return  string  The field input markup.
	 */
	protected function getInput()
	{
		$terms = array(JText::_('terms')=>JText::_('TB_TERMS_ACCEPTED'), JText::_('privacy')=>JText::_('TB_PRIVACY_ACCEPTED'), JText::_('opposition')=>JText::_('TB_OPPOSITION_ACCEPTED'));

		// Prepare output
		$html = array();
		$html[] = '<ul id="extras">';

		// Start a row for extra.
		foreach ($terms as $key=>$term)
		{
			if (array_key_exists($key, $this->value)) 
			{
				$html[] = '<li>';
				$html[] = '<a href="index.php?option=com_content&view=articles&filter_search=id:' . (int)$this->value[$key] . '" target="_blank">' . $term . '</a>';
				$html[] = '</li>';
			}
		}

		$html[] = '</ul>';

		return implode("\n", $html);
	}
}
