<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('demopage.application.component.controllerlist');

/**
 * Tour list controller class.
 *
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @since		1.6
 */
class TravelbookControllerTourExtra extends JControllerList
{
	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since	1.6
	 */
	protected $text_prefix = 'COM_TRAVELBOOK_TOURS_EXTRAS';
	
	/**
	 * Constructor.
	 *
	 * @param	array	$config	An optional associative array of configuration settings.

	 * @return	TravelbookControllerTour
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

//		$this->registerTask('edit', 'edit');
	}

	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function getModel($name = 'TourExtra', $prefix = 'TravelbookModel', $config = array('ignore_request' => false))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	/**
	 * Method to display a view.
	 *
	 * @param	boolean			$cachable	If true, the view output will be cached
	 * @param	array			$urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function extras()
	{
		parent::extras();
	}

	/**
	 * Method to display a view.
	 *
	 * @param	boolean			$cachable	If true, the view output will be cached
	 * @param	array			$urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		// Get multi models
		$view = $this->getView ('tourextra', 'html');
	    
		$tour = $this->getModel ('tour');
		$view->setModel($tour);
		
		$toursextras = $this->getModel ('toursextras', 'TravelbookModel', array('ignore_request' => false));
//		$view = $this->getView ('tourextra', 'html');
		$view->setModel($toursextras);
		
		$tourextra = $this->getModel ('tourextra', 'TravelbookModel', array('ignore_request' => false));
//		$view = $this->getView ('tourextra', 'html');
		$view->setModel($tourextra);
		
		$tourextralinked = $this->getModel ('tourextralinked');
//		$view = $this->getView ('tourextra', 'html');
		$view->setModel($tourextralinked);
		
		$tourextraunlinked = $this->getModel ('tourextraunlinked');
//		$view = $this->getView ('tourextra', 'html');
		$view->setModel($tourextraunlinked);
		
		// Load specific css
		JHtml::_('stylesheet','media/com_travelbook/administrator/css/com_travelbook.css', array(), false);
		
		JRequest::setVar('view', 'tourextra', 'get', true);
		JRequest::setVar('layout', 'default', 'get', true);
		$id = JRequest::getInt('id_1');
		JRequest::setVar('id', '0', 'get', true);
//		$view = JRequest::setVar('view', 'tourextra', 'get', true);
//		$layout = JRequest::setVar('layout', 'default', 'get', true);
		
		$view = JRequest::getCmd('view', 'tourextra');
		$layout = JRequest::getCmd('layout', 'default');
//		$id = JRequest::getInt('id');

		// Check for edit form.
		if ($view == 'tourextra' && $layout == 'edit' && !$this->checkEditId('com_travelbook.edit.tourextra', $id)) {
			// Somehow the user just went to the form - we don't allow that.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_travelbook&view=travelbook', false));

			return false;
		}

		parent::display();

		return $this;
	}

	/**
	 * Method to cancel the view.
	 *
	 *
	 * @return  boolean  True if access level checks pass, false otherwise.
	 * @since   11.1
	 */
	public function cancel()
	{
		JRequest::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$view = 'tour';
		$tour_id = JRequest::getVar('id_1', '0', 'get', 'string');
		
		parent::cancel();
		
		// Clean the session data and redirect.
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&view='.$view.$this->getRedirectToListAppend().'&layout=edit&id='.(int)$tour_id, false));

		return true;
	}

	/**
	 * Method to link extras with the tour.
	 *
	 *
	 * @return  boolean  True on success, False on error.
	 * @since   11.1
	 */
	public function link()
	{
		JRequest::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		if ( parent::link() ) {
			if ( JRequest::getVar( 'cid' ) ) {
				$this->setMessage(JText::_('COM_TRAVELBOOK_EXTRAS_ALREADY_LINKED'));
			} else {
				$cids = JRequest::getVar( 'cid_inv', array(), 'post', 'array' );	
				// Get the model.
				$model = $this->getModel('tourextra', 'TravelbookModel');
				// Make sure the item ids are integers
				JArrayHelper::toInteger($cids);
				
				// Link the items.
				if ($model->link($cids)) {
					$this->setMessage(JText::plural($this->text_prefix.'_N_ITEMS_LINKED', count($cids)));
					return true;
				} else {
					JError::raiseWarning(500, $model->getError());
					return false;
				}
			}
		} else {
			return false;
		}
	}

	/**
	 * Method to unlink extras with the tour.
	 *
	 *
	 * @return  boolean  True on success, False on error.
	 * @since   11.1
	 */
	public function unlink()
	{
		JRequest::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		if ( parent::unlink() ) {
			if ( JRequest::getVar( 'cid_inv' ) ) {
				$this->setMessage(JText::_('COM_TRAVELBOOK_EXTRAS_NOT_LINKED'));
			} else {
				$cids = JRequest::getVar('cid', array(), '', 'array');
				if (!is_array($cids) || count($cids) < 1) {
					JError::raiseWarning(500, JText::_($this->text_prefix.'_NO_ITEM_SELECTED'));
				} else {
					// Get the model.
					$model = $this->getModel('tourextra', 'TravelbookModel');
					// Make sure the item ids are integers
					JArrayHelper::toInteger($cids);
		
					// Unlink the items.
					if ($model->unlink($cids)) {
						$this->setMessage(JText::plural($this->text_prefix.'_N_ITEMS_UNLINKED', count($cids)));
					} else {
						$this->setMessage($model->getError());
					}
				}
				return true;
			}
		}
	}

	/**
	 * Check in of one or more records.
	 *
	 * @return  void
	 *
	 * @since   11.1
	 */
	public function checkin()
	{
//		$this->model = $this->getModel('TourExtra');
	    parent::checkin();   
	}

	/**
	 * Method to save the submitted ordering values for records.
	 *
	 * @return  void
	 *
	 * @since   11.1
	 */
	public function saveorder()
	{
		// Check for request forgeries.
		JRequest::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Get the input
		$pks = JRequest::getVar('cid', null, 'post', 'array');
		$order = JRequest::getVar('order', null, 'post', 'array');

		// Sanitize the input
		JArrayHelper::toInteger($pks);
		JArrayHelper::toInteger($order);

		// Get the model
		$model = $this->getModel();

		// Save the ordering
		$return = $model->saveorder($pks, $order);

		if ($return === false)
		{
			// Reorder failed
			$message = JText::sprintf('JLIB_APPLICATION_ERROR_REORDER_FAILED', $model->getError());
			$id_1 = JRequest::getvar('id_1', '0', 'get', 'string');
			$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&task='.$this->view_list.'.display&view='.$this->view_list.'&id_1='.(int)$id_1.$this->getRedirectToItemAppend(), false), $message, 'error');
			return false;
		} else
		{
			// Reorder succeeded.
			$this->setMessage(JText::_('JLIB_APPLICATION_SUCCESS_ORDERING_SAVED'));
			$id_1 = JRequest::getvar('id_1', '0', 'get', 'string');
			$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&task='.$this->view_list.'.display&view='.$this->view_list.'&id_1='.(int)$id_1.$this->getRedirectToItemAppend(), false), $message);
			return true;
		}
	}
	
}