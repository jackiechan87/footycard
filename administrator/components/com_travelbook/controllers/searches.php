<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.controller');

/**
 * Methods supporting a list of search terms.
 *
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @since		2.0
 */
class TravelbookControllerSearches extends JController
{
	/**
	 * Method to reset the seach log table.
	 *
	 * @return	boolean
	 */
	public function reset()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$model = $this->getModel('Searches');

		if (!$model->reset()) {
			JError::raiseWarning(500, $model->getError());
		}

		$this->setRedirect('index.php?option=com_travelbook&view=searches');
	}
}