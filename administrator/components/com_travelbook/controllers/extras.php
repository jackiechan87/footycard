<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

/**
 * Extras list controller class.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_travelbook
 * @since	1.6
 */
class TravelbookControllerExtras extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param	array	$config	An optional associative array of configuration settings.

	 * @return	TravelbookControllerExtras
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		$this->text_prefix = "COM_TRAVELBOOK_EXTRA";
		parent::__construct($config);
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param	string	$name	The name of the model.
	 * @param	string	$prefix	The prefix for the PHP class name.
	 *
	 * @return	JModel
	 * @since	1.6
	 */
	public function getModel($name = 'Extra', $prefix = 'TravelbookModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}
}
