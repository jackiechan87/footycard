<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

/**
 * Tours list controller class.
 *
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @since	2.0
 */
class TravelbookControllerTours extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param	array	$config	An optional associative array of configuration settings.

	 * @return	TravelbookControllerTours
	 * @see		JController
	 * @since	2.0
	 */
	public function __construct($config = array())
	{
		$this->text_prefix = "COM_TRAVELBOOK_TOUR";
	  
		// Tours default form can come from the tours or featured tours view.
		// Adjust the redirect view on the value of 'view' in the request.
		if (JRequest::getCmd('view') == 'featuredtours') {
			$this->view_list = 'featuredtours';
		}
		parent::__construct($config);

		$this->registerTask('unfeatured', 'featured');
	}

	/**
	 * Method to toggle the featured setting of a list of tours.
	 *
	 * @return	void
	 * @since	1.6
	 */
	function featured()
	{
		// Check for request forgeries
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$user = JFactory::getUser();
		$ids = JRequest::getVar('cid', array(), '', 'array');
		$values = array('featured' => 1, 'unfeatured' => 0);
		$task = $this->getTask();
		$value = JArrayHelper::getValue($values, $task, 0, 'int');

		// Access checks.
		foreach ($ids as $i => $id)
		{
			if (!$user->authorise('core.edit.state', 'com_travelbook.tour.'.(int) $id)) {
				// Prune items that you can't change.
				unset($ids[$i]);
				JError::raiseNotice(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
			}
		}

		if (empty($ids)) {
			JError::raiseWarning(500, JText::_('JERROR_NO_ITEMS_SELECTED'));
		}
		else {
			// Get the model.
			$model = $this->getModel();

			// Publish the items.
			if (!$model->featured($ids, $value)) {
				JError::raiseWarning(500, $model->getError());
			}
		}

		$this->setRedirect('index.php?option=com_travelbook&view=tours');
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param	string	$name	The name of the model.
	 * @param	string	$prefix	The prefix for the PHP class name.
	 *
	 * @return	JModel
	 * @since	1.6
	 */
	public function getModel($name = 'Tour', $prefix = 'TravelbookModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	public function delete()
	{
		parent::delete();

		$errors = count($this->_errors);
		if (!$errors) {
			//            $db = $this->getDbo();
			$user = JFactory::getUser();
			require_once JPATH_COMPONENT.'/controllers/dates.php';
			$datesController = new TravelbookControllerDates();
			$datesModel = $datesController->getModel();
			$datesTable = $datesModel->getTable();
			$this->context = 'dates';
			$db = $datesModel->getDbo();
			// Get items to remove from the request.
			$cids = JRequest::getVar('cid', array(), '', 'array');
			if (!is_array($cids) || count($cids) < 1) {
				JError::raiseWarning(500, JText::_('COM_TRAVELBOOK_TOUR_NO_ITEM_SELECTED'));
			} else {

				// Make sure the item ids are integers
				jimport('joomla.utilities.arrayhelper');
				JArrayHelper::toInteger($cid);

				// Remove the dates for this tour.
				foreach ($cids as $i => $cid) {
					$query = $db->getQuery(true);
					// Construct the query
					$query->select('d.id');
					$query->from('#__tb_dates AS d');
					$query->where('d.TID='.$cid);

					// Setup the query
					$db->setQuery($query->__toString());
					$pks = $db->loadAssocList('id', 'id');

					// Make sure the item ids are integers
					jimport('joomla.utilities.arrayhelper');
					JArrayHelper::toInteger($pks);
					// Remove the dates for this tour.
					if (is_array($pks) && count($pks)) {
						// first we have to trash the dates.
						if (!$datesTable->publish($pks, '-2', $user->id)) {
							$this->setError(JText::_('COM_TRAVELBOOK_DATE_ERROR_ITEMS_TRASHED'));
						}
						// now we have can remove the dates.
						if ($datesModel->delete($pks)) {
							$this->setMessage($this->message.' + '.JText::plural('COM_TRAVELBOOK_DATE_N_ITEMS_DELETED', count($pks)));
						} else {
							$this->setError(JText::_('COM_TRAVELBOOK_DATE_ERROR_ITEMS_DELETED'));
						}
					}
				}
			}
		} else {
			JError::raiseWarning(500, $model->getError());
		}
	  
		$this->setRedirect(JRoute::_('index.php?option=com_travelbook&view=tours', false));
	}
}