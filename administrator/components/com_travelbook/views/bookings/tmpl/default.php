<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_travelbook
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');

$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$saveOrder = $listOrder == 'a.ordering';
?>
<form
	action="<?php echo JRoute::_('index.php?option=com_travelbook&view=bookings');?>"
	method="post" name="adminForm" id="adminForm">
	<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?>
			</label> <input type="text" name="filter_search" id="filter_search"
				value="<?php echo $this->escape($this->state->get('filter.search')); ?>"
				title="<?php echo JText::_('COM_TRAVELBOOK_FILTER_SEARCH_DESC'); ?>" />

			<button type="submit" class="btn">
			<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>
			</button>
			<button type="button"
				onclick="document.id('filter_search').value='';this.form.submit();">
				<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>
			</button>
		</div>
		<div class="filter-select fltrt">
			<select name="filter_published" class="inputbox"
				onchange="this.form.submit()">
				<option value="">
				<?php echo JText::_('JOPTION_SELECT_PUBLISHED');?>
				</option>
				<?php echo JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.published'), true);?>
			</select> 
			<select name="filter_tour_id" class="inputbox"
				onchange="this.form.submit()">
				<option value="">
				<?php echo JText::_('COM_TRAVELBOOK_BOOKING_SELECT_TOUR');?>
				</option>
				<?php echo JHtml::_('select.options', JHtml::_('tour.options', false, array('filter.dates'=>true)), 'value', 'text', $this->state->get('filter.tour_id'));?>
			</select> 
			<select name="filter_date_id" class="inputbox"
				onchange="this.form.submit()">
				<option value="">
				<?php echo JText::_('COM_TRAVELBOOK_BOOKING_SELECT_DATE');?>
				</option>
				<?php echo JHtml::_('select.options', JHtml::_('date.options', $this->state->get('filter.tour_id')), 'value', 'text', $this->state->get('filter.date_id'));?>
			</select> 
			<select name="filter_client_id" class="inputbox"
				onchange="this.form.submit()">
				<option value="">
				<?php echo JText::_('COM_TRAVELBOOK_BOOKING_SELECT_CLIENT');?>
				</option>
				<?php echo JHtml::_('select.options', JHtml::_('client.options', 'com_travelbook'), 'value', 'text', $this->state->get('filter.client_id'));?>
			</select> 
			<select name="filter_access" class="inputbox"
				onchange="this.form.submit()">
				<option value="">
				<?php echo JText::_('JOPTION_SELECT_ACCESS');?>
				</option>
				<?php echo JHtml::_('select.options', JHtml::_('access.assetgroups'), 'value', 'text', $this->state->get('filter.access'));?>
			</select> 
		</div>
	</fieldset>
	<div class="clr"></div>

	<table class="adminlist">
		<thead>
			<tr>
				<th width="1%"><input type="checkbox" name="checkall-toggle"
					value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>"
					onclick="Joomla.checkAll(this)" />
				</th>
				<th width="10%"><?php echo JHtml::_('grid.sort', 'COM_TRAVELBOOK_BOOKING_CODE', 'a.title', $listDirn, $listOrder); ?>
				</th>
				<th width="10%"><?php echo JHtml::_('grid.sort', 'COM_TRAVELBOOK_BOOKINGS_CREATED', 'a.created', $listDirn, $listOrder); ?>
				</th>
				<th><?php echo JHtml::_('grid.sort', 'COM_TRAVELBOOK_BOOKING_TOUR', 't.title', $listDirn, $listOrder); ?>
				</th>
				<th width="10%"><?php echo JHtml::_('grid.sort', 'COM_TRAVELBOOK_BOOKING_DATE', 'd.departure', $listDirn, $listOrder); ?>
				</th>
				<th width="16%"><?php echo JHtml::_('grid.sort', 'COM_TRAVELBOOK_BOOKING_NAME', 'c.firstname', $listDirn, $listOrder); ?>
				</th>
				<th width="5%"><?php echo JHtml::_('grid.sort', 'JSTATUS', 'a.state', $listDirn, $listOrder); ?>
				</th>
				<th width="10%"><?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ORDERING', 'a.ordering', $listDirn, $listOrder); ?>
				<?php if ($saveOrder) :?> <?php echo JHtml::_('grid.order', $this->items, 'filesave.png', 'bookings.saveorder'); ?>
				<?php endif; ?>
				</th>
				<th width="10%"><?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ACCESS', 'access_level', $listDirn, $listOrder); ?>
				</th>
				<th width="1%" class="nowrap"><?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="10"><?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php foreach ($this->items as $i => $item) :
		$item->max_ordering = 0; //??
		$address = $item->address === 1 ? JText::_('COM_TRAVELBOOK_MRS') : JText::_('COM_TRAVELBOOK_MR');
			
		$ordering	= ($listOrder == 'a.ordering');
		//			$canCreate	= $user->authorise('core.create', 'com_travelbook.category.'.$item->catid);
		$canEdit	= $user->authorise('core.edit', 'com_travelbook.booking.'.$item->id);
		$canCheckin	= $user->authorise('core.manage', 'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
		$canEditOwn	= $user->authorise('core.edit.own', 'com_travelbook.booking.'.$item->id) && $item->created_by == $userId;
		$canChange	= $user->authorise('core.edit.state', 'com_travelbook.booking.'.$item->id) && $canCheckin;
		?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?>
				</td>
				<td><?php if ($item->checked_out) : ?> <?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'bookings.', $canCheckin); ?>
				<?php endif; ?> <?php if ($canEdit || $canEditOwn) : ?> <a
					href="<?php echo JRoute::_('index.php?option=com_travelbook&task=booking.edit&id='.$item->id);?>">
					<?php echo $this->escape($item->title); ?> </a> <?php else : ?> <?php echo $this->escape($item->title); ?>
					<?php endif; ?>
					<p class="smallsub">
					<?php echo $this->escape($item->info);?>
					</p>
				</td>
				<td class="center nowrap"><?php echo JHtml::_('date', $item->created, JText::_('DATE_FORMAT_TB7')); ?>
				</td>
				<td class="left"><?php if ($canEdit || $canEditOwn) : ?> <a
					href="<?php echo JRoute::_('index.php?option=com_travelbook&task=tour.edit&id='.$item->tour_id);?>">
					<?php echo $this->escape($item->tour); ?> </a> <?php else : ?> <?php echo $this->escape($item->tour); ?>
					<?php endif; ?>
				</td>
				<td class="left"><?php if ($canEdit || $canEditOwn) : ?> <a
					href="<?php echo JRoute::_('index.php?option=com_travelbook&task=date.edit&id='.$item->date_id);?>">
					<?php echo JHtml::_('date', $item->departure, JText::_('DATE_FORMAT_LC4')).'-'.JHtml::_('date', $item->arrival, JText::_('DATE_FORMAT_LC4')); ?>
				</a> <?php else : ?> <?php echo JHtml::_('date', $item->departure, JText::_('DATE_FORMAT_LC4')).'-'.JHtml::_('date', $item->arrival, JText::_('DATE_FORMAT_LC4')); ?>
				<?php endif; ?>
					<p class="smallsub">
					<?php echo '('.JText::plural('COM_TRAVELBOOK_BOOKINGS_N_DAYS', $this->escape($item->duration)).')';?>
					</p>
				</td>
				<td class="left"><?php if ($canEdit || $canEditOwn) : ?> <a
					href="<?php echo JRoute::_('index.php?option=com_travelbook&task=client.edit&id='.$item->client_id);?>">
					<?php echo strtoupper($this->escape($item->client.'/'.$item->lastname.'/'.$address)); ?>
				</a> <?php else : ?> <?php echo strtoupper($this->escape($item->client.'/'.$item->lastname.'/'.$address)); ?>
				<?php endif; ?>
				</td>
				<td class="center"><?php //echo JHtml::_('jgrid.published', $item->state, $i, 'bookings.', $canChange, 'cb', $item->publish_up, $item->publish_down); ?>
				<?php echo JHtml::_('jgrid.published', $item->state, $i, 'bookings.', $canChange, 'cb'); ?>
				</td>
				<td class="order"><?php if ($canChange) : ?> <?php if ($saveOrder) :?>
				<?php if ($listDirn == 'asc') : ?> <span><?php echo $this->pagination->orderUpIcon($i, ($item->catid == @$this->items[$i-1]->catid), 'bookings.orderup', 'JLIB_HTML_MOVE_UP', $ordering); ?>
				</span> <span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, ($item->catid == @$this->items[$i+1]->catid), 'bookings.orderdown', 'JLIB_HTML_MOVE_DOWN', $ordering); ?>
				</span> <?php elseif ($listDirn == 'desc') : ?> <span><?php echo $this->pagination->orderUpIcon($i, ($item->catid == @$this->items[$i-1]->catid), 'bookings.orderdown', 'JLIB_HTML_MOVE_UP', $ordering); ?>
				</span> <span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, ($item->catid == @$this->items[$i+1]->catid), 'bookings.orderup', 'JLIB_HTML_MOVE_DOWN', $ordering); ?>
				</span> <?php endif; ?> <?php endif; ?> <?php $disabled = $saveOrder ?  '' : 'disabled="disabled"'; ?>
					<input type="text" name="order[]" size="5"
					value="<?php echo $item->ordering;?>" <?php echo $disabled ?>
					class="text-area-order" /> <?php else : ?> <?php echo $item->ordering; ?>
					<?php endif; ?>
				</td>
				<td class="center"><?php echo $this->escape($item->access_level); ?>
				</td>
				<td class="center"><?php echo (int) $item->id; ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<?php // echo $this->loadTemplate('batch'); ?>

	<div>
		<input type="hidden" name="task" value="" /> <input type="hidden"
			name="boxchecked" value="0" /> <input type="hidden"
			name="filter_order" value="<?php echo $listOrder; ?>" /> <input
			type="hidden" name="filter_order_Dir"
			value="<?php echo $listDirn; ?>" />
			<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
