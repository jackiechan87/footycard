<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of search terms.
 *
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @since		2.0
 */
class TravelbookViewSearches extends JViewLegacy
{
	protected $enabled;
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->state = $this->get('State');
		$this->enabled = $this->state->params->get('search_enabled');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	2.0
	 */
	protected function addToolbar()
	{
		$canDo = TravelbookHelper::getActions();

		JToolBarHelper::title(JText::_('COM_TRAVELBOOK_MANAGER_SEARCHES'), 'search.png');

		if ($canDo->get('core.edit.state')) {
			JToolBarHelper::custom('searches.reset', 'refresh.png', 'refresh_f2.png', 'JSEARCH_RESET', false);
		}
		JToolBarHelper::divider();
		if ($canDo->get('core.admin')) {
			JToolBarHelper::preferences('com_travelbook');
		}
//		JToolBarHelper::divider();
//		JToolBarHelper::help('JHELP_COMPONENTS_SEARCH');
	}
}
