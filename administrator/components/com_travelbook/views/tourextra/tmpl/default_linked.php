<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('demopage.html.html.grid');

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');

$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->escape($this->linkedState->get('list.ordering'));
$listDirn = $this->escape($this->linkedState->get('list.direction'));
$canOrder = $user->authorise('core.edit.state', 'com_travelbook.category');
$saveOrder = $listOrder == 'ordering';
?>

<div class="width-50 fltlft">
	<h2><?php echo JText::_('COM_TRAVELBOOK_TOURS_EXTRAS_LINKED_EXTRAS')?></h2>
	<table class="adminlist">
		<thead>
			<tr>
				<th width="1%">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th class="title left">
					<?php echo JHtml::_('grid.sort', 'JGLOBAL_TITLE', 'a.title', $listDirn, $listOrder, 'tourextra.display'); ?>
				</th>
				<th width="20%" class=left>
					<?php echo JHtml::_('grid.sort', 'COM_TRAVELBOOK_TOURS_EXTRAS_TYPE', 'a.type', $listDirn, $listOrder, 'tourextra.display'); ?>
				</th>
				<th class="title left">
					<?php echo JHtml::_('grid.sort', 'COM_TRAVELBOOK_TOURS_EXTRAS_RATE', 'rate', $listDirn, $listOrder, 'tourextra.display'); ?>
				</th>
				<th width="20%" class=left>
					<?php echo JHtml::_('grid.sort', 'JCATEGORY', 'category_title', $listDirn, $listOrder, 'tourextra.display'); ?>
				</th>
				<th width="1%" class="nowrap">
					<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder, 'tourextra.display'); ?>
				</th>
				<th width="5%">
					<?php echo JHtml::_('grid.sort', 'JSTATUS', 'state', $listDirn, $listOrder, 'tourextra.display'); ?>
				</th>
				<th width="16%">
					<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ORDERING', 'ordering', $listDirn, $listOrder, 'tourextra.display'); ?>
					<?php if ($canOrder && $saveOrder) :?>
						<?php echo JHtml::_('grid.order',  $this->linkedItems, 'filesave.png', 'tourextra.saveorder'); ?>
					<?php endif; ?>
				</th>
				<th width="5%">
					<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ACCESS', 'a.access', $listDirn, $listOrder, 'tourextra.display'); ?>
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="9">
					<?php echo $this->linkedPagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php foreach ($this->linkedItems as $i => $item) :
			$ordering = ($listOrder == 'ordering');
			$item->cat_link = JRoute::_('index.php?option=com_categories&extension=com_travelbook&task=edit&type=other&cid[]='. $item->catid);
			$canCreate = $user->authorise('core.create', 'com_travelbook.category.'.$item->catid);
			$canCheckin = $user->authorise('core.manage', 'com_checkin') || $item->checked_out==$user->get('id') || $item->checked_out==0;
			$canChange = $user->authorise('core.edit.state', 'com_travelbook.tourextra.'.$item->id) && $canCheckin;
            $params = new JRegistry($item->attribs);
    		jimport('joomla.filesystem.path');
    		$name=$params->get('hs_extra','default.png');
    		$pathName = JPath::clean("$name");
    		
    		// Resize the Extra
    		jimport('joomla.filesystem.file');
    		$fileName = JFile::getName($pathName);
			$fileName = str_replace('.', '-'.$item->extra_id.'.', $fileName);
    		$thumbName = str_replace('.', '.thumb.', $fileName);
    		
    		$alt = $params->get('hs_alt') ? $params->get('hs_alt') : $fileName;
    		$title = $params->get('hs_title') ? $params->get('hs_title') : $fileName;
            
			?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center">
					<?php echo JHtml::_('grid.id', $i, $item->id); ?>
				</td>
				<!-- Title -->
				<td>
					<?php if ($item->checked_out) : ?>
						<?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'tourextra.', $canCheckin); ?>
					<?php endif; ?>
					<?php echo $this->escape($item->title); ?>
				</td>
				<td class="left">
					<?php echo JHtml::_('tbgrid.type', $item->type); ?>
				</td>
				<td class="left">
					<?php echo $item->type == 2 ? $this->currency->calculate($item->rate) : ''; ?>
				</td>
				<td class="left">
					<?php echo $this->escape($item->category_title); ?>
				</td>
				<td class="center">
					<?php echo (int) $item->id; ?>
				</td>
				<td class="center">
					<?php echo JHtml::_('jgrid.published', $item->state, $i, 'tourextra.', $canChange, 'cb', $item->publish_up, $item->publish_down); ?>
				</td>
				<td class="order">
					<?php if ($canChange) : ?>
						<?php if ($saveOrder) :?>
							<?php if ($listDirn == 'asc') : ?>
								<span><?php echo $this->linkedPagination->orderUpIcon($i, ($item->type == @$this->linkedItems[$i-1]->type), 'tourextra.orderup', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
								<span><?php echo $this->linkedPagination->orderDownIcon($i, $this->linkedPagination->total, ($item->type == @$this->linkedItems[$i+1]->type), 'tourextra.orderdown', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
							<?php elseif ($listDirn == 'desc') : ?>
								<span><?php echo $this->linkedPagination->orderUpIcon($i, ($item->catid == @$this->linkedItems[$i-1]->catid), 'tourextra.orderdown', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
								<span><?php echo $this->linkedPagination->orderDownIcon($i, $this->linkedPagination->total, ($item->catid == @$this->linkedItems[$i+1]->catid), 'tourextra.orderup', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
							<?php endif; ?>
						<?php endif; ?>
						<?php $disabled = $saveOrder ?  '' : 'disabled="disabled"'; ?>
						<input type="text" name="order[]" size="5" value="<?php echo $item->ordering;?>" <?php echo $disabled ?> class="text-area-order" />
					<?php else : ?>
						<?php echo $item->ordering; ?>
					<?php endif; ?>
				</td>
				<td class="center">
					<?php echo $this->escape($item->access_level); ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>