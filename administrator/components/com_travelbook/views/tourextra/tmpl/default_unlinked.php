<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('demopage.html.html.grid');

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');

$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->escape($this->unlinkedState->get('list.ordering'));
$listDirn = $this->escape($this->unlinkedState->get('list.direction'));
?>

<form action="<?php echo JRoute::_('index.php?option=com_travelbook&view=tourextra&id_1='.(int)$this->item->id); ?>" method="post" name="unlinkedForm" id="unlinkedForm">
	<div class="width-50 fltlft">
		<h2><?php echo JText::_('COM_TRAVELBOOK_TOURS_EXTRAS_UNLINKED_EXTRAS')?></h2>
		<table class="adminlist">
			<thead>
				<tr>
					<th width="1%">
						<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
					</th>
					<th class="title left">
						<?php echo JHtml::_('DPHtml.grid.sortUnlinked', 'JGLOBAL_TITLE', 'a.title', $listDirn, $listOrder, 'tourextra.display'); ?>
					</th>
					<th width="20%" class=left>
						<?php echo JHtml::_('DPHtml.grid.sortUnlinked', 'COM_TRAVELBOOK_TOURS_EXTRAS_TYPE', 'a.type', $listDirn, $listOrder, 'tourextra.display'); ?>
					</th>
					<th width="20%" class=left>
						<?php echo JHtml::_('DPHtml.grid.sortUnlinked', 'COM_TRAVELBOOK_TOURS_EXTRAS_RATE', 'rate', $listDirn, $listOrder, 'tourextra.display'); ?>
					</th>
					<th width="20%">
						<?php echo JHtml::_('DPHtml.grid.sortUnlinked', 'JCATEGORY', 'category_title', $listDirn, $listOrder, 'tourextra.display'); ?>
					</th>
					<th width="1%" class="nowrap">
						<?php echo JHtml::_('DPHtml.grid.sortUnlinked', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder, 'tourextra.display'); ?>
					</th>
					<th width="5%">
						<?php echo JHtml::_('DPHtml.grid.sortUnlinked', 'JSTATUS', 'state', $listDirn, $listOrder, 'tourextra.display'); ?>
					</th>
					<th width="10%">
						<?php echo JHtml::_('DPHtml.grid.sortUnLinked', 'JGRID_HEADING_ORDERING', 'ordering', $listDirn, $listOrder, 'tourextra.display'); ?>
					</th>
					<th width="5%">
						<?php echo JHtml::_('DPHtml.grid.sortUnlinked', 'JGRID_HEADING_ACCESS', 'access_level', $listDirn, $listOrder, 'tourextra.display'); ?>
					</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="9">
						<?php echo $this->linkedPagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php foreach ($this->unlinkedItems as $i => $item) :
				$item->cat_link = JRoute::_('index.php?option=com_categories&extension=com_travelbook&task=edit&type=other&cid[]='. $item->catid);
//				$canCreate = $user->authorise('core.create', 'com_travelbook.category.'.$item->catid);
				$canEdit = $user->authorise('core.edit', 'com_travelbook.tour.'.$item->id);
				$canEditOwn	= $user->authorise('core.edit.own', 'com_travelbook.tour.'.$item->id) && $item->created_by == $userId;
				$canCheckin = $user->authorise('core.manage', 'com_checkin') || $item->checked_out==$user->get('id') || $item->checked_out==0;

                $params = new JRegistry($item->attribs);
        		jimport('joomla.filesystem.path');
        		$name=$params->get('hs_extra','default.png');
        		$pathName = JPath::clean("$name");
        		
        		// Resize the Extra
        		jimport('joomla.filesystem.file');
        		$fileName = JFile::getName($pathName);
				$fileName = str_replace('.', '-'.$item->id.'.', $fileName);
        		$thumbName = str_replace('.', '.thumb.', $fileName);
        		
        		$alt = $params->get('hs_alt') ? $params->get('hs_alt') : $fileName;
        		$title = $params->get('hs_title') ? $params->get('hs_title') : $fileName;
			?>
				<tr class="row<?php echo $i % 2; ?>">
					<td class="center">
						<?php echo JHtml::_('grid.id', $i, $i, false, 'cid_inv'); ?>
						<input type="hidden" name="jform[type][]" value="<?php echo $item->type; ?>" />
					</td>
					<!-- Title -->
					<td>
						<?php if ($item->checked_out) : ?>
							<?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'tours.', $canCheckin); ?>
						<?php endif; ?>
						<?php if ($canEdit || $canEditOwn) : ?>
							<a href="<?php echo JRoute::_('index.php?option=com_travelbook&task=extra.edit&id='.(int) $item->id); ?>">
								<?php echo $this->escape($item->title); ?></a>
						<?php else : ?>
								<?php echo $this->escape($item->title); ?>
						<?php endif; ?>
					</td>
					<td class="left">
						<?php echo JHtml::_('tbgrid.type', $item->type); ?>
					</td>
					<td class="left">
						<?php $type = $item->type == 2 ? 'text' : 'hidden';?>
						<input type="<?php echo $type; ?>" name="jform[rate][]" value="<?php echo $item->rate; ?>" />
					</td>
					<!-- category -->
					<td class="left">
						<?php echo $this->escape($item->category_title); ?>
						<input type="hidden" name="jform[catid][]" value="<?php echo $item->catid; ?>" />
					</td>
					<td class="center">
						<?php echo (int) $item->id; ?>
						<input type="hidden" name="jform[id][]" value="<?php echo $item->id; ?>" />
					</td>
					<!-- State -->
					<td class="center">
						<?php echo JHtml::_('jgrid.published', $item->state, $i, 'tours.', false, 'cb', $item->publish_up, $item->publish_down); ?>
					</td>
					<td class="order">
						<input type="text" name="jform[ordering][]" size="5" value="<?php echo $item->ordering; ?>" class="text-area-order" />
					</td>
					<td class="center">
						<?php echo $this->escape($item->access_level); ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<div>
		<input type="hidden" name="task" value="" />
<!-- 
		<input type="hidden" name="id" value="<?php // echo $this->item->id; ?>" />
 -->
		<input type="hidden" name="id_1" value="<?php echo $this->item->id; ?>" />
		<input type="hidden" name="jform[tour_id]" value="<?php echo $this->item->id; ?>" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>	