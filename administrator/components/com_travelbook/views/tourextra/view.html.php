<?php
/**
 * Travelbook for JOOMLA!
 *
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
jimport('demopage.currency');

/**
 * View class for a list of tourextra.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_travelbook
 * @since		1.5
 */
class TravelbookViewTourExtra extends JView
{
	protected $item;
	protected $items;
	protected $linkedItems;
	protected $unlinkedItems;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		// Get the Tour
		$model = $this->getModel('tour');
		$tour_id = JRequest::getVar('id_1', '0', 'get', 'string');
		$this->item = $model->getItem((int)$tour_id);
		
		$this->state = $this->get('State');
		$this->canDo = TravelbookHelper::getActions('extra', $this->state->get('filter.category_id'));
		
		$model = $this->getModel('tourExtraLinked');
		$this->linkedItems = $model->getItems();
		$this->linkedState = $model->getState();
		$this->linkedPagination = $model->getPagination();
		
		$model = $this->getModel('tourextraunlinked');
		$this->unlinkedItems = $model->getItems();
		$this->unlinkedState = $model->getState();
		$this->unlinkedPagination = $model->getPagination();
		
        // Currency
		$params = $this->state->get('params');
        $this->currency = new DPCurrency($params);
		
        // Get Calculation Rules
//		$this->calculationRules = $this->get('CalculationRules');
				
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		
		$document = JFactory::getDocument();
		$url = '../libraries/demopage/scripts/core-uncompressed.js';
		$document->addScript($url);
		
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
// 		require_once JPATH_COMPONENT.'/helpers/tourextra.php';
		
		$state = $this->get('State');
		$canDo = TravelbookHelper::getActions('extra', $state->get('filter.category_id'));
		$user = JFactory::getUser();

		JToolBarHelper::title(JText::sprintf('COM_TRAVELBOOK_TOURS_EXTRAS', '<span>'.$this->item->title.'</span>'), 'tourextra.png');

		JToolBarHelper::custom('tourextra.link', 'link', 'link_over', JText::_('COM_TRAVELBOOK_LINK_TOURS_EXTRAS'), false, true);
		JToolBarHelper::custom('tourextra.unlink', 'unlink', 'unlink_over', JText::_('COM_TRAVELBOOK_UNLINK_TOURS_EXTRAS'), false, true);
		
		if ($canDo->get('core.edit.state')) {
			JToolBarHelper::publish('tourextra.publish', 'JTOOLBAR_PUBLISH', true);
			JToolBarHelper::unpublish('tourextra.unpublish', 'JTOOLBAR_UNPUBLISH', true);
			JToolBarHelper::divider();
			JToolBarHelper::checkin('tourextra.checkin');
		}

		JToolBarHelper::divider();
		JToolBarHelper::custom('tourextra.cancel', 'back', 'back_over', JText::_('COM_TRAVELBOOK_BACK_TO_TOUR'), false, true);
		JToolBarHelper::custom('tour.cancel', 'back', 'back_over', JText::_('COM_TRAVELBOOK_BACK_TO_TOUR_LIST'), false, true);
		
//		JToolBarHelper::help('JHELP_COMPONENTS_TRAVELBOOK_LINKS');
	}
}
