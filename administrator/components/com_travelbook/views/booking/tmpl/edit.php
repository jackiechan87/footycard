<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_travelbook
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');

// Create shortcut to parameters.
$params = $this->state->get('params');
$params = $params->toArray();

// This checks if the config options have ever been saved. If they haven't they will fall back to the original settings.
$editoroptions = isset($params['show_publishing_options']);

if (!$editoroptions):
$params['show_publishing_options'] = '1';
$params['show_booking_options'] = '1';
endif;

// Check if the booking uses configuration settings besides global. If so, use them.
if (!empty($this->item->attribs['show_publishing_options'])):
$params['show_publishing_options'] = $this->item->attribs['show_publishing_options'];
endif;
if (!empty($this->item->attribs['show_booking_options'])):
$params['show_booking_options'] = $this->item->attribs['show_booking_options'];
endif;

?>

<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if (task == 'booking.cancel' || document.formvalidator.isValid(document.id('item-form'))) {
			<?php echo $this->form->getField('bookingtext')->save(); ?>
			Joomla.submitform(task, document.getElementById('item-form'));
		} else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_travelbook&layout=edit&id='.(int) $this->item->id); ?>"
	method="post" name="adminForm" id="item-form" class="form-validate">
	<div class="width-60 fltlft">
		<fieldset class="adminform">
			<legend>
			<?php echo empty($this->item->id) ? JText::_('COM_TRAVELBOOK_NEW_BOOKING') : JText::sprintf('COM_TRAVELBOOK_EDIT_BOOKING', $this->item->id); ?>
			</legend>
			<ul class="adminformlist">
				<li><?php echo $this->form->getLabel('title'); ?> <?php echo $this->form->getInput('title'); ?></li>

				<li><?php echo $this->form->getLabel('alias'); ?> <?php echo $this->form->getInput('alias'); ?></li>

				<li><?php echo $this->form->getLabel('DID'); ?> <?php echo $this->form->getInput('DID'); ?></li>

				<li><?php echo $this->form->getLabel('CID'); ?> <?php echo $this->form->getInput('CID'); ?></li>

				<li><?php echo $this->form->getLabel('state'); ?> <?php echo $this->form->getInput('state'); ?></li>

				<li><?php echo $this->form->getLabel('access'); ?> <?php echo $this->form->getInput('access'); ?></li>

				<?php if ($this->canDo->get('core.admin')): ?>
				<li><span class="faux-label"><?php echo JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL'); ?>
				</span>
					<div class="button2-left">
						<div class="blank">
							<button type="button"
								onclick="document.location.href='#access-rules';">
								<?php echo JText::_('JGLOBAL_PERMISSIONS_ANCHOR'); ?>
							</button>
						</div>
					</div>
				</li>
				<?php endif; ?>

				<li><?php echo $this->form->getLabel('featured'); ?> <?php echo $this->form->getInput('featured'); ?>
				</li>

				<li><?php echo $this->form->getLabel('language'); ?> <?php echo $this->form->getInput('language'); ?>
				</li>

				<li><?php echo $this->form->getLabel('id'); ?> <?php echo $this->form->getInput('id'); ?>
				</li>
			</ul>

			<div class="clr"></div>
			<?php echo $this->form->getLabel('bookingtext'); ?>
			<div class="clr"></div>
			<?php echo $this->form->getInput('bookingtext'); ?>
		</fieldset>
	</div>

	<div class="width-40 fltrt">
	<?php echo JHtml::_('sliders.start', 'travelbook-sliders-'.$this->item->id, array('useCookie'=>1)); ?>
	<?php // Do not show the publishing options if the edit form is configured not to. ?>
	<?php if ($params['show_publishing_options'] || ( $params['show_publishing_options'] = '' && !empty($editoroptions)) ): ?>
		<?php echo JHtml::_('sliders.panel', JText::_('COM_TRAVELBOOK_FIELDSET_PUBLISHING'), 'publishing-details'); ?>
		<fieldset class="panelform">
			<ul class="adminformlist">
				<li><?php echo $this->form->getLabel('created_by'); ?> <?php echo $this->form->getInput('created_by'); ?>
				</li>

				<li><?php echo $this->form->getLabel('created_by_alias'); ?> <?php echo $this->form->getInput('created_by_alias'); ?>
				</li>

				<li><?php echo $this->form->getLabel('created'); ?> <?php echo $this->form->getInput('created'); ?>
				</li>

				<?php if ($this->item->modified_by) : ?>
				<li><?php echo $this->form->getLabel('modified_by'); ?> <?php echo $this->form->getInput('modified_by'); ?>
				</li>

				<li><?php echo $this->form->getLabel('modified'); ?> <?php echo $this->form->getInput('modified'); ?>
				</li>
				<?php endif; ?>

				<?php if ($this->item->version) : ?>
				<li><?php echo $this->form->getLabel('version'); ?> <?php echo $this->form->getInput('version'); ?>
				</li>
				<?php endif; ?>

			</ul>
		</fieldset>
	<?php endif; ?>
	<?php echo JHtml::_('sliders.panel', JText::_('COM_TRAVELBOOK_FIELDSET_EXTRAS'), 'extras'); ?>
		<fieldset class="panelform">
			<?php echo $this->form->getInput('extras'); ?>
		</fieldset>
	<?php echo JHtml::_('sliders.panel', JText::_('COM_TRAVELBOOK_FIELDSET_TERMS'), 'terms'); ?>
		<fieldset class="panelform">
			<?php echo $this->form->getInput('terms'); ?>
		</fieldset>

		<?php $fieldSets = $this->form->getFieldsets('attribs'); ?>
		<?php foreach ($fieldSets as $name => $fieldSet) : ?>
		<?php
		// If the parameter says to show the booking options or if the parameters have never been set, we will
		// show the booking options.
		if (false && $params['show_booking_options'] || (( $params['show_booking_options'] == '' && !empty($editoroptions) ))):

		// Go through all the fieldsets except the configuration and basic-limited, which are
		// handled separately below.
		if ($name != 'editorConfig' && $name != 'basic-limited') : ?>
		<?php echo JHtml::_('sliders.panel', JText::_($fieldSet->label), $name.'-options'); ?>
		<?php if (isset($fieldSet->description) && trim($fieldSet->description)) : ?>
		<p class="tip">
		<?php echo $this->escape(JText::_($fieldSet->description));?>
		</p>
		<?php endif; ?>
		<fieldset class="panelform">
			<ul class="adminformlist">
			<?php foreach ($this->form->getFieldset($name) as $field) : ?>
				<li><?php echo $field->label; ?> <?php echo $field->input; ?></li>
				<?php endforeach; ?>
			</ul>
		</fieldset>
		<?php endif ?>
		<?php // If we are not showing the options we need to use the hidden fields so the values are not lost.  ?>
		<?php  elseif ($name == 'basic-limited' && false): ?>
		<?php foreach ($this->form->getFieldset('basic-limited') as $field) : ?>
		<?php  echo $field->input; ?>
		<?php endforeach; ?>

		<?php endif; ?>
		<?php endforeach; ?>

		<?php
		// We need to make a separate space for the configuration
		// so that those fields always show to those wih permissions
		if ($this->canDo->get('core.admin') && false) : ?>
		<?php  echo JHtml::_('sliders.panel', JText::_('COM_TRAVELBOOK_SLIDER_EDITOR_CONFIG'), 'configure-sliders'); ?>
		<fieldset class="panelform">
			<ul class="adminformlist">
			<?php foreach ($this->form->getFieldset('editorConfig') as $field) : ?>
				<li><?php echo $field->label; ?> <?php echo $field->input; ?></li>
				<?php endforeach; ?>
			</ul>
		</fieldset>
		<?php endif ?>
		<?php echo JHtml::_('sliders.end'); ?>
	</div>

	<div class="clr"></div>
	<?php if ($this->canDo->get('core.admin')): ?>
	<div class="width-100 fltlft">
	<?php echo JHtml::_('sliders.start', 'permissions-sliders-'.$this->item->id, array('useCookie'=>1)); ?>

	<?php echo JHtml::_('sliders.panel', JText::_('COM_TRAVELBOOK_FIELDSET_BOOKING_RULES'), 'access-rules'); ?>
		<fieldset class="panelform">
		<?php echo $this->form->getLabel('rules'); ?>
		<?php echo $this->form->getInput('rules'); ?>
		</fieldset>

		<?php echo JHtml::_('sliders.end'); ?>
	</div>
	<?php endif; ?>
	<div>
		<input type="hidden" name="task" value="" /> <input type="hidden"
			name="return" value="<?php echo JRequest::getCmd('return');?>" />
			<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
