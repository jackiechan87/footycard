<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_travelbook
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$published = $this->state->get('filter.published');
?>
<fieldset class="batch">
	<legend>
	<?php echo JText::_('COM_TRAVELBOOK_EMAIL_BATCH_OPTIONS');?>
	</legend>
	<p>
	<?php echo JText::_('COM_TRAVELBOOK_EMAIL_BATCH_TIP'); ?>
	</p>
	<?php echo JHtml::_('batch.access');?>
	<?php echo JHtml::_('batch.language'); ?>

<fieldset class="combo" id="batch-choose-action">
	<input type="hidden" id="batch-category-id" value="null" name="batch[category_id]">
	
	<input type="hidden" checked="checked" value="c" id="batch[move_copy]c" name="batch[move_copy]">
</fieldset>

	<div id="email-batch-submit">
		<button type="submit" onclick="Joomla.submitbutton('email.batch');">
		<?php echo JText::_('JGLOBAL_BATCH_PROCESS'); ?>
		</button>
		<button type="button"
			onclick="document.id('batch-category-id').value='';document.id('batch-access').value='';document.id('batch-language-id').value=''">
			<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>
		</button>
	</div>
</fieldset>
