<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');

// Create shortcut to parameters.
$params = $this->state->get('params');
$params = $params->toArray();

// This checks if the config options have ever been saved. If they haven't they will fall back to the original settings.
$editoroptions = isset($params['show_publishing_options']);

if (!$editoroptions):
$params['show_publishing_options'] = '1';
$params['show_detail_options'] = '1';
$params['show_urls_images_backend'] = '0';
$params['show_urls_images_frontend'] = '0';
endif;

// Check if the detail uses configuration settings besides global. If so, use them.
if (!empty($this->item->attribs['show_publishing_options'])):
$params['show_publishing_options'] = $this->item->attribs['show_publishing_options'];
endif;
if (!empty($this->item->attribs['show_detail_options'])):
$params['show_detail_options'] = $this->item->attribs['show_detail_options'];
endif;
if (!empty($this->item->attribs['show_urls_images_backend'])):
$params['show_urls_images_backend'] = $this->item->attribs['show_urls_images_backend'];
endif;

?>

<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if (task == 'detail.cancel' || document.formvalidator.isValid(document.id('item-form'))) {
			<?php echo $this->form->getField('articletext')->save(); ?>
			Joomla.submitform(task, document.getElementById('item-form'));
		} else {
			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_travelbook&layout=edit&id='.(int) $this->item->id); ?>"
	method="post" name="adminForm" id="item-form" class="form-validate">
	<div class="width-60 fltlft">
		<fieldset class="adminform">
			<legend><?php echo empty($this->item->id) ? JText::_('COM_TRAVELBOOK_NEW_DETAIL') : JText::sprintf('COM_TRAVELBOOK_EDIT_DETAIL', $this->item->id); ?></legend>
			<ul class="adminformlist">
				<li><?php echo $this->form->getLabel('TID'); ?> <?php echo $this->form->getInput('TID'); ?>
				</li>

				<li><?php echo $this->form->getLabel('title'); ?> <?php echo $this->form->getInput('title'); ?>
				</li>

				<li><?php echo $this->form->getLabel('catid'); ?> <?php echo $this->form->getInput('catid'); ?>
				</li>
			</ul>
		</fieldset>

		<fieldset class="adminform">
			<legend><?php echo JText::_('COM_TRAVELBOOK_FIELD_DETAIL_ARTICLETEXT_LABEL'); ?></legend>
			<div class="clr"></div>
			<?php echo $this->form->getInput('articletext'); ?>
		</fieldset>

		<fieldset class="adminform">
			<legend><?php echo JText::_('COM_TRAVELBOOK_SETTINGS'); ?></legend>
			<ul class="adminformlist">
				<li><?php echo $this->form->getLabel('state'); ?> <?php echo $this->form->getInput('state'); ?>
				</li>

				<li><?php echo $this->form->getLabel('access'); ?> <?php echo $this->form->getInput('access'); ?>
				</li>

				<?php if ($this->canDo->get('core.admin')): ?>
				<li><span class="faux-label"><?php echo JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL'); ?>
				</span>
					<div class="button2-left">
						<div class="blank">
							<button type="button"
								onclick="document.location.href='#access-rules';">
								<?php echo JText::_('JGLOBAL_PERMISSIONS_ANCHOR'); ?>
							</button>
						</div>
					</div>
				</li>
				<?php endif; ?>

				<li><?php echo $this->form->getLabel('featured'); ?> <?php echo $this->form->getInput('featured'); ?>
				</li>

				<li><?php echo $this->form->getLabel('language'); ?> <?php echo $this->form->getInput('language'); ?>
				</li>

				<li><?php echo $this->form->getLabel('id'); ?> <?php echo $this->form->getInput('id'); ?>
				</li>
			</ul>
		</fieldset>
	</div>

	<div class="width-40 fltrt">
	<?php  if ($params['show_publishing_options'] || ( $params['show_publishing_options'] = '' && !empty($editoroptions)) ): ?>
		<fieldset class="panelform">
			<legend><?php echo JText::_('COM_TRAVELBOOK_FIELDSET_PUBLISHING'); ?></legend>
			<ul class="adminformlist">
				<li><?php echo $this->form->getLabel('created_by'); ?> <?php echo $this->form->getInput('created_by'); ?>
				</li>

				<li><?php echo $this->form->getLabel('created_by_alias'); ?> <?php echo $this->form->getInput('created_by_alias'); ?>
				</li>

				<li><?php echo $this->form->getLabel('created'); ?> <?php echo $this->form->getInput('created'); ?>
				</li>

				<li><?php echo $this->form->getLabel('publish_up'); ?> <?php echo $this->form->getInput('publish_up'); ?>
				</li>

				<li><?php echo $this->form->getLabel('publish_down'); ?> <?php echo $this->form->getInput('publish_down'); ?>
				</li>

				<?php if ($this->item->modified_by) : ?>
				<li><?php echo $this->form->getLabel('modified_by'); ?> <?php echo $this->form->getInput('modified_by'); ?>
				</li>

				<li><?php echo $this->form->getLabel('modified'); ?> <?php echo $this->form->getInput('modified'); ?>
				</li>
				<?php endif; ?>

				<?php if ($this->item->version) : ?>
				<li><?php echo $this->form->getLabel('version'); ?> <?php echo $this->form->getInput('version'); ?>
				</li>
				<?php endif; ?>

			</ul>
		</fieldset>
	<?php  endif; ?>

	<?php echo JHtml::_('sliders.start', 'content-sliders-'.$this->item->id, array('useCookie'=>1)); ?>
		<?php $fieldSets = $this->form->getFieldsets('attribs'); ?>
		<?php foreach ($fieldSets as $name => $fieldSet) : ?>

			<?php if ($params['show_detail_options'] || (( $params['show_detail_options'] == '' && !empty($editoroptions) ))): ?>
				<?php if ($name != 'editorConfig' && $name != 'basic-limited') : ?>
					<?php echo JHtml::_('sliders.panel', JText::_($fieldSet->label), $name.'-options'); ?>
					<?php if (isset($fieldSet->description) && trim($fieldSet->description)) : ?>
						<p class="tip"><?php echo $this->escape(JText::_($fieldSet->description));?></p>
					<?php endif; ?>
					<fieldset class="panelform">
						<ul class="adminformlist">
							<?php foreach ($this->form->getFieldset($name) as $field) : ?>
								<li><?php echo $field->label; ?> <?php echo $field->input; ?></li>
							<?php endforeach; ?>
						</ul>
					</fieldset>
				<?php endif ?>

			<?php elseif ($name == 'basic-limited'): ?>
				<?php foreach ($this->form->getFieldset('basic-limited') as $field) : ?>
					<?php echo $field->input; ?>
				<?php endforeach; ?>
			<?php endif; ?>
		<?php endforeach; ?>

	<?php echo JHtml::_('sliders.end'); ?>
	</div>

	<div class="clr"></div>
	<?php if ($this->canDo->get('core.admin')): ?>
	<div class="width-100 fltlft">
	<?php echo JHtml::_('sliders.start', 'permissions-sliders-'.$this->item->id, array('useCookie'=>1)); ?>

	<?php echo JHtml::_('sliders.panel', JText::_('COM_TRAVELBOOK_FIELDSET_DETAIL_RULES'), 'access-rules'); ?>
		<fieldset class="panelform">
		<?php echo $this->form->getLabel('rules'); ?>
		<?php echo $this->form->getInput('rules'); ?>
		</fieldset>

		<?php echo JHtml::_('sliders.end'); ?>
	</div>
	<?php endif; ?>
	<div>
		<input type="hidden" name="task" value="" /> <input type="hidden"
			name="return" value="<?php echo JRequest::getCmd('return');?>" />
			<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
