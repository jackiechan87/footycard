<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit an email.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_travelbook
 * @since		1.6
 */
class TravelbookViewEmail extends JView
{
	protected $form;
	protected $item;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		// Initialiase variables.
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');
		$this->state = $this->get('State');
		$this->canDo = TravelbookHelper::getActions('email', $this->state->get('filter.category_id'));

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		JRequest::setVar('hidemainmenu', true);
		$user = JFactory::getUser();
		$userId = $user->get('id');
		$isNew = ($this->item->id == 0);
		$checkedOut = !($this->item->checked_out == 0 || $this->item->checked_out == $userId);
		$canDo = TravelbookHelper::getActions('email', $this->state->get('filter.category_id'), $this->item->id);
		JToolBarHelper::title(JText::_('COM_TRAVELBOOK_PAGE_'.($checkedOut ? 'VIEW_EMAIL' : ($isNew ? 'ADD_EMAIL' : 'EDIT_EMAIL'))), 'email-add.png');

		// Built the actions for new and existing records.

		// For new records, check the create permission.
		if ($isNew && (count($user->getAuthorisedCategories('com_travelbook', 'core.create')) > 0)) {
			JToolBarHelper::apply('email.apply');
			JToolBarHelper::save('email.save');
			JToolBarHelper::save2new('email.save2new');
			JToolBarHelper::cancel('email.cancel');
		}
		else {
			// Can't save the record if it's checked out.
			if (!$checkedOut) {
				// Since it's an existing record, check the edit permission, or fall back to edit own if the owner.
				if ($canDo->get('core.edit') || ($canDo->get('core.edit.own') && $this->item->created_by == $userId)) {
					JToolBarHelper::apply('email.apply');
					JToolBarHelper::save('email.save');

					// We can save this record, but check the create permission to see if we can return to make a new one.
					if ($canDo->get('core.create')) {
						JToolBarHelper::save2new('email.save2new');
					}
				}
			}

			// If checked out, we can still save
			if ($canDo->get('core.create')) {
				JToolBarHelper::save2copy('email.save2copy');
			}

			JToolBarHelper::cancel('email.cancel', 'JTOOLBAR_CLOSE');
		}
//		JToolBarHelper::divider();
//		JToolBarHelper::help('JHELP_TRAVELBOOK_EMAIL_MANAGER_EDIT');
	}
}
