<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die; ?>

<?php $fieldSet = $this->form->getFieldset('email'); ?>

<?php if (isset($fieldSet->description) && trim($fieldSet->description)) : ?>
	<p class="tip"><?php echo $this->escape(JText::_($fieldSet->description)); ?></p>
<?php endif; ?>

<fieldset class="adminform">
	<legend><?php echo JText::_('COM_TRAVELBOOK_FIELDSET_EMAIL_OPTIONS'); ?></legend>
	<ul class="adminformlist">
	<?php foreach ($fieldSet as $field) : ?>
		<li><?php echo $field->label; ?> <?php echo $field->input; ?>
		</li>
		<?php endforeach; ?>
	</ul>
</fieldset>
