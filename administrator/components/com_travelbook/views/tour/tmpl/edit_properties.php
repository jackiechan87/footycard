<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die; ?>

<fieldset class="adminform">
	<legend><?php echo JText::_('COM_TRAVELBOOK_FIELDSET_PROPERTIES_OPTIONS'); ?></legend>
	<ul class="adminformlist">
		<li><?php echo $this->form->getLabel('destination'); ?> <?php echo $this->form->getInput('destination'); ?>
		</li>
		<li><?php echo $this->form->getLabel('activity'); ?> <?php echo $this->form->getInput('activity'); ?>
		</li>
		<li><?php echo $this->form->getLabel('style'); ?> <?php echo $this->form->getInput('style'); ?>
		</li>
	</ul>
</fieldset>
