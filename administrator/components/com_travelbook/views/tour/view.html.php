<?php
/**
 * @version		$Id$
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit a tour.
 *
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @since		2.0
 */
class TravelbookViewTour extends JViewLegacy
{
	protected $form;
	protected $item;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		if ($this->getLayout() == 'pagebreak') {
			// TODO: This is really dogy - should change this one day.
			$eName = JRequest::getVar('e_name');
			$eName = preg_replace( '#[^A-Z0-9\-\_\[\]]#i', '', $eName );
			$document = JFactory::getDocument();
			$document->setTitle(JText::_('COM_TRAVELBOOK_PAGEBREAK_DOC_TITLE'));
			$this->assignRef('eName', $eName);
			parent::display($tpl);
			return;
		}

		// Initialiase variables.
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');
		$this->state = $this->get('State');
		$this->canDo = TravelbookHelper::getActions('tour', $this->state->get('filter.category_id'));

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		JRequest::setVar('hidemainmenu', true);
		$user = JFactory::getUser();
		$userId = $user->get('id');
		$isNew = ($this->item->id == 0);
		$checkedOut = !($this->item->checked_out == 0 || $this->item->checked_out == $userId);
		$canDo = TravelbookHelper::getActions('tour', $this->state->get('filter.category_id'), $this->item->id);
		JToolBarHelper::title(JText::_('COM_TRAVELBOOK_PAGE_'.($checkedOut ? 'VIEW_TOUR' : ($isNew ? 'ADD_TOUR' : 'EDIT_TOUR'))), $isNew ? 'tour-add.png' : 'tour-edit.png');

		// Built the actions for new and existing records.

		// For new records, check the create permission.
		if ($isNew && (count($user->getAuthorisedCategories('com_content', 'core.create')) > 0)) {
			JToolBarHelper::apply('tour.apply');
			JToolBarHelper::save('tour.save');
			JToolBarHelper::save2new('tour.save2new');
			JToolBarHelper::cancel('tour.cancel');
		}
		else {
			// Manage Extras
			JToolBarHelper::custom('tourextra.display', 'edit', 'edit_over', JText::_('COM_TRAVELBOOK_MANAGE_EXTRAS'), false, true );
			JToolBarHelper::divider();
			// Can't save the record if it's checked out.
			if (!$checkedOut) {
				// Since it's an existing record, check the edit permission, or fall back to edit own if the owner.
				if ($canDo->get('core.edit') || ($canDo->get('core.edit.own') && $this->item->created_by == $userId)) {
					JToolBarHelper::apply('tour.apply');
					JToolBarHelper::save('tour.save');

					// We can save this record, but check the create permission to see if we can return to make a new one.
					if ($canDo->get('core.create')) {
						JToolBarHelper::save2new('tour.save2new');
					}
				}
			}

			// If checked out, we can still save
			if ($canDo->get('core.create')) {
				JToolBarHelper::save2copy('tour.save2copy');
			}

			JToolBarHelper::cancel('tour.cancel', 'JTOOLBAR_CLOSE');
		}
//		JToolBarHelper::divider();
//		JToolBarHelper::help('JHELP_TRAVELBOOK_TOUR_MANAGER_EDIT');
	}
}
