<?php
/**
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 *
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Component Controller
 *
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 */
class TravelbookController extends JControllerLegacy
{
	/**
	 * @var		string	The default view.
	 * @since	2.0
	 */
	protected $default_view = 'tours';

	/**
	 * Method to display a view.
	 *
	 * @param	boolean			If true, the view output will be cached
	 * @param	array			An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		// Load the submenu.
		TravelbookHelper::addSubmenu(JRequest::getCmd('view', 'tours'));
					
		$view = JRequest::getCmd('view', 'tours');
		$layout = JRequest::getCmd('layout', 'tours');
		$id = JRequest::getInt('id');

		// Check for tour edit form.
		if ($view == 'tour' && $layout == 'edit' && !$this->checkEditId('com_travelbook.edit.tour', $id)) {
			// Somehow the person just went to the form - we don't allow that.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_travelbook&view=tours', false));

			return false;
		}

		// Check for date edit form.
		if ($view == 'date' && $layout == 'edit' && !$this->checkEditId('com_travelbook.edit.date', $id)) {
			// Somehow the person just went to the form - we don't allow that.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_travelbook&view=dates', false));

			return false;
		}

		parent::display();

		$version = new TravelbookVersion();
		echo $version->renderVersion();
		
		return $this;
		
	}
}
