<?php
/**
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 *
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Script file of HelloWorld component
 */
class com_travelbookInstallerScript
{
	/**
	 * method to run before an install/update/uninstall method
	 *
	 * @return void
	 */
	function preflight($type, $parent)
	{
		$jversion = new JVersion();

		// Installing component manifest file version
		$this->release = $parent->get( "manifest" )->version;

		// Manifest file minimum Joomla version
		$this->minimum_joomla_release = $parent->get( "manifest" )->attributes()->version;

		// Show the essential information at the install/update back-end

		echo JText::sprintf('COM_TRAVELBOOK_PREFLIGHT_INSTALL');
		echo JText::sprintf('COM_TRAVELBOOK_PREFLIGHT_INSTALL_MANIFEST', $this->release);
		if ($this->getParam('version')) {
			echo JText::sprintf('COM_TRAVELBOOK_PREFLIGHT_CURRENT_MANIFEST', $this->getParam('version'));			
		}
		echo JText::sprintf('COM_TRAVELBOOK_PREFLIGHT_MINIMUM_VERSION', $this->release, $this->minimum_joomla_release);
		echo JText::sprintf('COM_TRAVELBOOK_PREFLIGHT_CURRENT_VERSION', $jversion->getShortVersion());

		// abort if the current Joomla release is older
		if( version_compare( $jversion->getShortVersion(), $this->minimum_joomla_release, 'lt' ) ) {
			Jerror::raiseWarning(null, JText::sprintf('COM_TRAVELBOOK_PREFLIGHT_ERROR_MINIMUM_VERSION', $this->release, $this->minimum_joomla_release));
			return false;
		}

		// abort if the component being installed is not newer than the currently installed version
		if ( $type == 'update' ) {
			$oldRelease = $this->getParam('version');
					if (version_compare($oldRelease, '2.0.1', 'lt')) {
				Jerror::raiseWarning(null, JText::_('COM_TRAVELBOOK_PREFLIGHT_ERROR_VERSION_20'));
				return false;
			}
			if (version_compare($oldRelease, '2.11', 'eq')) {
				$this->updateCategories();
			}
			if (version_compare($this->release, $oldRelease, 'lt')) {
				Jerror::raiseWarning(null, JText::sprintf('COM_TRAVELBOOK_PREFLIGHT_ERROR_VERSION_SEQUENZ', $oldRelease, $this->release));
				return false;
			}
		}

		// Check dependencies
		
		echo JText::sprintf('COM_TRAVELBOOK_PREFLIGHT', $this->release, JText::_($type));
	}

	/**
	 * method to install the component
	 *
	 * @return void
	 */
	function install($parent)
	{
		echo '<p>' . JText::sprintf('COM_TRAVELBOOK_INSTALL', $this->release) . '</p>';

		$this->updateCategories();
	}

	/**
	 * method to update the component
	 *
	 * @return void
	 */
	function update($parent)
	{
		
		
		echo '<p>' . JText::sprintf('COM_TRAVELBOOK_UPDATE', $this->release) . '</p>';
	}

	/**
	 * method to run after an install/update/uninstall method
	 *
	 * @return void
	 */
	function postflight($type, $parent) 
	{
		$params = array();
 		$params['steps'] = array('1','3');
 		
		$this->setParams($params);

		echo '<p>' . JText::sprintf('COM_TRAVELBOOK_POSTFLIGHT', $this->release, $type) . '</p>';
		
		$mediaParams = JComponentHelper::getParams('com_media');
		$path = JPATH_ROOT . '/' . $mediaParams->get('image_path', 'images') . '/com_travelbook/documents';
		
		$buffer = '<!DOCTYPE html><title></title>' . PHP_EOL;
		if (!JFolder::exists($path)) {
			$folderCreated = JFolder::create($path);
			echo '<p>';
			echo $folderCreated ? JText::sprintf('COM_TRAVELBOOK_POSTFLIGHT_FOLDER_CREATED_SUCCESS', $path) : JText::sprintf('COM_TRAVELBOOK_POSTFLIGHT_FOLDER_CREATED_FAILED', $path);
			echo '</p>';
			$fileWritten = JFile::write($path . '/index.html', $buffer, true);
			echo '<p>';
			echo $fileWritten ? JText::sprintf('COM_TRAVELBOOK_POSTFLIGHT_FILE_WRITTEN_SUCCESS', 'index.html', $path) : JText::sprintf('COM_TRAVELBOOK_POSTFLIGHT_FILE_WRITTEN_FAILED', 'index.html', $path);
			echo '</p>';
		}
		
	}

	/**
	 * method to install the component
	 *
	 * @return void
	 */
	function uninstall($parent) 
	{
		
		$this->release = $parent->get( "manifest" )->version;
		
		echo '<p>' . JText::sprintf('COM_TRAVELBOOK_UNINSTALL', $this->release) . '</p>';
	}

	/**
	 * get a parameter
	 *
	 * @return string	the value of the parameter
	 */
	function getParam($name)
	{
		$db = JFactory::getDbo();
		$db->setQuery('SELECT manifest_cache FROM #__extensions WHERE name = "com_travelbook"');
		$manifest = json_decode($db->loadResult(), true);
		
		return $manifest[$name];
	}

	/**
	 * sets parameter values in the component's row of the extension table
	 *
	 * @return void
	 */
	function setParams($param_array)
	{
		if ( count($param_array) > 0 ) {
			// read the existing component value(s)
			$db = JFactory::getDbo();
			$db->setQuery('SELECT params FROM #__extensions WHERE name = "com_travelbook"');
			$params = json_decode($db->loadResult(), true);
			// add the new variable(s) to the existing one(s)
			foreach ($param_array as $name => $value) {
				$params[(string) $name] = $value;
			}
			
			if (isset($params) && is_array($params))
			{
				$registry = new JRegistry;
				$registry->loadArray($params);
				$paramsString = (string) $registry;
			}
						
			// store the combined new and existing values back as a JSON string
			$db->setQuery('UPDATE #__extensions SET params = ' .
			$db->quote( $paramsString ) .
				' WHERE name = "com_travelbook"' );
			$db->query();
		}
	}

	private function updateCategories()
	{
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
		$db->setQuery('INSERT INTO `#__categories` (`asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`) VALUES (0, 1, 0, 0, 1, \'per-day\', \'com_travelbook.calculation\', \'TB_PER_DAY\', \'per-day\', \'\', \'\', 1, 0, \'0000-00-00 00:00:00\', 1, \'{"category_layout":"","extra":""}\', \'\', \'\', \'{"author":"","robots":""}\', ' . (int) $user->id . ', \'0000-00-00 00:00:00\', 0, \'0000-00-00 00:00:00\', 0, \'*\');');
		$db->query();		
		$db->setQuery('INSERT INTO `#__categories` (`asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`) VALUES (0, 1, 0, 0, 1, \'per-person\', \'com_travelbook.calculation\', \'TB_PER_PERSON\', \'per-person\', \'\', \'\', 1, 0, \'0000-00-00 00:00:00\', 1, \'{"category_layout":"","extra":""}\', \'\', \'\', \'{"author":"","robots":""}\', ' . (int) $user->id . ', \'0000-00-00 00:00:00\', 0, \'0000-00-00 00:00:00\', 0, \'*\');');
		$db->query();		
		$db->setQuery('INSERT INTO `#__categories` (`asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`) VALUES (0, 1, 0, 0, 1, \'per-rom\', \'com_travelbook.calculation\', \'TB_PER_ROOM\', \'per-room\', \'\', \'\', 1, 0, \'0000-00-00 00:00:00\', 1, \'{"category_layout":"","extra":""}\', \'\', \'\', \'{"author":"","robots":""}\', ' . (int) $user->id . ', \'0000-00-00 00:00:00\', 0, \'0000-00-00 00:00:00\', 0, \'*\');');
		$db->query();		
		$db->setQuery('INSERT INTO `#__categories` (`asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`) VALUES (0, 1, 0, 0, 1, \'one-off\', \'com_travelbook.calculation\', \'TB_ONE_OFF\', \'one-off\', \'\', \'\', 1, 0, \'0000-00-00 00:00:00\', 1, \'{"category_layout":"","extra":""}\', \'\', \'\', \'{"author":"","robots":""}\', ' . (int) $user->id . ', \'0000-00-00 00:00:00\', 0, \'0000-00-00 00:00:00\', 0, \'*\');');
		$db->query();		
	}
}