<?php
/**
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 *
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_travelbook')) {
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

// Register helper class
JLoader::register('TravelbookHelper', dirname(__FILE__) . '/helpers/travelbook.php');
JLoader::register('TravelbookVersion', dirname(__FILE__) . '/helpers/version.php');

$controller = JControllerLegacy::getInstance('Travelbook');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
