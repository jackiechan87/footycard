ALTER TABLE `#__tb_extras` CHANGE COLUMN `pricing_plan` `pricing_plan` VARCHAR(255) NOT NULL DEFAULT '' AFTER `type`;
ALTER TABLE `#__tb_tours_extras` ADD `type` TINYINT(3) NOT NULL DEFAULT '0' AFTER `state`;
ALTER TABLE `#__tb_tours_extras` ADD `rate` DECIMAL(10,5) NOT NULL DEFAULT '0.00' AFTER `type`;
