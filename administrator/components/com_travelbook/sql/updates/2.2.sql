ALTER TABLE `#__tb_bookings` ADD `extras` VARCHAR(255) NULL DEFAULT NULL AFTER `attribs`;	
ALTER TABLE `#__tb_bookings` ADD `terms` VARCHAR(255) NULL DEFAULT NULL AFTER `extras`;	
ALTER TABLE `#__tb_dates` ADD `minimum` INT(2) UNSIGNED NOT NULL DEFAULT '0' AFTER `available`;	
