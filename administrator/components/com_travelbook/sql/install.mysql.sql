CREATE TABLE IF NOT EXISTS `#__tb_tours` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` VARCHAR(255) NOT NULL DEFAULT '',
  `title_alias` VARCHAR(255) NOT NULL DEFAULT '',
  `alias` VARCHAR(255) NOT NULL DEFAULT '',
  `subtitle` VARCHAR(255) NOT NULL DEFAULT '',
  `introtext` MEDIUMTEXT NOT NULL,
  `fulltext` MEDIUMTEXT NOT NULL,
  `reference` VARCHAR(15) NOT NULL DEFAULT '',
  `note` VARCHAR(255) NOT NULL DEFAULT '',  
  `mask` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `parentid` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `catid` INT(11) NOT NULL DEFAULT '0',
  `extras` TEXT NOT NULL,
  `urls` TEXT NOT NULL,
  `images` TEXT NOT NULL,
  `date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` TINYINT(3) NOT NULL DEFAULT '1',
  `ordering` INT(11) NOT NULL DEFAULT '0',
  `attribs` VARCHAR(5120) NULL DEFAULT NULL,
  `hits` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `version` INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `featured` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` CHAR(7) NOT NULL COMMENT 'The language code for the tour.',
  `created_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` VARCHAR(255) NOT NULL DEFAULT '',
  `modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `metadesc` TEXT NOT NULL,
  `metakey` TEXT NOT NULL,
  `metadata` TEXT NOT NULL,
  `xreference` VARCHAR(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `destination` VARCHAR(255) NULL DEFAULT NULL,
  `activity` VARCHAR(255) NULL DEFAULT NULL,
  `style` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_catid` (`catid`),
  INDEX `idx_access` (`access`),
  INDEX `idx_state` (`state`),
  INDEX `idx_createdby` (`created_by`),
  INDEX `idx_checkout` (`checked_out`),
  INDEX `idx_featured_catid` (`featured`, `catid`),
  INDEX `idx_language` (`language`)
)
COMMENT='TRAVELbook - Tours'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS `#__tb_tours_frontpage` (
  `tour_id` INT(11) NOT NULL DEFAULT '0',
  `ordering` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tour_id`)
)
COMMENT='TRAVELbook - Tours-Frontpage'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `#__tb_tours_rating` (
  `tour_id` INT(11) NOT NULL DEFAULT '0',
  `rating_sum` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `rating_count` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastip` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`tour_id`)
)
COMMENT='TRAVELbook - Tours-Rating'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `#__tb_dates_rating` (
  `date_id` INT(11) NOT NULL DEFAULT '0',
  `rating_sum` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `rating_count` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastip` VARCHAR(50) NOT NULL DEFAULT ''
)
COMMENT='TRAVELbook - Dates-Rating'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `#__tb_dates` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` VARCHAR(255) NOT NULL DEFAULT '',
  `title_alias` VARCHAR(255) NOT NULL DEFAULT '',
  `alias` VARCHAR(255) NOT NULL DEFAULT '',
  `departure` DATE NOT NULL DEFAULT '0000-00-00',
  `duration` SMALLINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `arrival` DATE NOT NULL DEFAULT '0000-00-00',
  `introtext` MEDIUMTEXT NOT NULL,
  `fulltext` MEDIUMTEXT NOT NULL,
  `reference` VARCHAR(15) NOT NULL DEFAULT '',
  `note` VARCHAR(255) NOT NULL DEFAULT '',  
  `catid` INT(11) NOT NULL DEFAULT '0',
  `extras` TEXT NOT NULL,
  `urls` TEXT NOT NULL,
  `images` TEXT NOT NULL,
  `TID` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` TINYINT(3) NOT NULL DEFAULT '1',
  `ordering` INT(11) NOT NULL DEFAULT '0',
  `attribs` VARCHAR(5120) NULL DEFAULT NULL,
  `hits` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `version` INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `featured` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` CHAR(7) NOT NULL COMMENT 'The language code for the tour.',
  `created_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` VARCHAR(255) NOT NULL DEFAULT '',
  `modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `metadesc` TEXT NOT NULL,
  `metakey` TEXT NOT NULL,
  `metadata` TEXT NOT NULL,
  `xreference` VARCHAR(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `checked_out` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `rack_rate` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
  `single_rate` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
  `child_rate` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
  `size` INT(2) UNSIGNED NOT NULL DEFAULT '0',
  `available` INT(2) UNSIGNED NOT NULL DEFAULT '0',
  `minimum` INT(2) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `idx_tour` (`TID`),
  INDEX `idx_access` (`access`),
  INDEX `idx_state` (`state`),
  INDEX `idx_createdby` (`created_by`),
  INDEX `idx_checkout` (`checked_out`),
  INDEX `idx_featured_tour` (`featured`, `TID`),
  INDEX `idx_language` (`language`)
)
COMMENT='TRAVELbook - Dates'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS `#__tb_dates_frontpage` (
  `date_id` INT(11) NOT NULL DEFAULT '0',
  `ordering` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`date_id`)
)
COMMENT='TRAVELbook - Dates-Frontpage'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `#__tb_details` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` VARCHAR(255) NOT NULL DEFAULT '',
  `introtext` MEDIUMTEXT NOT NULL,
  `fulltext` MEDIUMTEXT NOT NULL,
  `catid` INT(11) NOT NULL DEFAULT '0',
  `state` TINYINT(3) NOT NULL DEFAULT '1',
  `extras` TEXT NOT NULL,
  `urls` TEXT NOT NULL,
  `TID` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` INT(11) NOT NULL DEFAULT '0',
  `attribs` VARCHAR(5120) NULL DEFAULT NULL,
  `version` INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` CHAR(7) NOT NULL COMMENT 'The language code for the tour.',
  `created_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` VARCHAR(255) NOT NULL DEFAULT '',
  `modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `publish_up` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `checked_out` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `idx_tour` (`TID`),
  INDEX `idx_access` (`access`),
  INDEX `idx_state` (`state`),
  INDEX `idx_createdby` (`created_by`),
  INDEX `idx_checkout` (`checked_out`),
  INDEX `idx_language` (`language`)
)
COMMENT='TRAVELbook - Details'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS `#__tb_tours_frontpage` (
  `tour_id` INT(11) NOT NULL DEFAULT '0',
  `ordering` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tour_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DEFAULT;

CREATE TABLE IF NOT EXISTS `#__tb_tours_rating` (
  `tour_id` INT(11) NOT NULL DEFAULT '0',
  `rating_sum` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `rating_count` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastip` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`tour_id`) 
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DEFAULT;

CREATE TABLE IF NOT EXISTS `#__tb_details` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` VARCHAR(255) NOT NULL DEFAULT '',
  `title_alias` VARCHAR(255) NOT NULL DEFAULT '',
  `alias` VARCHAR(255) NOT NULL DEFAULT '',
  `TID` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `introtext` MEDIUMTEXT NOT NULL,
  `fulltext` MEDIUMTEXT NOT NULL,
  `mask` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `urls` TEXT NOT NULL,
  `parentid` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `catid` INT(11) NOT NULL DEFAULT '0',
  `date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `info` VARCHAR(255) NOT NULL DEFAULT '',
  `state` TINYINT(3) NOT NULL DEFAULT '1',
  `ordering` INT(11) NOT NULL DEFAULT '0',
  `attribs` VARCHAR(5120) NULL DEFAULT NULL,
  `hits` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `version` INT(11) UNSIGNED NOT NULL DEFAULT '1',
  `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `featured` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` CHAR(7) NOT NULL COMMENT 'The language code for the tour.',
  `created_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` VARCHAR(255) NOT NULL DEFAULT '',
  `modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `metadesc` TEXT NOT NULL,
  `metakey` TEXT NOT NULL,
  `metadata` TEXT NOT NULL,
  `publish_up` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  INDEX `idx_catid` (`catid`),
  INDEX `idx_access` (`access`),
  INDEX `idx_state` (`state`),
  INDEX `idx_createdby` (`created_by`),
  INDEX `idx_checkout` (`checked_out`),
  INDEX `idx_featured_catid` (`featured`, `catid`),
  INDEX `idx_language` (`language`)
)
COMMENT='TRAVELbook - Details'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS `#__tb_log_searches` (
  `search_type` VARCHAR(128) NOT NULL DEFAULT '',
  `search_term` VARCHAR(128) NOT NULL DEFAULT '',
  `hits` INT(10) UNSIGNED NOT NULL DEFAULT '0'
)
COMMENT='TRAVELbook - Search Logs'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `#__tb_bookings` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the qjomy_assets table.',
	`title` VARCHAR(255) NOT NULL DEFAULT '',
	`title_alias` VARCHAR(255) NOT NULL DEFAULT '',
	`alias` VARCHAR(255) NOT NULL DEFAULT '',
	`introtext` MEDIUMTEXT NOT NULL,
	`fulltext` MEDIUMTEXT NOT NULL,
	`catid` INT(11) NOT NULL DEFAULT '0',
	`CID` INT(11) NOT NULL DEFAULT '0',
	`DID` INT(11) NOT NULL DEFAULT '0',
	`date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`info` VARCHAR(255) NOT NULL DEFAULT '',
	`state` TINYINT(3) NOT NULL DEFAULT '1',
	`ordering` INT(11) NOT NULL DEFAULT '0',
	`attribs` VARCHAR(5120) NULL DEFAULT NULL,
	`extras` VARCHAR(255) NULL DEFAULT NULL,
	`terms` VARCHAR(255) NULL DEFAULT NULL,
	`version` INT(11) UNSIGNED NOT NULL DEFAULT '1',
	`created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`created_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`created_by_alias` VARCHAR(255) NOT NULL DEFAULT '',
	`modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`modified_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`access` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`checked_out` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`),
	INDEX `idx_state` (`state`),
	INDEX `idx_access` (`access`),
	INDEX `idx_created_by` (`created_by`),
	INDEX `idx_checked_out` (`checked_out`)
)
COMMENT='TRAVELbook - Bookings'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0;
	
CREATE TABLE IF NOT EXISTS `#__tb_clients` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
	`title` VARCHAR(255) NOT NULL DEFAULT '',
	`title_alias` VARCHAR(255) NOT NULL DEFAULT '',
	`alias` VARCHAR(255) NOT NULL DEFAULT '',
	`address` VARCHAR(255) NOT NULL DEFAULT '',
	`firstname` VARCHAR(255) NOT NULL DEFAULT '',
	`lastname` VARCHAR(255) NOT NULL DEFAULT '',
	`birthdate` DATE NOT NULL DEFAULT '0000-00-00',
	`street` VARCHAR(64) NOT NULL DEFAULT '',
	`street_number` VARCHAR(5) NOT NULL DEFAULT '',
	`postcode` VARCHAR(5) NOT NULL DEFAULT '',
	`city` VARCHAR(255) NOT NULL DEFAULT '',
	`country` VARCHAR(64) NOT NULL DEFAULT '',
	`mobile` VARCHAR(32) NOT NULL DEFAULT '',
	`phone` VARCHAR(32) NOT NULL DEFAULT '',
	`fax` VARCHAR(32) NOT NULL DEFAULT '',
	`email` VARCHAR(100) NOT NULL DEFAULT '',
	`mode_mail` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1',
	`remark` MEDIUMTEXT NOT NULL,
	`introtext` MEDIUMTEXT NOT NULL,
	`fulltext` MEDIUMTEXT NOT NULL,
	`catid` INT(11) NOT NULL DEFAULT '0',
	`date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`info` VARCHAR(255) NOT NULL DEFAULT '',
	`state` TINYINT(3) NOT NULL DEFAULT '1',
	`ordering` INT(11) NOT NULL DEFAULT '0',
	`attribs` VARCHAR(5120) NULL DEFAULT NULL,
	`version` INT(11) UNSIGNED NOT NULL DEFAULT '1',
	`created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`created_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`created_by_alias` VARCHAR(255) NOT NULL DEFAULT '',
	`modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`modified_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`access` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`checked_out` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`),
	INDEX `idx_state` (`state`),
	INDEX `idx_access` (`access`),
	INDEX `idx_created_by` (`created_by`),
	INDEX `idx_checked_out` (`checked_out`)
)
COMMENT='TRAVELbook - Clients'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS `#__tb_guests` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
	`title` VARCHAR(255) NOT NULL DEFAULT '',
	`title_alias` VARCHAR(255) NOT NULL DEFAULT '',
	`alias` VARCHAR(255) NOT NULL DEFAULT '',
	`book_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`address` VARCHAR(255) NOT NULL DEFAULT '',
	`firstname` VARCHAR(255) NOT NULL DEFAULT '',
	`lastname` VARCHAR(255) NOT NULL DEFAULT '',
	`date_of_birth` DATE NOT NULL DEFAULT '0000-00-00',
	`introtext` MEDIUMTEXT NOT NULL,
	`fulltext` MEDIUMTEXT NOT NULL,
	`catid` INT(11) NOT NULL DEFAULT '0',
	`passport` VARCHAR(32) NOT NULL DEFAULT '',
	`state` TINYINT(3) NOT NULL DEFAULT '1',
	`ordering` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`version` INT(11) UNSIGNED NOT NULL DEFAULT '1',
	`created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`created_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`created_by_alias` VARCHAR(255) NOT NULL DEFAULT '',
	`modified_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`publish_up` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`publish_down` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`access` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`checked_out` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`),
	INDEX `idx_state` (`state`),
	INDEX `idx_access` (`access`),
	INDEX `idx_created_by` (`created_by`),
	INDEX `idx_checked_out` (`checked_out`),
	INDEX `idx_book_id` (`book_id`)
)
COMMENT='TRAVELbook - Guests'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS `#__tb_emails` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
	`title` VARCHAR(255) NOT NULL DEFAULT '',
	`alias` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_bin',
	`introtext` MEDIUMTEXT NOT NULL,
	`fulltext` MEDIUMTEXT NOT NULL,
	`mailbody_html` MEDIUMTEXT NOT NULL,
	`mailbody_plain` MEDIUMTEXT NOT NULL,
	`css` MEDIUMTEXT NOT NULL,
	`from` VARCHAR(100) NOT NULL DEFAULT '',
	`from_name` VARCHAR(100) NOT NULL DEFAULT '',
	`reply_to` VARCHAR(100) NOT NULL DEFAULT '',
	`reply_to_name` VARCHAR(100) NOT NULL DEFAULT '',
	`subject` VARCHAR(100) NOT NULL DEFAULT '',
	`cc` VARCHAR(100) NULL DEFAULT NULL,
	`bcc` VARCHAR(100) NULL DEFAULT NULL,
	`state` TINYINT(3) NOT NULL DEFAULT '0',
	`sectionid` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`mask` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`catid` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`created_by` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`created_by_alias` VARCHAR(255) NOT NULL DEFAULT '',
	`modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`modified_by` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`checked_out` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`publish_up` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`publish_down` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`attribs` VARCHAR(5120) NOT NULL,
	`version` INT(10) UNSIGNED NOT NULL DEFAULT '1',
	`parentid` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`ordering` INT(11) NOT NULL DEFAULT '0',
	`access` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`hits` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`featured` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Set if email is featured.',
	`language` CHAR(7) NOT NULL COMMENT 'The language code for the email.',
	`xreference` VARCHAR(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
	PRIMARY KEY (`id`),
	INDEX `idx_access` (`access`),
	INDEX `idx_checkout` (`checked_out`),
	INDEX `idx_state` (`state`),
	INDEX `idx_catid` (`catid`),
	INDEX `idx_createdby` (`created_by`),
	INDEX `idx_language` (`language`),
	INDEX `idx_xreference` (`xreference`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS `#__tb_tours_extras` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`id_1` INT(11) UNSIGNED NOT NULL,
	`id_2` INT(11) UNSIGNED NOT NULL,
	`ordering` INT(3) UNSIGNED NOT NULL DEFAULT '0',
	`state` TINYINT(3) NOT NULL DEFAULT '1',
	`type` TINYINT(3) NOT NULL DEFAULT '0',
	`rate` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
	`version` INT(11) UNSIGNED NOT NULL DEFAULT '1',
	`created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`language` CHAR(7) NOT NULL COMMENT 'The language code for the tour.',
	`created_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`catid` INT(11) NOT NULL DEFAULT '0',
	`created_by_alias` VARCHAR(255) NOT NULL DEFAULT '',
	`modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`modified_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`publish_up` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`publish_down` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`access` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`checked_out` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`),
	INDEX `idx_id_1` (`id_1`),
	INDEX `idx_id_2` (`id_2`),
	INDEX `idx_catid` (`catid`),
	INDEX `idx_state` (`state`)
)
COMMENT='TRAVELbook - Tours-Extras'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS `#__tb_extras` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
	`title` VARCHAR(255) NOT NULL DEFAULT '',
	`alias` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_bin',
	`title_alias` VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'Deprecated in Joomla! 3.0' COLLATE 'utf8_bin',
	`introtext` MEDIUMTEXT NOT NULL,
	`fulltext` MEDIUMTEXT NOT NULL,
	`state` TINYINT(3) NOT NULL DEFAULT '0',
	`type` TINYINT(3) NOT NULL DEFAULT '0',
	`pricing_plan` VARCHAR(255) NOT NULL DEFAULT '',
	`rate` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
	`sectionid` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`mask` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`catid` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`created_by` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`created_by_alias` VARCHAR(255) NOT NULL DEFAULT '',
	`modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`modified_by` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`checked_out` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`publish_up` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`publish_down` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`images` TEXT NOT NULL,
	`urls` TEXT NOT NULL,
	`attribs` VARCHAR(5120) NOT NULL,
	`version` INT(10) UNSIGNED NOT NULL DEFAULT '1',
	`parentid` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`ordering` INT(11) NOT NULL DEFAULT '0',
	`metakey` TEXT NOT NULL,
	`metadesc` TEXT NOT NULL,
	`access` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`hits` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`metadata` TEXT NOT NULL,
	`featured` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Set if extra is featured.',
	`language` CHAR(7) NOT NULL COMMENT 'The language code for the extra.',
	`xreference` VARCHAR(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
	PRIMARY KEY (`id`),
	INDEX `idx_access` (`access`),
	INDEX `idx_checkout` (`checked_out`),
	INDEX `idx_state` (`state`),
	INDEX `idx_catid` (`catid`),
	INDEX `idx_createdby` (`created_by`),
	INDEX `idx_featured_catid` (`featured`, `catid`),
	INDEX `idx_language` (`language`),
	INDEX `idx_xreference` (`xreference`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0;
