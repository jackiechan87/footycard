<?php
/**
* @version 1.4.0
* @package RSform!Pro 1.4.0
* @copyright (C) 2007-2011 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/

defined('_JEXEC') or die('Restricted access');

JHTML::_('behavior.tooltip');
?>
<form action="index.php" method="post" name="adminForm">
    <table width="100%">
   		<tr>
      		<td width="50%" valign="top">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td valign="top">
					<table class="adminlist">
						<tr>
							<td>
								<div id="cpanel">
								<h3><?php echo JText::_('RSFP_MANAGE_FORMS'); ?></h3>
								<div style="float: left">
									<div class="icon hasTip" title="<?php echo JText::_('RSFP_MANAGE_FORMS'); ?>">
										<a href="index.php?option=com_rsform&amp;task=forms.manage">
											<?php echo JHTML::_('image', 'administrator/components/com_rsform/assets/images/forms.png', JText::_('RSFP_MANAGE_FORMS')); ?>
											<span><?php echo JText::_('RSFP_MANAGE_FORMS'); ?></span>
										</a>
									</div>
								</div>
								<div style="float: left">
									<div class="icon hasTip" title="<?php echo JText::_('RSFP_MANAGE_SUBMISSIONS'); ?>">
										<a href="index.php?option=com_rsform&amp;task=submissions.manage">
											<?php echo JHTML::_('image', 'administrator/components/com_rsform/assets/images/viewdata.png', JText::_('RSFP_MANAGE_SUBMISSIONS')); ?>
											<span><?php echo JText::_('RSFP_MANAGE_SUBMISSIONS'); ?></span>
										</a>
									</div>
								</div>
								<div style="float: left">
									<div class="icon hasTip" title="<?php echo JText::_('RSFP_BACKUP_RESTORE'); ?>">
										<a href="index.php?option=com_rsform&amp;task=backup.restore">
											<?php echo JHTML::_('image', 'administrator/components/com_rsform/assets/images/backup.png', JText::_('RSFP_BACKUP_RESTORE')); ?>
											<span><?php echo JText::_('RSFP_BACKUP_RESTORE'); ?></span>
										</a>
									</div>
								</div>
								<span class="clr"></span>
								<h3><?php echo JText::_('RSFP_CONFIGURATION'); ?></h3>
								<div style="float: left">
									<div class="icon hasTip" title="<?php echo JText::_('RSFP_CONFIGURATION'); ?>">
										<a href="index.php?option=com_rsform&amp;task=configuration.edit">
											<?php echo JHTML::_('image', 'administrator/components/com_rsform/assets/images/config.png', JText::_('RSFP_CONFIGURATION')); ?>
											<span><?php echo JText::_('RSFP_CONFIGURATION'); ?></span>
										</a>
									</div>
								</div>
								<div style="float: left">
									<div class="icon hasTip" title="<?php echo JText::_('RSFP_UPDATES'); ?>">
										<a href="index.php?option=com_rsform&amp;task=updates.manage">
											<?php echo JHTML::_('image', 'administrator/components/com_rsform/assets/images/restore.png', JText::_('RSFP_UPDATES')); ?>
											<span><?php echo JText::_('RSFP_UPDATES'); ?></span>
										</a>
									</div>
								</div>
								<div style="float: left">
									<div class="icon hasTip" title="<?php echo JText::_('RSFP_SUPPORT'); ?>">
										<a href="index.php?option=com_rsform&amp;task=goto.support">
											<?php echo JHTML::_('image', 'administrator/components/com_rsform/assets/images/support.png', JText::_('RSFP_SUPPORT')); ?>
											<span><?php echo JText::_('RSFP_SUPPORT'); ?></span>
										</a>
									</div>
								</div>
								<div style="float: left">
									<div class="icon hasTip" title="<?php echo JText::_('RSFP_PLUGINS'); ?>">
										<a href="index.php?option=com_rsform&amp;task=goto.plugins">
											<?php echo JHTML::_('image', 'administrator/components/com_rsform/assets/images/samples.png', JText::_('RSFP_PLUGINS')); ?>
											<span><?php echo JText::_('RSFP_PLUGINS'); ?></span>
										</a>
									</div>
								</div>
								</div>
							</td>
						</tr>
					</table>
				</td>
				</tr>
				</table>
			</td>
		   
		   </tr>
		</table>
	<input type="hidden" name="option" value="com_rsform" />
	<input type="hidden" name="task" value="configuration.edit" />
</form>