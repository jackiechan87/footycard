<?php
/**
 *
 * Controller for the front end User maintenance
 *
 * @package	VirtueMart
 * @subpackage User
 * @author Oscar van Eijk
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: user.php 6355 2012-08-20 09:23:27Z Milbo $
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Load the controller framework
jimport('joomla.application.component.controller');
JLoader::import('user', JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_users' . DS . 'models');
JLoader::import('registration', 'components' . DS . 'com_users' . DS . 'models');

/**
 * VirtueMart Component Controller
 *
 * @package		VirtueMart
 */
class VirtueMartControllerUser extends JController
{

	public function __construct()
	{
		parent::__construct();
		$this->useSSL = VmConfig::get('useSSL',0);
		$this->useXHTML = true;

	}


	function edit(){

	}

	function editAddressST(){

		$view = $this->getView('user', 'html');

		$view->setLayout('edit_address');

		$ftask ='saveAddressST';
		$view->assignRef('fTask', $ftask);
		// Display it all
		$view->display();

	}

	/**
	 * This is for use in the cart, it calls a standard template for editing user adresses. It sets the task following into the form
	 * of the template to saveCartUser, the task saveCartUser just sets the right redirect in the js save(). This is done just to have the
	 * controll flow in the controller and not in the layout. The layout is everytime calling a standard joomla task.
	 *
	 * @author Max Milbers
	 */

	function editAddressCart(){
            $user = JFactory::getUser();
            if ($user->guest) {
                $app = JFactory::getApplication();
                $app->redirect(JRoute::_(JURI::root() . 'index.php?option=com_users&view=login&Itemid=512'), JText::_('JGLOBAL_YOU_MUST_LOGIN_FIRST'));
            }

            
		$view = $this->getView('user', 'html');

		$view->setLayout('edit_address');

		$ftask ='savecartuser';
		$view->assignRef('fTask', $ftask);

		// Display it all
		$view->display();

	}

	function editInformation(){

		$app = JFactory::getApplication();
		$user = JFactory::getUser();

		if($user->guest) {
			$app->redirect(JRoute::_('index.php?option=com_users&view=login&Itemid=512', false));
		}

		$view = $this->getView('user', 'html');

		$view->setLayout('edit_information');

		$ftask ='updateinformationuser';
		$view->assignRef('fTask', $ftask);

		// Display it all
		$view->display();

	}

	function authenticateMyAccount(){

		$app = JFactory::getApplication();
		$user = JFactory::getUser();

		if($user->guest) {
			$app->redirect(JRoute::_('index.php?option=com_users&view=login&Itemid=512', false));
		}
		else
		{
			$app->redirect(JRoute::_('index.php?option=com_content&view=article&id=136&Itemid=663', false));
		}
	}

	function updateInformationUser(){
		$msg = $this->saveData(true);

		//We may add here the option for silent registration.
		$this->setRedirect(JRoute::_(JURI::root() . 'index.php?option=com_virtuemart&view=user&task=editinformation&Itemid='.$_REQUEST['Itemid'],$this->useXHTML,$this->useSSL), $msg );
	}

	function subcribeAccount(){

		$app = JFactory::getApplication();
		$user = JFactory::getUser();

		if($user->guest) {
			$app->redirect(JRoute::_('index.php?option=com_users&view=login&Itemid=512', false));
		}

		$view = $this->getView('user', 'html');

		$view->setLayout('edit_information');

		$type = 'subcribeAccount';
		$view->assignRef('typeView', $type);

		$ftask ='updateSubcribeAccount';
		$view->assignRef('fTask', $ftask);

		// Display it all
		$view->display();

	}

	function updateSubcribeAccount(){
		$msg = $this->saveData(true);

		//We may add here the option for silent registration.
		$this->setRedirect( JRoute::_(JURI::root() . 'index.php?option=com_virtuemart&view=user&task=subcribeAccount&Itemid='.$_REQUEST['Itemid'],$this->useXHTML,$this->useSSL), $msg );
	}

	/**
	 * This is for use in the checkout process, it is the same like editAddressCart, but it sets the save task
	 * to saveCheckoutUser, the task saveCheckoutUser just sets the right redirect. This is done just to have the
	 * controll flow in the controller and not in the layout. The layout is everytime calling a standard joomla task.
	 *
	 * @author Max Milbers
	 */
	function editAddressCheckout(){

		$view = $this->getView('user', 'html');

		$view->setLayout('edit_address');

		$ftask ='savecheckoutuser';
		$view->assignRef('fTask', $ftask);

		// Display it all
		$view->display();
	}

	/**
	 * This function is called from the layout edit_adress and just sets the right redirect back to the cart
	 * We use here the saveData(true) function, because within the cart shouldnt be done any registration.
	 *
	 * @author Max Milbers
	 */
	function saveCheckoutUser(){

		$msg = $this->saveData(true);

		//We may add here the option for silent registration.
		$this->setRedirect( JRoute::_('index.php?option=com_virtuemart&view=cart&task=checkout',$this->useXHTML,$this->useSSL), $msg );
	}

	function registerCheckoutUser(){
		$msg = $this->saveData(true,true);
		$this->setRedirect(JRoute::_( 'index.php?option=com_virtuemart&view=cart&task=checkout',$this->useXHTML,$this->useSSL ),$msg);
	}

	/**
	 * This function is called from the layout edit_adress and just sets the right redirect back to the cart.
	 * We use here the saveData(true) function, because within the cart shouldnt be done any registration.
	 *
	 * @author Max Milbers
	 */
	function saveCartUser(){

		$msg = $this->saveData(true);
		$app = JFactory::getApplication();
		if($_REQUEST['addrtype'] == "BT"){
			$app->redirect(JRoute::_( JURI::root() . 'index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=ST&virtuemart_user_id[]=0&Itemid='.$_GET['Itemid'],$this->useXHTML,$this->useSSL),$msg);
		}
		else{
			$app->redirect(JRoute::_( JURI::root() . 'index.php?view=cart&task=edit_shipment&option=com_virtuemart&Itemid='.$_GET['Itemid'],$this->useXHTML,$this->useSSL ),$msg);
		}
//		$this->setRedirect(JRoute::_( 'index.php?option=com_virtuemart&view=cart',$this->useXHTML,$this->useSSL ),$msg);
	}

	function registerCartuser(){
		$msg = $this->saveData(true, true);
		$this->setRedirect(JRoute::_('index.php?option=com_virtuemart&view=cart',$this->useXHTML,$this->useSSL) , $msg);
	}


	/**
	 * This is the save function for the normal user edit.php layout.
	 * We use here directly the userModel store function, because this view is for registering also
	 * it redirects to the standard user view.
	 *
	 * @author Max Milbers
	 */
	function saveUser(){

		$msg = $this->saveData(false,true);

		$layout = JRequest::getWord('layout','edit');
		//$this->setRedirect( JRoute::_('index.php?option=com_virtuemart&view=user&layout='.$layout), $msg );
		if($msg === false) {
			$this->setRedirect( JRoute::_('index.php?option=com_virtuemart&view=user&layout='.$layout), $msg );
		} else {
			$this->setRedirect( JRoute::_(JURI::root().'index.php?option=com_users&view=login&Itemid=512'), $msg );
		}
	}

	function saveAddressST(){

		$msg = $this->saveData(false,true);
		$layout = 'edit';// JRequest::getWord('layout','edit');
		$this->setRedirect( JRoute::_('index.php?option=com_virtuemart&view=user&layout='.$layout), $msg );

	}

	/**
	 * Save the user info. The saveData function don't use the userModel store function for anonymous shoppers, because it would register them.
	 * We make this function private, so we can do the tests in the tasks.
	 *
	 * @author Max Milbers
	 * @author Valérie Isaksen
	 *
	 * @param boolean Defaults to false, the param is for the userModel->store function, which needs it to determine how to handle the data.
	 * @return String it gives back the messages.
	 */
	private function saveData($cart=false,$register=false) {
		$mainframe = JFactory::getApplication();
		$currentUser = JFactory::getUser();
		$msg = '';

		$data = JRequest::get('post');

		$data['address_type'] = JRequest::getWord('addrtype','BT');
		if($currentUser->guest!=1 || $register){
			$this->addModelPath( JPATH_VM_ADMINISTRATOR.DS.'models' );
			$userModel = VmModel::getModel('user');

			if(!$cart){
				// Store multiple selectlist entries as a ; separated string
				if (key_exists('vendor_accepted_currencies', $data) && is_array($data['vendor_accepted_currencies'])) {
					$data['vendor_accepted_currencies'] = implode(',', $data['vendor_accepted_currencies']);
				}

				$data['vendor_store_name'] = JRequest::getVar('vendor_store_name','','post','STRING',JREQUEST_ALLOWHTML);
				$data['vendor_store_desc'] = JRequest::getVar('vendor_store_desc','','post','STRING',JREQUEST_ALLOWHTML);
				$data['vendor_terms_of_service'] = JRequest::getVar('vendor_terms_of_service','','post','STRING',JREQUEST_ALLOWHTML);
			}

			//It should always be stored
			$ret = $userModel->store($data);

			if($currentUser->guest==1){
				$msg = (is_array($ret)) ? $ret['message'] : $ret;
				$usersConfig = &JComponentHelper::getParams( 'com_users' );
				$useractivation = $usersConfig->get( 'useractivation' );
				if (is_array($ret) && $ret['success'] && !$useractivation) {
					// Username and password must be passed in an array
					$credentials = array('username' => $ret['user']->username,
			  					'password' => $ret['user']->password_clear
					);
					$return = $mainframe->login($credentials);
				}
			}

		}

		$this->saveToCart($data, $register);
		return $msg;
	}

	/**
	 * This function just gets the post data and put the data if there is any to the cart
	 *
	 * @author Max Milbers
	 */
	private function saveToCart($data, $register = false){

		if(!class_exists('VirtueMartCart')) require(JPATH_VM_SITE.DS.'helpers'.DS.'cart.php');

		//not need save session when register new user
		if(!$register) {
			$cart = VirtueMartCart::getCart();
			$cart->saveAddressInCart($data, $data['address_type']);
		}
	}

	/**
	 * Editing a user address was cancelled when called from the cart; return to the cart
	 *
	 * @author Oscar van Eijk
	 */
	function cancelCartUser(){

		$this->setRedirect( JRoute::_('index.php?option=com_virtuemart&view=cart')  );
	}

	/**
	 * Editing a user address was cancelled during chaeckout; return to the cart
	 *
	 * @author Oscar van Eijk
	 */
	function cancelCheckoutUser(){
		$this->setRedirect( JRoute::_('index.php?option=com_virtuemart&view=cart&task=checkout',$this->useXHTML,$this->useSSL) );
	}

	/**
	 * Action cancelled; return to the previous view
	 *
	 * @author Oscar van Eijk
	 */
	function cancel()
	{
		$return = JURI::base();
		$this->setRedirect( $return );
	}


	function removeAddressST(){
		$db = JFactory::getDBO();
		$currentUser = JFactory::getUser();
		$virtuemart_userinfo_id = JRequest::getVar('virtuemart_userinfo_id');

		//Lets do it dirty for now
		//$userModel = VmModel::getModel('user');
		$msg = '';
		if ( isset($virtuemart_userinfo_id) && $currentUser->id != 0 ) {
			//$userModel -> deleteAddressST();
			$q = 'DELETE FROM #__virtuemart_userinfos  WHERE virtuemart_user_id="'. $currentUser->id .'" AND virtuemart_userinfo_id="'. $virtuemart_userinfo_id .'"';
			$db->setQuery($q);
			$db->query();

			$msg = vmInfo('Address has been successfully deleted.');
		}
		$layout = JRequest::getWord('layout','edit');
		$this->setRedirect( JRoute::_('index.php?option=com_virtuemart&view=user&layout='.$layout), $msg );
	}
        
        /**
         * After register, user will be asked to pay registration fee.
         */
        public function registerConfirm()
        {
            if (empty($_GET['userId'])) {
                // Push message
                return;
            }
            
            // Check if user is already activated.
            
            if(!class_exists('CurrencyDisplay')) require(JPATH_VM_ADMINISTRATOR.DS.'helpers'.DS.'currencydisplay.php');
            $dispatcher = JDispatcher::getInstance();

            JPluginHelper::importPlugin('vmshipment');
            JPluginHelper::importPlugin('vmcustom');
            JPluginHelper::importPlugin('vmpayment');
            $returnValues = $dispatcher->trigger('plgVmConfirmedRegister', array($_GET['userId'], 29));
//            var_dump(array_keys($returnValues));
            echo $returnValues[0];
//            $view = $this->getView('user', 'html');
//
//            $ftask ='savecheckoutuser';
//            $view->assignRef('fTask', $ftask);
//            $view->registerConfirm();
        }
        
        
                
        public function activate()
        {
//            $modelUser = JFactory::getUser(210);
//            var_dump($modelUser);
//            $modelUser->setId(210);
//            $token = $modelUser->get('activation');
////            echo $token;
            $token = 'da9c3282982759ad78fab962308d3ea6';
            $model = new UsersModelRegistration();
            $return = $model->activate($token);
            var_dump($return);
        }
}
// No closing tag
