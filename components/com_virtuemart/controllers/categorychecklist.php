<?php
/**
*
* Description
*
* @package	VirtueMart
* @subpackage
* @author RolandD
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: category.php 5333 2012-01-28 23:57:11Z Milbo $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Load the controller framework
jimport('joomla.application.component.controller');

/**
* Class Description
*
* @package VirtueMart
* @author RolandD
*/
class VirtueMartControllerCategorychecklist extends JController {

    /**
    * Method Description
    *
    * @access public
    * @author RolandD
    */
    public function __construct() {
     	 parent::__construct();

     	 $this->registerTask('browse','categorychecklist');
         $this->registerTask('savechecklist','categorychecklist');
         $this->registerTask('editaddresscart','editaddresscart');
   	}

	/**
	* Function Description
	*
	* @author RolandD
	* @author George
	* @access public
	*/
	public function display() {
//            echo JURI::root(JRoute::_('index.php?option=com_virtuemart&virtuemart_category_id=65&Itemid=697');
            if (!empty($_POST) && !empty($_POST['checkListType'])) {
                if ($this->savechecklist() && isset($_POST['trade']) && $_POST['trade'] == 1) {
                    $app = JFactory::getApplication();
                    if (isset($_GET['Itemid']) && !empty($_GET['Itemid'])) {
                        // Case one page process
                        if ($_POST['checkListType'] == 'tradeList') {
                            // find trade list id
                            $url = $this->getTradeListUrl('checkList');
                        } else {
                            // From checklist page in one page process, redirect to tradingcart
                            $url = JURI::root() . 'index.php?option=com_virtuemart&view=tradingcart';
                        }
                    } else {
                        // Case in team page, redirect to step 1 in one page process.
                        $url = $this->getTradeListUrl('tradeList');
                    }
                    $app->redirect($url);
                    return;
                }
            }
            
            if (JRequest::getvar('search')) {
                    $view = $this->getView('categorychecklist', 'html');
                    $view->display();
            } else {
                    // Display it all
                    $safeurlparams = array('virtuemart_category_id'=>'INT','virtuemart_manufacturer_id'=>'INT','virtuemart_currency_id'=>'INT','return'=>'BASE64','lang'=>'CMD','orderby'=>'CMD','limitstart'=>'CMD','order'=>'CMD','limit'=>'CMD');
                    parent::display(true, $safeurlparams);
            }
	}
        
        public function cleanUpList($list)
        {
            if (empty($list) || !is_array($list)) {
                return array();
            }
            foreach ($list as $key => $teamInfo) {
                if (empty($teamInfo['chosenCards'])) {
                    unset($list[$key]);
                }
            }
            return $list;
        }
        
        public function getTradeListUrl($type = 'tradeList')
        {
            $categoryModel = VmModel::getModel('category');
            $categoryId = JRequest::getInt('virtuemart_category_id', false);
            $category = $categoryModel->getCategory($categoryId);
            $parentId = $category->parents[2]->virtuemart_category_id;
            $childs = $categoryModel->getChildCategoryList(1, $parentId);
            if (count($childs) == 2) {
                if ($type == 'checkList') {
                    $neededCategoryId = $childs[0]->virtuemart_category_id;
                } else {
                    $neededCategoryId = $childs[1]->virtuemart_category_id;
                }
                return JRoute::_( 'index.php?option=com_virtuemart&virtuemart_category_id=' . $neededCategoryId . '&Itemid=697');
            } else {
                return JRoute::_(JURI::root());
            }
        }
        
        public function displayallchecklist()
        {
            if (!empty($_POST) && !empty($_POST['checkListType'])) {
                $this->saveAllCheckList();
                if (isset($_POST['trade']) && $_POST['trade'] == 1) {
                    $app = JFactory::getApplication();
                    $categoryModel = VmModel::getModel('Category');
                    $defaultTradeListId = $categoryModel->getDefaultCheckListId('tradeList');
                    $app->redirect(JRoute::_( 'index.php?option=com_virtuemart&virtuemart_category_id='.$defaultTradeListId.'&Itemid=697'));
                    return;
                }
            }
            
            $view = $this->getView('categorychecklist', 'html');
            $view->assign('checkListType', 'checkList');
            $view->displayall();
        }
        
        public function displayalltradelist()
        {
            if (!empty($_POST) && !empty($_POST['checkListType'])) {
                $this->saveAllCheckList();
                if (isset($_POST['trade']) && $_POST['trade'] == 1) {
                    $app = JFactory::getApplication();
                    $categoryModel = VmModel::getModel('Category');
                    $defaultTradeListId = $categoryModel->getDefaultCheckListId('tradeList');
                    $app->redirect(JRoute::_( 'index.php?option=com_virtuemart&virtuemart_category_id='.$defaultTradeListId.'&Itemid=697'));
                    return;
                }
            }
            
            $view = $this->getView('categorychecklist', 'html');
            $view->assign('checkListType', 'tradeList');
            $view->displayall();
        }
        
        public function saveAllCheckList()
        {
            $type = $_POST['checkListType'] == 'checkList' ? 'checkList' : 'tradeList';
            $type = 'useless_' . $type; 
            
            if (!isset($_SESSION[$type]) || !is_array($_SESSION[$type])) {
                $_SESSION[$type] = array();
            }
            if (!isset($_POST['checked_list']) || !is_array($_POST['checked_list'])) {
                $cardIds = array();
            } else {
                $cardIds = $_POST['checked_list'];
            }
            $_SESSION[$type] = $cardIds;
            JFactory::getApplication()->enqueueMessage(JText::_('Your selection have been saved successfully'), 'message');
        }
        
        public function checkIfExistsId($id, $list)
        {
            if (empty($id) || empty($list)) {
                return false;
            }
            foreach ($list as $teamId => $teamData) {
                foreach ($teamData['chosenCards'] as $playerId => $cards) {
                    foreach ($cards as $productId) {
                        if ($id == $productId) {
                            return true;
                        }
                    }
                }
            }
            
            return false;
        }
        
        public function checkProductExistLogic($postList, $postType)
        {
            if (empty($postList)) {
                return true;
            }
            if ($postType != 'checkList' && $postType != 'tradeList') {
                return true;
            }
            $sessionListType = $postType == 'checkList' ? 'tradeList' : 'checkList';
            if (!isset($_SESSION[$sessionListType]) || empty($_SESSION[$sessionListType])) {
                return true;
            }
            $modelProduct = VmModel::getModel('product');
            foreach ($postList as $playerId => $cards) {
                foreach ($cards as $productId) {
                    if ($this->checkIfExistsId($productId, $_SESSION[$sessionListType])) {
                        $productData = $modelProduct->getProduct($productId);
                        if (!empty($productData)) { 
                            JFactory::getApplication()->enqueueMessage(JText::_('Your selection have not been saved. Product ' . $productData->product_name . ' has been used in both lists.'), 'message');
                        } else {
                            JFactory::getApplication()->enqueueMessage(JText::_('Your selection have not been saved. Some products have been used in both lists.'), 'message');
                        }
                        return false;
                    }
                }
            }
            return true;
        }
        
        public function savechecklist() {
            $type = $_POST['checkListType'] == 'checkList' ? 'checkList' : 'tradeList';
            if(!isset($_REQUEST['Itemid']) || $_REQUEST['Itemid'] === '') {
                $type = 'useless_' . $type;
            }
            if (!isset($_SESSION[$type]) || !is_array($_SESSION[$type])) {
                $_SESSION[$type] = array();
            }
            $cateId = $_POST['categoryId'];
            if (!isset($_POST['checked_list']) || !is_array($_POST['checked_list'])) {
                $cardIds = array();
            } else {
                $cardIds = $_POST['checked_list'];
            }
            if (!$this->checkProductExistLogic($cardIds, $type)) {
                // cardIds in post list exists in another list.
                return false;
            }
            $_SESSION[$type][$cateId] = array(
                'imageUrl' => $_POST['categoryImageUrl'],
                'chosenCards' => $cardIds
            );
            $_SESSION[$type] = $this->cleanUpList($_SESSION[$type]);
            JFactory::getApplication()->enqueueMessage(JText::_('Your selection have been saved successfully'), 'message');
            return true;
        } 
        
        public function test() {
            $view = $this->getView('tradingcart', 'html');
            $checkList = $_SESSION['checkList'];
            $tradeList = $_SESSION['tradeList'];
            $checkListQuantity = $_SESSION['checkListQuantity'];
            $tradeListQuantity = $_SESSION['tradeListQuantity'];
            $isValid = $view->checkQuantity($checkList, $tradeList, $checkListQuantity, $tradeListQuantity);
            echo 'valid: ';
            var_dump($isValid);
            echo '<pre>';
            var_dump($_SESSION['checkList']);
            var_dump($_SESSION['tradeList']);
            var_dump($_SESSION['checkListQuantity']);
            var_dump($_SESSION['tradeListQuantity']);
            die();
        }

    public function editaddresscart()
    {
        if (!empty($_POST) && !empty($_POST['checkListType'])) {
        }

        $view = $this->getView('user', 'html');

        $view->setLayout('onepage_edit_address');

        $ftask ='savecartuser';
        $view->assignRef('fTask', $ftask);

        // Display it all
        $view->display();
    }

    /**
     * For selecting shipment, opens a new layout
     *
     * @author Max Milbers
     */
    public function edit_shipment() {


        $view = $this->getView('cart', 'html');
        $view->setLayout('onepage_select_shipment');

        // Display it all
        $view->display();
    }

}
// pure php no closing tag
