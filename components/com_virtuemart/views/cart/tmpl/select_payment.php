<?php
/**
 *
 * Layout for the payment selection
 *
 * @package	VirtueMart
 * @subpackage Cart
 * @author Max Milbers
 *
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: select_payment.php 5451 2012-02-15 22:40:08Z alatak $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
$step = 1;
?>

<?php
if (VmConfig::get('oncheckout_show_steps', 1)) {
    echo '<div class="checkoutStep" id="checkoutStep3">' . JText::_('COM_VIRTUEMART_USER_FORM_CART_STEP3') . '</div>';
}
?>
<form method="post" id="paymentForm" name="choosePaymentRate" action="" class="form-validate payment">
<?php
	echo "<h1 class='title-page-checkout-detail logined'><span><span>".JText::_('COM_VIRTUEMART_CART_SELECT_PAYMENT')."</span></span></h1>";
	if($this->cart->getInCheckOut()){
		$buttonclass = 'button vm-button-correct';
	} else {
		$buttonclass = 'default';
	}
?>
	<?php
	$cart = VirtueMartCart::getCart();
	if($cart->isTrading()){
		?>
		<div class="width90 floatleft">
			<a class="details step-cart"
			   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=65&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
				<?php echo JText::_('Step ') . $step . ' : '. JText::_('COM_VIRTUEMART_CART_YOU_WANT'); ?><span
					class="floatright"><?php echo JText::_('Modify'); ?>>></span>
			</a>
			<?php $step++;?>
		</div>

		<div class="width90 floatleft">

			<a class="details step-cart"
			   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=66&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
				<?php echo JText::_('Step ') . $step . ' : '. JText::_('COM_VIRTUEMART_CART_YOU_HAVE'); ?><span
					class="floatright"><?php echo JText::_('Modify'); ?>>></span>
			</a>
			<?php $step++;?>
		</div>

		<div class="width90 floatleft">

			<a class="details step-cart"
			   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=tradingcart', $this->useXHTML, $this->useSSL) ?>">
				<?php echo JText::_('Step ') . $step . ' : '. JText::_('REVIEW_YOUR_TRADE'); ?><span
					class="floatright"><?php echo JText::_('Modify'); ?>>></span>
			</a>
			<?php $step++;?>
		</div>
	<?php
	}
	?>
	<div class="width90 floatleft">

		<a class="details step-cart" href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=BT',$this->useXHTML,$this->useSSL) ?>">
			<?php echo JText::_('Step '). $step . ' : '.JText::_('COM_VIRTUEMART_USER_FORM_EDIT_BILLTO_LBL'); ?><span class="floatright"><?php echo JText::_('Modify');?>>></span>
		</a>
		<?php $step++;?>
		<input type="hidden" name="billto" value="<?php echo $this->cart->lists['billTo']; ?>"/>
	</div>

	<div class="width90 floatleft">
		<?php if(!isset($this->cart->lists['current_id'])) $this->cart->lists['current_id'] = 0; ?>
		<a class="details step-cart" href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=ST&virtuemart_user_id[]='.$this->cart->lists['current_id'],$this->useXHTML,$this->useSSL) ?>">
			<?php echo JText::_('Step ').$step . ' : '.JText::_('COM_VIRTUEMART_USER_FORM_ADD_SHIPTO_LBL'); ?><span class="floatright"><?php echo JText::_('Modify');?>>></span>
		</a>
		<?php $step++;?>
	</div>

	<div class="width90 floatleft">

		<?php if(!isset($this->cart->lists['current_id'])) $this->cart->lists['current_id'] = 0; ?>
		<a class="details step-cart" href="<?php echo JRoute::_('index.php?view=cart&task=edit_shipment',$this->useXHTML,$this->useSSL); ?>">
			<?php echo JText::_('Step ').$step . ' : '.JText::_('Delivery method'); ?><span class="floatright"><?php echo JText::_('Modify');?>>></span>
		</a>
		<?php $step++;?>
	</div>

	<div class="width90 floatleft">

		<?php if(!isset($this->cart->lists['current_id'])) $this->cart->lists['current_id'] = 0; ?>
		<a class="details step-cart" onclick="return false;" href="<?php echo JRoute::_('index.php?view=cart&task=editpayment',$this->useXHTML,$this->useSSL) ?>">
			<?php echo JText::_('Step ').$step . ' : '.JText::_('Payment method'); ?><span class="floatright"><?php echo JText::_('Modify');?>>></span>
		</a>
		<?php $step++;?>
	</div>
<?php
     if ($this->found_payment_method) {


    echo "<fieldset class='width90'>";
		foreach ($this->paymentplugins_payments as $paymentplugin_payments) {
		    if (is_array($paymentplugin_payments)) {
			foreach ($paymentplugin_payments as $paymentplugin_payment) {
                            $cart = VirtueMartCart::getCart();
                            if($cart->isFreeShipment()){ 
                                if (strpos($paymentplugin_payment, 'payment_id_3')) {
                                    echo $paymentplugin_payment.'<div style="clear:both" ></div>';
                                } else {
                                    $disabledPaymentRate = str_replace('name="virtuemart_paymentmethod_id"', 'name="virtuemart_paymentmethod_id" disabled="disabled"', $paymentplugin_payment);
                                    echo $disabledPaymentRate."<br />\n";
                                }
                            } else {
                                if (strpos($paymentplugin_payment, 'payment_id_3')) {
                                    $disabledPaymentRate = str_replace('name="virtuemart_paymentmethod_id"', 'name="virtuemart_paymentmethod_id" disabled="disabled"', $paymentplugin_payment);
                                    echo $disabledPaymentRate."<br />\n";
                                } else {
                                    echo $paymentplugin_payment.'<div style="clear:both" ></div>';
                                }
                            }
//			    echo $paymentplugin_payment.'<div style="clear:both" ></div>';
			}
		    }
		}
    echo "</fieldset>";
?>
		 <div class="width90 floatleft div-payment-method">
			 <div class="buttonBar-right">
				 <button class="<?php echo $buttonclass ?>" type="submit"><?php echo JText::_('COM_VIRTUEMART_SAVE'); ?></button>
				 <button class="<?php echo $buttonclass ?>" type="reset" onClick="if (confirm('Your cart will be cleared. Do you want to continue?')) {window.location.href='<?php echo JRoute::_('index.php?option=com_virtuemart&view=cart&task=cancel'); ?>'}" ><?php echo JText::_('COM_VIRTUEMART_CANCEL'); ?></button>
                                 <button class="<?php echo $buttonclass ?>" type="submit"><?php echo JText::_('Next'); ?></button>
			 </div>
		 </div>
	 <?php
    } else {
	 echo "<h1>".$this->payment_not_found_text."</h1>";
    }


    ?>

    <input type="hidden" name="option" value="com_virtuemart" />
    <input type="hidden" name="view" value="cart" />
    <input type="hidden" name="task" value="setpayment" />
    <input type="hidden" name="controller" value="cart" />
</form>

<div class="width90 floatleft">

	<a class="details step-cart" href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=cart',$this->useXHTML,$this->useSSL); ?>">
		<?php echo JText::_('Step ').$step . ' : '.JText::_('Confirm order'); ?><span class="floatright"><?php echo JText::_('Modify');?>>></span>
	</a>

</div>

<div class="clear"></div>
