<?php
/**
 *
 * Template for the shipment selection
 *
 * @package	VirtueMart
 * @subpackage Cart
 * @author Max Milbers
 *
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: cart.php 2400 2010-05-11 19:30:47Z milbo $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
$step = 1;
?>


<?php
if (VmConfig::get('oncheckout_show_steps', 1)) {
    echo '<div class="checkoutStep" id="checkoutStep2">' . JText::_('COM_VIRTUEMART_USER_FORM_CART_STEP2') . '</div>';
}
?>
<form method="post" id="userForm" name="chooseShipmentRate" action="" class="form-validate">
<?php
	echo "<h1 class='title-page-checkout-detail logined'><span><span>".JText::_('COM_VIRTUEMART_CART_SELECT_SHIPMENT')."</span></span></h1>";
	if($this->cart->getInCheckOut()){
		$buttonclass = 'button vm-button-correct';
	} else {
		$buttonclass = 'default';
	}
	?>

	<?php
        $cart = VirtueMartCart::getCart();
	if($cart->isTrading()){
		?>
		<div class="width90 floatleft">
			<a class="details step-cart"
			   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=65&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
				<?php echo JText::_('Step ') . $step . ' : '. JText::_('COM_VIRTUEMART_CART_YOU_WANT'); ?><span
					class="floatright"><?php echo JText::_('Modify'); ?>>></span>
			</a>
			<?php $step++;?>
		</div>

		<div class="width90 floatleft">

			<a class="details step-cart"
			   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=66&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
				<?php echo JText::_('Step ') . $step . ' : '. JText::_('COM_VIRTUEMART_CART_YOU_HAVE'); ?><span
					class="floatright"><?php echo JText::_('Modify'); ?>>></span>
			</a>
			<?php $step++;?>
		</div>

		<div class="width90 floatleft">

			<a class="details step-cart"
			   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=tradingcart', $this->useXHTML, $this->useSSL) ?>">
				<?php echo JText::_('Step ') . $step . ' : '. JText::_('REVIEW_YOUR_TRADE'); ?><span
					class="floatright"><?php echo JText::_('Modify'); ?>>></span>
			</a>
			<?php $step++;?>
		</div>
	<?php
	}
	?>
	<div class="width90 floatleft">

		<a class="details step-cart" href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=BT',$this->useXHTML,$this->useSSL) ?>">
			<?php echo JText::_('Step ').$step . ' : '.JText::_('COM_VIRTUEMART_USER_FORM_EDIT_BILLTO_LBL'); ?><span class="floatright"><?php echo JText::_('Modify');?>>></span>
		</a>
		<?php $step++;?>
		<input type="hidden" name="billto" value="<?php echo $this->cart->lists['billTo']; ?>"/>
	</div>

	<div class="width90 floatleft">
		<?php if(!isset($this->cart->lists['current_id'])) $this->cart->lists['current_id'] = 0; ?>
		<a class="details step-cart" href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=ST&virtuemart_user_id[]='.$this->cart->lists['current_id'],$this->useXHTML,$this->useSSL) ?>">
			<?php echo JText::_('Step ').$step . ' : '.JText::_('COM_VIRTUEMART_USER_FORM_ADD_SHIPTO_LBL'); ?><span class="floatright"><?php echo JText::_('Modify');?>>></span>
		</a>
		<?php $step++;?>
	</div>

	<div class="width90 floatleft">

		<?php if(!isset($this->cart->lists['current_id'])) $this->cart->lists['current_id'] = 0; ?>
		<a class="details step-cart" onclick="return false;" href="<?php echo JRoute::_('index.php?view=cart&task=edit_shipment',$this->useXHTML,$this->useSSL); ?>">
			<?php echo JText::_('Step ').$step . ' : '.JText::_('Delivery method'); ?><span class="floatright"><?php echo JText::_('Modify');?>>></span>
		</a>
		<?php $step++;?>
	</div>
<?php
    if ($this->found_shipment_method) {

            
	   echo "<fieldset class='width90'>\n";
            $orderModel = VmModel::getModel('orders');
            $user = JFactory::getUser();
            $totalTrades = $orderModel->getTotalTradingOrder($user->id);
	// if only one Shipment , should be checked by default
	    foreach ($this->shipments_shipment_rates as $shipment_shipment_rates) {
		if (is_array($shipment_shipment_rates)) {
		    foreach ($shipment_shipment_rates as $shipment_shipment_rate) {
                        if ($cart->isFirstTrade()) {
                            if (strpos($shipment_shipment_rate, 'shipment_id_4')) {
                                echo $shipment_shipment_rate."<br />\n";
                            } else { 
                                // disable Aus shipment
                                $disabledShipmentRate = str_replace('name="virtuemart_shipmentmethod_id"', 'name="virtuemart_shipmentmethod_id" disabled="disabled"', $shipment_shipment_rate);
                                echo $disabledShipmentRate."<br />\n";
                            }
                        } elseif ($cart->isTrading() && $totalTrades < 1) {
                            // show free shipment
                            if (strpos($shipment_shipment_rate, 'shipment_id_4')) {
                                continue;
                            } elseif (strpos($shipment_shipment_rate, 'shipment_id_5')) {
                                echo $shipment_shipment_rate."<br />\n";
                            } else { 
                                // disable Aus shipment
								// Jackie change process : user can choose free membership or not
//                                $disabledShipmentRate = str_replace('name="virtuemart_shipmentmethod_id"', 'name="virtuemart_shipmentmethod_id" disabled="disabled"', $shipment_shipment_rate);
//                                echo $disabledShipmentRate."<br />\n";
								echo $shipment_shipment_rate."<br />\n";
                            }
                        } else {
                            if (strpos($shipment_shipment_rate, 'shipment_id_4')) {
                                continue;
                            } elseif (strpos($shipment_shipment_rate, 'shipment_id_5')) {
                                // disable free shipment
                                $disabledShipmentRate = str_replace('name="virtuemart_shipmentmethod_id"', 'name="virtuemart_shipmentmethod_id" disabled="disabled"', $shipment_shipment_rate);
                                echo $disabledShipmentRate."<br />\n";
                            } else {
                                // show Aus shipment
                                echo $shipment_shipment_rate."<br />\n";
                            }
                        }
//			echo $shipment_shipment_rate."<br />\n";
		    }
		}
	    }
	    echo "</fieldset>\n";
		?>
		<div class="width90 floatleft">
                    <div class="buttonBar-right">

                        <button class="<?php echo $buttonclass ?>" type="submit" ><?php echo JText::_('COM_VIRTUEMART_SAVE'); ?></button>  &nbsp;
                        <button class="<?php echo $buttonclass ?>" type="reset" onClick="if (confirm('Your cart will be cleared. Do you want to continue?')) {window.location.href='<?php echo JRoute::_('index.php?option=com_virtuemart&view=cart&task=cancel'); ?>'}" ><?php echo JText::_('COM_VIRTUEMART_CANCEL'); ?></button>
                        <button class="<?php echo $buttonclass ?>" type="submit" ><?php echo JText::_('Next'); ?></button>  &nbsp;
                    </div>
		</div>
	<?php
    } else {
	 echo "<h1>".$this->shipment_not_found_text."</h1>";
    }

    ?>

    <input type="hidden" name="option" value="com_virtuemart" />
    <input type="hidden" name="view" value="cart" />
    <input type="hidden" name="task" value="setshipment" />
    <input type="hidden" name="controller" value="cart" />
</form>

<div class="width90 floatleft">

	<?php if(!isset($this->cart->lists['current_id'])) $this->cart->lists['current_id'] = 0; ?>
	<a class="details step-cart" href="<?php echo JRoute::_('index.php?view=cart&task=editpayment',$this->useXHTML,$this->useSSL) ?>">
		<?php echo JText::_('Step ').$step . ' : '.JText::_('Payment method'); ?><span class="floatright"><?php echo JText::_('Modify');?>>></span>
	</a>
	<?php $step++;?>
</div>

<div class="width90 floatleft">

	<a class="details step-cart" href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=cart',$this->useXHTML,$this->useSSL); ?>">
		<?php echo JText::_('Step ').$step . ' : '.JText::_('Confirm order'); ?><span class="floatright"><?php echo JText::_('Modify');?>>></span>
	</a>

</div>

<div class="clear"></div>
