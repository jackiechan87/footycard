<?php

/**
 *
 * Handle the tradesummary view
 *
 * @package	VirtueMart
 * @subpackage
 * @author RolandD
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: view.html.php 6357 2012-08-20 19:44:34Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Load the view framework
if (!class_exists('VmView'))
    require(JPATH_VM_SITE . DS . 'helpers' . DS . 'vmview.php');

/**
 * Handle the tradesummary view
 *
 * @package VirtueMart
 * @author RolandD
 * @todo set meta data
 * @todo add full path to breadcrumb
 */
class VirtuemartViewTradingcart extends VmView {

    public function display($tpl = null) {
        $checkList = is_array($_SESSION['checkList']) ? $_SESSION['checkList'] : array();
        $tradeList = is_array($_SESSION['tradeList']) ? $_SESSION['tradeList'] : array();
        $productId = $this->getTradingFreeProductId();
        $this->assign('tradingFreeProductId', $productId);
        if (!empty($_POST)) {
            // This is an "update" request
            // Get data from $_POST data and update it to session
            $checkListQuantity = $_POST['checkListQuantity'];
            $tradeListQuantity = $_POST['tradeListQuantity'];
            if (isset($_POST['removeCheckCards']) && is_array($_POST['removeCheckCards'])) {
                // Update check list
//                var_dump($_POST['removeCheckCards']); die();
                $checkList = $this->removeCardsFromList($checkList, $_POST['removeCheckCards']);
                foreach ($_POST['removeCheckCards'] as $cardId) {
                    if (array_key_exists($cardId, $checkListQuantity)) {
                        unset($checkListQuantity[$cardId]);
                    }
                }
            }
            if (isset($_POST['removeTradeCards']) && is_array($_POST['removeTradeCards'])) {
                // Update trade list
//                var_dump($_POST['removeCheckCards']); die();
                $tradeList = $this->removeCardsFromList($tradeList, $_POST['removeTradeCards']);
                foreach ($_POST['removeTradeCards'] as $cardId) {
                    if (array_key_exists($cardId, $tradeListQuantity)) {
                        unset($tradeListQuantity[$cardId]);
                    }
                }
            }
            $_SESSION['checkList'] = $checkList;
            $_SESSION['tradeList'] = $tradeList;
            $_SESSION['checkListQuantity'] = $checkListQuantity;
            $_SESSION['tradeListQuantity'] = $tradeListQuantity;
            
            if (isset($_POST['continueTrading'])) {
                // Go to home page
                $app = JFactory::getApplication();
                $app->redirect(JRoute::_(JURI::root() . 'index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=66&Itemid=697'));
                return;
            }
            if (isset($_POST['checkout'])) {
                // Check card quantity
                $isValidQuantity = $this->checkQuantity($checkList, $tradeList, $checkListQuantity, $tradeListQuantity);
//                var_dump($isValidQuantity);
//                die(); 
                if ($isValidQuantity) {
                    // Go to summary page
                    $app = JFactory::getApplication();
                    $app->redirect(JRoute::_(JURI::root() . 'index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=BT&Itemid=697'));
                    return;
                }
            }
        } else {
            // Load data from session
            $checkListQuantity = is_array($_SESSION['checkListQuantity']) ? $_SESSION['checkListQuantity'] : array();
            $tradeListQuantity = is_array($_SESSION['tradeListQuantity']) ? $_SESSION['tradeListQuantity'] : array();
        }
        $page_title = JText::_('COM_VIRTUEMART_TITLE_PAGE_ONE_TRADE');
        $this->assignRef('page_title', $page_title);
        $this->assign('checkList', $checkList);
        $this->assign('tradeList', $tradeList);
        $this->assign('checkListQuantity', $checkListQuantity);
        $this->assign('tradeListQuantity', $tradeListQuantity);
        
        parent::display();
    }

    public function removeCardsFromList($data, $cardIds)
    {
        $result = array();
        foreach ($data as $teamId => $teamData) {
            $newTeamData = $this->removeCardsFromTeam($teamData, $cardIds);
            if (!empty($newTeamData['chosenCards'])) {
                $result[$teamId] = $newTeamData;
            }
        }
        return $result;
    }
    public function removeCardsFromTeam($data, $cardIds)
    {
        $result = array(
            'imageUrl' => $data['imageUrl'],
            'chosenCards' => array(),
        );
        foreach ($data['chosenCards'] as $playerId => $playerData) {
            $newPlayerData = $this->removeCardsFromPlayer($playerData, $cardIds);
            if (!empty($newPlayerData)) {
                $result['chosenCards'][$playerId] = $newPlayerData;
            }
        }
        return $result;
    }
    
    public function removeCardsFromPlayer($data, $cardIds)
    {
        $result = array();
        foreach ($data as $key => $cardId) {
            if (in_array($cardId, $cardIds)) {
                continue;
            }
            $result[$key] = $cardId;
        }
        return $result;
    }
    
    public function countCardTypeQuantity($list, $quantityList, $type)
    {
        $result = 0;
        foreach ($list as $teamId => $teamData) {
            foreach ($teamData['chosenCards'] as $playerId => $cards) {
                if (!array_key_exists($type, $cards)) {
                    continue;
                }
                $cardId = $cards[$type];
                if (!array_key_exists($cardId, $quantityList)) {
                    continue;
                }
                $result += $quantityList[$cardId];
            }
        }
        
        return $result;
    }
    
    public function countSpecialtyQuantity($list, $quantityList)
    {
        $result = 0;
        foreach ($list as $teamId => $teamData) {
            if (!array_key_exists('specialty', $teamData['chosenCards'])) {
                continue;
            }
            foreach ($teamData['chosenCards']['specialty'] as $cardId) {
                if (!array_key_exists($cardId, $quantityList)) {
                    continue;
                }
                $result += $quantityList[$cardId];
            }
        }
        
        return $result;
    }
    
    public function checkQuantity($checkList, $tradeList, $checkListQuantity, $tradeListQuantity)
    {
        if (empty($checkList) || empty($tradeList) || empty($checkListQuantity) || empty($tradeListQuantity)) {
            return false;
        }
        
        $totalCheckListCommon = $this->countCardTypeQuantity($checkList, $checkListQuantity, 'C');
        $totalTradeListCommon = $this->countCardTypeQuantity($tradeList, $tradeListQuantity, 'C');
        $totalCheckListGold = $this->countCardTypeQuantity($checkList, $checkListQuantity, 'G');
        $totalTradeListGold = $this->countCardTypeQuantity($tradeList, $tradeListQuantity, 'G');
//        echo 'c c: ' . $totalCheckListCommon . '<br/>';
//        echo 't c: ' . $totalTradeListCommon . '<br/>';
//        echo 'c g: ' . $totalCheckListGold . '<br/>';
//        echo 't g: ' . $totalTradeListGold . '<br/>';
        $totalCheckListSpecial = $this->countSpecialtyQuantity($checkList, $checkListQuantity);
        $totalTradeListSpecial = $this->countSpecialtyQuantity($tradeList, $tradeListQuantity);
//        echo 'c sp: ' . $totalCheckListSpecial . '<br/>';
//        echo 't sp: ' . $totalTradeListSpecial . '<br/>';
        
        if ($totalTradeListCommon + $totalTradeListGold + $totalTradeListSpecial > 10) {
            JFactory::getApplication()->enqueueMessage(JText::_('MAX_TRADE_CARD_ALLOWED'), 'error');
            return false;
        }
        
        $result = true;
        $delta = $totalTradeListCommon - $totalCheckListCommon;
        if ($delta != 0) {
            $result = false;
            $this->getMessage($delta, $totalTradeListCommon, 'common');
        }
        $delta = $totalTradeListGold * 2 - $totalCheckListGold;
        if ($delta != 0) {
            $result = false;
            $this->getMessage($delta, $totalTradeListGold, 'gold');
        }
        $delta = $totalTradeListSpecial * 2 - $totalCheckListSpecial;
        if ($delta != 0) {
            $result = false;
            $this->getMessage($delta, $totalTradeListSpecial, 'specialty');
        }
        
        // Check specialty type.
        if (!$this->checkSpecialtyCards($tradeList, $checkList)) {
            $result = false;
        }
        
        return $result;
    }
    
    public function checkSpecialtyCards($tradeList, $checkList) 
    {
        // 
        $tradeListSpec = $this->getSpecialtyProductInfo($tradeList);
//        var_dump($tradeListSpec); die();
        if (empty($tradeListSpec)) {
            return true;
        }
        $checkListSpec = $this->getSpecialtyProductInfo($checkList);
        $result = true;
        $messages = array();
        foreach ($tradeListSpec as $type => $list) {
            if (!array_key_exists($type, $checkListSpec)) {
                JFactory::getApplication()->enqueueMessage(JText::_('You must select 2 "'.$type.'" cards from "Cards I have to Trade" to trade 1 "'.$type.'" that you want.'), 'error');
                $result = false;
                continue;
            }
            if ((2 * count($list)) != count($checkListSpec[$type])) {
                JFactory::getApplication()->enqueueMessage(JText::_('You must select 2 "'.$type.'" cards from "Cards I have to Trade" to trade 1 "'.$type.'" that you want.'), 'error');
                $result = false;
            }
        }
        return $result;
    }
    
    public function getSpecialtyProductInfo($list)
    {
        $result = array();
        $modelProduct = VmModel::getModel('product');
        foreach ($list as $teamId => $teamData) {
            if (!isset($teamData['chosenCards']['specialty'])
                    || empty($teamData['chosenCards']['specialty'])) {
                continue;
            }
            foreach ($teamData['chosenCards']['specialty'] as $productId) {
                $productData = $modelProduct->getProduct($productId);
                $type = str_replace('-', ' ', $this->getSpecialtyTypeFromProductName($productData->product_name));
                $result[$type][] = $productId;
            }
        }
        return $result;
    }
    
    public function getSpecialtyTypeFromProductName($productName)
    {
        $firstDash = strpos($productName, '-');
        $result = trim(substr($productName, $firstDash + 1));
        return $result;
    }


    public function getMessage($delta, $toTrade, $type)
    {
        if ($delta > 0) {
            $message = "You need $delta more $type card(s) from \"Cards I have to Trade\" to trade $toTrade $type card(s) that you want.";
        } else {
            $delta = abs($delta);
            $message = "You're offering $delta more $type card(s) from \"Cards I have to Trade\" than the number need to trade $toTrade $type card(s) that you want.";
        }
        JFactory::getApplication()->enqueueMessage(JText::_($message), 'error');
    }
    
    public function getTradingFreeProductId()
    {
        $orderModel = VmModel::getModel('orders');
        $user = JFactory::getUser();
        $productId = $orderModel->getTradingFeeProductId($user->id);
        return $productId;
    }
}

//no closing tag