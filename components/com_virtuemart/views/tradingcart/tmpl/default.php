<?php
/**
 *
 * Show the products in a trade summary
 *
 * @package    VirtueMart
 * @subpackage
 * @author RolandD
 * @author Max Milbers
 * @todo add pagination
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 6297 2012-07-24 19:19:34Z Milbo $
 */

//vmdebug('$this->category',$this->category);
vmdebug('$this->category ' . $this->category->category_name);
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.modal');
/* javascript for list Slide
  Only here for the order list
  can be changed by the template maker
*/
$js = "
jQuery(document).ready(function () {
	jQuery('.orderlistcontainer').hover(
		function() { jQuery(this).find('.orderlist').stop().show()},
		function() { jQuery(this).find('.orderlist').stop().hide()}
	)
});
";

$document = JFactory::getDocument();
$document->addScriptDeclaration($js);
$productModel = VmModel::getModel('product');
$step = 1;
function getInformationPlayer($id)
{
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query->select(array('product_name'))
        ->from($db->quoteName('#__virtuemart_products_en_gb'))
        //->join('INNER', $db->quoteName('#__virtuemart_categories_en_gb', 'b') . ' ON (' . $db->quoteName('b.virtuemart_category_id') . ' = ' . $db->quoteName('a.id') . ')')
        //->join('INNER', $db->quoteName('#__virtuemart_categories', 'c') . ' ON (' . $db->quoteName('c.virtuemart_category_id') . ' = ' . $db->quoteName('a.id') . ')')
        ->where($db->quoteName('virtuemart_product_id') . " = " . $id);
    $db->setQuery($query);
    $player = $db->loadObject();
    return $player;
}

?>
<h1 class="title-page-checkout-detail logined"><span><span><?php echo $this->page_title ?></span></span></h1>
<form method="POST">
    <div class="browse-view page-trading-cart">
        <!--    <h2><span><span>--><?php //echo JText::_('Trading Cart'); ?><!--</span></span></h2>-->
        <div>
            <div class="width90 floatleft">
                <?php 
$categoryModel = VmModel::getModel('Category');
$defaultCheckListId = $categoryModel->getDefaultCheckListId();
$defaultTradeListId = $categoryModel->getDefaultCheckListId('tradeList'); ?>
                <a class="details step-cart"
                   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id='.$defaultTradeListId.'&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
                    <?php echo JText::_('Step 1:') . ' ' . JText::_('COM_VIRTUEMART_CART_YOU_WANT'); ?><span
                        class="floatright"><?php echo JText::_('Modify'); ?>>></span>
                </a>
                <?php $step++;?>
            </div>

            <div class="width90 floatleft">

                <a class="details step-cart"
                   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id='.$defaultCheckListId.'&Itemid=697',  $this->useXHTML, $this->useSSL) ?>">
                    <?php echo JText::_('Step 2:') . ' ' . JText::_('COM_VIRTUEMART_CART_YOU_HAVE'); ?><span
                        class="floatright"><?php echo JText::_('Modify'); ?>>></span>
                </a>
                <?php $step++;?>
            </div>

            <div class="width90 floatleft">

                <a class="details step-cart"
                   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=tradingcart&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
                    <?php echo JText::_('Step 3:') . ' ' . JText::_('REVIEW_YOUR_TRADE'); ?><span
                        class="floatright"><?php echo JText::_('Modify'); ?>>></span>
                </a>
                <?php $step++;?>
            </div>

            <div class="floatleft width45 tradingcart" style="margin-left:50px;">
                <h3><?php echo JText::_('COM_VIRTUEMART_I_WANT'); ?></h3>

                <div class="">

                </div>
                <table>
                    <tr>
                        <th><?php echo JText::_('Remove') ?></th>
                        <th class="title-image"><?php echo JText::_('Image') ?></th>
                        <th><?php echo JText::_('Product Name') ?></th>
                        <th><?php echo JText::_('Type') ?></th>
                        <th><?php echo JText::_('Quantity') ?></th>
                    </tr>
                    <?php foreach ($this->tradeList as $teamId => $teamData) {
                        foreach ($teamData['chosenCards'] as $playerId => $cards) {
                            foreach ($cards as $categoryName => $cardId) {
                                $cardInfo = $productModel->getProduct($cardId);
                                $productModel->addImages($cardInfo);
                                ?>

                                <tr>
                                    <td class="checkbox-remove"><input type="checkbox" name="removeTradeCards[]"
                                                                       value="<?php echo $cardId; ?>"/></td>
                                    <td><?php echo $cardInfo->images[0]->displayMediaThumb('class="browseProductImage" border="0" title="' . $cardInfo->product_name . '" width="70px" height="70px" ', true, 'class="modal"'); ?></td>
                                    <td><?php echo $cardInfo->product_name; ?></td>
                                    <td><?php echo is_numeric($categoryName) ? strtoupper(substr($playerId, 0, 1)) : $categoryName; ?></td>
                                    <td class="trade-cart-quantity"><input type="number" min="1" readonly="readonly"
                                                                           name="tradeListQuantity[<?php echo $cardId; ?>]"
                                                                           value="<?php echo (isset($this->tradeListQuantity[$cardId])
                                                                               && $this->tradeListQuantity[$cardId] > 0) ? $this->tradeListQuantity[$cardId] : 1; ?>"/>
                                    </td>
                                </tr>
                            <?php }
                        }
                    } ?>
                </table>
            </div>
            <div class="floatleft width45 tradingcart">
                <h3><?php echo JText::_('COM_VIRTUEMART_I_WANT_TO_TRADE'); ?></h3>
                <table>
                    <tr>
                        <th><?php echo JText::_('Remove') ?></th>
                        <th class="title-image"><?php echo JText::_('Image') ?></th>
                        <th><?php echo JText::_('Product Name') ?></th>
                        <th><?php echo JText::_('Type') ?></th>
                        <th><?php echo JText::_('Quantity') ?></th>
                    </tr>
                    <?php foreach ($this->checkList as $teamId => $teamData) {
                        foreach ($teamData['chosenCards'] as $playerId => $cards) {
                            foreach ($cards as $categoryName => $cardId) {
                                $cardInfo = $productModel->getProduct($cardId);
                                $productModel->addImages($cardInfo);
                                ?>

                                <tr>
                                    <td class="checkbox-remove"><input type="checkbox" name="removeCheckCards[]"
                                                                       value="<?php echo $cardId; ?>"/></td>
                                    <td><?php echo $cardInfo->images[0]->displayMediaThumb('class="browseProductImage" border="0" title="' . $cardInfo->product_name . '" width="70px" height="70px" ', true, 'class="modal"'); ?></td>
                                    <td><?php echo $cardInfo->product_name; ?></td>
                                    <td><?php echo is_numeric($categoryName) ? strtoupper(substr($playerId, 0, 1)) : $categoryName; ?></td>
                                    <td class="trade-cart-quantity"><input type="number" min="1" readonly="readonly"
                                                                           name="checkListQuantity[<?php echo $cardId; ?>]"
                                                                           value="<?php echo (isset($this->checkListQuantity[$cardId])
                                                                               && $this->checkListQuantity[$cardId] > 0) ? $this->checkListQuantity[$cardId] : 1; ?>"/>
                                    </td>
                                </tr>
                            <?php }
                        }
                    } ?>
                </table>
            </div>
        </div>
        <div class="button-trading-card">
            <div id="button-update">
                <input type="submit" value="<?php echo JText::_('Update'); ?>"/>
            </div>
            <div id="button-continue">
                <input type="submit" name="continueTrading" value="<?php echo JText::_('Continue Trading'); ?>"/>
            </div>
            <div id="button-checkout">
                <input type="button" onclick="nextBtnClick()" value="<?php echo JText::_('Next') ?>">
                <input id="submitCheckout" type="submit" name="checkout" 
                       value="<?php echo JText::_('Next') ?>" style="visibility: hidden;"/>
            </div>
        </div>

        <div class="width90 floatleft">

            <a id="step4Link" class="details step-cart" <?php if ($this->address_type == 'BT'){ ?>onclick="return false;"<?php } ?>
               href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=BT&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
                <?php echo JText::_('Step ') . $step . ' : ' . JText::_('COM_VIRTUEMART_USER_FORM_EDIT_BILLTO_LBL'); ?><span
                    class="floatright"><?php echo JText::_('Modify'); ?>>></span>
            </a>
            <?php $step++;?>
            <input type="hidden" name="billto" value="<?php echo $this->cart->lists['billTo']; ?>"/>
        </div>

        <?php
        if ($this->address_type !== 'BT') {

            ?>
            <div class="width90 floatleft">
                <?php if (!isset($this->cart->lists['current_id'])) $this->cart->lists['current_id'] = 0; ?>
                <a class="details step-cart" <?php if ($this->address_type == 'ST'){ ?>onclick="return false;"<?php } ?>
                   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=ST&virtuemart_user_id[]=' . $this->cart->lists['current_id'].'&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
                    <?php echo JText::_('Step ') . $step . ' : ' . JText::_('COM_VIRTUEMART_USER_FORM_ADD_SHIPTO_LBL'); ?><span
                        class="floatright"><?php echo JText::_('Modify'); ?>>></span>
                </a>
                <?php $step++;?>
            </div>
        <?php
        }
        ?>
        <?php
        if ($this->address_type == 'BT') {
            ?>
            <div class="width90 floatleft">
                <?php if (!isset($this->cart->lists['current_id'])) $this->cart->lists['current_id'] = 0; ?>
                <a class="details step-cart" <?php if ($this->address_type == 'ST'){ ?>onclick="return false;"<?php } ?>
                   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=ST&virtuemart_user_id[]=' . $this->cart->lists['current_id'].'&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
                    <?php echo JText::_('Step ') . $step . ' : ' . JText::_('COM_VIRTUEMART_USER_FORM_ADD_SHIPTO_LBL'); ?><span
                        class="floatright"><?php echo JText::_('Modify'); ?>>></span>
                </a>
                <?php $step++;?>
            </div>
        <?php
        }
        ?>
        <div class="width90 floatleft">

            <?php if (!isset($this->cart->lists['current_id'])) $this->cart->lists['current_id'] = 0; ?>
            <a class="details step-cart"
               href="<?php echo JRoute::_('index.php?view=cart&task=edit_shipment&Itemid=697', $this->useXHTML, $this->useSSL); ?>">
                <?php echo JText::_('Step ') . $step . ' : ' . JText::_('Delivery method'); ?><span
                    class="floatright"><?php echo JText::_('Modify'); ?>>></span>
            </a>
            <?php $step++;?>
        </div>


        <div class="width90 floatleft">

            <?php if (!isset($this->cart->lists['current_id'])) $this->cart->lists['current_id'] = 0; ?>
            <a class="details step-cart"
               href="<?php echo JRoute::_('index.php?view=cart&task=editpayment&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
                <?php echo JText::_('Step ') . $step . ' : ' . JText::_('Payment method'); ?><span
                    class="floatright"><?php echo JText::_('Modify'); ?>>></span>
            </a>
            <?php $step++;?>
        </div>

        <div class="width90 floatleft">

            <a class="details step-cart"
               href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=cart&Itemid='.$_GET['Itemid'], $this->useXHTML, $this->useSSL) ?>">
                <?php echo JText::_('Step ') . $step . ' : ' . JText::_('CONFIRM_YOUR_TRADE'); ?><span
                    class="floatright"><?php echo JText::_('Modify'); ?>>></span>
            </a>

        </div>
    </div>
</form>
<script type="text/javascript">
    function nextBtnClick() {
        // Clear cart
        jQuery.ajax({
            url: 'index.php?option=com_virtuemart&view=tradingcart&task=deleteCart&Itemid=697',
            success: function() {
                // Add trading fee product to cart
                jQuery.ajax({
                    url: 'index.php?option=com_virtuemart&nosef=1&view=cart&task=addJS&format=json&action=trade&quantity[]=1&option=com_virtuemart&view=cart&virtuemart_product_id=<?php echo (int)$this->tradingFreeProductId; ?>',
                    success: function() {
                        jQuery('#submitCheckout').click();
                    }
                });
            }
        });        
    }
    </script>
