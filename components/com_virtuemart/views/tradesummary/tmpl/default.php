<?php
/**
*
* Show the products in a trade summary
*
* @package	VirtueMart
* @subpackage
* @author RolandD
* @author Max Milbers
* @todo add pagination
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: default.php 6297 2012-07-24 19:19:34Z Milbo $
*/

//vmdebug('$this->category',$this->category);
vmdebug('$this->category '.$this->category->category_name);
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
JHTML::_( 'behavior.modal' );
/* javascript for list Slide
  Only here for the order list
  can be changed by the template maker
*/
$js = "
jQuery(document).ready(function () {
	jQuery('.orderlistcontainer').hover(
		function() { jQuery(this).find('.orderlist').stop().show()},
		function() { jQuery(this).find('.orderlist').stop().hide()}
	)
});
";


$document = JFactory::getDocument();
$document->addScriptDeclaration($js);

$productModel = new VirtueMartModelProduct();
?>

<div class="width90 floatleft">

<?php 
$categoryModel = VmModel::getModel('Category');
$defaultCheckListId = $categoryModel->getDefaultCheckListId();
$defaultTradeListId = $categoryModel->getDefaultCheckListId('tradeList'); ?>
<!--	<a class="details step-cart"-->
<!--	   href="--><?php //echo JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=' . $arrayCurrentTradeCate[0], $this->useXHTML, $this->useSSL) ?><!--">-->
	<a class="details step-cart"
	   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=' . $defaultCheckListId . '&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
		<?php echo JText::_('Step 1:') . ' ' . JText::_('COM_VIRTUEMART_CART_YOU_WANT'); ?><span
			class="floatright"><?php echo JText::_('Modify'); ?>>></span>
	</a>

</div>

<div class="width90 floatleft">

	<a class="details step-cart"
	   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=' . $defaultTradeListId . '&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
		<?php echo JText::_('Step 2:') . ' ' . JText::_('COM_VIRTUEMART_CART_YOU_HAVE'); ?><span
			class="floatright"><?php echo JText::_('Modify'); ?>>></span>
	</a>

</div>

<div class="width90 floatleft">

	<a class="details step-cart"
	   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=tradingcart', $this->useXHTML, $this->useSSL) ?>">
		<?php echo JText::_('Step 3:') . ' ' . JText::_('REVIEW_YOUR_TRADE'); ?><span
			class="floatright"><?php echo JText::_('Modify'); ?>>></span>
	</a>

</div>

        <div class="width90 floatleft">

            <a class="details step-cart" href="javascript:void(0);" style="cursor: default;"
               data-href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=40', $this->useXHTML, $this->useSSL) ?>" disabled="disabled">
                <?php echo JText::_('Step 4:') . ' ' . JText::_('Delivery method'); ?><span
                    class="floatright"><?php echo JText::_('Modify'); ?>>></span>
            </a>

        </div>
        <div class="width90 floatleft">

            <a class="details step-cart" href="javascript:void(0);" style="cursor: default;"
               data-href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=40', $this->useXHTML, $this->useSSL) ?>">
                <?php echo JText::_('Step 5:') . ' ' . JText::_('Payment method'); ?><span
                    class="floatright"><?php echo JText::_('Modify'); ?>>></span>
            </a>

        </div>
<div class="width90 floatleft">

	<a class="details step-cart"
	   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=tradesummary', $this->useXHTML, $this->useSSL) ?>">
		<?php echo JText::_('Step 6:') . ' ' . JText::_('CONFIRM_YOUR_TRADE'); ?><span
			class="floatright"><?php echo JText::_('Modify'); ?>>></span>
	</a>

</div>

<div id="page-trade">
	<!-- Trade for-->
	<div class="floatleft width30 trading-for">
		<div class="trade-header">
			<h1><?php echo JText::_('Trade For?');?></h1>
		</div>
		<div class="trade-border-content">
			<div class="trade-content">
				<table border="1">
					<tr class="row-first-right">
						<td></td>
						<td>
							<?php
							echo JText::_('Stt');
							?>
						</td>
						<td><?php echo JText::_('Player')?></td>
						<td><?php echo JText::_('C');?></td>
						<td><?php echo JText::_('G');?></td>
						<!--<td><?php echo JText::_('S');?></td>-->
					</tr>
					<?php
					$index = 1;
					$arrayTradeSummary = array();
					foreach($this->tradeList as $val)
					{
						foreach($val['chosenCards'] as $k => $card)
						{
                                                    if ($k == 'specialty') {
                                                        foreach ($card as $cardId) {
                                                        $class = ($index%2 == 1) ? 'menu-category-first' : 'menu-category-second' ;
                                                        $cardInfo = $productModel->getProduct($cardId);
                                                        $productModel->addImages($cardInfo);
                                                        
                                                        $arrayTradeSummary[] = array(
											'product-name' => $cardInfo->product_name,
											'image' => $val['imageUrl'],
											'cart' => JText::_('SP'),
											'cartId' => $cardId
										);
                                                        ?>
                                                            <tr class="<?php echo $class;?>">
								<td><img class="image-team" src="<?php echo $cardInfo->images[0]->file_url;?>"></td>
								<td><div class="category-number"><?php echo $index;?></div></td>
								<td>
									<?php
									echo $cardInfo->product_name;
									?>
								</td>
                                                                <td colspan="3">
									Specialty
								</td>
							</tr>
                                                        <?php
							$index++;
                                                        }
                                                        continue;
                                                    }
                                                    
							$player = $productModel->getProduct($k);
							$productModel->addImages($player);
                                                        $class = ($index%2 == 1) ? 'menu-category-first' : 'menu-category-second' ;
							?>
							<tr class="<?php echo $class;?>">
								<td><img class="image-team" src="<?php echo $player->images[0]->file_url;?>"></td>
								<td><div class="category-number"><?php echo $index;?></div></td>
								<td>
									<?php
									echo $player->product_name;
									?>
								</td>
								<td>
									<?php
									if (array_key_exists(JText::_('C'),$card))
									{
										$arrayTradeSummary[] = array(
											'product-name' => $player->product_name,
											'image' => $val['imageUrl'],
											'cart' => JText::_('C'),
											'cartId' => $card[JText::_('C')]
										);
										echo JText::_('T');
									}
									?>
								</td>
								<td>
									<?php
									if (array_key_exists(JText::_('G'),$card))
									{
										$arrayTradeSummary[] = array(
											'product-name' => $player->product_name,
											'image' => $val['imageUrl'],
											'cart' => JText::_('G'),
											'cartId' => $card[JText::_('G')]
										);
										echo JText::_('T');
									}
									?>
								</td>
							</tr>
							<?php
							$index++;
						}
					}
					?>
				</table>
			</div>
		</div>
	</div>

	<!-- Trade What-->
	<div class="floatleft width30 trade-what">
		<div class="trade-header">
			<h1><?php echo JText::_('Trade What?');?></h1>
		</div>
		<div class="trade-border-content">
			<div class="trade-content">
				<table border="1">
					<tr class="row-first-right">
						<td></td>
						<td>
							<?php
							echo JText::_('Stt');
							?>
						</td>
						<td><?php echo JText::_('Player')?></td>
						<td><?php echo JText::_('C');?></td>
						<td><?php echo JText::_('G');?></td>
						<!--<td><?php echo JText::_('S');?></td>-->
					</tr>
					<?php
					$index = 1;
					$arrayCheckListSummary = array();
					foreach($this->checkList as $val)
					{
						foreach($val['chosenCards'] as $k => $card)
						{
                                                    if ($k == 'specialty') {
                                                        foreach ($card as $cardId) {
                                                        $class = ($index%2 == 1) ? 'menu-category-first' : 'menu-category-second' ;
                                                        $cardInfo = $productModel->getProduct($cardId);
                                                        $productModel->addImages($cardInfo);
                                                        $arrayCheckListSummary[] = array(
											'product-name' => $cardInfo->product_name,
											'image' => $val['imageUrl'],
											'cart' => JText::_('SP'),
											'cartId' => $cardId
										);
                                                        ?>
                                                            <tr class="<?php echo $class;?>">
								<td><img class="image-team" src="<?php echo $cardInfo->images[0]->file_url;?>"></td>
								<td><div class="category-number"><?php echo $index;?></div></td>
								<td>
									<?php
									echo $cardInfo->product_name;
									?>
								</td>
                                                                <td colspan="3">
									Specialty
								</td>
							</tr>
                                                        <?php
							$index++;
                                                        }
                                                        continue;
                                                    }
							$player = $productModel->getProduct($k);
							$productModel->addImages($player);
							$class = ($index%2 == 1) ? 'menu-category-first' : 'menu-category-second' ;
							?>
							<tr class="<?php echo $class;?>">
								<td><img class="image-team" src="<?php echo $player->images[0]->file_url;?>"></td>
								<td><div class="category-number"><?php echo $index;?></div></td>
								<td>
									<?php
									echo $player->product_name;
									?>
								</td>
								<td>
									<?php
									if (array_key_exists(JText::_('C'),$card))
									{
										$arrayCheckListSummary[] = array(
											'product-name' => $player->product_name,
											'image' => $val['imageUrl'],
											'cart' => JText::_('C'),
											'cartId' =>$card[JText::_('C')]
										);
										echo JText::_('T');
									}
									?>
								</td>
								<td>
									<?php
									if (array_key_exists(JText::_('G'),$card))
									{
										$arrayCheckListSummary[] = array(
											'product-name' => $player->product_name,
											'image' => $val['imageUrl'],
											'cart' => JText::_('G'),
											'cartId' => $card[JText::_('G')]
										);
										echo JText::_('T');
									}
									?>
								</td>
							</tr>
							<?php
							$index++;
						}
					}
					?>
				</table>
			</div>
		</div>
	</div>

	<!-- trade summary-->
	<div class="floatleft width35 trade-summary">
		<div class="trade-header">
			<h1><?php echo JText::_('Trade Summary');?></h1>
		</div>
		<div class="trade-border-content">
			<div class="trade-content">
				<table border="1">
					<tr class="row-first-right">
						<td></td>
						<td class="trade-summary-title-for"><?php echo JText::_('Trade for?')?></td>
						<td></td>
						<td></td>
						<td class="trade-summary-title-what"><?php echo JText::_('Trade what?')?></td>
						<td></td>
						<td></td>
					</tr>
					<?php
					$totalTradeSummary = count($arrayTradeSummary);
					$totalCheckListSummary = count($arrayCheckListSummary);

					$i = (($totalTradeSummary > $totalCheckListSummary) || ($totalTradeSummary == $totalCheckListSummary)) ? $totalTradeSummary : $totalCheckListSummary;
					if($i > 0)
					{
						for($j = 1; $j <= $i; $j++)
						{

							$class = ($j%2 == 1) ? 'menu-category-first' : 'menu-category-second' ;
							?>
							<tr class="<?php echo $class;?>">
								<td><div class="category-number"><?php echo $j;?></div></td>
								<?php
								if (isset($arrayTradeSummary[$j-1]))
								{
									$player = $productModel->getProduct($arrayTradeSummary[$j-1]['cartId']);
									$productModel->addImages($player);
									?>
									<td>
										<?php
										echo '<div class="thumbnail-club"><img src="'.$player->images[0]->file_url.'"></div>';
										echo '<div class="div-club-name"><span>'.$player->product_name.'</span></div>';
										?>
									</td>
									<td>
										<?php
										echo $arrayTradeSummary[$j-1]['cart'];
										?>
									</td>
									<td>
										<?php
										if(isset($this->tradeListQuantity[$arrayTradeSummary[$j-1]['cartId']])) {
											echo $this->tradeListQuantity[$arrayTradeSummary[$j-1]['cartId']];
										}
										else
										{
											echo 1;
										}
										?>
									</td>
								<?php
								}
								else
								{
									echo "<td></td><td></td><td></td>";
								}

								if (isset($arrayCheckListSummary[$j-1])) {
									$player = $productModel->getProduct($arrayCheckListSummary[$j-1]['cartId']);
									$productModel->addImages($player);
									?>
									<td>
										<?php
										echo '<div class="thumbnail-club"><img src="'.$player->images[0]->file_url.'"></div>';
										echo '<div class="div-club-name"><span>'.$player->product_name.'</span></div>';
										?>
									</td>
									<td>
										<?php
										echo $arrayCheckListSummary[$j-1]['cart'];
										?>
									</td>
									<td>
										<?php
										if(isset($this->checkListQuantity[$arrayCheckListSummary[$j-1]['cartId']])) {
											echo $this->checkListQuantity[$arrayCheckListSummary[$j-1]['cartId']];
										}
										else
										{
											echo 1;
										}
										?>
									</td>
								<?php
								}
								else
								{
									echo "<td></td><td></td><td></td>";
								}
								?>

							</tr>
						<?php
						}
					}
					?>
				</table>
			</div>
		</div>

		<div id="form-trade-summary">
			<form id="member-registration" action="#" method="post" enctype="multipart/form-data">
				<div>
					<div class="div-button">
						<a href="<?php echo JRoute::_('index.php?option=com_trading&task=submitTrade&Itemid=691');?>" id="button-trade"><?php echo JText::_('Submit Trade');?></a>
					</div>
					<div class="div-button">
						<a href="<?php echo JRoute::_('index.php?option=com_trading&task=resetCart');?>" id="button-clear"><?php echo JText::_('Clear');?></a>
					</div>
					<!--					<a href="--><?php //echo JRoute::_('');?><!--" title="--><?php //echo JText::_('JCANCEL');?><!--">--><?php //echo JText::_('JCANCEL');?><!--</a>-->
					<input type="hidden" name="option" value="com_virtuemart" />
					<input type="hidden" name="task" value="trading" />
				</div>
			</form>
		</div>
	</div>
</div>