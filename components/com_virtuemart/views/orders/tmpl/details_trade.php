<?php
/**
 *
 * Order detail view
 *
 * @package	VirtueMart
 * @subpackage Orders
 * @author Oscar van Eijk, Valerie Isaksen
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: details_order.php 5341 2012-01-31 07:43:24Z alatak $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

if (intval($this->orderdetails['details']['BT']->isTrading)) { 
    $checkList = json_decode($this->orderdetails['details']['BT']->tradewhat, true);
    $tradeList = json_decode($this->orderdetails['details']['BT']->tradefor, true);
    $checkListQuantity = json_decode($this->orderdetails['details']['BT']->tradewhatquantity, true);
    $tradeListQuantity = json_decode($this->orderdetails['details']['BT']->tradeforquantity, true);
    
    $productModel = VmModel::getModel('Product');
//    $productModel = new VirtueMartModelProduct();
?>

<div class="floatleft width45 tradingcart" >
    <h3><?php echo JText::_('COM_VIRTUEMART_I_WANT'); ?></h3>

    <div class="">

    </div>
    <table>
        <tr>
            <th><?php echo JText::_('COM_VIRTUEMART_I_WANT') ?></th>
            <th><?php echo JText::_('Type') ?></th>
            <th><?php echo JText::_('Quantity') ?></th>
        </tr>
        <?php foreach ($tradeList as $teamId => $teamData) {
            foreach ($teamData['chosenCards'] as $playerId => $cards) {
                foreach ($cards as $categoryName => $cardId) {
                    $cardInfo = $productModel->getProduct($cardId);
                    ?>

                    <tr>
                        <td><?php echo $cardInfo->product_name; ?></td>
                        <td><?php echo is_numeric($categoryName) ? strtoupper(substr($playerId, 0, 1)) : $categoryName; ?></td>
                        <td class="trade-cart-quantity"><?php echo (isset($tradeListQuantity[$cardId])
                                                                               && $tradeListQuantity[$cardId] > 0) ? $tradeListQuantity[$cardId] : 1; ?></td>
                    </tr>
                <?php }
            }
        } ?>
    </table>
</div>
<div class="floatleft width45 tradingcart">
    <h3><?php echo JText::_('COM_VIRTUEMART_I_WANT_TO_TRADE'); ?></h3>
    <table>
        <tr>
            <th><?php echo JText::_('COM_VIRTUEMART_I_WANT_TO_TRADE') ?></th>
            <th><?php echo JText::_('Type') ?></th>
            <th><?php echo JText::_('Quantity') ?></th>
        </tr>
        <?php foreach ($checkList as $teamId => $teamData) {
            foreach ($teamData['chosenCards'] as $playerId => $cards) {
                foreach ($cards as $categoryName => $cardId) {
                    $cardInfo = $productModel->getProduct($cardId);
                    ?>

                    <tr>
                        <td><?php echo $cardInfo->product_name; ?></td>
                        <td><?php echo is_numeric($categoryName) ? strtoupper(substr($playerId, 0, 1)) : $categoryName; ?></td>
                        <td class="trade-cart-quantity"><?php echo (isset($checkListQuantity[$cardId])
                                                                               && $checkListQuantity[$cardId] > 0) ? $checkListQuantity[$cardId] : 1; ?></td>
                    </tr>
                <?php }
            }
        } ?>
    </table>
</div>
<?php } ?>