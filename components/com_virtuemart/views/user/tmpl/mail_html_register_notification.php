<?php
defined('_JEXEC') or die('');

/**
 * Renders the email for the user send in the registration process
 * @package    VirtueMart
 * @subpackage User
 * @author Max Milbers
 * @author Valérie Isaksen
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: view.html.php 2459 2010-07-02 17:30:23Z milbo $
 */

$li = '<br />';
$db = JFactory::getDbo();
$query = $db->getQuery(true);
$query->select(array('introtext'))
    ->from($db->quoteName('#__content'))
    ->where($db->quoteName('id') . " = 141");
$db->setQuery($query);
$fullArticle = $db->loadResult();

$activationLink = JURI::base();
if (!empty($this->activationLink)) {
    $activationLink = JURI::base() . $this->activationLink;
}

$support = JURI::base() . 'index.php?option=com_contact&view=contact&id=1&Itemid=662';
$myAccount = JURI::base() . 'index.php?option=com_virtuemart&view=user&task=authenticateMyAccount';

$array_find = array('[displayname]', '[username]', '[password]', '[nominated]', '[linkmyaccount]', '[linksupport]', '[activationlink]');
$array_replace = array(
    $this->user->name,
    $this->user->username,
    $this->user->password_clear,
    $this->user->YourDonaionName,
    $myAccount,
    $support,
    $activationLink
);
//replace username
$fullArticle = str_replace($array_find, $array_replace, $fullArticle);
?>
<html>
<head>

</head>

<body style="word-wrap: break-word;">
<style type="text/css">
    #footy-table td,#footy-table span,#footy-table p,#footy-table th {
        font-family: 'Calibri' !important;
    }
    div > table td,div > table th{
        font-family: 'Calibri' !important;
    }
</style>

<div style="" width="100%">
    <table style="margin: auto;" cellpadding="0" cellspacing="0" width="100%" id="footy-table">
        <tr>
            <td>
                <?php echo $fullArticle; ?>
            </td>
        </tr>
    </table>
</div>
</body>
</html>