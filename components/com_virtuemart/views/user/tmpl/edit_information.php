<?php
/**
 *
 * Enter address data for the cart, when anonymous users checkout
 *
 * @package	VirtueMart
 * @subpackage User
 * @author Oscar van Eijk, Max Milbers
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: edit_address.php 6172 2012-06-28 07:24:53Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
// vmdebug('user edit address',$this->userFields['fields']);
// Implement Joomla's form validation
JHTML::_('behavior.formvalidation');
JHTML::stylesheet('vmpanels.css', JURI::root() . 'components/com_virtuemart/assets/css/');

if ($this->fTask === 'savecartuser') {
    $rtask = 'registercartuser';
	$url=0;
} else {
    $rtask = 'registercheckoutuser';
	$url = JRoute::_('index.php?option=com_virtuemart&view=cart&task=checkout',$this->useXHTML,$this->useSSL);
}
if($this->typeView === 'subcribeAccount')
{
	$title = JText::_('Subcribe / Unsubscribe to Newsletters');
}
else
{
	$title = JText::_('Edit Account Details');;
}
$urlCancel = 'index.php?option=com_content&view=article&id=136&Itemid=663';
//if ItemId is page information
if(isset($_REQUEST['Itemid']) && $_REQUEST['Itemid'] == 693) {
	$urlCancel = 'index.php?option=com_content&view=article&id=111&Itemid=661';
}
?>
<!--<h1>--><?php //echo $this->page_title ?><!--</h1>-->
<h1 class="title-page-checkout-detail"><span><span><?php echo $title; ?></span></span></h1>
<script language="javascript">
	jQuery(document).ready(function($){
		$('#virtuemart_country_id').attr('id',$('#virtuemart_country_id').parents('tr').find('label').attr('for'));
	})
    function myValidator(f, t)
    {
        f.task.value=t; //this is a method to set the task of the form on the fTask.
        if (document.formvalidator.isValid(f)) {
            f.submit();
            return true;
        } else {
            var msg = '<?php echo addslashes(JText::_('COM_VIRTUEMART_USER_FORM_MISSING_REQUIRED_JS')); ?>';
            alert (msg+' ');
        }
        return false;
    }

    function callValidatorForRegister(f){

        var elem = jQuery('#userForm');

	return myValidator(f, '<?php echo $rtask ?>');

    }


</script>
<div class="page-edit-information">
<fieldset>
    <form method="post" id="userForm" name="userForm" class="form-validate">

	<?php
		if (!class_exists('VirtueMartCart'))
			require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');

		if (count($this->userFields['functions']) > 0) {
			echo '<script language="javascript">' . "\n";
			echo join("\n", $this->userFields['functions']);
			echo '</script>' . "\n";
		}
		echo $this->loadTemplate('userfields');

	?>

	<div class="control-buttons">
	    <?php
			$rview = 'user';

			$buttonclass = 'button vm-button-correct';
		?>


		<button class="<?php echo $buttonclass ?>" type="submit" onclick="javascript:return myValidator(userForm, '<?php echo $this->fTask; ?>');" ><?php echo JText::_('COM_VIRTUEMART_SAVE'); ?></button>
		<button class="default" type="reset" onclick="window.location.href='<?php echo JRoute::_($urlCancel); ?>'" ><?php echo JText::_('COM_VIRTUEMART_CANCEL'); ?></button>
	</div>

</fieldset>

<input type="hidden" name="option" value="com_virtuemart" />
<input type="hidden" name="view" value="user" />
<input type="hidden" name="controller" value="user" />
<input type="hidden" name="task" value="<?php echo $this->fTask; // I remember, we removed that, but why?   ?>" />
<input type="hidden" name="layout" value="<?php echo $this->getLayout(); ?>" />
<input type="hidden" name="address_type" value="<?php echo $this->address_type; ?>" />
<?php if(!empty($this->virtuemart_userinfo_id)){
	echo '<input type="hidden" name="shipto_virtuemart_userinfo_id" value="'.(int)$this->virtuemart_userinfo_id.'" />';
}
echo JHTML::_('form.token');
?>
</form>
</div>