<?php
/**
 *
 * Show the products in a category
 *
 * @package    VirtueMart
 * @subpackage
 * @author RolandD
 * @author Max Milbers
 * @todo add pagination
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 6297 2012-07-24 19:19:34Z Milbo $
 */
//vmdebug('$this->category',$this->category);
vmdebug('$this->category ' . $this->category->category_name);
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.modal');
/* javascript for list Slide
  Only here for the order list
  can be changed by the template maker
 */
$js = "
jQuery(document).ready(function () {
	jQuery('.orderlistcontainer').hover(
		function() { jQuery(this).find('.orderlist').stop().show()},
		function() { jQuery(this).find('.orderlist').stop().hide()}
	)
});
";

$document = JFactory::getDocument();
$document->addScriptDeclaration($js);
$step = 1;
$vmModel = new VirtueMartModelCategory;
//Get parent
$categoryParentCurrent = $vmModel->getParentCategory($this->category->virtuemart_category_id);
$catagories = array();
if (isset($categoryParentCurrent->virtuemart_category_id)) {
    $categoryParent = $vmModel->getCategoryParentByChild($categoryParentCurrent->virtuemart_category_id);

    if (isset($categoryParent->category_parent_id)) {
        $catagories = $vmModel->getChildCategoryList($categoryParent->virtuemart_vendor_id, $categoryParent->category_parent_id);
    }
}

?>
<h1 class="title-page-checkout-detail"><span><span><?php echo $this->page_title ?></span></span></h1>
<div class="div-list-category onepage-trading">
    <?php
    if (count($catagories) > 0) {
        $div = '';
        if ($categoryParent->file_url) {
            $div = 'list_category_child';
            ?>
            <div class="logo_category_parent hidden">
                <img src="<?php echo JURI::root() . $categoryParent->file_url ?>">
            </div>
        <?php } ?>
        <div class="hidden <?php echo $div; ?>">
            <form action="" id="submit-change-category" method="POST">
                <?php

                foreach ($catagories as $k => $c) {
                    $checked = '';

                    if ($k != (count($catagories) - 1)) {
                        ?>
                        <div class="div-categories">
                            <input type="radio" name="category" cateview="category"
                                   value="<?php echo $c->virtuemart_category_id ?>"><span><?php echo $c->category_name; ?></span>
                        </div>
                    <?php
                    } else {
                        $childs = VirtueMartModelCategory::getChildCategoryList(1, $c->virtuemart_category_id);
                        if (count($childs) > 0) {
                            foreach ($childs as $child) {
                                $checked = '';
                                if ($child->virtuemart_category_id === $this->category->virtuemart_category_id) {
                                    $checked = 'checked';
                                }
                                $arrayCurrentTradeCate[] =  $child->virtuemart_category_id;
                                ?>
                                <div class="div-categories">
                                    <input type="radio" name="category" cateview="categorychecklist"
                                           value="<?php echo $child->virtuemart_category_id ?>" <?php echo $checked; ?> ><span><?php echo $child->category_name; ?></span>
                                </div>
                            <?php
                            }
                        }
                    }
                }
                ?>
            </form>
        </div>
    <?php } ?>
</div>
<div class="page-categorychecklist onepage-trading">
    <form id="checkListForm" action="" method="POST">
        <div class="width90 floatleft">
<?php 
$categoryModel = VmModel::getModel('Category');
$defaultCheckListId = $categoryModel->getDefaultCheckListId();
$defaultTradeListId = $categoryModel->getDefaultCheckListId('tradeList'); ?>
<!--            <a class="details step-cart"-->
<!--               href="--><?php //echo JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=' . $arrayCurrentTradeCate[0], $this->useXHTML, $this->useSSL) ?><!--">-->
            <a class="details step-cart"
               href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=' . $defaultTradeListId . '&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
                <?php echo JText::_('Step 1:') . ' ' . JText::_('COM_VIRTUEMART_CART_YOU_WANT'); ?><span
                    class="floatright"><?php echo JText::_('Modify'); ?>>></span>
            </a>

        </div>

        <?php
        if($this->checkListType == 'checkList'){
            ?>
            <div class="width90 floatleft">

                <a class="details step-cart"
                   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=' . $defaultCheckListId . '&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
                    <?php echo JText::_('Step 2:') . ' ' . JText::_('COM_VIRTUEMART_CART_YOU_HAVE'); ?><span
                        class="floatright"><?php echo JText::_('Modify'); ?>>></span>
                </a>

            </div>
        <?php
        }
        ?>
        <div class="browse-view width90">
            <?php
            $arrayCheckListIDs = array();
            $arrayTradeListIDs = array();
            /**
             * Show All teams
             */
            ?>
            <div id="category-slider">
                <?php
                $teamCategoryModel = $categoryModel;
                $teamCategory_id = 7;
                $vendorId = '1';
                $cache = JFactory::getCache('com_virtuemart','callback');
                $teamCategories = $cache->call( array( 'VirtueMartModelCategory', 'getChildCategoryList' ),$vendorId, $teamCategory_id );
                $teamCategoryModel->addImages($teamCategories);
                $db1 = JFactory::getDbo();
                foreach ($teamCategories as $teamCategory) {
                    $caturl = '';
                    $teamTradeCate = $vmModel->getChildCategoryList($teamCategory->images[0]->virtuemart_vendor_id, $teamCategory->virtuemart_category_id);

                    if(count($teamTradeCate) > 5 ){
                        $teamTradeCateID = $teamTradeCate[(count($teamTradeCate) - 1)];

                        $teamTradeChildCate = $vmModel->getChildCategoryList($teamCategory->images[0]->virtuemart_vendor_id, $teamTradeCateID->virtuemart_category_id);
                        if(count($teamTradeChildCate) == 2)
                        {
                            // Get Check list Category ID of current Team
                            $checkListCateID = $teamTradeChildCate[0]->virtuemart_category_id;

                            // Get Trade list Category ID of current Team
                            $tradeListCateID = $teamTradeChildCate[1]->virtuemart_category_id;

                            $caturl = JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=' . $checkListCateID . '&Itemid=697');
                            if($this->checkListType == 'tradeList'){
                                $caturl = JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=' . $tradeListCateID . '&Itemid=697');
                            }

                        }


                        $active= '';
                        if($this->teamCategory->virtuemart_category_id == $teamCategory->virtuemart_category_id){
                            $active = 'active';
                        }
                    }
                    $step = 'checklist';
                    if(in_array($this->category->virtuemart_category_id,$arrayTradeListIDs)){
                        $step = 'tradelist';
                    }
                    ?>

                    <div class="category-slider-image">
                        <?php
                        if($caturl){
                            echo '<a href="'.$caturl.'">';
                        }
                        ?>
                            <img src="<?php echo $teamCategory->images[0]->file_url; ?>"
                                 title="<?php echo $teamCategory->category_name; ?>" class="<?php echo $active?>">
                        <?php
                        if($caturl) {
                            echo '</a>';
                        }
                        ?>

                    </div>
                <?php
                }
                ?>
            </div>

            <?php
            if ($this->category->category_name) {
                $nameCate = $this->category->category_name;
                if (!empty($categoryParent->category_name)) {
                    $nameCate .= " - " . $categoryParent->category_name;
                }
                ?>
                <!--                <h2><span><span>--><?php //echo $nameCate; ?><!--</span></span></h2>-->
            <?php } ?>
            <?php
            // Show child categories
            if (!empty($this->products)) {
                // Category and Columns Counter
                $iBrowseCol = 1;
                $iBrowseProduct = 1;

                // Calculating Products Per Row
                $BrowseProducts_per_row = $this->perRow;
                $Browsecellwidth = ' width' . floor(100 / $BrowseProducts_per_row);

                // Separator
                $verticalseparator = " vertical-separator";

                // Count products ?? why not just count ($this->products)  ?? note by Max Milbers
                $BrowseTotalProducts = 0;
                foreach ($this->products as $product) {
                    $BrowseTotalProducts++;
                }
                ?>
                <input type="hidden" name="checkListType" value="<?php echo $this->checkListType; ?>"/>
                <input type="hidden" name="categoryId"
                       value="<?php echo $this->teamCategory->virtuemart_category_id; ?>"/>
                <?php echo $this->teamCategory->images[0]->displayMediaThumb('id="teamCategoryImage", style="display: none;"', false); ?>
                <input type="hidden" id="categoryImageUrl" name="categoryImageUrl" value=""/>
                <div id="listcards">
                    <div class="team-player-list">
                <table class="playerlist">
                    <thead>
                    <td><img src="<?php echo JURI::root() . $categoryParent->file_url ?>" width="30"></td>
                    <td><?php echo JText::_('Card Name') ?></td>
                    <td><?php echo JText::_('C') ?></td>
                    <!--<td><?php echo JText::_('S') ?></td>--> 
                    <td><?php echo JText::_('G') ?></td>

                    </thead>
                    <tbody>
                    <?php
                    // Start the Output
                    foreach ($this->products as $product) {

                        // Show Products
                        ?>

                        <tr>
                            <td algin="center"><span class="stt"><?php echo $iBrowseProduct; ?></span></td>
                            <td>
                                <!--                            <div class="div-product">-->
                                <!--                                --><?php
                                //                                /** @todo make image popup
                                //                                    Show image for each cards
                                //                                 */
                                //                                echo $product->images[0]->displayMediaThumb('class="browseProductImage" border="0" title="' . $product->product_name . '" ', true, 'class="modal"');
                                //                                ?>
                                <!--                            </div>-->
                                <h3 class="catProductTitle"><?php echo JHTML::link($product->link, $product->product_name); ?></h3>


                            </td>
                            <?php foreach ($product->childs as $child) { 
                                if (substr($child->category_name, 0, 1) == 'S') {
                                    continue;
                                }
                            ?>
                                <td align="center">
                                    <input
                                        name="checked_list[<?php echo $product->virtuemart_product_id; ?>][<?php echo substr($child->category_name, 0, 1); ?>]"
                                        type="checkbox" value="<?php echo $child->virtuemart_product_id; ?>"
                                        style="display: block;"
                                        <?php echo is_array($this->chosenCards[$product->virtuemart_product_id]) && in_array($child->virtuemart_product_id, $this->chosenCards[$product->virtuemart_product_id]) ? 'checked="checked"' : ''; ?> >
                                </td>
                            <?php } ?>

                        </tr>
                        <?php
                        $iBrowseProduct++;
                        if($iBrowseProduct == 7){
                            echo "<tr class='td-last-child'><td colspan='5'><div>".JText::_('My Cards')."<a href='".JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&task=displayallchecklist&virtuemart_category_id=7&Itemid=689')."' class='button-view' target='blank'>".JText::_('View')."</a></div></td></tr>";
                            echo "</tbody></table>";
                            ?>
                    <table class="playerlist">
                        <thead>
                        <td><img src="<?php echo JURI::root() . $categoryParent->file_url ?>" width="30"></td>
                        <td><?php echo JText::_('Card Name') ?></td>
                        <td><?php echo JText::_('C') ?></td>
                        <!--<td><?php echo JText::_('S') ?></td>-->
                        <td><?php echo JText::_('G') ?></td>

                        </thead>
                        <tbody>
                    <?php
                        }
                    } // end of foreach ( $this->products as $product )
                    echo "<tr class='td-last-child'><td colspan='5'> <div>".JText::_('My Doubles')."<a href='".JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&task=displayalltradelist&virtuemart_category_id=7&Itemid=690')."' class='button-view' target='blank'>".JText::_('View')."</a></div></td></tr>";
                    ?>

                    </tbody>
                </table>
                    </div>
                    <div class="team-specialty">
                <table class="playerlist" >
                    <thead>
                    <td><img src="<?php echo JURI::root() . $categoryParent->file_url ?>" width="30"></td>
                    <td><?php echo JText::_('Card Name') ?></td>
                    <td>&nbsp;</td>

                    </thead>
                    <tbody>
                    <?php
                    // Start the Output
                    foreach ($this->specialtyCards as $product) {

                        // Show Products
                        ?>

                        <tr>
                            <td algin="center"><span class="stt"><?php echo $iBrowseProduct; ?></span></td>
                            <td>
                                <!--                            <div class="div-product">-->
                                <!--                                --><?php
                                //                                /** @todo make image popup
                                //                                    Show image for each cards
                                //                                 */
                                //                                echo $product->images[0]->displayMediaThumb('class="browseProductImage" border="0" title="' . $product->product_name . '" ', true, 'class="modal"');
                                //                                ?>
                                <!--                            </div>-->
                                <h3 class="catProductTitle"><?php echo JHTML::link($product->link, $product->product_name); ?></h3>


                            </td>
                            <td align="center">
                                <input
                                    name="checked_list[specialty][]"
                                    type="checkbox" value="<?php echo $product->virtuemart_product_id; ?>"
                                    style="display: block;"
                                    <?php echo is_array($this->chosenCards['specialty']) && in_array($product->virtuemart_product_id, $this->chosenCards['specialty']) ? 'checked="checked"' : ''; ?> >
                            </td>

                        </tr>
                        <?php
                        $iBrowseProduct++;
                    } // end of foreach ( $this->products as $product )
                    ?>
                    </tbody>
                </table>
                    </div>
                <?php
                $class = '';
                $checkIsTrade = false;
                if (isset($this->checkListType) && $this->checkListType === 'tradeList') {
                    $class = 'show-trade';
                    $checkIsTrade = true;
                }
                ?>
                <div class="formAction">
                    <div class="div-button-save <?php echo $class ?>" style="width: 100%;" >
                        <button type="button" class="button-save" style="float: left; width: 92px;"
                                onclick="saveSelectedCards()"><?php echo JText::_('Save'); ?></button>
                        <input type="submit" style="visibility: hidden;" name="save" value="1" id="submitSave"/>
                    </div>
                    <div class="div-button-print" style="margin: 10px 0; width: 100%;">
                        <button type="button" class="button-print" onclick="clearSomething()"
                                style="float: left; width: 92px;" ><?php echo JText::_('Clear'); ?></button>
                    </div>
                    <div style="margin: 10px 0; width: 100%;">
                        <a href="javascript:void(0);" style="margin-left: 0px; width: 32px;"
                           data-href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=tradingcart&Itemid=697'); ?>"
                           id="link-trade" onclick="buttonTradeClick()"><?php echo JText::_('Next'); ?></a>
                        <input type="submit" style="visibility: hidden" name="trade" value="1" id="submitTrade"/>
                    </div>
                </div>
                </div>
            <?php
            } else {
                echo JText::_('No Product');
            }
            //    } elseif ($this->search !== null)
            //        echo JText::_('COM_VIRTUEMART_NO_RESULT') . ($this->keyword ? ' : (' . $this->keyword . ')' : '')
            ?>
            <div class="clear"></div>
        </div>
        <!-- end browse-view -->

        <?php
        if($this->checkListType == 'tradeList'){
            ?>
            <div class="width90 floatleft">

                <a class="details step-cart"
                   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=' . $defaultCheckListId . '&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
                    <?php echo JText::_('Step 2:') . ' ' . JText::_('COM_VIRTUEMART_CART_YOU_HAVE'); ?><span
                        class="floatright"><?php echo JText::_('Modify'); ?>>></span>
                </a>

            </div>
        <?php
        }
        ?>
        <div class="width90 floatleft">

            <a class="details step-cart"
               href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=tradingcart&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
                <?php echo JText::_('Step 3:') . ' ' . JText::_('REVIEW_YOUR_TRADE'); ?><span
                    class="floatright"><?php echo JText::_('Modify'); ?>>></span>
            </a>

        </div>
        <div class="width90 floatleft">

            <a class="details step-cart" <?php if ($this->address_type == 'BT'){ ?>onclick="return false;"<?php } ?>
               href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=BT&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
                <?php echo JText::_('Step 4:') . ' ' . JText::_('COM_VIRTUEMART_USER_FORM_EDIT_BILLTO_LBL'); ?><span
                    class="floatright"><?php echo JText::_('Modify'); ?>>></span>
            </a>

            <input type="hidden" name="billto" value="<?php echo $this->cart->lists['billTo']; ?>"/>
        </div>

        <?php
        if ($this->address_type !== 'BT') {

            ?>
            <div class="width90 floatleft">
                <?php if (!isset($this->cart->lists['current_id'])) $this->cart->lists['current_id'] = 0; ?>
                <a class="details step-cart" <?php if ($this->address_type == 'ST'){ ?>onclick="return false;"<?php } ?>
                   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=ST&virtuemart_user_id[]=' . $this->cart->lists['current_id'] . '&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
                    <?php echo JText::_('Step 5:') . ' ' . JText::_('COM_VIRTUEMART_USER_FORM_ADD_SHIPTO_LBL'); ?><span
                        class="floatright"><?php echo JText::_('Modify'); ?>>></span>
                </a>

            </div>
        <?php
        }
        ?>
        <?php
        if ($this->address_type == 'BT') {
            ?>
            <div class="width90 floatleft">
                <?php if (!isset($this->cart->lists['current_id'])) $this->cart->lists['current_id'] = 0; ?>
                <a class="details step-cart" <?php if ($this->address_type == 'ST'){ ?>onclick="return false;"<?php } ?>
                   href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=user&task=editaddresscart&addrtype=ST&virtuemart_user_id[]=' . $this->cart->lists['current_id'] . '&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
                    <?php echo JText::_('Step 5:') . ' ' . JText::_('COM_VIRTUEMART_USER_FORM_ADD_SHIPTO_LBL'); ?><span
                        class="floatright"><?php echo JText::_('Modify'); ?>>></span>
                </a>

            </div>
        <?php
        }
        ?>
        <div class="width90 floatleft">

            <?php if (!isset($this->cart->lists['current_id'])) $this->cart->lists['current_id'] = 0; ?>
            <a class="details step-cart"
               href="<?php echo JRoute::_('index.php?view=cart&task=edit_shipment&Itemid=697', $this->useXHTML, $this->useSSL); ?>">
                <?php echo JText::_('Step 6:') . ' ' . JText::_('Delivery method'); ?><span
                    class="floatright"><?php echo JText::_('Modify'); ?>>></span>
            </a>

        </div>


        <div class="width90 floatleft">

            <?php if (!isset($this->cart->lists['current_id'])) $this->cart->lists['current_id'] = 0; ?>
            <a class="details step-cart"
               href="<?php echo JRoute::_('index.php?view=cart&task=editpayment&Itemid=697', $this->useXHTML, $this->useSSL) ?>">
                <?php echo JText::_('Step 7:') . ' ' . JText::_('Payment method'); ?><span
                    class="floatright"><?php echo JText::_('Modify'); ?>>></span>
            </a>

        </div>
        <div class="width90 floatleft">

            <a class="details step-cart"
               href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=cart&Itemid='.$_GET['Itemid'], $this->useXHTML, $this->useSSL) ?>">
                <?php echo JText::_('Step 8:') . ' ' . JText::_('CONFIRM_YOUR_TRADE'); ?><span
                    class="floatright"><?php echo JText::_('Modify'); ?>>></span>
            </a>

        </div>
    </form>
</div>
<script type="text/javascript">
    jQuery('#categoryImageUrl').val(jQuery('#teamCategoryImage').attr('src'));
    function saveSelectedCards() {
        jQuery('#submitSave').click();
    }
    function clearSomething() {
        jQuery('#listcards :checkbox').removeAttr('checked');
        jQuery('#submitSave').click();
    }
    function saveSelectedCardsOld(callback) {
        jQuery.ajax({
            url: 'index.php?option=com_virtuemart&view=categorychecklist&task=savechecklist&' + jQuery('#checkListForm').serialize(),
            success: function (data) {
                if (callback != null && callback == 'gotoTrading') {
                    gotoTrading();
                    return;
                }
                alert('Your selection have been saved successfully');

            },
            error: function () {
                alert('Can not save your selected cards. Please, try again later.')
            }
        });
    }

    function buttonTradeClick() {
        jQuery('#submitTrade').click();
    }

    function gotoTrading() {
        window.location = jQuery('#link-trade').attr('data-href');
    }
</script>
