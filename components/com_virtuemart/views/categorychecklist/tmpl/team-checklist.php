<?php
/**
 *
 * Show the products in a category
 *
 * @package	VirtueMart
 * @subpackage
 * @author RolandD
 * @author Max Milbers
 * @todo add pagination
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 6297 2012-07-24 19:19:34Z Milbo $
 */
//vmdebug('$this->category',$this->category);
vmdebug('$this->category ' . $this->category->category_name);
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.modal');
/* javascript for list Slide
  Only here for the order list
  can be changed by the template maker
 */
$js = "
jQuery(document).ready(function () {
	jQuery('.orderlistcontainer').hover(
		function() { jQuery(this).find('.orderlist').stop().show()},
		function() { jQuery(this).find('.orderlist').stop().hide()}
	)
});
";

$document = JFactory::getDocument();
$document->addScriptDeclaration($js);

$vmModel = new VirtueMartModelCategory;
//Get parent
$categoryParentCurrent = $vmModel->getParentCategory($this->category->virtuemart_category_id);
$catagories = array();
if (isset($categoryParentCurrent->virtuemart_category_id)) {
    $categoryParent = $vmModel->getCategoryParentByChild($categoryParentCurrent->virtuemart_category_id);

    if(isset($categoryParent->category_parent_id)) {
        $catagories = $vmModel->getChildCategoryList($categoryParent->virtuemart_vendor_id, $categoryParent->category_parent_id);
    }
}

?>

<div class="div-list-category" xmlns="http://www.w3.org/1999/html">
    <?php
    if(count($catagories) > 0) {
        $div = '';
        if($categoryParent->file_url) {
            $div = 'class="list_category_child"';
            ?>
            <div class="logo_category_parent">
                <img src="<?php echo JURI::root().$categoryParent->file_url?>">
            </div>
        <?php }?>
        <div <?php echo $div;?> >
            <form action="" id="submit-change-category" method="POST">
                <?php

                foreach ($catagories as $k => $c) {
                    $checked = '';

                    if($k != (count($catagories)-1))
                    {
                        ?>
                        <div class="div-categories">
                            <input type="radio" name="category" cateview="category" value="<?php echo $c->virtuemart_category_id?>"><span><?php echo $c->category_name;?></span>
                        </div>
                    <?php
                    }
                    else {
                        $childs = VirtueMartModelCategory::getChildCategoryList(1,$c->virtuemart_category_id);
                        if(count($childs) > 0) {
                            foreach ($childs as $child) {
                                $checked = '';
                                if($child->virtuemart_category_id === $this->category->virtuemart_category_id) {
                                    $checked = 'checked';
                                }
                                ?>
                                <div class="div-categories">
                                    <input type="radio" name="category" cateview="categorychecklist" value="<?php echo $child->virtuemart_category_id?>" <?php echo $checked; ?> ><span><?php echo $child->category_name;?></span>
                                </div>
                            <?php
                            }
                        }
                    }
                }
                ?>
            </form>
        </div>
    <?php }?>
</div>
<div class="page-categorychecklist">
<form id="checkListForm" action="" method="POST">
<div class="browse-view">
    <?php
    if ($this->category->category_name) {
        $nameCate = $this->category->category_name;
        if (!empty($categoryParent->category_name))
        {
            $nameCate .= " - ". $categoryParent->category_name;
        }
        ?>
        <h2><span><span><?php echo $nameCate; ?></span></span></h2>
    <?php } ?>
    <?php
    // Show child categories
    if (!empty($this->products)) {
// Category and Columns Counter
        $iBrowseCol = 1;
        $iBrowseProduct = 1;

// Calculating Products Per Row
        $BrowseProducts_per_row = $this->perRow;
        $Browsecellwidth = ' width' . floor(100 / $BrowseProducts_per_row);

// Separator
        $verticalseparator = " vertical-separator";

// Count products ?? why not just count ($this->products)  ?? note by Max Milbers
        $BrowseTotalProducts = 0;
        foreach ($this->products as $product) {
            $BrowseTotalProducts++;
        }
        ?>
        <input type="hidden" name="checkListType" value="<?php echo $this->checkListType; ?>"/>
        <input type="hidden" name="categoryId" value="<?php echo $this->teamCategory->virtuemart_category_id; ?>"/>
        <?php echo $this->teamCategory->images[0]->displayMediaThumb('id="teamCategoryImage", style="display: none;"', false); ?>
        <input type="hidden" id="categoryImageUrl" name="categoryImageUrl" value=""/>
        <?php
        // Start the Output
        foreach ($this->products as $product) {
            // Show the horizontal seperator
            if ($iBrowseCol == 1 && $iBrowseProduct > $BrowseProducts_per_row) {
                ?>
            <?php
            }

            // this is an indicator wether a row needs to be opened or not
            if ($iBrowseCol == 1) {
                ?>
                <div class="row">
            <?php
            }

            // Show the vertical seperator
            if ($iBrowseProduct == $BrowseProducts_per_row or $iBrowseProduct % $BrowseProducts_per_row == 0) {
                $show_vertical_separator = ' ';
            } else {
                $show_vertical_separator = $verticalseparator;
            }

            // Show Products
            ?>
            <div class="product floatleft<?php echo $Browsecellwidth . $show_vertical_separator ?>">
                <div class="spacer" style="text-align: center">
                    <div class="div-product">
                        <?php
                        /** @todo make image popup */
                        if (count($product->images) && is_object($product->images[0])) { 
                            echo $product->images[0]->displayMediaThumb('class="browseProductImage" border="0" title="' . $product->product_name . '" ', true, 'class="modal"'); 
                        } else {
//                            var_dump($product->images); 
                        }
                        //                            foreach ($product->childs as $playerCards) {
                        //                                echo $playerCards->images[0]->displayMediaThumb('class="browseProductImage" border="0" title="' . $product->product_name . '" ', true, 'class="modal"');
                        //                            }
                        ?>
                    </div>
                    <div>
                        <h3 class="catProductTitle"><?php echo JHTML::link($product->link, $product->product_name); ?></h3>

                        <div>
                            <div class="" style="display : block;">
                                <?php foreach ($product->childs as $child) { 
                                ?>
                                    <div style="display: inline-block;text-align: center;">
                                        <input
                                            name="checked_list[<?php echo $product->virtuemart_product_id; ?>][<?php echo substr($child->category_name, 0, 1); ?>]"
                                            type="checkbox" value="<?php echo $child->virtuemart_product_id; ?>"
                                            style="display: block;"
                                            <?php echo is_array($this->chosenCards[$product->virtuemart_product_id]) && in_array($child->virtuemart_product_id, $this->chosenCards[$product->virtuemart_product_id]) ? 'checked="checked"' : ''; ?> >
                                        <label><?php echo substr($child->category_name, 0, 1); ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            // Do we need to close the current row now?
            if ($iBrowseCol == $BrowseProducts_per_row || $iBrowseProduct == $BrowseTotalProducts) {
                ?>
                <div class="clear"></div>
                </div> <!-- end of row -->
                <?php
                $iBrowseCol = 1;
            } else {
                $iBrowseCol++;
            }

            $iBrowseProduct++;
        } // end of foreach ( $this->products as $product ) 
        ?>
                
                <?php
        // Specialty cards
        foreach ($this->specialtyCards as $product) {
            // this is an indicator wether a row needs to be opened or not
            if ($iBrowseCol == 1) {
                ?>
                <div class="row">
            <?php
            }

            // Show the vertical seperator
            if ($iBrowseProduct == $BrowseProducts_per_row or $iBrowseProduct % $BrowseProducts_per_row == 0) {
                $show_vertical_separator = ' ';
            } else {
                $show_vertical_separator = $verticalseparator;
            }

            // Show Products
            ?>
            <div class="product floatleft<?php echo $Browsecellwidth . $show_vertical_separator ?>">
                <div class="spacer" style="text-align: center">
                    <div class="div-product">
                        <?php
                        /** @todo make image popup */
                        echo $product->images[0]->displayMediaThumb('class="browseProductImage" border="0" title="' . $product->product_name . '" ', true, 'class="modal"');
                        //                            foreach ($product->childs as $playerCards) {
                        //                                echo $playerCards->images[0]->displayMediaThumb('class="browseProductImage" border="0" title="' . $product->product_name . '" ', true, 'class="modal"');
                        //                            }
                        ?>
                    </div>
                    <div>
                        <h3 class="catProductTitle"><?php echo JHTML::link($product->link, $product->product_name); ?></h3>

                        <div>
                            <div class="" style="display : block;">
                                <div style="display: inline-block;text-align: center;">
                                    <input
                                    name="checked_list[specialty][]"
                                    type="checkbox" value="<?php echo $product->virtuemart_product_id; ?>"
                                    style="display: block;"
                                    <?php echo is_array($this->chosenCards['specialty']) && in_array($product->virtuemart_product_id, $this->chosenCards['specialty']) ? 'checked="checked"' : ''; ?> >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            // Do we need to close the current row now?
            if ($iBrowseCol == $BrowseProducts_per_row || $iBrowseProduct == $BrowseTotalProducts) {
                ?>
                <div class="clear"></div>
                </div> <!-- end of row -->
                <?php
                $iBrowseCol = 1;
            } else {
                $iBrowseCol++;
            }

            $iBrowseProduct++;
        } // end of foreach ( $this->products as $product ) 
        ?>
<?php
// Do we need a final closing row tag?
        if ($iBrowseCol != 1) {
            ?>
            <div class="clear"></div>

        <?php
        }
        $class = '';
        $checkIsTrade = false;
        if(isset($this->checkListType) && $this->checkListType === 'tradeList') {
            $class = 'show-trade';
            $checkIsTrade = true;
        }

        $user = JFactory::getUser();
            ?>
            <div class="formAction">
                <?php
                if(!$user->guest) {
                ?>
                <div class="div-button-save <?php echo $class?>">
                    <button type="button" class="button-save"
                            onclick="saveSelectedCards()"><?php echo JText::_('Save');?></button>
                    <input type="submit" style="visibility: hidden;" name="save" value="1" id="submitSave"/>
                </div>
                <div class="div-button-print">
                    <!--                <button type="button" class="button-print">--><?php //echo JText::_('Print');
                    ?><!--</button>-->
                    <input type="submit" style="visibility: hidden" name="print" value="1" id="submitPrint"/>
                    <?php
                    if ($checkIsTrade) {
                        $categoryModel = VmModel::getModel('Category');
                        $defaultTradeListId = $categoryModel->getDefaultCheckListId('tradeList');
                        ?>
                        <a href="<?php echo JRoute::_(JURI::root() . 'index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id=' . $defaultTradeListId . '&Itemid=697'); ?>"
                           id="link-trade"><?php echo JText::_('Trade'); ?></a>
                        <!--<input type="submit" style="visibility: hidden" name="trade" value="1" id="submitTrade" />-->
                    <?php
                    }
                    ?>
                </div>
            </div>
        <?php
        } else {
            ?>
                <div id="div-message">
                    <a class="message-trading" href="<?php echo JRoute::_(JURI::root() . 'index.php?option=com_users&view=login&Itemid=512'); ?>"><?php echo JText::_('JGLOBAL_YOU_MUST_LOGIN_FIRST'); ?></a>
                </div>
        <?php
        }
    }else {
        echo JText::_('No Product');
    }
//    } elseif ($this->search !== null)
//        echo JText::_('COM_VIRTUEMART_NO_RESULT') . ($this->keyword ? ' : (' . $this->keyword . ')' : '')
        ?>
    <div class="clear"></div>
</div><!-- end browse-view -->
</form>
    </div>
<script type="text/javascript">
    jQuery('#categoryImageUrl').val(jQuery('#teamCategoryImage').attr('src'));
    function saveSelectedCards() {
        jQuery('#submitSave').click();
    }
    function saveSelectedCardsOld(callback) {
        jQuery.ajax({
            url: 'index.php?option=com_virtuemart&view=categorychecklist&task=savechecklist&' + jQuery('#checkListForm').serialize(),
            success: function(data) {
                if (callback != null && callback == 'gotoTrading') {
                    gotoTrading();
                    return;
                }
                alert('Your selection have been saved successfully');
                
            },
            error: function () {
                alert('Can not save your selected cards. Please, try again later.')
            }
        });
    }
    
    function buttonTradeClick() {
        jQuery('#submitTrade').click();
    }
    
    function gotoTrading() {
        window.location = jQuery('#link-trade').attr('data-href');
    }
</script>