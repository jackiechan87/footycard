<?php
/**
 *
 * Show the products in a category
 *
 * @package	VirtueMart
 * @subpackage
 * @author RolandD
 * @author Max Milbers
 * @todo add pagination
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 6297 2012-07-24 19:19:34Z Milbo $
 */
//vmdebug('$this->category',$this->category);
vmdebug('$this->category ' . $this->category->category_name);
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.modal');
/* javascript for list Slide
  Only here for the order list
  can be changed by the template maker
 */
$js = "
jQuery(document).ready(function () {
	jQuery('.orderlistcontainer').hover(
		function() { jQuery(this).find('.orderlist').stop().show()},
		function() { jQuery(this).find('.orderlist').stop().hide()}
	)
});
";

$document = JFactory::getDocument();
$document->addScriptDeclaration($js);

$vmModel = new VirtueMartModelCategory;
//Get parent
//$categoryParentCurrent = $vmModel->getParentCategory($this->category->virtuemart_category_id);
//$catagories = array();
//if (isset($categoryParentCurrent->virtuemart_category_id)) {
//    $categoryParent = $vmModel->getCategoryParentByChild($categoryParentCurrent->virtuemart_category_id);
//
//    if(isset($categoryParent->category_parent_id)) {
//        $catagories = $vmModel->getChildCategoryList($categoryParent->virtuemart_vendor_id, $categoryParent->category_parent_id);
//    }
//}

?>

<form id="checkListForm" action="" method="POST">
<div class="browse-view">
    <h2><span><span><?php echo $this->checkListType == 'checkList' ? 'My Cards' : 'Cards I Can Trade'; ?></span></span></h2>
    <?php 
    // Show child categories
    if (!empty($this->data)) {
?>
        <input type="hidden" name="checkListType" value="<?php echo $this->checkListType; ?>" />
        <input type="hidden" name="categoryId" value="<?php echo $this->teamCategory->virtuemart_category_id; ?>" />
<?php
        // Start the Output
foreach ($this->data as $teamId => $teamInfo) {
    if (!empty($teamInfo->products) && is_array($teamInfo->products)) {
        echo '<h3>' . $teamInfo->category_name . '</h3>';
        $chosenCards = $teamInfo->chosenCards;
    // Category and Columns Counter
            $iBrowseCol = 1;
            $iBrowseProduct = 1;

    // Calculating Products Per Row
            $BrowseProducts_per_row = $this->perRow;
            $Browsecellwidth = ' width' . floor(100 / $BrowseProducts_per_row);

    // Separator
            $verticalseparator = " vertical-separator";

    // Count products ?? why not just count ($this->products)  ?? note by Max Milbers
            $BrowseTotalProducts = 0;
            foreach ($teamInfo->products as $product) {
                $BrowseTotalProducts ++;
            }
            
        foreach ($teamInfo->products as $product) {
            // Show the horizontal seperator
            if ($iBrowseCol == 1 && $iBrowseProduct > $BrowseProducts_per_row) {
                ?>
                <?php
            }

            // this is an indicator wether a row needs to be opened or not
            if ($iBrowseCol == 1) {
                ?>
                <div class="row">
                    <?php
                }

                // Show the vertical seperator
                if ($iBrowseProduct == $BrowseProducts_per_row or $iBrowseProduct % $BrowseProducts_per_row == 0) {
                    $show_vertical_separator = ' ';
                } else {
                    $show_vertical_separator = $verticalseparator;
                }

                // Show Products 
                ?>
            <div class="product floatleft<?php echo $Browsecellwidth . $show_vertical_separator ?>">
                    <!--<div class="spacer" style="text-align: center">-->
                        <div>
                            <h7 class="catProductTitle" style="display: inline-block; width: 75px; text-align: left;">
                                <?php echo JHTML::link($product->link, $product->product_name); ?>
                            </h7>

                            <div style="display: block; padding-top: 5px;">
                                <div class="" style="display : block;">
                                <?php foreach ($product->childs as $child) { 
                                ?>
                                    <div style="display: inline-block;text-align: center;">
                                        <input name="checked_list[<?php echo $teamId; ?>][chosenCards][<?php echo $product->virtuemart_product_id; ?>][<?php echo substr($child->category_name, 0, 1); ?>]" type="checkbox" value="<?php echo $child->virtuemart_product_id; ?>" style="display: block;"
                                               <?php echo is_array($chosenCards[$product->virtuemart_product_id]) && in_array($child->virtuemart_product_id, $chosenCards[$product->virtuemart_product_id]) ? 'checked="checked"' : ''; ?> >
                                        <label><?php echo substr($child->category_name, 0, 1); ?></label> 
                                    </div>
                                <?php } ?>
                                </div>			
                            </div>
                        </div>
                    <!--</div>-->
                </div>
                <?php
                // Do we need to close the current row now?
                if ($iBrowseCol == $BrowseProducts_per_row || $iBrowseProduct == $BrowseTotalProducts) {
                    ?>
                    <div class="clear"></div>
                </div> <!-- end of row -->
                <?php
                $iBrowseCol = 1;
            } else {
                $iBrowseCol ++;
            }

            $iBrowseProduct ++;
        } // end of foreach ( $this->products as $product ) 
    }
}

// Start specialty section
echo '<h2 style="margin-top: 20px"><span><span>Specialty Cards</span></span></h2>';
        $chosenCards = $this->chosenSpecialtyCards;
foreach ($this->specialtyCategories as $teamInfo) {
    if (!empty($teamInfo->products) && is_array($teamInfo->products)) {
        echo '<h3>' . $teamInfo->category_name . '</h3>';
    // Category and Columns Counter
            $iBrowseCol = 1;
            $iBrowseProduct = 1;

    // Calculating Products Per Row
            $BrowseProducts_per_row = $this->perRow;
            $Browsecellwidth = ' width' . floor(100 / $BrowseProducts_per_row);

    // Separator
            $verticalseparator = " vertical-separator";

    // Count products ?? why not just count ($this->products)  ?? note by Max Milbers
            $BrowseTotalProducts = 0;
            foreach ($teamInfo->products as $product) {
                $BrowseTotalProducts ++;
            }
            
        foreach ($teamInfo->products as $product) {
            // Show the horizontal seperator
            if ($iBrowseCol == 1 && $iBrowseProduct > $BrowseProducts_per_row) {
                ?>
                <?php
            }

            // this is an indicator wether a row needs to be opened or not
            if ($iBrowseCol == 1) {
                ?>
                <div class="row">
                    <?php
                }

                // Show the vertical seperator
                if ($iBrowseProduct == $BrowseProducts_per_row or $iBrowseProduct % $BrowseProducts_per_row == 0) {
                    $show_vertical_separator = ' ';
                } else {
                    $show_vertical_separator = $verticalseparator;
                }

                // Show Products 
                ?>
            <div class="product floatleft<?php echo $Browsecellwidth . $show_vertical_separator ?>">
                    <!--<div class="spacer" style="text-align: center">-->
                        <div>
                            <h7 class="catProductTitle" style="display: inline-block; width: 75px; text-align: left;">
                                <?php echo JHTML::link($product->link, $product->product_name); ?>
                            </h7>

                            <div style="display: block; padding-top: 5px;">
                                <div class="" style="display : block;">
                                    <div style="display: inline-block;text-align: center;">
                                        <input name="checked_list[<?php echo $this->allSpecialtyCards[$product->virtuemart_product_id]; ?>][chosenCards][specialty][]"
                                    type="checkbox" value="<?php echo $product->virtuemart_product_id; ?>"
                                    style="display: block;"
                                    <?php echo is_array($chosenCards) && array_key_exists($product->virtuemart_product_id, $chosenCards) ? 'checked="checked"' : ''; ?> >
                                        
                                    </div>
                                </div>			
                            </div>
                        </div>
                    <!--</div>-->
                </div>
                <?php
                // Do we need to close the current row now?
                if ($iBrowseCol == $BrowseProducts_per_row || $iBrowseProduct == $BrowseTotalProducts) {
                    ?>
                    <div class="clear"></div>
                </div> <!-- end of row --> 
                <?php
                $iBrowseCol = 1;
            } else {
                $iBrowseCol ++;
            }

            $iBrowseProduct ++;
        } // end of foreach ( $this->products as $product ) 
    }
}
        ?>
        <?php
// Do we need a final closing row tag?
        if ($iBrowseCol != 1) {
            ?>
            <div class="clear"></div>

            <?php
        }
    } elseif ($this->search !== null)
        echo JText::_('COM_VIRTUEMART_NO_RESULT') . ($this->keyword ? ' : (' . $this->keyword . ')' : '')
        ?>
    <div class="clear"></div>
    <?php
        $class = '';
        $checkIsTrade = false;
        if(isset($this->checkListType) && $this->checkListType === 'tradeList') {
            $class = 'show-trade';
            $checkIsTrade = true;
        }
    ?>
    <div class="formAction">
        <div class="div-button-save <?php echo $class?>">
            <button type="button" class="button-save" onclick="saveSelectedCards()"><?php echo JText::_('Save');?></button>
            <input type="submit" style="visibility: hidden;" name="save" value="1" id="submitSave" />
        </div>
        <div class="div-button-print">
<!--            <button type="button" class="button-print">--><?php //echo JText::_('Print');?><!--</button>-->
            <input type="submit" style="visibility: hidden" name="print" value="1" id="submitPrint" />
            <?php
                if($checkIsTrade) {
                    $categoryModel = VmModel::getModel('Category');
                    $defaultTradeListId = $categoryModel->getDefaultCheckListId('tradeList');
            ?>
                    <a href="javascript:void(0);" data-href="<?php echo JRoute::_(JURI::root() . 'index.php?option=com_virtuemart&view=categorychecklist&virtuemart_category_id='.$defaultTradeListId.'&Itemid=697');?>" 
                       id="link-trade" onclick="buttonTradeClick()"><?php echo JText::_('Trade');?></a>
                    <input type="submit" style="visibility: hidden" name="trade" value="1" id="submitTrade" />
            <?php
                }
            ?>
        </div>
    </div>
</div><!-- end browse-view -->
</form>
<script type="text/javascript">
    jQuery('#categoryImageUrl').val(jQuery('#teamCategoryImage').attr('src'));
    function saveSelectedCards() {
        jQuery('#submitSave').click();
    }
    function saveSelectedCardsOld(callback) {
        jQuery.ajax({
            url: 'index.php?option=com_virtuemart&view=categorychecklist&task=savechecklist&' + jQuery('#checkListForm').serialize(),
            success: function(data) {
                if (callback != null && callback == 'gotoTrading') {
                    gotoTrading();
                    return;
                }
                alert('Your selection have been saved successfully');
                
            },
            error: function () {
                alert('Can not save your selected cards. Please, try again later.')
            }
        });
    }
    
    function buttonTradeClick() {
        jQuery('#submitTrade').click();
    }
    
    function gotoTrading() {
        window.location = jQuery('#link-trade').attr('data-href');
    }
</script>