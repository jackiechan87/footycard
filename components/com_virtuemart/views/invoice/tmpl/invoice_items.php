<?php
/**
*
* Order items view
*
* @package	VirtueMart
* @subpackage Orders
* @author Max Milbers, Valerie Isaksen
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: details_items.php 5432 2012-02-14 02:20:35Z Milbo $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

 if ( VmConfig::get('show_tax')) {
    $colspan=7;
 } else {
    $colspan=8;
 }
?>

<?php
if ($this->orderDetails['details']['BT']->tradewhat) {
$checkList = json_decode($this->orderDetails['details']['BT']->tradewhat, true);
$tradeList = json_decode($this->orderDetails['details']['BT']->tradefor, true);
$checkListQuantity = json_decode($this->orderDetails['details']['BT']->tradewhatquantity, true);
$tradeListQuantity = json_decode($this->orderDetails['details']['BT']->tradeforquantity, true);

$productModel = VmModel::getModel('Product');
$categoryModel = VmModel::getModel('Category');
?>
<br />
<table width="100%" cellspacing="0" cellpadding="0" style="border:1px solid gray;border-bottom:none;">
	<tr style="line-height:35px" align="left" class="sectiontableheader">
		<td style="background-color:#e1e1e1; border-right: 1px solid gray;border-bottom: 1px solid gray;" align="center"><strong><?php echo JText::_('COM_VIRTUEMART_I_WANT') ?></strong></td>
		<td style="background-color:#e1e1e1; border-right: 1px solid gray;border-bottom: 1px solid gray;" align="center"><strong><?php echo JText::_('Type') ?></strong></td>
		<td style="background-color:#e1e1e1; border-bottom: 1px solid gray;" align="center" width="11%"><strong><?php echo JText::_('Quantity') ?></strong></td>
	</tr>
	<?php foreach ($tradeList as $teamId => $teamData) {
		foreach ($teamData['chosenCards'] as $playerId => $cards) {
			foreach ($cards as $categoryName => $cardId) {
				$cardInfo = $productModel->getProduct($cardId);
				$categoryInfo = $categoryModel->getCategory($cardInfo->virtuemart_category_id);
				?>

				<tr style="line-height:30px" valign="top">
					<td width="60%" style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-left:1%;" align="left"><?php echo $cardInfo->product_name; ?></td>
					<td width="20%" style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="center"><?php echo $categoryInfo->category_name; ?></td>
					<td width="20%" style="border-bottom: 1px solid gray;padding-right:1%;" align="center"><?php echo (isset($tradeListQuantity[$cardId])
							&& $tradeListQuantity[$cardId] > 0) ? $tradeListQuantity[$cardId] : 1; ?></td>
				</tr>
			<?php }
		}
	} ?>
</table>
<br />
	<table width="100%" cellspacing="0" cellpadding="0" style="border:1px solid gray;border-bottom:none;">
	<tr style="line-height:35px" align="left" class="sectiontableheader">
		<td width="60%" style="background-color:#e1e1e1; border-right: 1px solid gray;border-bottom: 1px solid gray;" align="center"><strong><?php echo JText::_('COM_VIRTUEMART_I_WANT_TO_TRADE') ?></strong></td>
		<td width="20%" style="background-color:#e1e1e1; border-right: 1px solid gray;border-bottom: 1px solid gray;" align="center"><strong><?php echo JText::_('Type') ?></strong></td>
		<td width="20%" style="background-color:#e1e1e1; border-bottom: 1px solid gray;" align="center" width="11%"><strong><?php echo JText::_('Quantity') ?></strong></td>
	</tr>
	<?php foreach ($checkList as $teamId => $teamData) {
		foreach ($teamData['chosenCards'] as $playerId => $cards) {
			foreach ($cards as $categoryName => $cardId) {
				$cardInfo = $productModel->getProduct($cardId);
				$categoryInfo = $categoryModel->getCategory($cardInfo->virtuemart_category_id);
				?>

				<tr style="line-height:30px" valign="top">
					<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-left:1%;" align="left"><?php echo $cardInfo->product_name; ?></td>
					<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="center"><?php echo $categoryInfo->category_name; ?></td>
					<td style="border-bottom: 1px solid gray;padding-right:1%;" align="center"><?php echo (isset($checkListQuantity[$cardId])
							&& $checkListQuantity[$cardId] > 0) ? $checkListQuantity[$cardId] : 1; ?></td>
				</tr>
			<?php }
		}
	} ?>
</table>
	<br />
<?php
}?>
<table class="html-email" width="100%" cellspacing="0" cellpadding="0" style="border:1px solid gray; margin-top:10px;">
	<tr style="line-height:25px" align="left" class="sectiontableheader">
		<td style="background-color:#e1e1e1; border-right: 1px solid gray;border-bottom: 1px solid gray;" align="center" width="5%"><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_SKU') ?></strong></td>
		<td style="background-color:#e1e1e1; border-right: 1px solid gray;border-bottom: 1px solid gray;" align="center" colspan="2" width="38%" ><strong><?php echo JText::_('COM_VIRTUEMART_PRODUCT_NAME_TITLE') ?></strong></td>
		<td style="background-color:#e1e1e1; border-right: 1px solid gray;border-bottom: 1px solid gray;" align="center" width="10%"><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PRODUCT_STATUS') ?></strong></td>
		<td style="background-color:#e1e1e1; border-right: 1px solid gray;border-bottom: 1px solid gray;" align="center" width="10%" ><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PRICE') ?></strong></td>
		<td style="background-color:#e1e1e1; border-right: 1px solid gray;border-bottom: 1px solid gray;" align="center" width="6%"><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_QTY') ?></strong></td>
		<?php if ( VmConfig::get('show_tax')) { ?>
		<td style="background-color:#e1e1e1; border-right: 1px solid gray;border-bottom: 1px solid gray;" align="center" width="10%" ><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PRODUCT_TAX') ?></strong></td>
		  <?php } ?>
		<td style="background-color:#e1e1e1; border-right: 1px solid gray;border-bottom: 1px solid gray;" align="center" width="11%"><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_SUBTOTAL_DISCOUNT_AMOUNT') ?></strong></td>
		<td style="background-color:#e1e1e1; border-bottom: 1px solid gray;" align="center" width="11%"><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_TOTAL') ?></strong></td>
	</tr>

<?php
	foreach($this->orderDetails['items'] as $item) {
		$qtt = $item->product_quantity ;
		$product_link = JURI::root().'index.php?option=com_virtuemart&view=productdetails&virtuemart_category_id=' . $item->virtuemart_category_id .
			'&virtuemart_product_id=' . $item->virtuemart_product_id;

		?>
		<tr style="line-height:25px" valign="top">
			<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-left:1%;" align="left">
				<?php echo $item->order_item_sku; ?>
			</td>
			<td style="padding-left:1%; border-right: 1px solid gray;border-bottom: 1px solid gray;" align="left" colspan="2" >
				<a href="<?php echo $product_link; ?>"><?php echo $item->order_item_name; ?></a>
				<?php
// 				vmdebug('$item',$item);
					if (!empty($item->product_attribute)) {
							if(!class_exists('VirtueMartModelCustomfields'))require(JPATH_VM_ADMINISTRATOR.DS.'models'.DS.'customfields.php');
							$product_attribute = VirtueMartModelCustomfields::CustomsFieldOrderDisplay($item,'FE');
						echo $product_attribute;
					}
				?>
			</td>
			<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;" align="center">
				<?php echo $this->orderstatuses[$item->order_status]; ?>
			</td>
			<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"   class="priceCol" >
			    <?php echo '<span >'.$this->currency->priceDisplay($item->product_item_price) .'</span><br />'; ?>
			</td>
			<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right" >
				<?php echo $qtt; ?>
			</td>
			<?php if ( VmConfig::get('show_tax')) { ?>
				<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right" class="priceCol"><?php echo "<span  class='priceColor2'>".$this->currency->priceVerifiedDisplay($item->product_tax ,0, $qtt)."</span>" ?></td>
                                <?php } ?>
			<td style="border-right: 1px solid gray;border-bottom: 1px solid gray; padding-right:1%;" align="right" class="priceCol" >
				<?php echo  $this->currency->priceVerifiedDisplay( $item->product_subtotal_discount );  //No quantity is already stored with it ?>
			</td>
			<td style="border-bottom: 1px solid gray;padding-right:1%;" align="right"  class="priceCol">
				<?php
				$item->product_basePriceWithTax = (float) $item->product_basePriceWithTax;
				$class = '';
				if(!empty($item->product_basePriceWithTax) && $item->product_basePriceWithTax != $item->product_final_price ) {
					echo '<span class="line-through" >'.$this->currency->priceVerifiedDisplay($item->product_basePriceWithTax,0,$qtt) .'</span><br />' ;
				}

				echo $this->currency->priceVerifiedDisplay(  $item->product_subtotal_with_tax ,0); //No quantity or you must use product_final_price ?>
			</td>
		</tr>

<?php
	}
?>
<!--<tr style="line-height:25px"><td style="padding-right:1%;border-right: 1px solid gray;" colspan="--><?php //echo $colspan ?><!--"></td></tr>-->
 <tr style="line-height:25px" class="sectiontableentry1">
			<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" colspan="6" align="right"><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PRODUCT_PRICES_TOTAL'); ?></td>

                        <?php if ( VmConfig::get('show_tax')) { ?>
			<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"><?php echo "<span  class='priceColor2'>".$this->currency->priceVerifiedDisplay($this->orderDetails['details']['BT']->order_tax)."</span>" ?></td>
                        <?php } ?>
			<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"><?php echo "<span  class='priceColor2'>".$this->currency->priceVerifiedDisplay($this->orderDetails['details']['BT']->order_discountAmount)."</span>" ?></td>
			<td style="border-bottom: 1px solid gray;padding-right:1%;" align="right"><?php echo $this->currency->priceVerifiedDisplay($this->orderDetails['details']['BT']->order_salesPrice) ?></td>
		  </tr>
<?php
if ($this->orderDetails['details']['BT']->coupon_discount <> 0.00) {
    $coupon_code=$this->orderDetails['details']['BT']->coupon_code?' ('.$this->orderDetails['details']['BT']->coupon_code.')':'';
	?>
	<tr style="line-height:25px">
		<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right" class="pricePad" colspan="5"><?php echo JText::_('COM_VIRTUEMART_COUPON_DISCOUNT').$coupon_code ?></td>
			<td align="right"></td>

			<?php if ( VmConfig::get('show_tax')) { ?>
				<td align="right"> </td>
                                <?php } ?>
		<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"><?php echo '- '.$this->currency->priceVerifiedDisplay($this->orderDetails['details']['BT']->coupon_discount); ?></td>
		<td style="border-bottom: 1px solid gray;padding-right:1%;" align="right"></td>
	</tr>
<?php  } ?>


	<?php
		foreach($this->orderDetails['calc_rules'] as $rule){
			if ($rule->calc_kind== 'DBTaxRulesBill') { ?>
			<tr style="line-height:25px" >
				<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" colspan="6"  align="right" class="pricePad"><?php echo $rule->calc_rule_name ?> </td>

                                   <?php if ( VmConfig::get('show_tax')) { ?>
				<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"> </td>
                                <?php } ?>
				<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"> <?php echo  $this->currency->priceVerifiedDisplay($rule->calc_amount);  ?></td>
				<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"><?php echo  $this->currency->priceVerifiedDisplay($rule->calc_amount);  ?> </td>
			</tr>
			<?php
			} elseif ($rule->calc_kind == 'taxRulesBill') { ?>
			<tr style="line-height:25px" >
				<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" colspan="6"  align="right" class="pricePad"><?php echo $rule->calc_rule_name ?> </td>
				<?php if ( VmConfig::get('show_tax')) { ?>
				<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"><?php echo $this->currency->priceVerifiedDisplay($rule->calc_amount); ?> </td>
				 <?php } ?>
				<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"><?php    ?> </td>
				<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"><?php echo $this->currency->priceVerifiedDisplay($rule->calc_amount);   ?> </td>
			</tr>
			<?php
			 } elseif ($rule->calc_kind == 'DATaxRulesBill') { ?>
			<tr style="line-height:25px" >
				<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" colspan="6"   align="right" class="pricePad"><?php echo $rule->calc_rule_name ?> </td>
				<?php if ( VmConfig::get('show_tax')) { ?>
				<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"> </td>
				 <?php } ?>
				<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"><?php  echo   $this->currency->priceVerifiedDisplay($rule->calc_amount);  ?> </td>
				<td style="border-bottom: 1px solid gray;padding-right:1%;" align="right"><?php echo $this->currency->priceVerifiedDisplay($rule->calc_amount);  ?> </td>
			</tr>

			<?php
			 }

		}
		?>


	<tr style="line-height:25px">
		<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right" class="pricePad" colspan="6"><?php echo $this->orderDetails['shipmentName'] ?></td>

		<?php if ( VmConfig::get('show_tax')) { ?>
		<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"><span class='priceColor2'><?php echo $this->currency->priceVerifiedDisplay($this->orderDetails['details']['BT']->order_shipment_tax) ?></span> </td>
		<?php } ?>
		<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"></td>
		<td style="border-bottom: 1px solid gray;padding-right:1%;" align="right"><?php echo $this->currency->priceVerifiedDisplay($this->orderDetails['details']['BT']->order_shipment + $this->orderDetails['details']['BT']->order_shipment_tax); ?></td>
	</tr>

	<tr style="line-height:25px">
		<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right" class="pricePad" colspan="6"><?php echo $this->orderDetails['paymentName'] ?></td>

		<?php if ( VmConfig::get('show_tax')) { ?>
		<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"><span class='priceColor2'><?php echo $this->currency->priceVerifiedDisplay($this->orderDetails['details']['BT']->order_payment_tax) ?></span> </td>
		<?php } ?>
		<td style="border-right: 1px solid gray;border-bottom: 1px solid gray;padding-right:1%;" align="right"></td>
		<td style="border-bottom: 1px solid gray;padding-right:1%;" align="right"><?php echo $this->currency->priceVerifiedDisplay($this->orderDetails['details']['BT']->order_payment + $this->orderDetails['details']['BT']->order_payment_tax); ?></td>
	</tr>

	<tr style="line-height:25px">
		<td style="border-right: 1px solid gray;padding-right:1%;" align="right" class="pricePad" colspan="6"><strong><?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_TOTAL') ?></strong></td>

		<?php if ( VmConfig::get('show_tax')) { ?>
		<td style="border-right: 1px solid gray;padding-right:1%;" align="right"><span class='priceColor2'><?php echo $this->currency->priceVerifiedDisplay($this->orderDetails['details']['BT']->order_billTaxAmount); ?></span></td>
		<?php } ?>
		<td style="border-right: 1px solid gray;padding-right:1%;" align="right"><span class='priceColor2'><?php echo $this->currency->priceVerifiedDisplay($this->orderDetails['details']['BT']->order_billDiscountAmount); ?></span></td>
		<td style="padding-right:1%;" align="right"><strong><?php echo $this->currency->priceVerifiedDisplay($this->orderDetails['details']['BT']->order_total); ?></strong></td>
	</tr>

</table>