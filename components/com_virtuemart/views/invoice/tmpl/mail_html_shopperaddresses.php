<?php
/**
 *
 * Layout for the order email
 * shows the chosen adresses of the shopper
 * taken from the stored order
 *
 * @package	VirtueMart
 * @subpackage Order
 * @author Max Milbers,   Valerie Isaksen
 *
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 *
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>
<table class="html-email" cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td width="100%">
			<div style="width: 40%; border:1px solid gray; float: left; margin-right:5%;">
				<h1 style="border-bottom:1px solid gray ;margin:0px;padding-left: 2%;height: 30px;line-height: 30px; background-color: #e1e1e1;font-size: 110%;">
					<?php echo JText::_('COM_VIRTUEMART_USER_FORM_BILLTO_LBL'); ?>
				</h1>
				<div style="padding:10px 0px;">
					<?php
					$arrayKey = array('first_name', 'last_name', 'address_1', 'city', 'virtuemart_state_id', 'virtuemart_country_id', 'zip');
					$fullname = $this->escape($this->userfields['fields']['first_name']['value']). ' ' .$this->escape($this->userfields['fields']['last_name']['value']);
					$address = $this->escape($this->userfields['fields']['address_1']['value']);
					$region = $this->escape($this->userfields['fields']['city']['value']). ', ' .$this->escape($this->userfields['fields']['virtuemart_state_id']['value']);
					$country = $this->escape($this->userfields['fields']['virtuemart_country_id']['value']). ' ' .$this->escape($this->userfields['fields']['zip']['value']);

//					foreach ($this->userfields['fields'] as $field) {
//					if (!empty($field['value']) && ($field['name'] != 'email' && $field['name'] != 'DateofBirth' && $field['name'])) {
//					?><!--<!-- span class="titles">--><?php //echo $field['title'] ?><!--</span -->
<!--					<span style="margin-left: 2%" class="values vm2--><?php //echo '-' . $field['name'] ?><!--" >--><?php //echo $this->escape($field['value']) ?><!--</span>-->
<!--					--><?php //if ($field['name'] != 'title' and $field['name'] != 'first_name' and $field['name'] != 'middle_name' and $field['name'] != 'zip') { ?>
<!--						<br class="clear" />-->
<!--					--><?php
//					}
//					}
//
//					}

					?>
					<span style="margin-left: 2%" class="values" ><?php echo $fullname; ?></span><br />
					<span style="margin-left: 2%" class="values" ><?php echo $address; ?></span><br />
					<span style="margin-left: 2%" class="values" ><?php echo $region; ?></span><br />
					<span style="margin-left: 2%" class="values" ><?php echo $country; ?></span><br />
				</div>
			</div>
			<div style="width: 40%; border:1px solid gray; float: left; margin-right:5%;">
				<h1 style="border-bottom:1px solid gray ;margin:0px;padding-left: 2%;height: 30px;line-height: 30px; background-color: #e1e1e1;font-size: 110%;">
					<?php echo JText::_('COM_VIRTUEMART_USER_FORM_SHIPTO_LBL'); ?>
				</h1>
				<div style="padding:10px 0px;">
					<?php
					$shiptofullname = $this->escape($this->shipmentfields['fields']['first_name']['value']). ' ' .$this->escape($this->shipmentfields['fields']['last_name']['value']);
					$shiptoaddress = $this->escape($this->shipmentfields['fields']['address_1']['value']);
					$shiptoregion = $this->escape($this->shipmentfields['fields']['city']['value']). ', ' .$this->escape($this->shipmentfields['fields']['virtuemart_state_id']['value']);
					$shiptocountry = $this->escape($this->shipmentfields['fields']['virtuemart_country_id']['value']). ' ' .$this->escape($this->shipmentfields['fields']['zip']['value']);

//					foreach ($this->shipmentfields['fields'] as $field) {
//
//					if (!empty($field['value'])) {
//					?><!--<!-- span class="titles">--><?php //echo $field['title'] ?><!--</span -->
<!--					<span style="margin-left: 2%" class="values vm2--><?php //echo '-' . $field['name'] ?><!--" >--><?php //echo $this->escape($field['value']) ?><!--</span>-->
<!--					--><?php //if ($field['name'] != 'title' and $field['name'] != 'first_name' and $field['name'] != 'middle_name' and $field['name'] != 'zip') { ?>
<!--						<br class="clear" />-->
<!--					--><?php
//					}
//					}
//					}

					?>
					<span style="margin-left: 2%" class="values" ><?php echo $shiptofullname; ?></span><br />
					<span style="margin-left: 2%" class="values" ><?php echo $shiptoaddress; ?></span><br />
					<span style="margin-left: 2%" class="values" ><?php echo $shiptoregion; ?></span><br />
					<span style="margin-left: 2%" class="values" ><?php echo $shiptocountry; ?></span><br />
				</div>
			</div>

		</td>
<!--		<th width="50%">-->
<!--			--><?php //echo JText::_('COM_VIRTUEMART_USER_FORM_BILLTO_LBL'); ?>
<!--		</th>-->
<!--		<th width="50%" >-->
<!--			--><?php //echo JText::_('COM_VIRTUEMART_USER_FORM_SHIPTO_LBL'); ?>
<!--		</th>-->
		</tr>
<!--    <tr>-->
<!--	<td valign="top" width="50%">-->
<!---->
<!--	    --><?php
//
//	    foreach ($this->userfields['fields'] as $field) {
//		if (!empty($field['value'])) {
//			?><!--<!-- span class="titles">--><?php //echo $field['title'] ?><!--</span -->
<!--	    	    <span class="values vm2--><?php //echo '-' . $field['name'] ?><!--" >--><?php //echo $this->escape($field['value']) ?><!--</span>-->
<!--			--><?php //if ($field['name'] != 'title' and $field['name'] != 'first_name' and $field['name'] != 'middle_name' and $field['name'] != 'zip') { ?>
<!--			    <br class="clear" />-->
<!--			    --><?php
//			}
//		    }
//
//	    }
//	    ?>
<!---->
<!--	</td>-->
<!--	<td valign="top" width="50%">-->
<!--	    --><?php
//	    foreach ($this->shipmentfields['fields'] as $field) {
//
//		if (!empty($field['value'])) {
//			    ?><!--<!-- span class="titles">--><?php //echo $field['title'] ?><!--</span -->
<!--			    <span class="values vm2--><?php //echo '-' . $field['name'] ?><!--" >--><?php //echo $this->escape($field['value']) ?><!--</span>-->
<!--			    --><?php //if ($field['name'] != 'title' and $field['name'] != 'first_name' and $field['name'] != 'middle_name' and $field['name'] != 'zip') { ?>
<!--		    	    <br class="clear" />-->
<!--				--><?php
//			    }
//			}
//	    }
//
//	    ?>
<!--	</td>-->
<!--    </tr>-->
</table>

