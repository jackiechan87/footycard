<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 */
class TravelbookControllerBooking extends JControllerForm
{
	/**
	 * @since	1.6
	 */
	protected $view_item = 'bookingform';

	/**
	 * @since	1.6
	 */
	protected $view_list = 'categories';

	/**
	 * @since	1.6
	 */
	protected $text_prefix = 'COM_TRAVELBOOK_BOOKING';

	/**
	 * Method to add a new record.
	 *
	 * @return	boolean	True if the booking can be added, false if not.
	 * @since	1.6
	 */
	public function add()
	{
		if (!parent::add()) {
			// Redirect to the return page.
			$this->setRedirect($this->getReturnPage());
		}
	}

	/**
	 * Method override to check if you can add a new record.
	 *
	 * @param	array	An array of input data.
	 *
	 * @return	boolean
	 * @since	1.6
	 */
	protected function allowAdd($data = array())
	{
		// Initialise variables.
		$user = JFactory::getUser();
		$categoryId	= JArrayHelper::getValue($data, 'catid', JRequest::getInt('catid'), 'int');
		$allow = true;

		if ($categoryId) {
			// If the category has been passed in the data or URL check it.
			$allow = $user->authorise('core.create', 'com_travelbook.category.'.$categoryId);
		}

		if ($allow === null) {
			// In the absense of better information, revert to the component permissions.
			return parent::allowAdd();
		}
		else {
			return $allow;
		}
	}

	/**
	 * Method override to check if you can edit an existing record.
	 *
	 * @param	array	$data	An array of input data.
	 * @param	string	$key	The name of the key for the primary key.
	 *
	 * @return	boolean
	 * @since	1.6
	 */
	protected function allowEdit($data = array(), $key = 'id')
	{
		// Initialise variables.
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$user = JFactory::getUser();
		$userId = $user->get('id');
		$asset = 'com_travelbook.booking.'.$recordId;

		// Check general edit permission first.
		if ($user->authorise('core.edit', $asset)) {
			return true;
		}

		// Fallback on edit.own.
		// First test if the permission is available.
		if ($user->authorise('core.edit.own', $asset)) {
			// Now test the owner is the user.
			$ownerId = (int) isset($data['created_by']) ? $data['created_by'] : 0;
			if (empty($ownerId) && $recordId) {
				// Need to do a lookup from the model.
				$record = $this->getModel()->getItem($recordId);

				if (empty($record)) {
					return false;
				}

				$ownerId = $record->created_by;
			}

			// If the owner matches 'me' then do the test.
			if ($ownerId == $userId) {
				return true;
			}
		}

		// Since there is no asset tracking, revert to the component permissions.
		return parent::allowEdit($data, $key);
	}

	/**
	 * Method to cancel an edit.
	 *
	 * @param	string	$key	The name of the primary key of the URL variable.
	 *
	 * @return	Boolean	True if access level checks pass, false otherwise.
	 * @since	1.6
	 */
	public function cancel($key = 'a_id')
	{
		parent::cancel($key);

		// Redirect to the return page.
		$this->setRedirect($this->getReturnPage());
	}

	/**
	 * Method to edit an existing record.
	 *
	 * @param	string	$key	The name of the primary key of the URL variable.
	 * @param	string	$urlVar	The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
	 *
	 * @return	Boolean	True if access level check and checkout passes, false otherwise.
	 * @since	1.6
	 */
	public function edit($key = null, $urlVar = 'a_id')
	{
		$result = parent::edit($key, $urlVar);

		return $result;
	}

	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param	string	$name	The model name. Optional.
	 * @param	string	$prefix	The class prefix. Optional.
	 * @param	array	$config	Configuration array for model. Optional.
	 *
	 * @return	object	The model.
	 *
	 * @since	1.5
	 */
	public function getModel($name = 'bookingform', $prefix = '', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	/**
	 * Gets the URL arguments to append to an item redirect.
	 *
	 * @param	int		$recordId	The primary key id for the item.
	 * @param	string	$urlVar		The name of the URL variable for the id.
	 *
	 * @return	string	The arguments to append to the redirect URL.
	 * @since	1.6
	 */
	protected function getRedirectToItemAppend($recordId = null, $urlVar = 'a_id')
	{
		// Need to override the parent method completely.
		$append = '';

		$tmpl = JRequest::getCmd('tmpl');
		$layout = JRequest::getCmd('layout', 'edit');
		$itemId	= JRequest::getInt('Itemid');
		$dateId = JRequest::getInt('date_id');
		$catId = JRequest::getInt('catid', null, 'get');
		$return	= $this->getReturnPage();

		// Setup redirect info.
		$append .= $tmpl ? '&tmpl='.$tmpl : '';
		$append .= '&layout=edit';
		$append .= $recordId ? '&'.$urlVar.'='.$recordId : '';
		$append .= $dateId ? '&date_id='.$dateId : '';
		$append .= $itemId ? '&Itemid='.$itemId : '';
		$append .= $catId ? '&catid='.$catId : '';
		$append .= $return ? '&return='.base64_encode($return) : '';
		
		return $append;
	}

	/**
	 * Gets the URL arguments to append to an item redirect.
	 *
	 * @param	int		$recordId	The primary key id for the item.
	 * @param	string	$urlVar		The name of the URL variable for the id.
	 *
	 * @return	string	The arguments to append to the redirect URL.
	 * @since	1.6
	 */
	protected function getRedirectToTourAppend($recordId = null, $urlVar = 'a_id')
	{
		// Need to override the parent method completely.
		$append = '';

		$tmpl = JRequest::getCmd('tmpl');
		$layout = JRequest::getCmd('layout', 'edit');
		$itemId	= JRequest::getInt('Itemid');
		$dateId = JRequest::getInt('date_id');
		$catId = JRequest::getInt('catid', null, 'get');
		$return	= $this->getReturnPage();

		// Setup redirect info.
		$append .= $tmpl ? '&tmpl='.$tmpl : '';
		$append .= '&layout=edit';
		$append .= $recordId ? '&'.$urlVar.'='.$recordId : '';
		$append .= $dateId ? '&date_id='.$dateId : '';
		$append .= $itemId ? '&Itemid='.$itemId : '';
		$append .= $catId ? '&catid='.$catId : '';
		$append .= $return ? '&return='.base64_encode($return) : '';
		
		return $append;
	}

	/**
	 * Get the return URL.
	 *
	 * If a "return" variable has been passed in the request
	 *
	 * @return	string	The return URL.
	 * @since	1.6
	 */
	protected function getReturnPage()
	{
		$return = JRequest::getVar('return', null, 'default', 'base64');

		if (empty($return) || !JUri::isInternal(base64_decode($return))) {
			return JURI::base();
		}
		else {
			return base64_decode($return);
		}
	}

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 *
	 * @param	JModel	$model		The data model object.
	 * @param	array	$validData	The validated data.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function postSaveHook(JModel &$model, $validData = array())
	{
		$task = $this->getTask();

		$BID = $model->getState('bookingform.id', 0);
        JRequest::setVar('BID', $BID, 'get');
		
        $model->setState('date.id', JRequest::getInt('date_id'));
		$date = $model->getDate();
		$this->setMessage(JText::sprintf('COM_TRAVELBOOK_BOOKING_SUCCESS', '<em>'.$date->tour_title.'</em>'));

		if ($task == 'save') {
			$itemId	= JRequest::getInt('Itemid');
			$date->slug = $date->tour_alias ? ($date->tour_id.':'.$date->tour_alias) : $date->tour_id;
			$date->catslug = $date->tour_category_alias ? ($date->tour_catid.':'.$date->tour_category_alias) : $date->tour_catid;
			$this->setRedirect(JRoute::_('index.php?option=com_travelbook&view=tour&id='.$date->slug.'&catid='.$date->catslug.($itemId ? '&Itemid='.$itemId : ''), false));
//			$this->setRedirect(JRoute::_('index.php?option=com_travelbook&view=tour'.$this->getRedirectToTourAppend(), false));
		}
	}

	/**
	 * Method to save a record.
	 *
	 * @param	string	$key	The name of the primary key of the URL variable.
	 * @param	string	$urlVar	The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
	 *
	 * @return	Boolean	True if successful, false otherwise.
	 * @since	1.6
	 */
	public function save($key = null, $urlVar = 'a_id')
	{
		// Load the backend helper for filtering.
		require_once JPATH_ADMINISTRATOR.'/components/com_travelbook/helpers/travelbook.php';
		
		$params = JComponentHelper::getParams('com_travelbook');
		
		// lets save the jform
        $jform = JRequest::getVar('jform', array(), 'post', 'none', 'array');
        JRequest::setVar('jform', $jform, 'get');

        $this->_model = 'bookingform';
        $this->context = 'bookingform';
        
        // firstly try to save the client. because we need the id for the booking
		$result = $this->saveClient($key, $urlVar);

		// secondly try to save the booking. because we need the id for the guests
		$jform['CID'] = JRequest::getInt('CID', 0, 'get');
        JRequest::setVar('jform', $jform, 'post');
		if (!$result) {
			$this->setMessage($this->getError(), 'error');
			$app = JFactory::getApplication();
			$app->setUserState('com_travelbook.edit.bookingform.data', $jform);
		} else {
			$result = parent::save($key, $urlVar);
		}

		// thirdly try to save the guests. because we need the id for the clients<<>>guests relation
		if (!$result) {
			$this->setMessage($this->getError(), 'error');
			$app = JFactory::getApplication();
			$app->setUserState('com_travelbook.edit.bookingform.data', $jform);
		} else {
			$result = $this->saveGuests($key, $urlVar);
		}
		
		// fourthly try to save the clients<<>>guests relation.
		if ($result) {
		}
		
		// fifthly try to save the clients<<>>bookings relation.
		if ($result) {
		}
		
		// now we send emails.
		if ($result && $params->get('send_email')) {
			$result = $this->submit();
		}
		
		// lastly we reduce the availability in the date.
		if ($result) {
			$date_id = (int)JRequest::getVar('date_id', '', 'get', 'string');
			$number_guests = 0;
			foreach ($jform['guests']['selected'] as $selection) {
				if ($selection === '1') {
					$number_guests++;					
				}
			}
			$result = $this->getModel('date')->reduceAvailability($date_id, $number_guests);
		}
		
		// If ok, redirect to the return page.
		if (!$result) {
			$this->setRedirect(JRoute::_('index.php?option=com_travelbook&view=bookingform'.$this->getRedirectToItemAppend(), false));
//			$this->setRedirect($this->getReturnPage());
		}

		return $result;
	}

	/**
	 * Method to save all guests records.
	 *
	 * @param	string	$key	The name of the primary key of the URL variable.
	 * @param	string	$urlVar	The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
	 *
	 * @return	Boolean	True if successful, false otherwise.
	 * @since	1.6
	 */
	protected function saveGuests($key = null, $urlVar = 'a_id')
	{
		$BID = JRequest::getInt('BID', 0, 'get');
        $jform = JRequest::getVar('jform', array(), 'post', 'none', 'array');

        require_once JPATH_COMPONENT.'/controllers/guest.php';
		$guestController = new TravelbookControllerGuest();

		$this->context = 'guestform';
		$this->_model = 'guestform';

		$guests = array();
        foreach ($jform['guests'] as $keys => $values) {
	        foreach ($values as $key => $value) {
        		$guests[$key][$keys] = $value;
        	}
        }

		foreach ($guests as $key => $guest) {
	        if (($guest['selected'] !== '1')) {
	        	unset($guests[$key]);
		    } 
		}
        
        $result = true;
        foreach ($guests as $guest) {
			if ($result) {
	        	$guest['book_id'] = $BID;
				JRequest::setVar('jform', $guest, 'post');
				$result = $guestController->save();
			} else {
				break;
			}
		}

		if (!$result) {
			$this->setError(JText::_('COM_TRAVELBOOK_BOOKING_ERROR_GUEST_UNIQUE_ALIAS'));
			$this->setMessage($this->getError(), 'error');
			$app = JFactory::getApplication();
			$app->setUserState('com_travelbook.edit.bookingform.data', $jform);
		}

		return $result;
	}
	
	/**
	 * Method to save a client record.
	 *
	 * @param	string	$key	The name of the primary key of the URL variable.
	 * @param	string	$urlVar	The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
	 *
	 * @return	Boolean	True if successful, false otherwise.
	 * @since	1.6
	 */
	protected function saveClient($key = null, $urlVar = 'a_id')
	{
        $jform = JRequest::getVar('jform', array(), 'get', 'none', 'array');
		
        require_once JPATH_COMPONENT.'/controllers/client.php';
        $clientController = new TravelbookControllerClient();

        $this->_model = 'clientform';
        $this->context = 'clientform';
        JRequest::setVar('jform', $jform['client'], 'post');
        $result = $clientController->save();
		
		if (!$result) {
			$this->setError($clientController->getError());
			$this->setMessage($this->getError(), 'error');		
			$app = JFactory::getApplication();
			$app->setUserState('com_travelbook.edit.bookingform.data', $jform);
		}

		return $result;
	}

	public function submit()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$app = JFactory::getApplication();
		$config = array('ignore_request' => false);
		$dateModel = $this->getModel('date', 'travelbookModel', $config);
		$emailModel = $this->getModel('email', 'travelbookModel', $config);
		$emailsModel = $this->getModel('emails', 'travelbookModel', $config);
		$params = JComponentHelper::getParams('com_travelbook');

		// Get the data from GET
		$date_id = (int)JRequest::getVar('date_id', '', 'get', 'string');
		$data = JRequest::getVar('jform', array(), 'get', 'array');
		
		// Get the date and tour
		$date = $dateModel->getItem($date_id);
		$emailsForDate = $this->_allEmails($date);
		
		// Get all email items
		$emails = $emailsModel->getItems();
		
		// Filter all emails with relation in attribs
		foreach ($emails as $key => $email)
		{
			if (!in_array($email->id, $emailsForDate))
			{
				unset($emails[$key]);
			}	
		}
		
		// Check for a valid session cookie
		if($params->get('validate_session', true)) {
			if(JFactory::getSession()->getState() != 'active'){
				JError::raiseWarning(403, JText::_('COM_TRAVELBOOK_SESSION_INVALID'));
				return false;
			}
		}

		// Contact plugins
		JPluginHelper::importPlugin('travelbook');
		$dispatcher	= JDispatcher::getInstance();

		// Send the email
		$sent = false;
		foreach ($emails as $email) {
			$results = $dispatcher->trigger('onSubmitEmail', array('booking', &$email, &$data));
			$sent = $this->_sendEmail($data, $email);
		}
		
		// Set the success message if it was a success
		if (!($sent instanceof Exception)) {
			$msg = JText::_('COM_CONTACT_EMAIL_THANKS');
		} else {
			$msg = '' ;
		}

		return true;
	}

	private function _allEmails($date)
	{
		$array = array();

		$haystack = array('category_tour_attribs', 'tour_attribs', 'category_date_attribs', 'attribs'); 
		 
		foreach ($haystack as $needdle) {
			$params = new JRegistry($date->$needdle);
			$emails = $params->get('email_ids');
			if (isset($emails))
			{
				if (is_array($emails))
				{
					$array = array_merge ($array, $emails);
				}
				else 
				{
					$array[] = $emails;
				}
			}
		}
		
		return $array;
	}
	
	private function _sendEmail($data, $email)
	{
			$app = JFactory::getApplication();
			$params = new JRegistry($email->attribs);
			
			if ($id = $params->get('contact_id'))
			{
				$this->addModelPath('components/com_contact/models', 'ContactModel');
				$contactModel = $this->getModel('contact', 'ContactModel');
				$contactModel->setState('params', new JRegistry());
				$contact = $contactModel->getItem($id);
			} 
			else
			{
				return false;
			}
			
			$sitename = $app->getCfg('sitename');
			
			
			// Send E-Mail to Client
			$mail = JFactory::getMailer();
			
			$from = $params->get('from');
			$fromName = $params->get('from_name');
			
			$recipient = ($params->get('recipient') == 'client') ? $data['client']['email'] : $contact->email_to;
			$subject = $sitename.': '.$email->subject;
			$body = $data['client']['mode'] === 'HTML' ? stripslashes($email->mailbody_html) : stripslashes($email->mailbody_plain);
			$mode = $data['client']['mode'] === 'HTML' ? true : false;

			$cc = $params->get('cc', '') != '' ? explode(',', $params->get('cc')) : null;
			$bcc = $params->get('bcc', '')  != '' ? explode(',', $params->get('bcc')) : null;
			$attachment = null;
			$replyTo = $params->get('reply_to', '') != '' ? $params->get('reply_to') : null;
			$replyToName = $params->get('reply_to_name', '') != '' ? $params->get('reply_to_name') : null;
			
	 		$sent = $mail->sendMail($from, $fromName, $recipient, $subject, $body, $mode, $cc, $bcc, $attachment, $replyTo, $replyToName);
			
			// check whether email copy function activated
			if ($params->get('email_copy', false)) {
				$mail = JFactory::getMailer();
				
				$from = $params->get('from');
				$fromName = $params->get('from_name');
				$recipient = $params->get('from');
				$subject = JText::sprintf('COM_TRAVELBOOK_EMAIL_COPYSUBJECT_OF', $email->subject);
				$body = $params->get('mode', 'plain_text') === 'HTML' ? stripslashes($email->mailbody_html) : stripslashes($email->mailbody_plain);
				$copytext = JText::sprintf('COM_TRAVELBOOK_EMAIL_COPYTEXT_OF', $data['client']['firstname'].' '.$data['client']['lastname'], $sitename);
				$copytext .= "\r\n\r\n".$body;
				
				$mode = $params->get('mode', 'plain_text') === 'HTML' ? true : false;

	 			$sent = $mail->sendMail($from, $fromName, $recipient, $subject, $copytext, $mode);
			}
			
			return $sent;
	}
}
