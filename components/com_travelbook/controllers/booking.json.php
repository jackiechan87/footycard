<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');
jimport('demopage.currency');

/**
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 */
class TravelbookControllerBooking extends JControllerForm
{
	/**
	 * Method to render currencies.
	 *
	 * @return	void
	 * @since	2.1
	 */
	function render()
	{
		$return = array();

		$values = JRequest::getVar('values', array(), 'get', 'array');
		
		// Currency
		$params = JComponentHelper::getParams( 'com_travelbook' );
		$currency = new DPCurrency($params);

		$return = array();
		foreach ($values as $key => $value) {
        	$return[$key] = $currency->calculate($value/100);
		}
        
		// Send the response.
		echo json_encode($return);
		JFactory::getApplication()->close();
	}
}
