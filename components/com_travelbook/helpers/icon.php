<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Travelbook Component HTML Helper
 *
 * @static
 * @package		Joomla.Site
 * @subpackage	com_travelbook
 * @since 1.5
 */
class JHtmlIcon
{
	static function create($category, $params)
	{
		$uri = JFactory::getURI();

		$url = 'index.php?option=com_travelbook&task=tour.add&return='.base64_encode($uri).'&a_id=0&catid=' . $category->id;

		if ($params->get('show_icons')) {
			$text = JHtml::_('image', 'system/new.png', JText::_('JNEW'), NULL, true);
		} else {
			$text = JText::_('JNEW').'&#160;';
		}

		$button =  JHtml::_('link', JRoute::_($url), $text);

		$output = '<span class="hasTip" title="'.JText::_('COM_TRAVELBOOK_CREATE_TOUR').'">'.$button.'</span>';
		return $output;
	}

	static function email($tour, $params, $attribs = array())
	{
		require_once JPATH_SITE . '/components/com_mailto/helpers/mailto.php';
		$uri	= JURI::getInstance();
		$base	= $uri->toString(array('scheme', 'host', 'port'));
		$template = JFactory::getApplication()->getTemplate();
		$link	= $base.JRoute::_(TravelbookHelperRoute::getTourRoute($tour->slug, $tour->catid) , false);
		$url	= 'index.php?option=com_mailto&tmpl=component&template='.$template.'&link='.MailToHelper::addLink($link);

		$status = 'width=400,height=350,menubar=yes,resizable=yes';

		if ($params->get('show_icons')) {
			$text = JHtml::_('image', 'system/emailButton.png', JText::_('JGLOBAL_EMAIL'), NULL, true);
		} else {
			$text = '&#160;'.JText::_('JGLOBAL_EMAIL');
		}

		$attribs['title']	= JText::_('JGLOBAL_EMAIL');
		$attribs['onclick'] = "window.open(this.href,'win2','".$status."'); return false;";

		$output = JHtml::_('link', JRoute::_($url), $text, $attribs);
		return $output;
	}

	/**
	 * Display an edit icon for the tour.
	 *
	 * This icon will not display in a popup window, nor if the tour is trashed.
	 * Edit access checks must be performed in the calling code.
	 *
	 * @param	object	$tour	The tour in question.
	 * @param	object	$params		The tour parameters
	 * @param	array	$attribs	Not used??
	 *
	 * @return	string	The HTML for the tour edit icon.
	 * @since	1.6
	 */
	static function edit($tour, $params, $attribs = array())
	{
		// Initialise variables.
		$user	= JFactory::getUser();
		$userId	= $user->get('id');
		$uri	= JFactory::getURI();

		// Ignore if in a popup window.
		if ($params && $params->get('popup')) {
			return;
		}

		// Ignore if the state is negative (trashed).
		if ($tour->state < 0) {
			return;
		}

		JHtml::_('behavior.tooltip');

		// Show checked_out icon if the tour is checked out by a different user
		if (property_exists($tour, 'checked_out') && property_exists($tour, 'checked_out_time') && $tour->checked_out > 0 && $tour->checked_out != $user->get('id')) {
			$checkoutUser = JFactory::getUser($tour->checked_out);
			$button = JHtml::_('image', 'system/checked_out.png', NULL, NULL, true);
			$date = JHtml::_('date', $tour->checked_out_time);
			$tooltip = JText::_('JLIB_HTML_CHECKED_OUT').' :: '.JText::sprintf('COM_TRAVELBOOK_CHECKED_OUT_BY', $checkoutUser->name).' <br /> '.$date;
			return '<span class="hasTip" title="'.htmlspecialchars($tooltip, ENT_COMPAT, 'UTF-8').'">'.$button.'</span>';
		}

		$url	= 'index.php?option=com_travelbook&task=tour.edit&a_id='.$tour->id.'&return='.base64_encode($uri);
		$icon	= $tour->state ? 'edit.png' : 'edit_unpublished.png';
		$text	= JHtml::_('image', 'system/'.$icon, JText::_('JGLOBAL_EDIT'), NULL, true);

		if ($tour->state == 0) {
			$overlib = JText::_('JUNPUBLISHED');
		}
		else {
			$overlib = JText::_('JPUBLISHED');
		}

		$date = JHtml::_('date', $tour->created);
		$author = $tour->created_by_alias ? $tour->created_by_alias : $tour->author;

		$overlib .= '&lt;br /&gt;';
		$overlib .= $date;
		$overlib .= '&lt;br /&gt;';
		$overlib .= JText::sprintf('COM_TRAVELBOOK_WRITTEN_BY', htmlspecialchars($author, ENT_COMPAT, 'UTF-8'));

		$button = JHtml::_('link', JRoute::_($url), $text);

		$output = '<span class="hasTip" title="'.JText::_('COM_TRAVELBOOK_EDIT_ITEM').' :: '.$overlib.'">'.$button.'</span>';

		return $output;
	}


	static function print_popup($tour, $params, $attribs = array())
	{
		$url  = TravelbookHelperRoute::getTourRoute($tour->slug, $tour->catid);
		$url .= '&tmpl=component&print=1&layout=default&page='.@ $request->limitstart;

		$status = 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no';

		// checks template image directory for image, if non found default are loaded
		if ($params->get('show_icons')) {
			$text = JHtml::_('image', 'system/printButton.png', JText::_('JGLOBAL_PRINT'), NULL, true);
		} else {
			$text = JText::_('JGLOBAL_ICON_SEP') .'&#160;'. JText::_('JGLOBAL_PRINT') .'&#160;'. JText::_('JGLOBAL_ICON_SEP');
		}

		$attribs['title']	= JText::_('JGLOBAL_PRINT');
		$attribs['onclick'] = "window.open(this.href,'win2','".$status."'); return false;";
		$attribs['rel']		= 'nofollow';

		return JHtml::_('link', JRoute::_($url), $text, $attribs);
	}

	static function print_screen($tour, $params, $attribs = array())
	{
		// checks template image directory for image, if non found default are loaded
		if ($params->get('show_icons')) {
			$text = JHtml::_('image', 'system/printButton.png', JText::_('JGLOBAL_PRINT'), NULL, true);
		} else {
			$text = JText::_('JGLOBAL_ICON_SEP') .'&#160;'. JText::_('JGLOBAL_PRINT') .'&#160;'. JText::_('JGLOBAL_ICON_SEP');
		}
		return '<a href="#" onclick="window.print();return false;">'.$text.'</a>';
	}

	static function icalSmall($event, $dates, $params, $attribs = array())
	{
		$departure = is_array($dates) ? $dates[0]->departure : $dates;
        $print = JRequest::getBool('print');

        $url = TravelbookHelperRoute::getTourRoute($event->slug, $event->catid);
		$url .= '&tmpl=component&ical=1&layout=ical&format=ical';

		if ($params->get('show_icons')) {
			$text = '<span class="ical-calendar small">';
			$text .= '<span class="ical-weekday small">'.JHtml::_('date', $departure, 'D').'</span>';
			$text .= '<span class="ical-day small">'.JHtml::_('date', $departure, 'j').'</span>';
			$text .= '</span>';
		} else {
			$text = JText::_('JGLOBAL_ICON_SEP') .'&#160;'. JText::_('JGLOBAL_ICAL') .'&#160;'. JText::_('JGLOBAL_ICON_SEP');
		}

		$attribs['title'] = JText::_('COM_TRAVELBOOK_ICAL');

		$output = $print ? '' : JHtml::_('link', $url, $text, $attribs);

		return $output;

	}

	static function icalXXL($event, $dates, $params, $attribs = array())
	{
        $ical = JRequest::getBool('ical');

        $url = TravelbookHelperRoute::getTourRoute($event->slug, $event->catid);
		$url .= '&tmpl=component&ical=1&layout=ical&format=ical';

		$text = '<span class="ical-calendar">';
		$text .= '<span class="ical-weekday">'.JHtml::_('date', $dates[0]->departure, 'l').'</span>';
		$text .= '<span class="ical-day">'.JHtml::_('date', $dates[0]->departure, 'j').'</span>';
		$text .= '</span>';

		$attribs['title'] = JText::_('COM_TRAVELBOOK_ICAL');

		$output = $print ? '' : JHtml::_('link', $url, $text, $attribs).'<span class="ical-caption">iCal</span>';

		return $output;

	}
}
