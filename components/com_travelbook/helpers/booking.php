<?php
/**
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 *
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * TRAVELbook Booking Helper
 *
 * @package		Travelbook.Administrator
 * @subpackage	com_travelbook
 * @since 2.2
 */
final class TravelbookHelpersBooking
{
	
	/**
	 * @since	2.2
	 */
	private static $instance = NULL;
	public $_config = array();
	private $_lang = 'en-gb';

	public static $bar = 1;
	
	/**
	 * Constructor
	 *
	 * @since	1.1
	 */
	private function __construct($params) 
	{
		$this->_params = $params;

		$this->_config = array(1 => false, 2 => false, 3 => false);
		foreach ($this->_params->get('steps', array(1,3)) as $step) {
			$this->_config[$step] = true;
		}

		$this->_currency = new DPCurrency($this->_params);

		$conf = JFactory::getConfig();
		$this->_local = in_array($conf->get('language', 'en-GB'), array('en-GB', 'de-DE')) ? strtolower($conf->get('language', 'en-GB')) : 'en-gb';
	}

	// Diese statische Methode gibt die Instanz zurueck.
	public static function getInstance($params) {
		if (NULL === self::$instance)
		{
			self::$instance = new self($params);
		}
		
		return self::$instance;
	}

	private function __clone() {}

	/**
	 * Method to render the navigation.
	 *
	 * @since	1.1
	 */
	public function &renderNavigation( $prev, $current, $next )
	{

		$name = array(JText::_('TB_NAVIGATION_CONTACT'), JText::_('TB_NAVIGATION_SERVICES'), JText::_('TB_NAVIGATION_SUMMARY'));

		$class = array("current", "inactive", "inactive");
		for ($i = 1; $i < $current; $i++) {
			array_unshift($class, "active" );
		}

		$onClick = array("", "", "");
		for ($i = ($current+1); $i > 1; $i--) {
			array_unshift($onClick, "onclick='collapseElem(\"mainForm_". $current . "\"); expandElem(\"mainForm_" . ($i-1) . "\");'" );
		}

		$navigation = "<div class='navigation'>";
		$navigation .= "<ul class='tabs-nav'>";
		for ($i = 0; $i < 3; $i++) {
			if ($this->_config[$i+1]) {
				$navigation .= "<li class='" . $class[$i] . "' " . $onClick[$i] . ">" . $name[$i] . "</li>";
			}
		}
		$navigation .= "</ul>";
		$navigation .= "<div class='clear'></div>";
		$navigation .= "</div>";

		return $navigation;
	}

	/**
	 * Method to get the Previous and Next Buttons.
	 *
	 * @since	1.1
	 */
	public function &renderButtons( $current, $prev, $next )
	{

		$onClickPrevious = 'collapseElem("mainForm_'.$current.'"); expandElem("mainForm_'.$prev.'");';

		$onClickNext = '';
		$onClickNext .= 'return myValidate("mainForm_'.$current.'", "mainForm_'.$next.'", "'.JText::_( 'TB_VALIDATION_MESSAGE', true ).'");';

		$buttons = "<div class='navigation'>";
		if ($current > 1) {
			$buttons .= "<a id='previous_" . $current . "' tabindex='" . ($current*100) . "' name='previous_" . $current . "' class='button left' onclick='" . $onClickPrevious . "'>";
			$buttons .= "<span class='sd-right'>" . JText::_('TB_PREVIOUS') . "</span>";
			$buttons .= "</a>";

		}
		if ($current < 3) {
			$buttons .= "<a id='next_" . $current . "' tabindex='" . ($current*100+99) . "' name='next_" . $current . "' class='button right' onclick='" . $onClickNext . "'>";
			$buttons .= "<span class='sd-right'>" . JText::_('TB_NEXT') . "</span>";
			$buttons .= "</a>";
		}
		if ($current == 3) {
			$buttons .= "<button tabindex='" . ($current*100+98) . "' class='button right' type='submit' onclick='return Joomla.booknow(\"booking.save\")'>";
			$buttons .= "<span class='sd-right'>" . JText::_('TB_APPLY') . "</span>";
			$buttons .= "</button>";

			$buttons .= "<button tabindex='" . ($current*100+99) . "' class='button right' type='button' onclick='Joomla.booknow(\"booking.cancel\")'>";
			$buttons .= "<span class='sd-right'>" . JText::_('TB_CANCEL') . "</span>";
			$buttons .= "</button>";
		}
		$buttons .= "</div>";

		return $buttons;
	}

	/**
	 * Method to calculate the total Price.
	 *
	 * @since	1.1
	 */
	public function &calcTotalPrice($selection, $rackRate, $singleRate = 0)
	{

		$totalPrice = $selection * $rackRate + ($selection % 2)*$singleRate;

		return $totalPrice;
	}

	/**
	 * Method to render the optional extras.
	 *
	 * @since	1.1
	 */
	public function &renderExtras($extra, $date)
	{
		$categories = JCategories::getInstance('travelbook.calculation', array());
		
		$output = '';
		$output .= '<span class="extras-col1">';
		$output .= '<input type="checkbox" id="extra-' . $extra->tours_extras_id . '" class="cb" name="jform[extras][]" value="' . (int)$extra->id . '"/>';
		$output .= $this->renderExtra($date, $extra);
		$output .= '</span>';

		$output .= '<span class="extras-col2 filler">';
		$output .= $this->renderAjax(0, 'extra-' . $extra->tours_extras_id . '-rate');
		$output .= '</span>';
		
		$output .= '<span class="extras-col3 filler">';
		$output .= '<label class="extra" for="extra-' . $extra->tours_extras_id . '"><span class="text">' . strip_tags($extra->introtext) . '</span>&nbsp;';
		$output .= '<span class="pricing-plan">(';
		$output .= '<span class="rate">' . $this->_currency->calculate($extra->rate) . '</span>';
		$output .= '&nbsp;';
		if ($extra->pricing_plan)
		{
			$pricingPlanArray = array();
			foreach (json_decode($extra->pricing_plan) as $pricingPlan)
			{
				$pricingPlanArray[] = JTEXT::_($categories->get((int)$pricingPlan)->title);
			}
			$output .= '<span class="plan">' . implode(', ', $pricingPlanArray) . '</span>';
			$output .= ')</span>';
		}
		$output .= '</label>';
		$output .= '</span>';

		return $output;
	}
	
	/**
	 * Method to render the optional extras.
	 *
	 * @since	1.1
	 */
	public function &renderExtrasSummary($extra, $date)
	{
		$categories = JCategories::getInstance('travelbook.calculation', array());
		
		$output = '';

		$output .= '<span class="summary-col2 filler">';
		$output .= $this->renderExtraSummary($date, $extra);
		$output .= $this->renderAjax($extra->rate, 'extra-' . $extra->tours_extras_id . '-rate-summary');
		$output .= '</span>';
		
		$output .= '<span class="summary-col3 filler">';
		$output .= '<span class="text">' . strip_tags($extra->introtext) . '</span>';
		$output .= '</span>';

		return $output;
	}
	
	/**
	 * Method to render the optional extras.
	 *
	 * @since	1.1
	 */
	public function &renderAjax($value, $id)
	{
		$output = '';
		$output .= '<span id="' . $id . '" class="render ajax">';
		$output .= '<span class="value" style="display: none">' . (100*$value) . '</span>';
		$output .= '<span class="amount">' . $this->_currency->calculate($value) . '</span>';
		$output .= '</span>';
		
		return $output;
	}
	
	/**
	 * Method to render the optional extras.
	 *
	 * @since	1.1
	 */
	public function &renderExtra($date, $extra)
	{
		$categories = JCategories::getInstance('travelbook.calculation', array());

		$output = '';
		$output .= '<span class="calc-extras" style="display: none;">';
		$output .= '<span class="extraTotal">' . (100*$extra->rate) . '</span>';

		if ($extra->pricing_plan)
		{
			foreach (json_decode($extra->pricing_plan) as $pricingPlan)
			{
				switch ($categories->get((int)$pricingPlan)->alias) {
					case 'per-day':
						$factor = $date->duration;
						break;
					case 'per-person':
						$factor = $this->_params->get('number_guests_default');
						break;
					case 'per-room':
						$factor = ceil($this->_params->get('number_guests_default') / 2);
						break;
					case 'one-off':
						$factor = 1;
						break;
					default:
						$factor = 0;
						break;
				}

				$output .= '<span class="extraFactor ' . $categories->get((int)$pricingPlan)->alias . '" style="display: none;">' . $factor . '</span>';
			}
		}
		$output .= '</span>';
		
		return $output;
	}
	
	/**
	 * Method to render the optional extras.
	 *
	 * @since	1.1
	 */
	public function &renderExtraSummary($date, $extra)
	{
		$categories = JCategories::getInstance('travelbook.calculation', array());

		$output = '';
		$output .= '<span class="calc-extras-summary">';

		if ($extra->pricing_plan)
		{
			foreach (json_decode($extra->pricing_plan) as $pricingPlan)
			{
				switch ($categories->get((int)$pricingPlan)->alias) {
					case 'per-day':
						$factor = $date->duration;
						break;
					case 'per-person':
						$factor = $this->_params->get('number_guests_default');
						break;
					case 'per-room':
						$factor = ceil($this->_params->get('number_guests_default') / 2);
						break;
					case 'one-off':
						$factor = 1;
						break;
					default:
						$factor = 0;
						break;
				}

				$output .= '<span class="extraFactor ' . $categories->get((int)$pricingPlan)->alias . '">' . $factor . '</span>&nbsp;x&nbsp;';
			}
		}
		$output .= '</span>';
		
		return $output;
	}
}
