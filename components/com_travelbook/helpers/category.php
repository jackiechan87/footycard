<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.categories');

/**
 * Travelbook Component Category Tree
 *
 * @static
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @since 2.0
 */
class TravelbookTourCategories extends JCategories
{
    public function __construct($options = array())
	{
		$options['table'] = '#__tb_tours';
		$options['extension'] = 'com_travelbook.tour';
		parent::__construct($options);
	}
}

class TravelbookDateCategories extends JCategories
{
	public function __construct($options = array())
	{
		$options['table'] = '#__tb_dates';
		$options['extension'] = 'com_travelbook.date';
		parent::__construct($options);
	}
}

class TravelbookCalculationCategories extends JCategories
{
	public function __construct($options = array())
	{
		$options['table'] = '#__tb_extras';
		$options['extension'] = 'com_travelbook.calculation';
		parent::__construct($options);
	}
	
	public function getNodes()
	{
		return $this->_nodes;
	}
}

class TravelbookDestinationCategories extends JCategories
{
	public function __construct($options = array())
	{
		$options['table'] = '#__tb_tours';
		$options['extension'] = 'com_travelbook.destination';
		parent::__construct($options);
	}
	
	public function getNodes()
	{
		return $this->_nodes;
	}
}

class TravelbookActivityCategories extends JCategories
{
	public function __construct($options = array())
	{
		$options['table'] = '#__tb_tours';
		$options['extension'] = 'com_travelbook.activity';
		parent::__construct($options);
	}
	
	public function getNodes()
	{
		return $this->_nodes;
	}
}

class TravelbookStyleCategories extends JCategories
{
	public function __construct($options = array())
	{
		$options['table'] = '#__tb_tours';
		$options['extension'] = 'com_travelbook.style';
		parent::__construct($options);
	}
	
	public function getNodes()
	{
		return $this->_nodes;
	}
}
