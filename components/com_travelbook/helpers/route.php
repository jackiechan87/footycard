<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.helper');
jimport('joomla.application.categories');

/**
 * Travelbook Component Route Helper
 *
 * @static
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @since 2.0
 */
abstract class TravelbookHelperRoute
{
	protected static $lookup;

	/**
	 * @param	int	The route of the tour item
	 */
	public static function getTourRoute($id, $catid = 0, $language = 0)
	{
		$needles = array(
			'tour'  => array((int) $id)
		);
		//Create the link
		$link = 'index.php?option=com_travelbook&view=tour&id='.$id;
		if ((int)$catid > 1)
		{
			$categories = JCategories::getInstance('travelbook.tour');
			$category = $categories->get((int)$catid, true);
			if($category)
			{
				$needles['category'] = array_reverse($category->getPath());
				$needles['categories'] = $needles['category'];
				$link .= '&catid='.$catid;
			}
		}
			if ($language && $language != "*" && JLanguageMultilang::isEnabled()) {
				$db = JFactory::getDBO();
				$query = $db->getQuery(true);
				$query->select('a.sef AS sef');
				$query->select('a.lang_code AS lang_code');
				$query->from('#__languages AS a');
				//$query->where('a.lang_code = ' .$language);
				$db->setQuery($query);
				$langs = $db->loadObjectList();
				foreach ($langs as $lang) {
					if ($language == $lang->lang_code) {
						$language = $lang->sef;
						$link .= '&lang='.$language;
					}
				}
			}

		if ($item = self::_findItem($needles)) {
			$link .= '&Itemid='.$item;
		}
		elseif ($item = self::_findItem()) {
			$link .= '&Itemid='.$item;
		}

		return $link;
	}

	/**
	 * @param	int	The route of the tour item
	 */
	public static function getBookingRoute($date_id, $tour_id, $language = 0)
	{
		$needles = array(
			'book'  => array((int) $date_id)
		);
		//Create the link
		$link = 'index.php?option=com_travelbook&view=bookingform&date_id='.$date_id;

		if ($language && $language != "*" && JLanguageMultilang::isEnabled()) {
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);
			$query->select('a.sef AS sef');
			$query->select('a.lang_code AS lang_code');
			$query->from('#__languages AS a');
			//$query->where('a.lang_code = ' .$language);
			$db->setQuery($query);
			$langs = $db->loadObjectList();
			foreach ($langs as $lang) {
				if ($language == $lang->lang_code) {
					$language = $lang->sef;
					$link .= '&lang='.$language;
				}
			}
		}

		if ($item = self::_findItem($needles)) {
			$link .= '&Itemid='.$item;
		}
		elseif ($item = self::_findItem()) {
			$link .= '&Itemid='.$item;
		}

		$link .= '&return=' . base64_encode(self::getTourRoute($tour_id));
		
		return $link;
	}

	/**
	 * 
	 * @param	int	The route of the tour item
	 */
	public static function getDatesRoute($tour_id, $catid = 0, $language = 0)
	{
		$needles = array(
			'dates' => array(0)
		);
		//Create the link
		$link = 'index.php?option=com_travelbook&view=dates&tour_id='.$tour_id;
		if ((int)$catid > 1)
		{
			$categories = JCategories::getInstance('travelbook.date');
			$category = $categories->get((int)$catid, true);
			if($category)
			{
				$needles['category'] = array_reverse($category->getPath());
				$needles['categories'] = $needles['category'];
				$link .= '&catid='.$catid;
			}
		}
			if ($language && $language != "*" && JLanguageMultilang::isEnabled()) {
				$db = JFactory::getDBO();
				$query = $db->getQuery(true);
				$query->select('a.sef AS sef');
				$query->select('a.lang_code AS lang_code');
				$query->from('#__languages AS a');
				//$query->where('a.lang_code = ' .$language);
				$db->setQuery($query);
				$langs = $db->loadObjectList();
				foreach ($langs as $lang) {
					if ($language == $lang->lang_code) {
						$language = $lang->sef;
						$link .= '&lang='.$language;
					}
				}
			}

		if ($item = self::_findItem($needles)) {
			$link .= '&Itemid='.$item;
		}
		elseif ($item = self::_findItem()) {
			$link .= '&Itemid='.$item;
		}

		return $link;
	}

	/**
	 * @param	int	The route of the tour item
	 */
	public static function getClientRoute($id, $catid = 0, $language = 0)
	{
		$needles = array(
			'client'  => array((int) $id)
		);
		//Create the link
		$link = 'index.php?option=com_travelbook&view=client&id='.$id;
		if ((int)$catid > 1)
		{
			$categories = JCategories::getInstance('travelbook.client');
			$category = $categories->get((int)$catid, true);
			if($category)
			{
				$needles['category'] = array_reverse($category->getPath());
				$needles['categories'] = $needles['category'];
				$link .= '&catid='.$catid;
			}
		}
			if ($language && $language != "*" && JLanguageMultilang::isEnabled()) {
				$db = JFactory::getDBO();
				$query = $db->getQuery(true);
				$query->select('a.sef AS sef');
				$query->select('a.lang_code AS lang_code');
				$query->from('#__languages AS a');
				//$query->where('a.lang_code = ' .$language);
				$db->setQuery($query);
				$langs = $db->loadObjectList();
				foreach ($langs as $lang) {
					if ($language == $lang->lang_code) {
						$language = $lang->sef;
						$link .= '&lang='.$language;
					}
				}
			}

		if ($item = self::_findItem($needles)) {
			$link .= '&Itemid='.$item;
		}
		elseif ($item = self::_findItem()) {
			$link .= '&Itemid='.$item;
		}

		return $link;
	}

	
	public static function getCategoryRoute($catid)
	{
		if ($catid instanceof JCategoryNode)
		{
			$id = $catid->id;
			$category = $catid;
		}
		else
		{
			$id = (int) $catid;
			$category = JCategories::getInstance('travelbook.tour')->get($id);
		}

		if($id < 1)
		{
			$link = '';
		}
		else
		{
			$needles = array(
				'category' => array($id)
			);

			if ($item = self::_findItem($needles))
			{
				$link = 'index.php?Itemid='.$item;
			}
			else
			{
				//Create the link
				$link = 'index.php?option=com_travelbook&view=category&id='.$id;
				if($category)
				{
					$catids = array_reverse($category->getPath());
					$needles = array(
						'category' => $catids,
						'categories' => $catids
					);
					if ($item = self::_findItem($needles)) {
						$link .= '&Itemid='.$item;
					}
					elseif ($item = self::_findItem()) {
						$link .= '&Itemid='.$item;
					}
				}
			}
		}

		return $link;
	}

	public static function getDestinationRoute($destid)
	{
		if ($destid instanceof JCategoryNode)
		{
			$id = $destid->id;
			$destination = $destid;
		}
		else
		{
			$id = (int) $destid;
			$destination = JCategories::getInstance('travelbook.destination')->get($id);
		}

		if($id < 1)
		{
			$link = '';
		}
		else
		{
			$needles = array(
				'category' => array($id)
			);

			if ($item = self::_findItem($needles))
			{
				$link = 'index.php?Itemid='.$item;
			}
			else
			{
				//Create the link
				$link = 'index.php?option=com_travelbook&view=destination&id='.$id;
				if($destination)
				{
					$destids = array_reverse($destination->getPath());
					$needles = array(
						'category' => $destids,
						'categories' => $destids
					);
					if ($item = self::_findItem($needles)) {
						$link .= '&Itemid='.$item;
					}
					elseif ($item = self::_findItem()) {
						$link .= '&Itemid='.$item;
					}
				}
			}
		}

		return $link;
	}

	public static function getFormRoute($id)
	{
		//Create the link
		if ($id) {
			$link = 'index.php?option=com_travelbook&task=tour.edit&a_id='. $id;
		} else {
			$link = 'index.php?option=com_travelbook&task=tour.edit&a_id=0';
		}

		return $link;
	}

	protected static function _findItem($needles = null)
	{
		$app = JFactory::getApplication();
		$menus = $app->getMenu('site');

		// Prepare the reverse lookup array.
		if (self::$lookup === null)
		{
			self::$lookup = array();

			$component = JComponentHelper::getComponent('com_travelbook');
			$items = $menus->getItems('component_id', $component->id);
			foreach ($items as $item)
			{
				if (isset($item->query) && isset($item->query['view']))
				{
					$view = $item->query['view'];
					if (!isset(self::$lookup[$view])) {
						self::$lookup[$view] = array();
					}
					if (isset($item->query['id'])) {
						self::$lookup[$view][$item->query['id']] = $item->id;
					}
				}
			}
		}

		if ($needles)
		{
			foreach ($needles as $view => $ids)
			{
				if (isset(self::$lookup[$view]))
				{
					foreach($ids as $id)
					{
						if (isset(self::$lookup[$view][(int)$id])) {
							return self::$lookup[$view][(int)$id];
						}
					}
				}
			}
		}
		else
		{
			$active = $menus->getActive();
			if ($active && $active->component == 'com_travelbook') {
				return $active->id;
			}
		}

		return null;
	}
}
