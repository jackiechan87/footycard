<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * This models supports retrieving lists of emails.
 *
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @since		2.0
 */
class TravelbookModelEmails extends JModelList
{

	/**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'a.id',
				'title', 'a.title',
				'alias', 'a.alias',
				'checked_out', 'a.checked_out',
				'checked_out_time', 'a.checked_out_time',
				'state', 'a.state',
				'access', 'a.access', 'access_level',
				'created', 'a.created',
				'created_by', 'a.created_by',
				'ordering', 'a.ordering',
				'language', 'a.language',
				'publish_up', 'a.publish_up',
				'publish_down', 'a.publish_down'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState($ordering = 'ordering', $direction = 'ASC')
	{
		$app = JFactory::getApplication();

		// List state information
		$this->setState('list.limit', 0);
		$this->setState('list.start', 0);

		$orderCol = JRequest::getCmd('filter_order', 'a.ordering');
		if (!in_array($orderCol, $this->filter_fields)) {
			$orderCol = 'a.ordering';
		}
		$this->setState('list.ordering', $orderCol);

		$listOrder = JRequest::getCmd('filter_order_Dir', 'ASC');
		if (!in_array(strtoupper($listOrder), array('ASC', 'DESC', ''))) {
			$listOrder = 'ASC';
		}
		$this->setState('list.direction', $listOrder);

		$params = $app->getParams();
		$this->setState('params', $params);
		$user = JFactory::getUser();

		if ((!$user->authorise('core.edit.state', 'com_travelbook')) &&  (!$user->authorise('core.edit', 'com_travelbook'))){
			// filter on published for those who do not have edit or edit.state rights.
			$this->setState('filter.published', 1);
		}

		$this->setState('filter.language', $app->getLanguageFilter());

		// process show_noauth parameter
		if (!$params->get('show_noauth')) {
			$this->setState('filter.access', true);
		}
		else {
			$this->setState('filter.access', false);
		}

		$this->setState('layout', JRequest::getCmd('layout'));
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param	string		$id	A prefix for the store id.
	 *
	 * @return	string		A store id.
	 * @since	1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':'.$this->getState('filter.published');
		$id .= ':'.$this->getState('filter.access');
		$id .= ':'.$this->getState('filter.author_id');
		$id .= ':'.$this->getState('filter.author_id.include');
		$id .= ':'.$this->getState('filter.author_alias');
		$id .= ':'.$this->getState('filter.author_alias.include');
		$id .= ':'.$this->getState('filter.relative_date');

		return parent::getStoreId($id);
	}

	/**
	 * Get the master query for retrieving a list of emails subject to the model state.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	function getListQuery()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'a.id, a.title, a.subject, a.mailbody_html, a.mailbody_plain, a.css, a.created, ' .
				// use created if publish_up is 0
				'CASE WHEN a.publish_up = 0 THEN a.created ELSE a.publish_up END as publish_up,' .
				'a.publish_down, a.attribs, a.access'
			)
		);

		$query->from('#__tb_emails AS a');

		// Join over the dates.
//		$query->join('LEFT', '#__tb_dates AS d ON d.TID = a.id');
		
		// Join over the users for the author and modified_by names.
		$query->select("ua.email AS author_email");

		$query->join('LEFT', '#__users AS ua ON ua.id = a.created_by');

		// Join on contact table
		$subQuery = $db->getQuery(true);
		$subQuery->select('contact.user_id, MAX(contact.id) AS id, contact.language');
		$subQuery->from('#__contact_details AS contact');
		$subQuery->where('contact.published = 1');
		$subQuery->group('contact.user_id, contact.language');
		$query->select('contact.id as contactid' );
		$query->join('LEFT', '(' . $subQuery . ') AS contact ON contact.user_id = a.created_by');
		
		// Filter by access level.
		if ($access = $this->getState('filter.access')) {
			$user = JFactory::getUser();
			$groups	= implode(',', $user->getAuthorisedViewLevels());
			$query->where('a.access IN ('.$groups.')');
		}

		// Filter by published state
		$published = $this->getState('filter.published');
		if (is_numeric($published)) {
			// Use email state if badcats.id is null, otherwise, force 0 for unpublished
			$query->where('a.state = ' . (int) $published);
		}
		elseif (is_array($published)) {
			JArrayHelper::toInteger($published);
			$published = implode(',', $published);
			// Use email state if badcats.id is null, otherwise, force 0 for unpublished
			$query->where('a.state IN ('.$published.')');
		}

		// Filter by a single or group of emails.
		$emailId = $this->getState('filter.email_id');

		if (is_numeric($emailId)) {
			$type = $this->getState('filter.email_id.include', true) ? '= ' : '<> ';
			$query->where('a.id '.$type.(int) $emailId);
		}
		elseif (is_array($emailId)) {
			JArrayHelper::toInteger($emailId);
			$emailId = implode(',', $emailId);
			$type = $this->getState('filter.email_id.include', true) ? 'IN' : 'NOT IN';
			$query->where('a.id '.$type.' ('.$emailId.')');
		}

		// Filter by start and end dates.
		$nullDate = $db->Quote($db->getNullDate());
		$nowDate = $db->Quote(JFactory::getDate()->toSql());

		$query->where('(a.publish_up = '.$nullDate.' OR a.publish_up <= '.$nowDate.')');
		$query->where('(a.publish_down = '.$nullDate.' OR a.publish_down >= '.$nowDate.')');
		
		// Filter by language
		if ($this->getState('filter.language')) {
			$query->where('a.language in ('.$db->quote(JFactory::getLanguage()->getTag()).','.$db->quote('*').')');
			$query->where('(contact.language in ('.$db->quote(JFactory::getLanguage()->getTag()).','.$db->quote('*').') OR contact.language IS NULL)');
		}

		// Add the list ordering clause.
		$query->order($this->getState('list.ordering', 'a.ordering').' '.$this->getState('list.direction', 'ASC'));
		$query->group('a.id, a.title, a.subject, a.alias, a.mailbody_html, a.mailbody_plain, a.attribs, a.access, a.state, a.publish_down, ua.email, contact.id, a.ordering');

//		echo nl2br(str_replace('#__','jos_',$query));
		
		return $query;
	}

	/**
	 * Method to get a list of emails.
	 *
	 * Overriden to inject convert the attribs field into a JParameter object.
	 *
	 * @return	mixed	An array of objects on success, false on failure.
	 * @since	1.6
	 */
	public function getItems()
	{
		$items = parent::getItems();
		$user = JFactory::getUser();
		$userId = $user->get('id');
		$guest = $user->get('guest');
		$groups = $user->getAuthorisedViewLevels();

		// Get the global params
		$globalParams = JComponentHelper::getParams('com_travelbook', true);

		// Convert the parameter fields into objects.
		foreach ($items as &$item)
		{
			$emailParams = new JRegistry;
			$emailParams->loadString($item->attribs);

			// Unpack readmore and layout params
			$item->alternative_readmore = $emailParams->get('alternative_readmore');
			$item->layout = $emailParams->get('layout');

			$item->params = clone $this->getState('params');

			// For blogs, email params override menu item params only if menu param = 'use_email'
			// Otherwise, menu item params control the layout
			// If menu item is 'use_email' and there is no email param, use global
			if ((JRequest::getString('layout') == 'blog') || (JRequest::getString('view') == 'featured')
				|| ($this->getState('params')->get('layout_type') == 'blog')) {
				// create an array of just the params set to 'use_email'
				$menuParamsArray = $this->getState('params')->toArray();
				$emailArray = array();

				foreach ($menuParamsArray as $key => $value)
				{
					if ($value === 'use_email') {
						// if the email has a value, use it
						if ($emailParams->get($key) != '') {
							// get the value from the email
							$emailArray[$key] = $emailParams->get($key);
						}
						else {
							// otherwise, use the global value
							$emailArray[$key] = $globalParams->get($key);
						}
					}
				}

				// merge the selected email params
				if (count($emailArray) > 0) {
					$emailParams = new JRegistry;
					$emailParams->loadArray($emailArray);
					$item->params->merge($emailParams);
				}
			}
			else {
				// For non-blog layouts, merge all of the email params
				$item->params->merge($emailParams);
			}

			// get display date
			switch ($item->params->get('list_show_date'))
			{
				case 'modified':
					$item->displayDate = $item->modified;
					break;

				case 'published':
					$item->displayDate = ($item->publish_up == 0) ? $item->created : $item->publish_up;
					break;

				default:
				case 'created':
					$item->displayDate = $item->created;
					break;
			}

			// Compute the asset access permissions.
			// Technically guest could edit an email, but lets not check that to improve performance a little.
			if (!$guest) {
				$asset	= 'com_travelbook.email.'.$item->id;

				// Check general edit permission first.
				if ($user->authorise('core.edit', $asset)) {
					$item->params->set('access-edit', true);
				}
				// Now check if edit.own is available.
				elseif (!empty($userId) && $user->authorise('core.edit.own', $asset)) {
					// Check for a valid user and that they are the owner.
					if ($userId == $item->created_by) {
						$item->params->set('access-edit', true);
					}
				}
			}

			$access = $this->getState('filter.access');

			if ($access) {
				// If the access filter has been set, we already have only the emails this user can view.
				$item->params->set('access-view', true);
			}
			else {
				// If no access filter is set, the layout takes some responsibility for display of limited information.
				if ($item->catid == 0 || $item->category_access === null) {
					$item->params->set('access-view', in_array($item->access, $groups));
				}
				else {
					$item->params->set('access-view', in_array($item->access, $groups) && in_array($item->category_access, $groups));
				}
			}
		}

		return $items;
	}
	public function getStart()
	{
		return $this->getState('list.start');
	}
}
