<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Base this model on the backend version.
require_once JPATH_ADMINISTRATOR.'/components/com_travelbook/models/booking.php';

/**
 * Travelbook Component Booking Model
 *
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @since 2.0
 */
class TravelbookModelBookingForm extends TravelbookModelBooking
{
	protected $_date = null;
	protected $_extras = null;
	
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since	1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication();

		// Load state from the request.
		$pk = JRequest::getInt('a_id');
		$this->setState('booking.id', $pk);

		// Load date_id from the request.
		$DID = JRequest::getInt('date_id');
		$this->setState('date.id', $DID);
		
		$this->setState('booking.catid', JRequest::getInt('catid'));

		$return = JRequest::getVar('return', null, 'default', 'base64');
		$this->setState('return_page', base64_decode($return));

		// Load the parameters.
		$params	= $app->getParams();
		$this->setState('params', $params);

		$this->setState('layout', JRequest::getCmd('layout'));
	}

	/**
	 * Method to get booking data.
	 *
	 * @param	integer	The id of the booking.
	 *
	 * @return	mixed	Content item data object on success, false on failure.
	 */
	public function getItem($itemId = null)
	{
		// Initialise variables.
		$itemId = (int) (!empty($itemId)) ? $itemId : $this->getState('booking.id');

		// Get a row instance.
		$table = $this->getTable();

		// Attempt to load the row.
		$return = $table->load($itemId);

		// Check for a table object error.
		if ($return === false && $table->getError()) {
			$this->setError($table->getError());
			return false;
		}

		$properties = $table->getProperties(1);
		$value = JArrayHelper::toObject($properties, 'JObject');

		// Convert attrib field to Registry.
		$value->params = new JRegistry;
		$value->params->loadString($value->attribs);

		// Compute selected asset permissions.
		$user = JFactory::getUser();
		$userId = $user->get('id');
		$asset = 'com_travelbook.booking.'.$value->id;

		// Check general edit permission first.
		if ($user->authorise('core.edit', $asset)) {
			$value->params->set('access-edit', true);
		}
		// Now check if edit.own is available.
		elseif (!empty($userId) && $user->authorise('core.edit.own', $asset)) {
			// Check for a valid user and that they are the owner.
			if ($userId == $value->created_by) {
				$value->params->set('access-edit', true);
			}
		}

		// Check edit state permission.
		if ($itemId) {
			// Existing item
			$value->params->set('access-change', $user->authorise('core.edit.state', $asset));
		}
		else {
			// New item.
			$catId = (int) $this->getState('booking.catid');

			if ($catId) {
				$value->params->set('access-change', $user->authorise('core.edit.state', 'com_travelbook.category.'.$catId));
				$value->catid = $catId;
			}
			else {
				$value->params->set('access-change', $user->authorise('core.edit.state', 'com_travelbook'));
			}
		}

		$value->bookingtext = $value->introtext;
		if (!empty($value->fulltext)) {
			$value->bookingtext .= '<hr id="system-readmore" />'.$value->fulltext;
		}

		return $value;
	}

	/**
	 * Get the return URL.
	 *
	 * @return	string	The return URL.
	 * @since	1.6
	 */
	public function getReturnPage()
	{
		return base64_encode($this->getState('return_page'));
	}

	/**
	 * Method to get a tour.
	 *
	 * Overriden to inject convert the attribs field into a JParameter object.
	 *
	 * @return	mixed	An array of objects on success, false on failure.
	 * @since	1.0
	 */
    public function getDate()
	{
		$pk = $this->getState('date.id');
		if ($this->_date === null) {
		    $model = JModelLegacy::getInstance('Date', 'TravelbookModel', array('ignore_request' => true));
			$model->setState('params', JFactory::getApplication()->getParams());
			$model->setState('filter.published', 1);
			$model->setState('filter.access', $this->getState('filter.access'));
			$model->setState('filter.language', $this->getState('filter.language'));

			$this->_date = $model->getItem($pk);

			if ($this->_date === false) {
				$this->setError($model->getError());
			}
		}

		return $this->_date;
	}

	/**
	 * Method to get a tour.
	 *
	 * Overriden to inject convert the attribs field into a JParameter object.
	 *
	 * @return	mixed	An array of objects on success, false on failure.
	 * @since	1.0
	 */
    public function getExtras()
	{
		if ($this->_extras === null) {
		    $model = JModelLegacy::getInstance('Extras', 'TravelbookModel', array('ignore_request' => true));
			$model->setState('params', JFactory::getApplication()->getParams());

			$model->setState('filter.published', 1);
			$model->setState('filter.access', $this->getState('filter.access'));
			$model->setState('filter.date_id', $this->getState('date.id'));
			$model->setState('filter.language', $this->getState('filter.language'));

			$model->setState('list.ordering', 'a.type');
			
			$this->_extras = $model->getItems();

			if ($this->_extras === false) {
				$this->setError($model->getError());
			}
		}

		return $this->_extras;
	}

}
