<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

//jimport('joomla.application.component.model');
jimport('joomla.application.component.modelform');

/**
 * Travelbook Component Search Model
 *
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @since 2.0
 */
class TravelbookModelSearch extends JModelForm
{
    /**
     * Search data array
     *
     * @var array
     */
    var $_data = null;

    /**
     * Search total
     *
     * @var integer
     */
    var $_total = null;

    /**
     * Search areas
     *
     * @var integer
     */
//    var $_areas = null;

    /**
     * Pagination object
     *
     * @var object
     */
    var $_pagination = null;

    /**
     * Constructor
     *
     * @since 1.5
     */
    function __construct()
    {
        parent::__construct();

        //Get configuration
        $app = JFactory::getApplication();
        $config = JFactory::getConfig();

        // Get the pagination request variables
        $this->setState('limit', $app->getUserStateFromRequest('com_travelbook.limit', 'limit', $config->get('list_limit'), 'int'));
        $this->setState('limitstart', JRequest::getVar('limitstart', 0, '', 'int'));

        $searchtype = JRequest::getWord('searchtype', 'advanced');
        $input = JRequest::getVar($searchtype);
        $ordering = $input['ordering'];
        // Set the search parameters
        if ($searchtype == 'keyword') {
            $keyword = urldecode($input['keyword']);
            $match = JRequest::getWord('searchphrase', 'all');
            $this->setSearch($keyword, $match, $ordering);
        }

        //Set the advanced cirterias
        $criterias = JRequest::getVar('advanced');
        $this->setCriterias($criterias);

    }

    /**
     * Method to set the search parameters
     *
     * @access	public
     * @param string search string
     * @param string mathcing option, exact|any|all
     * @param string ordering option, newest|oldest|popular|alpha|category
     */
    function setSearch($keyword, $match = 'all', $ordering = 'newest')
    {
        if (isset($keyword)) {
            $this->setState('originalKeyword', $keyword);
            if($match !== 'exact') {
                $keyword = preg_replace('#\xE3\x80\x80#s', ' ', $keyword);
            }
            $this->setState('keyword', $keyword);
        }

        if (isset($match)) {
            $this->setState('match', $match);
        }

        if (isset($ordering)) {
            $this->setState('ordering', $ordering);
        }
    }

    /**
     * Method to set the search areas
     *
     * @access	public
     * @param	array	Active areas
     * @param	array	Search areas
     */
//    function setAreas($active = array(), $search = array())
//   {
//      $this->_areas['active'] = $active;
//        $this->_areas['search'] = $search;
//    }

    /**
     * Method to set the search parameters
     *
     * @access	public
     * @param string search string
     * @param string mathcing option, exact|any|all
     * @param string ordering option, newest|oldest|popular|alpha|category
     */
    function setCriterias($criterias = array())
    {
        if (isset($criterias)) {
            $this->setState('criterias', $criterias);
        }
    }

    /**
     * Method to get tour + date data containting the keyword
     *
     * @access public
     * @return array
     */
    function getKeywordResults()
    {
        // Lets load the content if it doesn't already exist
        if (empty($this->_data))
        {
//            $areas = $this->getAreas();

            JPluginHelper::importPlugin('travelbook');
            $dispatcher = JDispatcher::getInstance();
            $results = $dispatcher->trigger('onTravelbookKeywordSearch', array(
            $this->getState('keyword'),
            $this->getState('match'),
            $this->getState('ordering'))
            );

            $rows = array();
            foreach ($results as $result) {
                $rows = array_merge((array) $rows, (array) $result);
            }

            $this->_total = count($rows);
            if ($this->getState('limit') > 0) {
                $this->_data = array_splice($rows, $this->getState('limitstart'), $this->getState('limit'));
            } else {
                $this->_data = $rows;
            }
        }

        return $this->_data;
    }

    /**
     * Method to get tour + date data matching the search criteria
     *
     * @access public
     * @return array
     */
    function getAdvancedResults()
    {
        // Lets load the content if it doesn't already exist
        if (empty($this->_data))
        {
//            $areas = $this->getAreas();

            $app = JFactory::getApplication();
            $params = $app->getParams();
            
            JPluginHelper::importPlugin('travelbook');
            $dispatcher = JDispatcher::getInstance();
            $results = $dispatcher->trigger('onTravelbookAdvancedSearch', array(
            $this->getState('criterias'),
            $this->getState('ordering'),
            $params->get('relevance', 100)
            )
            );

            $rows = array();
            foreach ($results as $result) {
                $rows = array_merge((array) $rows, (array) $result);
            }

            $this->_total = count($rows);
            if ($this->getState('limit') > 0) {
                $this->_data = array_splice($rows, $this->getState('limitstart'), $this->getState('limit'));
            } else {
                $this->_data = $rows;
            }
        }

        return $this->_data;
    }

    /**
     * Method to get the total number of weblink items for the category
     *
     * @access public
     * @return integer
     */
    function getTotal()
    {
        return $this->_total;
    }

    /**
     * Method to get a pagination object of the weblink items for the category
     *
     * @access public
     * @return integer
     */
    function getPagination()
    {
        // Lets load the content if it doesn't already exist
        if (empty($this->_pagination))
        {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
        }

        return $this->_pagination;
    }

    /**
     * Method to get the search areas
     *
     * @since 1.5
     */
//    function getAreas()
//    {
        // Load the Category data
//        if (empty($this->_areas['search']))
//        {
//            $areas = array();

//            JPluginHelper::importPlugin('search');
//            $dispatcher = JDispatcher::getInstance();
//            $searchareas = $dispatcher->trigger('onTravelbookSearchAreas');

//            foreach ($searchareas as $area) {
//                if (is_array($area)) {
//                    $areas = array_merge($areas, $area);
//                }
//            }

//            $this->_areas['search'] = $areas;
//        }

//        return $this->_areas;
//    }

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		Data for the form.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 *
	 * @return	mixed	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = false)
	{
	    
		// Get the form.
		$form = $this->loadForm('com_travelbook.search', 'components/com_travelbook/models/forms/search.xml', array('control' => 'jform', 'load_data' => $loadData), false, '');
		if (empty($form)) {
			return false;
		}
		$jinput = JFactory::getApplication()->input;

		// The front end calls this model and uses a_id to avoid id clashes so we need to check for that first.
		if ($jinput->get('a_id'))
		{
			$id =  $jinput->get('a_id', 0);
		}
		// The back end uses id so we use that the rest of the time and set it to 0 by default.
		else
		{
			$id =  $jinput->get('id', 0);
		}
		// Determine correct permissions to check.
		if ($this->getState('tour.id'))
		{
			$id = $this->getState('tour.id');
			// Existing record. Can only edit in selected categories.
			$form->setFieldAttribute('catid', 'action', 'core.edit');
			// Existing record. Can only edit own tours in selected categories.
			$form->setFieldAttribute('catid', 'action', 'core.edit.own');
		}
		else
		{
			// New record. Can only create in selected categories.
			$form->setFieldAttribute('catid', 'action', 'core.create');
		}

		$user = JFactory::getUser();

		// Check for existing tour.
		// Modify the form based on Edit State access controls.
		if ($id != 0 && (!$user->authorise('core.edit.state', 'com_travelbook.tour.'.(int) $id))
		|| ($id == 0 && !$user->authorise('core.edit.state', 'com_travelbook'))
		)
		{
			// Disable fields for display.
			$form->setFieldAttribute('featured', 'disabled', 'true');
			$form->setFieldAttribute('ordering', 'disabled', 'true');
			$form->setFieldAttribute('publish_up', 'disabled', 'true');
			$form->setFieldAttribute('publish_down', 'disabled', 'true');
			$form->setFieldAttribute('state', 'disabled', 'true');

			// Disable fields while saving.
			// The controller has already verified this is an tour you can edit.
			$form->setFieldAttribute('featured', 'filter', 'unset');
			$form->setFieldAttribute('ordering', 'filter', 'unset');
			$form->setFieldAttribute('publish_up', 'filter', 'unset');
			$form->setFieldAttribute('publish_down', 'filter', 'unset');
			$form->setFieldAttribute('state', 'filter', 'unset');

		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_travelbook.edit.tour.data', array());

		if (empty($data)) {
			$data = $this->getItem();

			// Prime some default values.
			if ($this->getState('tour.id') == 0) {
				$app = JFactory::getApplication();
				$data->set('catid', JRequest::getInt('catid', $app->getUserState('com_travelbook.tours.filter.category_id')));
			}
		}

		return $data;
	}
}