<?php
/**
 * @package		Joomla.Site
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

jimport('joomla.application.categories');

/**
 * Build the route for the com_travelbook component
 *
 * @param	array	An array of URL arguments
 * @return	array	The URL arguments to use to assemble the subsequent URL.
 * @since	1.5
 */
function TravelbookBuildRoute(&$query)
{
    $segments = array();

    // get a menu item based on Itemid or currently active
    $app = JFactory::getApplication();
    $menu = $app->getMenu();
    $params = JComponentHelper::getParams('com_travelbook');
    $advanced = $params->get('sef_advanced_link', 0);

    // we need a menu item.  Either the one specified in the query, or the current active one if none specified
    if (empty($query['Itemid'])) {
        $menuItem = $menu->getActive();
        $menuItemGiven = false;
    }
    else {
        $menuItem = $menu->getItem($query['Itemid']);
        $menuItemGiven = true;
    }

    if (isset($query['view'])) {
        $view = $query['view'];
    }
    else {
        // we need to have a view in the query or it is an invalid URL
        return $segments;
    }
    
    // ical
    if (isset($query['layout']) && $query['layout']=='ical') {
        $segments[] = $query['layout'];
        $segments[] = $query['id'];
        unset($query['layout']);
        return $segments;
    }
    
    // are we dealing with a tour or category that is attached to a menu item?
    if (($menuItem instanceof stdClass) && $menuItem->query['view'] == $query['view'] && isset($query['id']) && $menuItem->query['id'] == intval($query['id'])) {
        unset($query['view']);

        if (isset($query['catid'])) {
            unset($query['catid']);
        }

        if (isset($query['layout'])) {
            unset($query['layout']);
        }

        unset($query['id']);

        return $segments;
    }

    if ($view == 'category' || $view == 'tour')
    {
        if (!$menuItemGiven) {
            $segments[] = $view;
        }

        unset($query['view']);

        if ($view == 'tour') {
            if (isset($query['id']) && isset($query['catid']) && $query['catid']) {
                $catid = $query['catid'];
                // Make sure we have the id and the alias
                if (strpos($query['id'], ':') === false) {
                    $db = JFactory::getDbo();
                    $aquery = $db->setQuery($db->getQuery(true)
                    ->select('alias')
                    ->from('#__tb_tours')
                    ->where('id='.(int)$query['id'])
                    );
                    $alias = $db->loadResult();
                    $query['id'] = $query['id'].':'.$alias;
                }
            } else {
                // we should have these two set for this view.  If we don't, it is an error
                return $segments;
            }
        }
        else {
            if (isset($query['id'])) {
                $catid = $query['id'];
            } else {
                // we should have id set for this view.  If we don't, it is an error
                return $segments;
            }
        }

        if ($menuItemGiven && isset($menuItem->query['id'])) {
            $mCatid = $menuItem->query['id'];
        } else {
            $mCatid = 0;
        }

        $categories = JCategories::getInstance('travelbook.tour');
        $category = $categories->get($catid);

        if (!$category) {
            // we couldn't find the category we were given.  Bail.
            return $segments;
        }

        $path = array_reverse($category->getPath());

        $array = array();

        foreach($path as $id) {
            if ((int)$id == (int)$mCatid) {
                break;
            }

            list($tmp, $id) = explode(':', $id, 2);

            $array[] = $id;
        }

        $array = array_reverse($array);

        if (!$advanced && count($array)) {
            $array[0] = (int)$catid.':'.$array[0];
        }

        $segments = array_merge($segments, $array);

        if ($view == 'tour') {
            if ($advanced) {
                list($tmp, $id) = explode(':', $query['id'], 2);
            }
            else {
                $id = $query['id'];
            }
            $segments[] = $id;
        }
        unset($query['id']);
        unset($query['catid']);
    }

    if ($view == 'archive') {
        if (!$menuItemGiven) {
            $segments[] = $view;
            unset($query['view']);
        }

        if (isset($query['year'])) {
            if ($menuItemGiven) {
                $segments[] = $query['year'];
                unset($query['year']);
            }
        }

        if (isset($query['year']) && isset($query['month'])) {
            if ($menuItemGiven) {
                $segments[] = $query['month'];
                unset($query['month']);
            }
        }
    }

    if ($view == 'search') {
        // view
        $segments[] = $query['view'];
        unset($query['view']);

        $segments[] = $query['searchtype'];
        $segments[] = count($query[$query['searchtype']]);
        foreach ($query[$query['searchtype']] as $search) {
            $segments[] = $search == '' ? 'NA' : $search;
        }

//        $tourCategory = JCategories::getInstance('travelbook.tour')->get($query[$query['searchtype']]['category']);
        
        unset($query[$query['searchtype']]);
        unset($query['searchtype']);

        // limit
        if (isset($query['limit'])) {
            $segments[] = $query['limit'];
            unset($query['limit']);
        }
        
        // itemid
        if (isset($query['Itemid'])) {
                $segments[] = $query['Itemid'];
        }
    }

    // if the layout is specified and it is the same as the layout in the menu item, we
    // unset it so it doesn't go into the query string.
    if (isset($query['layout'])) {
        if ($menuItemGiven && isset($menuItem->query['layout'])) {
            if ($query['layout'] == $menuItem->query['layout']) {

                unset($query['layout']);
            }
        }
        else {
            if ($query['layout'] == 'default') {
                unset($query['layout']);
            }
        }
    }

    return $segments;
}



/**
 * Parse the segments of a URL.
 *
 * @param	array	The segments of the URL to parse.
 *
 * @return	array	The URL attributes to be used by the application.
 * @since	1.5
 */
function TravelbookParseRoute($segments)
{
    $vars = array();

    //Get the active menu item.
    $app = JFactory::getApplication();
    $menu = $app->getMenu();
    $item = $menu->getActive();
    $params = JComponentHelper::getParams('com_travelbook');
    $advanced = $params->get('sef_advanced_link', 0);
    $db = JFactory::getDBO();

    // Count route segments
    $count = count($segments);
    
    // search
    if ($count && $segments[0] == 'search') {
        $vars['view'] = $segments[0];
        $vars['searchtype'] = $segments[1];
        $count = $segments[2];
        switch ($vars['searchtype']) {
            case 'keyword':
                $keyword = array();
                $keyword['keyword'] = $segments[3] == 'NA' ? '' : $segments[3];
                $keyword['ordering'] = $segments[4] == 'NA' ? '' : $segments[4];
                $vars['keyword'] = $keyword;
                break;
            case 'expert':
                break;
            case 'advanced':
            default:
                $advanced = array();
                $advanced['category'] = $segments[3] == 'NA' ? '' : $segments[3];
                $advanced['destination'] = $segments[4] == 'NA' ? '' : $segments[4];
                $advanced['activity'] = $segments[5] == 'NA' ? '' : $segments[5];
                $advanced['departure'] = $segments[6] == 'NA' ? '' : preg_replace('/:/', '-', $segments[6], 1);
                $advanced['arrival'] = $segments[7] == 'NA' ? '' : preg_replace('/:/', '-', $segments[7], 1);
                $advanced['duration-from'] = $segments[8] == 'NA' ? '' : $segments[8];
                $advanced['duration-to'] = $segments[9] == 'NA' ? '' : $segments[9];
                $advanced['budget-from'] = $segments[10] == 'NA' ? '' : $segments[10];
                $advanced['budget-to'] = $segments[11] == 'NA' ? '' : $segments[11];
                $advanced['ordering'] = $segments[12] == 'NA' ? '' : $segments[12];
                $vars['advanced'] = $advanced;
                break;
        }
         
        $vars['limit'] = $segments[$count+3];
        $vars['Itemid'] = isset($segments[$count+4]) ? $segments[$count+4] : '';
        
        return $vars;
    }

    // ical
    if ($count && $segments[0] == 'ical') {
        $vars['id'] = $segments[1];
        $vars['view'] = 'tour';
        $vars['layout'] = 'ical';
        return $vars;
    }

    // Standard routing for tours.  If we don't pick up an Itemid then we get the view from the segments
    // the first segment is the view and the last segment is the id of the tour or category.
    if (!isset($item)) {
        $vars['view'] = $segments[0];
        $vars['id'] = $segments[$count - 1];

        return $vars;
    }

    // if there is only one segment, then it points to either an tour or a category
    // we test it first to see if it is a category.  If the id and alias match a category
    // then we assume it is a category.  If they don't we assume it is an tour
    if ($count == 1) {
        // we check to see if an alias is given.  If not, we assume it is an tour
        if (strpos($segments[0], ':') === false) {
            $vars['view'] = 'tour';
            $vars['id'] = (int)$segments[0];
            return $vars;
        }

        list($id, $alias) = explode(':', $segments[0], 2);

        // first we check if it is a category
        $category = JCategories::getInstance('travelbook.tour')->get($id);

        if ($category && $category->alias == $alias) {
            $vars['view'] = 'category';
            $vars['id'] = $id;

            return $vars;
        } else {
            $query = 'SELECT alias, catid FROM #__tb_tours WHERE id = '.(int)$id;
            $db->setQuery($query);
            $tour = $db->loadObject();

            if ($tour) {
                if ($tour->alias == $alias) {
                    $vars['view'] = 'tour';
                    $vars['catid'] = (int)$tour->catid;
                    $vars['id'] = (int)$id;

                    return $vars;
                }
            }
        }
    }

    // if there was more than one segment, then we can determine where the URL points to
    // because the first segment will have the target category id prepended to it.  If the
    // last segment has a number prepended, it is an tour, otherwise, it is a category.
    if (!$advanced) {
        $cat_id = (int)$segments[0];

        $tour_id = (int)$segments[$count - 1];

        if ($tour_id > 0) {
            $vars['view'] = 'tour';
            $vars['catid'] = $cat_id;
            $vars['id'] = $tour_id;
        } else {
            $vars['view'] = 'category';
            $vars['id'] = $cat_id;
        }

        return $vars;
    }

    // we get the category id from the menu item and search from there
    $id = $item->query['id'];
    $category = JCategories::getInstance('travelbook.tour')->get($id);

    if (!$category) {
        JError::raiseError(404, JText::_('COM_TRAVELBOOK_ERROR_PARENT_CATEGORY_NOT_FOUND'));
        return $vars;
    }

    $categories = $category->getChildren();
    $vars['catid'] = $id;
    $vars['id'] = $id;
    $found = 0;

    foreach($segments as $segment)
    {
        $segment = str_replace(':', '-', $segment);

        foreach($categories as $category)
        {
            if ($category->alias == $segment) {
                $vars['id'] = $category->id;
                $vars['catid'] = $category->id;
                $vars['view'] = 'category';
                $categories = $category->getChildren();
                $found = 1;
                break;
            }
        }

        if ($found == 0) {
            if ($advanced) {
                $db = JFactory::getDBO();
                $query = 'SELECT id FROM #__tb_tours WHERE catid = '.$vars['catid'].' AND alias = '.$db->Quote($segment);
                $db->setQuery($query);
                $cid = $db->loadResult();
            } else {
                $cid = $segment;
            }

            $vars['id'] = $cid;

            if ($item->query['view'] == 'archive' && $count != 1){
                $vars['year']	= $count >= 2 ? $segments[$count-2] : null;
                $vars['month'] = $segments[$count-1];
                $vars['view']	= 'archive';
            }
            else {
                $vars['view'] = 'tour';
            }
        }

        $found = 0;
    }

    return $vars;
}
