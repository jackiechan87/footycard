<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
jimport('joomla.methods');
jimport('joomla.environment.uri');

/**
 * OpenSearch View class for the Travelbook component
 *
 * @static
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @since 2.0
 */
class TravelbookViewSearch extends JViewLegacy
{
	function display($tpl = null)
	{
		$doc = JFactory::getDocument();
		$app = JFactory::getApplication();

		$params = JComponentHelper::getParams('com_travelbook');
		$doc->setShortName($params->get('opensearch_name', $app->getCfg('sitename')));
		$doc->setDescription($params->get('opensearch_description', $app->getCfg('MetaDesc')));

		// Add the URL for the search
		$searchUri = JURI::base().'index.php?option=com_travelbook&searchword={searchTerms}';

		// Find the menu item for the search
		$menu = $app->getMenu();
		$items = $menu->getItems('link', 'index.php?option=com_travelbook&view=search');
		if (isset($items[0])) {
			$searchUri .= '&Itemid='.$items[0]->id;
		}

		$htmlSearch = new JOpenSearchUrl();
		$htmlSearch->template = JRoute::_($searchUri);
		$doc->addUrl($htmlSearch);
	}
}
