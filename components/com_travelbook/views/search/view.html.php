<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
jimport('demopage.currency');

/**
 * Search View class
 *
 * @static
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @since 2.0
 */
class TravelbookViewSearch extends JViewLegacy
{
    function display($tpl = null)
    {
        require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/search.php';

        // Initialise some variables
        $app = JFactory::getApplication();
        $pathway = $app->getPathway();
        $uri = JFactory::getURI();

        $error = null;
        $rows = null;
        $results = null;
        $total = 0;

        // Get some data from the model
        $areas = $this->get('areas');
        $state = $this->get('state');
        $searchword = $state->get('keyword');
        $criterias = $state->get('criterias');
		
		$this->destinations = JCategories::getInstance('travelbook.destination');
		$this->activities = JCategories::getInstance('travelbook.activity');
		$this->styles = JCategories::getInstance('travelbook.style');
		
		$params = $app->getParams();

        $this->form = $this->get('form');

        $menus = $app->getMenu();
        $menu = $menus->getActive();

        // because the application sets a default page title, we need to get it
        // right from the menu item itself
        if (is_object($menu)) {
            $menu_params = new JRegistry;
            $menu_params->loadString($menu->params);
            if (!$menu_params->get('page_title')) {
                $params->set('page_title',	JText::_('COM_TRAVELBOOK_SEARCH'));
            }
        }
        else {
            $params->set('page_title',	JText::_('COM_TRAVELBOOK_SEARCH'));
        }

        // Currency
//		$cParams = JComponentHelper::getParams( 'com_travelbook' );
        $this->currency = new DPCurrency($params);
        
		$title = $params->get('page_title');
        if ($app->getCfg('sitename_pagetitles', 0) == 1) {
            $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
        }
        elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
            $title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
        }
        $this->document->setTitle($title);

        if ($params->get('menu-meta_description'))
        {
            $this->document->setDescription($params->get('menu-meta_description'));
        }

        if ($params->get('menu-meta_keywords'))
        {
            $this->document->setMetadata('keywords', $params->get('menu-meta_keywords'));
        }

        if ($params->get('robots'))
        {
            $this->document->setMetadata('robots', $params->get('robots'));
        }

        $this->searchType = JRequest::getWord('searchtype', 'advanced', 'get');

        // log the search
        switch ($this->searchType) {
            case 'keyword':
                SearchHelper::logSearchKeyword($searchword);
                //limit searchword
                $lang = JFactory::getLanguage();
                $upper_limit = $lang->getUpperLimitSearchWord();
                $lower_limit = $lang->getLowerLimitSearchWord();
                if (SearchHelper::limitSearchWord($searchword)) {
                    $error = JText::sprintf('COM_TRAVELBOOK_ERROR_SEARCH_MESSAGE', $lower_limit, $upper_limit);
                }

                //sanatise searchword
                if (SearchHelper::santiseSearchWord($searchword, $state->get('match'))) {
                    $error = JText::_('COM_TRAVELBOOK_ERROR_IGNOREKEYWORD');
                }

                if (!$searchword) {
                    $error = JText::_('COM_TRAVELBOOK_ERROR_ENTERKEYWORD');
                }
                // put the filtered results back into the model
                $state->set('keyword', $searchword);
                break;
            case 'advanced':
                SearchHelper::logSearchAdvanced($criterias);
                $searchword = '';
                break;
            case 'expert':
                SearchHelper::logSearchExpert($searchword);
                break;

            default:
                ;
                break;
        }


        // for next release, the checks should be done in the model perhaps...
        if ($error == null) {
            $results = $this->get($this->searchType.'Results');
            $total = $this->get('total');
            $pagination	= $this->get('pagination');

            require_once JPATH_SITE . '/components/com_travelbook/helpers/route.php';

            for ($i=0, $count = count($results); $i < $count; $i++)
            {
                $row = &$results[$i]->text;

                if ($state->get('match') == 'exact') {
                    $searchwords = array($searchword);
                    $needle = $searchword;
                }
                else {
                    $searchworda = preg_replace('#\xE3\x80\x80#s', ' ', $searchword);
                    $searchwords = preg_split("/\s+/u", $searchworda);
                    $needle = $searchwords[0];
                }
                
                // attach destinations
                $destinations = json_decode($results[$i]->destination);
                $destinationArray = array();
				foreach ($destinations as $key => &$destination) {
			        $destinationArray[$key] = $this->destinations->get($destination)->title;
				}
				$results[$i]->destinationsArray = $destinationArray;

				// attach activities
                $activities = json_decode($results[$i]->activity);
                $activitiesArray = array();
                foreach ($activities as $key => &$activity) {
			        $activitiesArray[$key] = $this->activities->get($activity)->title;
				}
				$results[$i]->activitiesArray = $activitiesArray;

				// attach styles
                $styles = json_decode($results[$i]->style);
                $stylesArray = array();
                foreach ($styles as $key => &$style) {
			        $stylesArray[$key] = $this->styles->get($style)->title;
				}
				$results[$i]->stylesArray = $stylesArray;
				
		        $lang = JFactory::getLanguage();
                $configLength = $lang->getSearchDisplayedCharactersNumber();
                $length = $params->get('search_content_length', $configLength);
                $row = SearchHelper::prepareSearchContent($row, $needle, $length);
 //               $row = empty($needle) ? $row : SearchHelper::prepareSearchContent($row, $needle);
                $searchwords = array_unique($searchwords);
                
                if (!empty($needle)) {
                    $searchRegex = '#(';
                    $x = 0;
    
                    foreach ($searchwords as $k => $hlword)
                    {
                        $searchRegex .= ($x == 0 ? '' : '|');
                        $searchRegex .= preg_quote($hlword, '#');
                        $x++;
                    }
                    $searchRegex .= ')#iu';
    
                    $row = preg_replace($searchRegex, '<span class="highlight">\0</span>', $row);
                }

                $result = &$results[$i];
                if ($result->created) {
                    $created = JHtml::_('date', $result->created, JText::_('DATE_FORMAT_LC3'));
                }
                else {
                    $created = '';
                }

//                $result->text = JHtml::_('content.prepare', $result->text, '', 'com_travelbook.search');
                $result->created = $created;
                $result->count = $i + 1;
            }
        }

		if ($this->searchType == 'advanced' && $criterias['ordering'] == 'relevance') {
    		$results = $this->_sortDatas($results);
		}
        
        // Check for layout override
		$active	= $app->getMenu()->getActive();
        if (isset($active->query['layout'])) {
            $this->setLayout($active->query['layout']);
        }

        //Escape strings for HTML output
        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

        $this->assignRef('pagination', $pagination);
        $this->assignRef('results', $results);
        $this->assignRef('lists', $lists);
        $this->assignRef('params', $params);

        $this->assign('ordering', $state->get('ordering'));
        $this->assign('searchword', $searchword);
        $this->assign('originalKeyword', $state->get('originalKeyword'));
        $this->assign('searchphrase', $state->get('match'));
        $this->assign('searchareas', $areas);

        $this->assign('criterias', $state->get('criterias'));

        $this->assign('total', $total);
        $this->assign('error', $error);
        $this->assign('action', $uri);

		JHtml::stylesheet('com_travelbook/com_travelbook.search.css', false, true);
//		$doc = JFactory::getDocument();
//		$style = 'media/com_travelbook/css/layout.php?Itemid='.JRequest::getInt('Itemid');
//		$doc->addStyleSheet($style);
		
		parent::display($tpl);
    }

	/**
	 * Sorts the searchresults by relevanc
	 * @return true
	 */
	private function _sortDatas($datas)
	{
		function order_up($a, $b)
		{
			if ($a->featured  == $b->featured )
			{
				// featured is the same, sort by relevance
				if ($a->relevance == $b->relevance) return 0;
				return $a->relevance > $b->relevance ? -1 : 1;
			}

			// sort the featured first:
			return $a->featured < $b->featured ? 1 : -1;
		}

		function order_down($a, $b)
		{
			if ($a->featured  == $b->featured )
			{
				// featured is the same, sort by relevance
				if ($a->relevance == $b->relevance) return 0;
				return $a->relevance < $b->relevance ? -1 : 1;
			}

			// sort the featured first:
			return $a->featured < $b->featured ? 1 : -1;
		}

		// Sort and print the resulting array
		if (strtolower(JRequest::getVar('filter_order_Dir', 'desc', 'post', 'string')) == 'desc') {
			usort($datas, 'order_up');
		} else {
			usort($datas, 'order_down');
		}

		return $datas;
	}
}