<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die; 

JHtml::_('behavior.tooltip');
?>

<div class="clr"></div>

<ul class="search-results<?php echo $this->pageclass_sfx; ?>">
	<?php $i=0; ?>
    <?php foreach($this->results as $result) : ?>
    	<?php $i++; ?>
	    <?php $images = json_decode($result->images); ?>
        <li id="item-<?php echo $result->id; ?>">
			<!-- Title -->
        	<div class="result-title">
       			<h2 class="tour-title">
        		    <span class="small"><?php echo $this->pagination->limitstart + $i.'. ';?></span>
            		<?php if ($result->href) :?>
                			<a href="<?php echo JRoute::_($result->href); ?>"<?php if ($result->browsernav == 1) :?> target="_blank"<?php endif;?>>
                				<?php echo $this->escape($result->title);?>
                			</a>
            		<?php else:?>  			
            			<?php echo $this->escape($result->title);?>
            		<?php endif; ?>
                	<?php if ($result->section) : ?>
                		<span class="result-category">
                			<span class="small<?php echo $this->pageclass_sfx; ?>">
                				(<?php echo $this->escape($result->section); ?>)
                			</span>
                		</span>
                	<?php endif; ?>
           		</h2>
    		</div>

            <!-- # Relevance -->
        	<?php if ($this->params->get('show_relevances') && $this->searchType == 'advanced') : ?>
            	<div class="relevance hasTip" title="<?php echo JText::_('COM_TRAVELBOOK_TOOLTIP_TITLE_RELEVANCE'); ?>::<?php echo JText::_('COM_TRAVELBOOK_TOOLTIP_RELEVANCE'); ?>">            	
        			<div class="star0" style="clip: rect(0px, <?php echo (63*$result->relevance); ?>px, 16px, auto)">&nbsp;</div>
        			<div class="star1">&nbsp;</div>
    			</div>
			<?php endif; ?>
    
		    <!-- Image -->
    		<?php if ($this->params->get('show_base_tour_image') == 1) :?>
                <?php if (isset($images->image_intro) and !empty($images->image_intro)) : ?>
                	<?php $imgfloat = (empty($images->float_intro)) ? $this->params->get('float_intro') : $images->float_intro; ?>
                	<div class="img-intro-<?php echo htmlspecialchars($imgfloat); ?>">
                	<img
                		<?php 
                		    if ($images->image_intro_caption):
                			echo 'class="caption"'.' title="' .htmlspecialchars($images->image_intro_caption) .'"';
                		    endif; 
                        ?>
                		src="<?php echo htmlspecialchars($images->image_intro); ?>" alt="<?php echo htmlspecialchars($images->image_intro_alt); ?>"/>
                	</div>
                <?php endif; ?>
    	    <?php endif; ?>
    
    		<!-- Text -->
        	<div class="result-text">
        		<?php echo $result->text; ?>
        	</div>

			<!-- Date -->
        	<?php if ($this->params->get('search_show_date')) : ?>
        		<div class="result-created<?php echo $this->pageclass_sfx; ?>">
        			<?php echo JText::sprintf('JGLOBAL_CREATED_DATE_ON', $result->created); ?>
        		</div>
        	<?php endif; ?>
        
    		<?php if ($this->params->get('show_base_tour_image') == 1) :?>
    			<div class="clr"></div>
     		<?php endif; ?>

			<!-- Characteritica -->
        	<?php if ($this->params->get('show_characteristica')) : ?>
        		<ul class="result-characteristica<?php echo $this->pageclass_sfx; ?>">
                    <!-- Rate -->
                	<?php if ($this->params->get('show_rate')) : ?>
                        <li>
            	            <?php echo JText::_('COM_TRAVELBOOK_RATE').': '.JText::sprintf('COM_TRAVELBOOK_RATE_FROM', $this->currency->calculate($result->minRate))."<br />"; ?>
                        </li>
        			<?php endif; ?>
        			
                    <!-- Duration -->
                	<?php if ($this->params->get('show_duration')) : ?>
                        <li>
                        	<?php 
                            	$duration = $result->minDuration == $result->maxDuration ? JText::plural('COM_TRAVELBOOK_DURATION_N_DAY', $result->minDuration) : JText::plural('COM_TRAVELBOOK_DURATION_N_DAYS', $result->minDuration, $result->maxDuration);
                        	    echo JText::_('COM_TRAVELBOOK_DURATION').': '.$duration."<br />"; 
                            ?>
                        </li>
        			<?php endif; ?>
                    
                    <!-- # Dates -->
                	<?php if ($this->params->get('show_count_dates')) : ?>
                        <li>
            	            <?php $dates = $result->dates; ?>
                    		<?php if ($this->params->get('link_dates')) : ?>
                    			<a href="<?php echo JRoute::_(TravelbookHelperRoute::getDatesRoute($result->slug));?>">
            			            <?php echo JText::plural('COM_TRAVELBOOK_COUNT_N_DATES', $dates)."<br />"; ?>
                    			</a>
                    		<?php else : ?>
            		            <?php echo JText::plural('COM_TRAVELBOOK_COUNT_N_DATES', $dates)."<br />"; ?>
            				<?php endif; ?>
                        </li>
        			<?php endif; ?>
                        
                	<!-- Hits -->
                	<?php if ($this->params->get('show_search_hits')) : ?>
                        <li>
            	            <?php echo JText::_('COM_TRAVELBOOK_HITS').":&nbsp;".$result->hits."<br />"; ?>
                        </li>
        			<?php endif; ?>
        			
                	<!-- Characteritica -->
                	<?php if ($this->params->get('show_characteristica')) : ?>
            			<li>
                            <?php echo JText::plural('COM_TRAVELBOOK_DESTINATIONS_N', count($result->destinationsArray)).":&nbsp;".implode(",&nbsp;", $result->destinationsArray);?>
                        </li>
                        <li>
                            <?php echo JText::plural('COM_TRAVELBOOK_ACTIVITIES_N', count($result->activitiesArray)).":&nbsp;".implode(",&nbsp;", $result->activitiesArray);?>
                        </li>
                        <li>
                            <?php echo JText::plural('COM_TRAVELBOOK_STYLES_N', count($result->stylesArray)).":&nbsp;".implode(",&nbsp;", $result->stylesArray); ?>
                        </li>
        			<?php endif; ?>
				</ul>
			<?php endif; ?>
			<div class="border"></div>
        </li>
    <?php endforeach; ?>
</ul>