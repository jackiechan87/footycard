<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$lang = JFactory::getLanguage();
$upper_limit = $lang->getUpperLimitSearchWord(); ?>

<form id="searchForm" action="<?php echo JRoute::_('index.php?option=com_travelbook');?>" method="post">

	<fieldset class="word">
		<?php  $this->form->setFieldAttribute('keyword', 'maxlength', $upper_limit, 'keyword'); ?>
		<?php echo $this->form->getLabel('keyword', 'keyword'); ?>
		<?php echo $this->form->getInput('keyword', 'keyword', $this->escape($this->originalKeyword)); ?>
		<button name="Search" onclick="this.form.submit()" class="button"><?php echo JText::_('COM_TRAVELBOOK_SEARCH');?></button>
		<input type="hidden" name="task" value="search" />
		<input type="hidden" name="searchtype" value="keyword" />
	</fieldset>

	<div class="searchintro<?php echo $this->params->get('pageclass_sfx'); ?>">
		<?php if (!empty($this->searchword)):?>
		<p><?php echo JText::plural('COM_TRAVELBOOK_SEARCH_KEYWORD_N_RESULTS', $this->total);?></p>
		<?php endif;?>
	</div>

	<fieldset class="phrases">
		<?php echo $this->form->getLabel('match', 'general'); ?>
		<?php echo $this->form->getInput('match', 'general', $this->escape($this->searchphrase)); ?>
	</fieldset>
	<fieldset class="ordering">
		<?php echo $this->form->getLabel('ordering', 'general'); ?>
		<?php echo $this->form->getInput('ordering', 'general', $this->escape($this->ordering)); ?>
	</fieldset>

<?php if ($this->total > 0) : ?>

	<div class="form-limit">
		<label for="limit">
			<?php echo JText::_('JGLOBAL_DISPLAY_NUM'); ?>
		</label>
		<?php echo $this->pagination->getLimitBox(); ?>
	</div>
<p class="counter">
		<?php echo $this->pagination->getPagesCounter(); ?>
	</p>

<?php endif; ?>

</form>
