<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$lang = JFactory::getLanguage();
$upper_limit = $lang->getUpperLimitSearchWord(); ?>

<fieldset class="advanced<?php echo $this->params->get('pageclass_sfx'); ?>">
	<legend><?php echo JText::_('COM_TRAVELBOOK_SEARCH_CRITERIAS');?></legend>
<div class="subcolumns">
  <div class="c33l">
    <div class="subcl">
        <?php 
        $output = null;
        $this->form->setFieldAttribute('category', 'id', 'component_category', 'advanced');
        $output .= $this->form->getLabel('category', 'advanced');
        $output .= $this->form->getInput('category', 'advanced', $this->escape($this->criterias['category']));
        echo $output; 
        ?>
    </div>
  </div>
  <div class="c33l">
    <div class="subcl">
        <?php 
        $output = null;
        $this->form->setFieldAttribute('destination', 'id', 'component_destination', 'advanced');
        $output .= $this->form->getLabel('destination', 'advanced');
        $output .= $this->form->getInput('destination', 'advanced', $this->escape($this->criterias['destination']));
        echo $output; 
        ?>
    </div>
  </div>
  <div class="c33r">
    <div class="subcr">
        <?php 
        $output = null;
        $this->form->setFieldAttribute('activity', 'id', 'component_activity', 'advanced');
        $output .= $this->form->getLabel('activity', 'advanced');
        $output .= $this->form->getInput('activity', 'advanced', $this->escape($this->criterias['activity']));
        echo $output; 
        ?>
	</div>
  </div>
</div>
<div class="subcolumns">
  <div class="c33l">
    <div class="subcl">
        <?php 
        $output = null;
        $departure = $this->criterias['departure'] == '' ? '' : JHtml::_('date', $this->escape($this->criterias['departure']), JText::_('DATE_FORMAT_TB1'));
        $this->form->setFieldAttribute('departure', 'id', 'component_departure', 'advanced');
        $this->form->setFieldAttribute('departure', 'format', JText::_('DATE_FORMAT_TB6'), 'advanced');
        $output .= $this->form->getLabel('departure', 'advanced');
        $output .= $this->form->getInput('departure', 'advanced', $departure);
        echo $output; 
        ?>
    </div>
  </div>
  <div class="c33l">
    <div class="subcl">
        <?php 
        $output = null;
        $arrival = $this->criterias['arrival'] == '' ? '' : JHtml::_('date', $this->escape($this->criterias['arrival']), JText::_('DATE_FORMAT_TB1'));
        $this->form->setFieldAttribute('arrival', 'id', 'component_arrival', 'advanced');
        $this->form->setFieldAttribute('arrival', 'format', JText::_('DATE_FORMAT_TB6'), 'advanced');
        $output .= $this->form->getLabel('arrival', 'advanced');
        $output .= $this->form->getInput('arrival', 'advanced', $arrival);
        echo $output; 
        ?>
    </div>
  </div>
  <div class="c33r">
    <div class="subcr"></div>
  </div>
</div>
<div class="subcolumns">
  <div class="c33l">
    <div class="subcl">
        <?php 
        $output = null;
        $this->form->setFieldAttribute('duration-from', 'id', 'component_duration-from', 'advanced');
        $output .= $this->form->getLabel('duration-from', 'advanced');
        $output .= $this->form->getInput('duration-from', 'advanced', $this->escape($this->criterias['duration-from']));
        $output .= '<span class="left">'.JText::_('COM_TRAVELBOOK_SEARCH_TO').'</span>';
        $this->form->setFieldAttribute('duration-to', 'id', 'component_duration-to', 'advanced');
        $output .= $this->form->getInput('duration-to', 'advanced', $this->escape($this->criterias['duration-to']));
        $output .= '<span class="left">&nbsp;'.JText::_('COM_TRAVELBOOK_SEARCH_DAYS').'</span>';
        echo $output; 
        ?>
    </div>
  </div>
  <div class="c33l">
    <div class="subcl">
        <?php 
        $output = null;
        $this->form->setFieldAttribute('budget-from', 'id', 'component_budget-from', 'advanced');
        $output .= $this->form->getLabel('budget-from', 'advanced');
        $output .= $this->form->getInput('budget-from', 'advanced', $this->escape($this->criterias['budget-from']));
        $output .= '<span class="left">'.JText::_('COM_TRAVELBOOK_SEARCH_TO').'</span>';
        $this->form->setFieldAttribute('budget-to', 'id', 'component_budget-to', 'advanced');
        $output .= $this->form->getInput('budget-to', 'advanced', $this->escape($this->criterias['budget-to']));
        $output .= '<span class="left">&nbsp;'.JText::_('COM_TRAVELBOOK_SEARCH_EURO').'</span>';
        echo $output; 
        ?>
    </div>
  </div>
  <div class="c33r">
    <div class="subcr"></div>
  </div>
</div>


	<?php // echo $output; ?>
	<button name="search" onclick="this.form.submit()" class="button"><?php echo JText::_('COM_TRAVELBOOK_SEARCH');?></button>
	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="searchtype" value="advanced" />
</fieldset>