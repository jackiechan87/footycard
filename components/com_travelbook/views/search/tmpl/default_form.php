<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$lang = JFactory::getLanguage();
$upper_limit = $lang->getUpperLimitSearchWord(); ?>

<form id="searchForm" action="<?php echo JRoute::_('index.php?option=com_travelbook');?>" method="post">
	<?php echo $this->loadTemplate('form_'.$this->searchType); ?>
    <fieldset>
      <legend><?php echo JText::_('COM_TRAVELBOOK_SEARCH_RESULT');?></legend>
      <div class="subcolumns">
        <div class="c50l">
          <div class="subcl">
            <div class="searchintro<?php echo $this->params->get('pageclass_sfx'); ?>">
              <div class="label"><?php echo JText::plural('COM_TRAVELBOOK_SEARCH_KEYWORD_N_RESULTS', $this->total);?></div>
            </div>
          </div>
        </div>
        <div class="c25l">
          <div class="subcl">
            <div class="ordering<?php echo $this->params->get('pageclass_sfx'); ?>">
                <?php echo $this->form->getLabel('ordering', $this->searchType); ?> 
                <?php echo $this->form->getInput('ordering', $this->searchType, $this->escape($this->ordering)); ?> 
            </div>
          </div>
        </div>
        <div class="c25r">
          <div class="subcr">
            <?php if ($this->total > 0) : ?>
            <div class="form-limit">
              <label for="limit"> <?php echo JText::_('JGLOBAL_DISPLAY_NUM'); ?> </label>
              <?php echo $this->pagination->getLimitBox(); ?> 
            </div>
            <?php else : ?>
				<input type="hidden" name="limit" value="20" />
            <?php endif; ?>
          </div>
        </div>
      </div>
    </fieldset>
</form>