<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$lang = JFactory::getLanguage();
$upper_limit = $lang->getUpperLimitSearchWord(); ?>

<fieldset class="word">
	<div class="subcolumns">
    	<?php  $this->form->setFieldAttribute('keyword', 'maxlength', $upper_limit, 'keyword'); ?>
    	<?php echo $this->form->getLabel('keyword', 'keyword'); ?>
    	<?php echo $this->form->getInput('keyword', 'keyword', $this->escape($this->originalKeyword)); ?>
    </div>
	<div class="subcolumns">
    <div class="phrases">
    	<?php echo $this->form->getLabel('match', 'general'); ?>
    	<?php echo $this->form->getInput('match', 'general', $this->escape($this->searchphrase)); ?>
    </div>
    </div>

	<button name="Search" onclick="this.form.submit()" class="button"><?php echo JText::_('COM_TRAVELBOOK_SEARCH');?></button>
	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="searchtype" value="keyword" />
</fieldset>

