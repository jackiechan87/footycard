<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');

?>
<div id="travelbook" class="tours-list<?php echo $this->pageclass_sfx;?>">
    <?php if ($this->params->get('show_page_heading', 1)) : ?>
    	<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
    <?php endif; ?>

    <?php if ($this->params->get('show_base_description')) : ?>
        <?php if($this->params->get('tours_description')) : ?>
        	<div class="base-desc">
        	    <?php echo  JHtml::_('content.prepare', $this->params->get('tours_description'), '', 'com_travelbook.tours'); ?>
        	</div>
        <?php endif; ?>
	<?php endif; ?>     
    
    <?php echo $this->loadTemplate('items'); ?>
</div>