<?php

/**
 * @package		Joomla.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$class = ' class="first"'; 
?>

<ul>
<?php $i = 0; ?>
<?php foreach($this->items as $id => $item) : ?>
	<?php
    $images = json_decode($item->images);
	$i++;
	$class = $i==1 ? ' first' : '';
	$class .= $i==count($this->items) ? ' last' : '';
	$class = ' class="'.trim($class).'"';
	if ($this->params->get('show_base_empty_tours') || $item->numitems) :
	?>
	<li id="item-<?php echo $id; ?>"<?php echo $class; ?>>
		<h2 class="tour-title"><a href="<?php echo JRoute::_(TravelbookHelperRoute::getTourRoute($item->id, $item->catid));?>">
			<?php echo $this->escape($item->title); ?></a>
		</h2>

<!-- Category -->
        <?php if ($this->params->get('show_base_category')) : ?>
        	<?php 
        	    $title = $this->escape($item->category_title);
        	    $url = '<a href="'.JRoute::_(TravelbookHelperRoute::getCategoryRoute($item->catslug)).'">'.$title.'</a>';
        	?>
        	<h3 class="category-name"><?php echo ($this->params->get('link_base_category') and $item->catslug) ? $url : $title; ?></h3>
        <?php endif; ?>

<!-- Image -->
		<?php if ($this->params->get('show_base_tour_image') == 1) :?>
            <?php if (isset($images->image_intro) and !empty($images->image_intro)) : ?>
            	<?php $imgfloat = (empty($images->float_intro)) ? $this->params->get('float_intro') : $images->float_intro; ?>
            	<div class="img-intro-<?php echo htmlspecialchars($imgfloat); ?>">
            	<img
            		<?php 
            		    if ($images->image_intro_caption):
            			echo 'class="caption"'.' title="' .htmlspecialchars($images->image_intro_caption) .'"';
            		    endif; 
                    ?>
            		src="<?php echo htmlspecialchars($images->image_intro); ?>" alt="<?php echo htmlspecialchars($images->image_intro_alt); ?>"/>
            	</div>
            <?php endif; ?>
	    <?php endif; ?>

		<?php if ($this->params->get('show_base_tour_desc') == 1) :?>
    		<?php if ($item->introtext) : ?>
    			<div class="tour-desc">
    				<?php echo JHtml::_('content.prepare', $item->introtext, '', 'com_travelbook.tours'); ?>
    			</div>
     		<?php endif; ?>
        <?php endif; ?>

		<?php if ($this->params->get('show_base_tour_image') == 1) :?>
			<div class="clr"></div>
 		<?php endif; ?>

		<?php if ($this->params->get('show_base_num_dates') == 1) :?>
   			<div class="date-num">
				<?php echo JText::sprintf('COM_TRAVELBOOK_NUM_DATES', $item->numitems); ?>
    		</div>
			<div class="clr"></div>
		<?php endif; ?>

	</li>
	<?php endif; ?>
<?php endforeach; ?>
</ul>