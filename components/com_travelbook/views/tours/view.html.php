<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * Travelbook tours view.
 *
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @since 2.0
 */
class TravelbookViewTours extends JViewLegacy
{
    protected $state = null;
    protected $item = null;
    protected $items = null;

    /**
     * Display the view
     *
     * @return	mixed	False on error, null otherwise.
     */
    function display($tpl = null)
    {
        // Initialise variables
        $state = $this->get('State');
        $items = $this->get('Items');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            JError::raiseWarning(500, implode("\n", $errors));
            return false;
        }

        if ($items === false)
        {
            JError::raiseError(404, JText::_('COM_TRAVELBOOK_ERROR_TOUR_NOT_FOUND'));
            return false;
        }

        foreach ($items AS $item) {
            $item->catslug = $item->category_alias ? ($item->catid.':'.$item->category_alias) : $item->catid;
        }


        $params = &$state->params;

        //Escape strings for HTML output
        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

        $this->assign('maxLevelcat', $params->get('maxLevelcat', -1));
        $this->assignRef('params', $params);
        $this->assignRef('parent', $parent);
        $this->assignRef('items', $items);

        $this->_prepareDocument();

        parent::display($tpl);
        }

        /**
         * Prepares the document
         */
        protected function _prepareDocument()
        {
			JHtml::stylesheet('com_travelbook/com_travelbook.tours.css', false, true);
        	
			$app = JFactory::getApplication();
            $menus = $app->getMenu();
            $title = null;

            // Because the application sets a default page title,
            // we need to get it from the menu item itself
            $menu = $menus->getActive();
            if ($menu)
            {
                $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
            } else {
                $this->params->def('page_heading', JText::_('JGLOBAL_TOURS'));
            }
            $title = $this->params->get('page_title', '');
            if (empty($title)) {
                $title = $app->getCfg('sitename');
            }
            elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
                $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
            }
            elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
                $title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
            }
            $this->document->setTitle($title);

            if ($this->params->get('menu-meta_description'))
            {
                $this->document->setDescription($this->params->get('menu-meta_description'));
            }

            if ($this->params->get('menu-meta_keywords'))
            {
                $this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
            }

            if ($this->params->get('robots'))
            {
                $this->document->setMetadata('robots', $this->params->get('robots'));
            }
        }
    }
