<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die; ?>

<div class="items-more">

<h3><?php echo JText::_('COM_TRAVELBOOK_MORE_TOURS'); ?></h3>
<ol>
<?php foreach ($this->link_items as &$item) : ?>
	<li>
		<a href="<?php echo JRoute::_(TravelbookHelperRoute::getTourRoute($item->slug, $item->catid)); ?>"><?php echo $item->title; ?></a>
	</li>
<?php endforeach; ?>
</ol>
</div>