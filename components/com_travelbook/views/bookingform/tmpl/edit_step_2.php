<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_travelbook
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; 

// Create shortcut to parameters.
$params = $this->state->get('params');

JHtml::_('behavior.tooltip');

$statesTitle = array(0 => 'TBINCLUDED', 1 => 'TBEXCLUDED', 2 => 'TBOPTIONAL', 3 => 'TBFACULTATIV');
$statesDesc = array(0 => 'TBINCLUDED_DESC', 1 => 'TBEXCLUDED_DESC', 2 => 'TBOPTIONAL_DESC', 3 => 'TBFACULTATIV_DESC');
?>

<fieldset>
	<legend><?php echo JText::_('COM_TRAVELBOOK_BOOKING_EXTRAS'); ?></legend>
	<?php $type = -1; ?>
	<ul class="start">
		<li></li>
	<?php foreach ($this->extras as $extra) : ?>
		<?php if ($extra->type !== $type): ?>
			</ul>
				<?php if ($extra->type === '2') : ?>
					<span class="extras-col1">&nbsp;</span>
					<span class="extras-col2 filler"><?php echo $this->_booking->renderAjax(0, 'extras-subtotal'); ?></span>
					<span class="extras-col3 filler"><?php echo JText::_('TB_MARKUPS')?></span>			
				<?php endif; ?>
				<h5>
				<?php echo $params->get('show_tooltips', false) ? JHTML::tooltip(JTEXT::_($statesDesc[$extra->type]), JTEXT::_($statesTitle[$extra->type]), 'plg_travelbook_detailextras/tooltip.png', '') : ''; ?>
				<?php echo JTEXT::_($statesTitle[$extra->type]); ?>
				</h5>
			<ul class="extras">
		<?php endif; ?>	
			<?php if (in_array($extra->type, array(2))) : ?>
				<?php if (!in_array(0, json_decode($extra->pricing_plan, true))) : ?>
				<li><?php echo $this->_booking->renderExtras($extra, $this->date); ?></li>
				<?php endif; ?>
			<?php else : ?>
				<li><span class="text"><?php echo strip_tags($extra->introtext); ?></span></li>
			<?php endif; ?>
		<?php $type = $extra->type; ?>
		<?php endforeach; ?>
	</ul>
</fieldset>
