<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_travelbook
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; 

require_once JPATH_BASE . '/components/com_content/helpers/route.php';

// Create shortcut to parameters.
$params = $this->state->get('params');
$numberGuestsDefault = $params->get('number_guests_default');

$required = ( $this->params->get('validation', false) ) ? ' required' : ''; 
$validateRequiredCheck = ( $this->params->get('validation', false) ) ? ' required validate-required-check' : ''; 
$selected = explode( ':', JRequest::getVar('tour', '0', '', 'string'));
$double = floor($numberGuestsDefault / 2);
$single = $numberGuestsDefault % 2;
?>

<fieldset>
	<legend><?php echo JText::_('TB_THANKYOU'); ?></legend>
		<!-- Tour -->
		<div class='summary-col'>
			<div class='summary-col1'><?php echo JText::_('TB_TOUR'); ?></div>
			<div class='summary-col3'><?php echo $this->date->tour_title; ?></div>
		</div>
		<!-- Date -->
		<div class='summary-col'>
			<div class='summary-col1'><?php echo JText::_('TB_DATE'); ?></div>
			<div class='summary-col3'><?php echo JHtml::_('date', $this->date->departure, JText::_('DATE_FORMAT_TB3')) . ' - ' . JHtml::_('date', $this->date->arrival, JText::_('DATE_FORMAT_TB3')); ?></div>
		</div>
		
		<!-- Adults -->
		<div class='summary-col'>
			<div class='summary-col1'><?php echo JText::_('TB_PAX'); ?></div>
 			<div class='summary-col2 filler'><span class="extraFactor per-person"><?php echo $numberGuestsDefault; ?></span>&nbsp;x&nbsp;<?php echo $this->currency->calculate($this->date->rack_rate); ?></div>		
			<div class='summary-col3 filler'><span class="extraFactor per-person"><?php echo $numberGuestsDefault; ?></span>&nbsp;<?php echo JText::plural('TB_ADULTS_N', $numberGuestsDefault); ?></div>
		</div>

		<!-- Occupancy -->
		<div class='col' id='summary-occupancy-double'>
			<div class='summary-col1'><?php echo JText::_('TB_OCCUPANCY'); ?></div>
			<div class='summary-col2'>&nbsp;</div>
			<div class='summary-col3' id='summary-occupancy-double-text'><?php echo JText::sprintf('TB_DOUBLE', $double); ?></div>
		</div>
		<div class='summary-col' id="summary-occupancy-single">
			<div class='summary-col1'>&nbsp;</div>
			<?php if ($this->date->single_rate > 0) : ?>
				<div class='summary-col2 filler' id='summary-occupancy-single-rate'><span id="summary-occupancy-single-pax-total"><?php echo $single; ?></span>&nbsp;x&nbsp;<?php echo $this->currency->calculate($this->date->single_rate); ?></div>
				<div class='summary-col3 filler' id='summary-occupancy-single-text'><?php echo JText::sprintf('TB_SINGLE', $single); ?></div>
			<?php  else :?>
				<div class='summary-col2' id='summary-occupancy-single-rate'>&nbsp;</div>
				<div class='summary-col3' id='summary-occupancy-single-text'><?php echo JText::sprintf('TB_SINGLE', $single); ?></div>
			<?php endif; ?>
		</div>

		<!-- Sub-Total -->
		<?php $showExtras = $this->_booking->_config[2] && count($this->extras); ?>
		<?php if ($showExtras) : ?>
			<div class='clear'></div>
			<hr />
		<?php endif; ?>
		<div class='summary-col' style='display: <?php echo $showExtras ? "block" : "none"; ?>;'>
			<div class='summary-col1 sub'><?php echo JText::_('TB_SUBTOTAL_RATE'); ?></div>
			<div class='summary-col2 sub'><?php echo $this->_booking->renderAjax($this->_booking->calcTotalPrice($numberGuestsDefault, $this->date->rack_rate, $this->date->single_rate), 'summary-tour-rate'); ?></div>
		</div>

		<!-- Extras -->
		<div style='display: <?php echo $showExtras ? "block" : "none"; ?>;'>
			<?php echo $this->loadTemplate('step_3_extras'); ?>
		</div>
		<?php if ($showExtras) : ?>
			<!-- Sub-Total Extras-->
			<div class='clear'></div>
			<hr />
			<div class='summary-col'>
				<div class='summary-col1 sub'><?php echo JText::_('TB_SUBTOTAL_RATE_EXTRAS'); ?></div>
				<div class='summary-col2 sub'><?php echo $this->_booking->renderAjax(0, 'summary-extras-subtotal'); ?></div>
			</div>
		<?php endif; ?>

		<!-- Total -->
		<div class='clear'></div>
		<hr />
		<div class='summary-col'>
			<div class='summary-col1'><?php echo JText::_('TB_TOTAL_RATE'); ?></div>
			<div class='summary-col2'><?php echo $this->_booking->renderAjax($this->_booking->calcTotalPrice($numberGuestsDefault, $this->date->rack_rate, $this->date->single_rate), 'summary-total-rate'); ?></div>
			<div class='summary-col3'></div>
		</div>
		<!-- Guests -->
		<div class='clear'></div>
		<hr />
		<div id='summary_guests' class='summary-col'>
			<div class='summary-col1'><?php echo JText::_('TB_GUESTS'); ?></div>
			<div class='summary-col2'>&nbsp;</div>
			<div class='summary-col3'>
			    <div class="label">
			        <span class='serial'><?php echo JText::_('TB_ITEM'); ?></span>
			        <span class='title'><?php echo JText::_('TB_TITLE_LABEL'); ?></span>
			        <span class='firstname'><?php echo JText::_('TB_BILLING_FIRSTNAME_LABEL'); ?></span>
			        <span class='lastname'><?php echo JText::_('TB_BILLING_LASTNAME_LABEL'); ?></span>
			        <span class='birthdate'><?php echo JText::_('TB_BILLING_BIRTHDATE_LABEL'); ?></span>
			    </div>
				<?php for ($i=1; $i < (1+$this->params->get('number_guests_max', 8)); $i++) {
					$style = $i <= $numberGuestsDefault ? 'style="display: block;"' : "style='display: none;'"; 
					$odd = $i%2; ?>
					<div id='summary_guest_<?php echo $i; ?>' class='row<?php echo $i%2; ?>' <?php echo $style; ?>>
				        <span class='serial'><?php echo $i; ?>.</span>
				        <span class='title html' id='jform_guests_address_<?php echo ($i-1); ?>_summary'></span>
				        <span class='firstname html' id='jform_guests_firstname_<?php echo ($i-1); ?>_summary'></span>
				        <span class='lastname html' id='jform_guests_lastname_<?php echo ($i-1); ?>_summary'></span>
				        <span class='birthdate html' id='jform_guests_date_of_birth_<?php echo ($i-1); ?>_summary'></span>
					</div>
				<?php }?>
			</div>
		</div>
		<!-- Terms & Condition, Privacy Policy, Opposition Instruction -->
		<?php if ( $this->params->get('termsConditions', true) || $this->params->get('privacyPolicy', true) || $this->params->get('cancellationTerms', true) ) : ?>
			<div class='clear'></div>
			<hr />
			<div class='summary-col'>
				<!-- Terms -->
				<?php if ($this->params->get('termsConditions', true)) : ?>
					<fieldset class="checkboxes required" id="summary_terms_terms">
						<div class='summary-col1 terms'>
					        <div class='checkbox terms'>
					        	<input class='validate-required-check' type='checkbox' name='jform[terms][<?php echo JText::_("terms"); ?>]' value='<?php echo $this->params->get('termsArticle', false); ?>' id='summary_terms_terms0' />
					        </div>
						</div>
						<div class='summary-col2 terms'>&nbsp;</div>
						<div class='summary-col3 terms'>
							<div class='terms'>
								<label for='summary_terms_terms0' class='checkbox required'><?php echo JText::_('TB_ACCEPT_TERMS') ?></label>
							</div>
							<div class='terms'>
								<?php if ($article = $this->params->get('termsArticle', false)) : ?>
									<?php $link = JRoute::_(ContentHelperRoute::getArticleRoute($article, 0, 0) , false); ?>
									<a href='<?php echo $link; ?>' target='_blank'><?php echo JText::_('TB_READ_TERMS'); ?></a>
								<?php endif; ?>
							</div>
							<div class='terms'>
							<?php if (($pdf = $this->params->get('termsPdf', false)) && ($this->params->get('termsPdf', false) !== '-1')) : ?>
								<a href='<?php echo JURI::base(); ?>images/com_travelbook/documents/<?php echo $pdf; ?>'><?php echo JText::_('TB_DOWNLOAD_TERMS'); ?></a>																		
							<?php endif; ?>
							</div>
						</div>
					</fieldset>
				<?php else: ?>
					<input type='hidden' id='summary_terms_terms0' value='' />
				<?php endif; ?>
				<!-- Privacy Policy -->
				<?php if ($this->params->get('privacyPolicy', true)) : ?>
					<fieldset class="checkboxes required" id="summary_terms_privacy">
						<div class='summary-col1 terms'>
					        <div class='checkbox terms'>
					        	<input class='validate-required-check' type='checkbox' name='jform[terms][<?php echo JText::_("privacy"); ?>]' value='<?php echo $this->params->get('policyArticle', false); ?>' id='summary_terms_privacy0' />
					        </div>
						</div>
						<div class='summary-col2 terms'>&nbsp;</div>
						<div class='summary-col3 terms'>
							<div class='terms'>
								<label for='summary_terms_privacy0' class='checkbox required'><?php echo JText::_('TB_ACCEPT_PRIVACY') ?></label>
							</div>
							<div class='terms'>
							<?php if ($article = $this->params->get('policyArticle', false)) : ?>
								<?php $link = JRoute::_(ContentHelperRoute::getArticleRoute($article, 0, 0) , false); ?>
								<a href='<?php echo $link; ?>' target='_blank'><?php echo JText::_('TB_READ_PRIVACY'); ?></a>
							<?php endif; ?>
							</div>
							<div class='terms'>
							<?php if (($pdf = $this->params->get('policyPdf', false)) && ($this->params->get('policyPdf', false) !== '-1')) : ?>
								<a href='<?php echo JURI::base(); ?>images/com_travelbook/documents/<?php echo $pdf; ?>'><?php echo JText::_('TB_DOWNLOAD_PRIVACY'); ?></a>
							<?php endif; ?>
							</div>
						</div>
					</fieldset>
				<?php else: ?>
					<input type='hidden' id='summary_terms_privacy0' value='' />
				<?php endif; ?>
				<!-- Opposition -->
				<?php if ($this->params->get('cancellationTerms', true)) : ?>
					<fieldset class="checkboxes required" id="summary_terms_opposition">
						<div class='summary-col1 terms'>
					        <div class='checkbox terms'>
					        	<input class='validate-required-check' type='checkbox' name='jform[terms][<?php echo JText::_("opposition"); ?>]' value='<?php echo $this->params->get('cancellationArticle', false); ?>' id='summary_terms_opposition0' />
					        </div>
						</div>
						<div class='summary-col2 terms'>&nbsp;</div>
						<div class='summary-col3 terms'>
							<div class='terms'>
								<label for='summary_terms_opposition0' class='checkbox required'><?php echo JText::_('TB_ACCEPT_OPPOSITION') ?></label>
							</div>
							<div class='terms'>
							<?php if ($article = $this->params->get('cancellationArticle', false)) : ?>
								<?php $link = JRoute::_(ContentHelperRoute::getArticleRoute($article, 0, 0) , false); ?>
								<a href='<?php echo $link; ?>' target='_blank'><?php echo JText::_('TB_READ_OPPOSITION'); ?></a>
							<?php endif; ?>
							</div>
							<div class='terms'>
							<?php if (($pdf = $this->params->get('cancellationPdf', false)) && ($this->params->get('cancellationPdf', false) !=='-1')) : ?>
								<a href='<?php echo JURI::base(); ?>images/com_travelbook/documents/<?php echo $pdf; ?>'><?php echo JText::_('TB_DOWNLOAD_OPPOSITION'); ?></a>																		
							<?php endif; ?>
							</div>
						</div>
				</fieldset>
				<?php else: ?>
					<input type='hidden' id='summary_terms_opposition0' value='' />
				<?php endif; ?>
			</div>
	<?php endif; ?> 
</fieldset>
