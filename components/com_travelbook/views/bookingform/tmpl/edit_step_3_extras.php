<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_travelbook
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; 

// Create shortcut to parameters.
$params = $this->state->get('params');

JHtml::_('behavior.tooltip');

$statesTitle = array(0 => 'TBINCLUDED', 1 => 'TBEXCLUDED', 2 => 'TBOPTIONAL', 3 => 'TBFACULTATIV');
$statesDesc = array(0 => 'TBINCLUDED_DESC', 1 => 'TBEXCLUDED_DESC', 2 => 'TBOPTIONAL_DESC', 3 => 'TBFACULTATIV_DESC');
?>

<div class='clear'></div>
<hr />
<?php $type = -1; ?>
<ul class="start">
	<li></li>
<?php foreach ($this->extras as $extra) : ?>
	<?php if ($extra->type !== $type): ?>
		</ul>
		<span class="summary-col1 extras"><?php echo JTEXT::_($statesTitle[$extra->type]); ?></span>
		<ul class="extras">
	<?php endif; ?>	
		<?php if (in_array($extra->type, array(2))) : ?>
			<?php if (!in_array(0, json_decode($extra->pricing_plan, true))) : ?>
			<li id="extra-<?php echo $extra->tours_extras_id; ?>-summary"><?php echo $this->_booking->renderExtrasSummary($extra, $this->date); ?></li>
			<?php endif; ?>
		<?php else : ?>
			<li><span class="summary-col3"><?php echo strip_tags($extra->introtext); ?></span></li>
		<?php endif; ?>
	<?php $type = $extra->type; ?>
	<?php endforeach; ?>
</ul>
