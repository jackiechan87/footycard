<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_travelbook
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Create a shortcuts.
$params = $this->params;
$images = json_decode($this->date->tour_images);
$showDescription = ($params->get('booking_show_dates', true) || $params->get('booking_show_image', true) || $params->get('booking_show_text', true) || $params->get('booking_show_rates', true));
?>

<?php if ($showDescription) : ?>
	<div class="booking-description">
<?php endif; ?>
<!-- Dates & Duration -->
<?php if ($params->get('booking_show_dates', true)) : ?>
	<div class="booking-dates-duration">
		<span class="booking-dates"><?php echo JHtml::_('date', $this->date->departure, JText::_('DATE_FORMAT_TB3')) . ' - ' . JHtml::_('date', $this->date->arrival, JText::_('DATE_FORMAT_TB3')); ?></span>
		&nbsp;
		<span class="booking-duration"><?php echo JText::sprintf('COM_TRAVELBOOK_DATE_DURATION_N_DAYS', $this->date->duration); ?></span>
	</div>
<?php endif; ?>

<!-- Image -->
<?php if ($params->get('booking_show_image', true)) : ?>
	<?php if (isset($images->image_intro) and !empty($images->image_intro)) : ?>
		<?php $imgfloat = (empty($images->float_intro)) ? $params->get('float_intro') : $images->float_intro; ?>
		<div class="img-intro-<?php echo htmlspecialchars($imgfloat); ?>">
		<img
			<?php if ($images->image_intro_caption):
				echo 'class="caption"'.' title="' .htmlspecialchars($images->image_intro_caption) .'"';
			endif; ?>
			src="<?php echo htmlspecialchars($images->image_intro); ?>" alt="<?php echo htmlspecialchars($images->image_intro_alt); ?>"/>
		</div>
	<?php endif; ?>
<?php endif; ?>

<!-- Text -->
<?php if ($params->get('booking_show_text', true)) : ?>
	<div class="booking-text">
		<?php echo $this->date->tour_introtext; ?>
	</div>
<?php endif; ?>

<!-- Rates -->
<?php if ($params->get('booking_show_rates', true)) : ?>
	<div class="booking-rates clr">
		<span><?php echo JText::sprintf('COM_TRAVELBOOK_DATE_RACK_RATE', $this->currency->calculate($this->date->rack_rate)); ?></span>
		<?php echo $this->date->single_rate > 0 ? '</br><span>' . JText::sprintf('COM_TRAVELBOOK_DATE_SINGLE_RATE', $this->currency->calculate($this->date->single_rate)) . '</span>' : ''; ?>
	</div>
<?php endif; ?>

<?php if ($showDescription) : ?>
	</div>
<?php endif; ?>

<div class="prices-col1 filler"><span class="price-label"><?php echo JText::_('TB_PRICE')?></span></div>
<div class="prices-col2 filler">
	<?php echo $this->_booking->renderAjax($this->_booking->calcTotalPrice($params->get('number_guests_default'), $this->date->rack_rate, $this->date->single_rate), 'tour-rate'); ?>
</div>
<?php if (false) : ?>
<div class="prices-col1 filler"><span class="extras-price-label"><?php echo JText::_('TB_MARKUPS')?></span></div>
<div class="prices-col2 filler">
	<?php echo $this->_booking->renderAjax(0, 'extras-rate'); ?>
</div>

<div class="prices-col1 filler">
<span class="total-price-label"><?php echo JText::_('TB_TOTAL')?></span>
<span class="total-price-info"><?php echo JText::_('TB_INCLUSIVE')?></span>
</div>
<div class="prices-col2 filler">
	<?php echo $this->_booking->renderAjax($this->_booking->calcTotalPrice($params->get('number_guests_default'), $this->date->rack_rate, $this->date->single_rate), 'total-rate'); ?>
</div>
<?php endif; ?>