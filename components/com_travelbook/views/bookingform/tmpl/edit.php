<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_travelbook
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Create shortcut to parameters.
$params = $this->state->get('params');

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.calendar');
JHtml::_('behavior.formvalidation');
?>

<script type="text/javascript">
	Joomla.booknow = function(task) {
		if (task == 'booking.cancel' || document.formvalidator.isValid(document.id('bookingForm'))) {
			Joomla.submitform(task, document.id('bookingForm'));
		} else {
			msg = '<?php echo $this->escape(JText::_('COM_TRAVELBOOK_BOOKING_VALIDATION_FAILED'));?>';
			summaryTerms = document.id('summary_terms_terms0');
			if (summaryTerms && ((summaryTerms.get('type')=='checkbox') && !summaryTerms.checked)){msg += '\n\n\t<?php echo JText::_( 'TB_ACCEPT_TERMS', true ); ?>';}
			summaryPrivacy = document.id('summary_terms_privacy0');
			if (summaryPrivacy && ((summaryPrivacy.get('type')=='checkbox') && !summaryPrivacy.checked)){msg += '\n\n\t<?php echo JText::_( 'TB_ACCEPT_PRIVACY', true ); ?>';}
			summaryOpposition = document.id('summary_terms_opposition0');
			if (summaryOpposition && ((summaryOpposition.get('type')=='checkbox') && !summaryOpposition.checked)){msg += '\n\n\t<?php echo JText::_( 'TB_ACCEPT_OPPOSITION', true ); ?>';}
			alert(msg);
		}
		return false;
	}
</script>

<div id="travelbook" class="edit item-page<?php echo $this->pageclass_sfx; ?>">
<div id="ajax-loading">
	<div id="ajax-loader"></div>
</div>

<?php if ($params->get('show_page_heading', 1)) : ?>
<h1>
	<?php echo $this->escape($params->get('page_heading')); ?>
</h1>
<?php endif; ?>

<!-- Titel -->
<?php if ($params->get('show_title', true)) : ?>
	<h2>
	<?php if ($params->get('link_titles', true) && !empty($this->date->readmore_link)) : ?>
		<a href="<?php echo $this->date->readmore_link; ?>">
		<?php echo $this->escape($this->date->tour_title); ?></a>
	<?php else : ?>
		<?php echo $this->escape($this->date->tour_title); ?>
	<?php endif; ?>
	</h2>
<?php endif; ?>
<!-- Category -->
<?php if ($params->get('show_category')) : ?>
	<?php 
	    $title = $this->escape($this->date->tour_category_title);
	    $url = '<a href="'.JRoute::_(TravelbookHelperRoute::getCategoryRoute($this->date->catslug)).'">'.$title.'</a>';
	?>
	<h3 class="category-name"><?php echo ($params->get('link_category') and $this->date->catslug) ? $url : $title; ?></h3>
<?php endif; ?>

<!-- Tour and Date -->
<?php echo $this->loadTemplate('tour'); ?>

<form action="<?php echo JRoute::_('index.php?option=com_travelbook&a_id='.(int) $this->item->id).'&amp;date_id='.(int)$this->state->get('date.id', 0); ?>" method="post" name="bookingForm" id="bookingForm" class="form-validate">
	<?php
	$prev = 0;
	for ($i=1; $i<4; $i++) {
		$current = $i;
		$next = 3;
		for ( $j = $next; $j > $i; $j-- ) {
			if ($this->_booking->_config[$j]) {
				$next = $j;		
			}
		}

		echo "<div id='mainForm_" . $i . "' class='booking-step border' style='display: block;'>";
		
		// Navigation
		if ( $this->params->get('display_navigation', true) ) {
			echo $this->_booking->renderNavigation( $prev, $current, $next );
		}
		
		// Page
		echo $this->loadTemplate('step_' . $i);
		
		// Validation
		if ( $this->params->get('validation', true) ) {
			echo "<span id='ajax_validation_" . $i . "'>&nbsp;</span>\n";
		} else {
			echo "<input type='hidden' id='ajax_validation_" . $i . "' />\n";
		}

		// Buttons
		echo $this->_booking->renderButtons($current, $prev, $next);
		
		echo "</div>";
		
		if ( $this->_booking->_config[$i] ) {
			$prev=$i;
		}
	}
	?>
	
	<?php $numberGuestsDefault = $params->get('number_guests_default'); ?>
	<input type="hidden" id="form-pax" name="jform[pax]" value="<?php echo (int) $numberGuestsDefault; ?>" />
	<input type="hidden" id="form-single" name="jform[single]" value="<?php echo (int) ($numberGuestsDefault % 2); ?>" />
	<input type="hidden" id="form-double" name="jform[double]" value="<?php echo (int) floor($numberGuestsDefault / 2); ?>" />
	<input type="hidden" id="form-total-rate" name="jform[total]" value="<?php echo $this->currency->calculate($this->_booking->calcTotalPrice($params->get('number_guests_default'), $this->date->rack_rate, $this->date->single_rate)); ?>" />
	
	<input type="hidden" name="jform[DID]" value="<?php echo (int)$this->state->get('date.id', 0); ?>" />
	<input type="hidden" name="jform[CID]" value="<?php echo (int)$this->state->get('client.id', 0); ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="return" value="<?php echo $this->return_page;?>" />
	<?php echo JHtml::_( 'form.token' ); ?>
</form>
</div>
<div id="ajax-failure"></div>