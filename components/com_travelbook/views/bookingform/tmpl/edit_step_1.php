<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_travelbook
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; 

// Create shortcut to parameters.
$params = $this->state->get('params');
?>

<fieldset>
	<legend><?php echo JText::_('COM_TRAVELBOOK_BOOKINGFORM_CLIENT'); ?></legend>
	<div class="description">
		<?php echo JText::sprintf('COM_TRAVELBOOK_BOOKINGFORM_CLIENT_LABEL', JText::_($params->get('company', 'COM_TRAVELBOOK_CONFIG_BOOKING_FIELD_COMPANY_DEFAULT'))); ?>
	</div>

	<?php if (is_null($this->item->id)):?>
		<div class="formelm">
		<span class="label">&nbsp;</span>
		<?php echo $this->form->getInput('address', 'client'); ?>
		</div>
		<div class="formelm">
		<?php echo $this->form->getLabel('degree', 'client'); ?>
		<?php echo $this->form->getInput('degree', 'client'); ?>
		</div>
		<div class="formelm">
		<?php echo $this->form->getLabel('firstname', 'client'); ?>
		<?php echo $this->form->getInput('firstname', 'client'); ?>
		</div>
		<div class="formelm">
		<?php echo $this->form->getLabel('lastname', 'client'); ?>
		<?php echo $this->form->getInput('lastname', 'client'); ?>
		</div>
		<div class="formelm">
		<?php echo $this->form->getLabel('street', 'client'); ?>
		<?php echo $this->form->getInput('street', 'client'); ?>
		</div>
		<div class="formelm">
		<?php echo $this->form->getLabel('street_number', 'client'); ?>
		<?php echo $this->form->getInput('street_number', 'client'); ?>
		</div>
		<div class="formelm">
		<?php echo $this->form->getLabel('postcode', 'client'); ?>
		<?php echo $this->form->getInput('postcode', 'client'); ?>
		</div>
		<div class="formelm">
		<?php echo $this->form->getLabel('city', 'client'); ?>
		<?php echo $this->form->getInput('city', 'client'); ?>
		</div>
		<div class="formelm">
		<?php echo $this->form->getLabel('country', 'client'); ?>
		<?php echo $this->form->getInput('country', 'client'); ?>
		</div>
		<div class="formelm">
		<?php echo $this->form->getLabel('email', 'client'); ?>
		<?php echo $this->form->getInput('email', 'client'); ?>
		</div>
		<div class="formelm">
		<?php echo $this->form->getLabel('mode', 'client'); ?>
		<?php echo $this->form->getInput('mode', 'client'); ?>
		</div>
		<div class="formelm">
		<?php echo $this->form->getLabel('phone', 'client'); ?>
		<?php echo $this->form->getInput('phone', 'client'); ?>
		</div>
		<div class="formelm">
		<?php echo $this->form->getLabel('mobile', 'client'); ?>
		<?php echo $this->form->getInput('mobile', 'client'); ?>
		</div>
	<?php endif; ?>
</fieldset>

<fieldset>
	<legend><?php echo JText::_('COM_TRAVELBOOK_BOOKINGFORM_GUESTS'); ?></legend>
	<div class="description">
		<?php echo JText::_('COM_TRAVELBOOK_BOOKINGFORM_GUESTS_LABEL'); ?>
	</div>
	<div class="formelm distance">
		<?php echo $this->form->getInput('inherit_lastname', 'utilities'); ?>
		<?php echo $this->form->getLabel('inherit_lastname', 'utilities'); ?>
	</div>
	<div class="formelm distance">
		<?php echo $this->form->getLabel('hitchhike', 'utilities'); ?>
		<?php echo $this->form->getInput('hitchhike', 'utilities'); ?>
	</div>
	<?php if (is_null($this->item->id)):?>
		<!-- Display Fields for Guests, not more than places are available for this date -->
		<?php for ($i=0; $i < $this->params->get('number_guests_default'); $i++) : ?>
			<div class="guest" id="guest-<?php echo $i; ?>">
<?php
	$id = $this->form->getFieldAttribute('selected', 'name', null, 'guests').(string)$i;
	$this->form->setFieldAttribute('selected', 'id', $id, 'guests');
	echo $this->form->getInput('selected', 'guests', '1');
?>
<!-- address -->
				<div class="formelm">
					<span class="label">&nbsp;</span>
					<fieldset class="radio inputbox" id="jform_guests_address<?php echo $i; ?>">
						<input type="radio" class="radio titles" value="0" name="jform[guests][address][<?php echo $i; ?>]" id="jform_guests_address_<?php echo $i; ?>-0" <?php echo (isset($this->data['guests']['address'][$i]) && $this->data['guests']['address'][$i]==='0') ? null : 'checked="checked"'; ?> />
						<label class="radio" for="jform_guests_address_<?php echo $i; ?>-0"><?php echo JText::_('COM_TRAVELBOOK_FIELD_BOOKING_ADDRESS_OPTION_MR'); ?></label>
						<input type="radio" class="radio titles" value="1" name="jform[guests][address][<?php echo $i; ?>]" id="jform_guests_address_<?php echo $i; ?>-1" <?php echo (isset($this->data['guests']['address'][$i]) && $this->data['guests']['address'][$i]==='0') ? 'checked="checked"' : null; ?> />
						<label class="radio" for="jform_guests_address_<?php echo $i; ?>-1"><?php echo JText::_('COM_TRAVELBOOK_FIELD_BOOKING_ADDRESS_OPTION_MRS'); ?></label>
					</fieldset>
				</div>
<!-- firstname -->
<?php
	$id = $this->form->getFieldAttribute('firstname', 'name', null, 'guests') . '_' . (string)$i;
	$this->form->setFieldAttribute('firstname', 'id', $id, 'guests');
?>
				<div class="formelm">
				<?php echo $this->form->getLabel('firstname', 'guests').PHP_EOL; ?>
				<?php echo $this->form->getInput('firstname', 'guests', isset($this->data['guests']) ? $this->data['guests']['firstname'][$i] : null).PHP_EOL; ?>
				</div>
<!-- lastname -->
<?php
	$id = $this->form->getFieldAttribute('lastname', 'name', null, 'guests') . '_' . (string)$i;
	$this->form->setFieldAttribute('lastname', 'id', $id, 'guests');
?>
				<div class="formelm">
				<?php echo $this->form->getLabel('lastname', 'guests').PHP_EOL; ?>
				<?php echo $this->form->getInput('lastname', 'guests', isset($this->data['guests']) ? $this->data['guests']['lastname'][$i] : null).PHP_EOL; ?>
				</div>
<!-- date of birth -->
<?php
	$id = $this->form->getFieldAttribute('date_of_birth', 'name', null, 'guests') . '_' . (string)$i;
	$this->form->setFieldAttribute('date_of_birth', 'id', $id, 'guests');
?>
				<div class="formelm">
				<?php echo $this->form->getLabel('date_of_birth', 'guests').PHP_EOL; ?>
				<?php echo $this->form->getInput('date_of_birth', 'guests', isset($this->data['guests']) ? $this->data['guests']['date_of_birth'][$i] : null).PHP_EOL; ?>
				</div>

<?php if ($i == ($this->params->get('number_guests_default')-1)) : ?>
	<?php if ($i < (min($this->date->available, $this->params->get('number_guests_max', 8))-1)) : ?>
		<div id="plus-<?php echo ($i+1); ?>" class="plus"><?php echo JText::_('COM_TRAVELBOOK_BOOKING_PLUS'); ?></div>
	<?php endif; ?>
<?php endif; ?>
			</div>
		<?php endfor; ?>
		
		<?php $j = $i; ?>
		
		<!--  Now lets render the hidden guest fields -->
		<!-- Display Hidden Fields for Guests, not more than places are available for this date -->
		<?php for ($i=$j; $i < min($this->date->available, $this->params->get('number_guests_max', 8)); $i++) : ?>
			<div class="guest" id="guest-<?php echo $i; ?>" style="display: none;">
				<span id="minus-<?php echo $i; ?>" class="minus"><?php echo JText::_('COM_TRAVELBOOK_BOOKING_MINUS'); ?></span>
				<?php $id = $this->form->getFieldAttribute('selected', 'name', null, 'guests').(string)$i; ?>
				<?php $this->form->setFieldAttribute('selected', 'id', $id, 'guests'); ?>
				<?php $this->form->setFieldAttribute('selected', 'disabled', 'true', 'guests'); ?>
				<?php echo $this->form->getInput('selected', 'guests', '0'); ?>
				<div class="formelm">
					<span class="label">&nbsp;</span>
					<fieldset class="radio inputbox" id="jform_guests_address<?php echo $i; ?>">
						<input type="radio" class="radio titles" value="0" name="jform[guests][address][<?php echo $i; ?>]" id="jform_guests_address_<?php echo $i; ?>-0" checked="checked" disabled="disabled" />
						<label class="radio" for="jform_guests_address_<?php echo $i; ?>-0"><?php echo JText::_('COM_TRAVELBOOK_FIELD_BOOKING_ADDRESS_OPTION_MR'); ?></label>
						<input type="radio" class="radio titles" value="1" name="jform[guests][address][<?php echo $i; ?>]" id="jform_guests_address_<?php echo $i; ?>-1" disabled="disabled" />
						<label class="radio" for="jform_guests_address_<?php echo $i; ?>-1"><?php echo JText::_('COM_TRAVELBOOK_FIELD_BOOKING_ADDRESS_OPTION_MRS'); ?></label>
					</fieldset>
				</div>
				<div class="formelm">
				<?php $id = $this->form->getFieldAttribute('firstname', 'name', null, 'guests') . '_' . (string)$i; ?>
				<?php $this->form->setFieldAttribute('firstname', 'id', $id, 'guests'); ?>
				<?php $this->form->setFieldAttribute('firstname', 'disabled', 'true', 'guests'); ?>
				<?php echo $this->form->getLabel('firstname', 'guests'); ?>
				<?php echo $this->form->getInput('firstname', 'guests', isset($this->data['guests']) ? $this->data['guests']['firstname'][($i-$j)] : null); ?>
				</div>
				<div class="formelm">
				<?php $id = $this->form->getFieldAttribute('lastname', 'name', null, 'guests') . '_' . (string)$i; ?>
				<?php $this->form->setFieldAttribute('lastname', 'id', $id, 'guests'); ?>
				<?php $this->form->setFieldAttribute('lastname', 'disabled', 'true', 'guests'); ?>
				<?php echo $this->form->getLabel('lastname', 'guests'); ?>
				<?php echo $this->form->getInput('lastname', 'guests', isset($this->data['guests']) ? $this->data['guests']['lastname'][($i-$j)] : null); ?>
				</div>
				<div class="formelm">
				<?php $id = $this->form->getFieldAttribute('date_of_birth', 'name', null, 'guests') . '_' . (string)$i; ?>
				<?php $this->form->setFieldAttribute('date_of_birth', 'id', $id, 'guests'); ?>
				<?php $this->form->setFieldAttribute('date_of_birth', 'disabled', 'true', 'guests'); ?>
				<?php echo $this->form->getLabel('date_of_birth', 'guests'); ?>
				<?php echo $this->form->getInput('date_of_birth', 'guests', isset($this->data['guests']) ? $this->data['guests']['date_of_birth'][($i-$j)] : null); ?>
				</div>
				<?php if ($i < (min($this->date->available, $this->params->get('number_guests_max', 8))-1)) : ?>
					<div id="plus-<?php echo ($i+1); ?>" class="plus"><?php echo JText::_('COM_TRAVELBOOK_BOOKING_PLUS'); ?></div>
				<?php endif; ?>
			</div>
		<?php endfor; ?>
		
	<?php endif; ?>
</fieldset>