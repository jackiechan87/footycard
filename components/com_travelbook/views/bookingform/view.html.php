<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_travelbook
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.view');
jimport('demopage.currency');

require_once JPATH_COMPONENT.DS.'helpers'.DS.'booking.php';

/**
 * HTML Booking View class for the Travelbook Component
 *
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @since		2.0
 */
class TravelbookViewBookingForm extends JViewLegacy
{
	protected $form;
	protected $item;
	protected $date;
	protected $extras;
	protected $numberGuestsDefault;
	protected $return_page;
	protected $state;

	public function display($tpl = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication();
		$user = JFactory::getUser();

		// Get model data.
		$this->state = $this->get('State');
		$this->item = $this->get('Item');
		$this->form = $this->get('Form');
	
		$this->return_page = $this->get('ReturnPage');

		$this->date = $this->get('Date');
		$this->extras = $this->get('Extras');
		
		// Add router helpers.
		$this->date->slug = $this->date->tour_alias ? ($this->date->tour_id.':'.$this->date->tour_alias) : $this->date->tour_id;
		$this->date->catslug = $this->date->tour_category_alias ? ($this->date->tour_catid.':'.$this->date->tour_category_alias) : $this->date->tour_catid;
		
		$this->date->readmore_link = JRoute::_(TravelbookHelperRoute::getTourRoute($this->date->slug, $this->date->catslug));
		$this->data = $app->getUserState('com_travelbook.edit.bookingform.data', array());

//		if (empty($this->item->id)) {
//			$authorised = $user->authorise('core.create', 'com_travelbook') || (count($user->getAuthorisedCategories('com_travelbook', 'core.create')));
//		}
//		else {
//			$authorised = $this->item->params->get('access-edit');
//		}

//		if ($authorised !== true) {
//			JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
//			return false;
//		}

		if (!empty($this->item) && isset($this->item->id)) {
			$this->form->bind($this->item);
		}

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseWarning(500, implode("\n", $errors));
			return false;
		}

		// Create a shortcut to the parameters.
		$params	= &$this->state->params;
		$tParams = new JRegistry($this->date->tour_attribs);
		$dParams = new JRegistry($this->date->attribs);
		$params->merge($tParams);
		$params->merge($dParams);
		
		$params->set('number_guests_default', min($this->date->available, $params->get('number_guests_default', 2)));
		
		// Create a new Booking Class
		$this->_booking = TravelbookHelpersBooking::getInstance($params);
		
		// Currency
        $this->currency = new DPCurrency($params);
		
        //Escape strings for HTML output
		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

		$this->params = $params;
		$this->user = $user;

		if ($this->params->get('enable_category') == 1) {
			$this->form->setFieldAttribute('catid', 'default',  $params->get('catid', 1));
		}
		
		$this->_prepareDocument();
		$this->_prepareAutocomplete();
		
		parent::display($tpl);

		$doc = JFactory::getDocument();
		
		// add form validation
		if ($params->get('validation', true)) {
			$doc->addScript( 'media/com_travelbook/js/com_travelbook.validate.config.js' );
		} else {
			$doc->addScript( 'media/com_travelbook/js/com_travelbook.no.validate.config.js' );
		}
	}

	/**
	 * Prepares the document
	 */
	protected function _prepareDocument()
	{
		JHtml::stylesheet('com_travelbook/com_travelbook.booking.css', false, true);

		$doc = JFactory::getDocument();
		
		$rackRate = (string)(100*$this->date->rack_rate);
		$singleRate = (string)(100*$this->date->single_rate);
//		$childRate = (string)(100*$this->date->child_rate);
		$numberGuestsDefault = $this->params->get('number_guests_default');
		$mr = JText::_('COM_TRAVELBOOK_FIELD_BOOKING_ADDRESS_OPTION_MR');
		$mrs = JText::_('COM_TRAVELBOOK_FIELD_BOOKING_ADDRESS_OPTION_MRS');
		$script = <<<EOD
var rackRate = {$rackRate};
var singleRate = {$singleRate};
var numberGuestsDefault = {$numberGuestsDefault};
var title = new Array("{$mr}", "{$mrs}"); 
EOD;
		
		$doc = JFactory::getDocument();
		$doc->addScriptDeclaration($script);
		
		$doc->addScript('media/com_travelbook/js/com_travelbook.config.js');
		$doc->addScript('media/com_travelbook/js/com_travelbook.ajax.js');
		$doc->addScript('media/com_travelbook/js/com_travelbook.booking.js');
		
		$app = JFactory::getApplication();
		$menus = $app->getMenu();
		$pathway = $app->getPathway();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();
		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		} else {
			$this->params->def('page_heading', JText::_('COM_TRAVELBOOK_BOOKINGFORM_EDIT_BOOKING'));
		}

		$title = $this->params->def('page_title', JText::_('COM_TRAVELBOOK_BOOKINGFORM_EDIT_BOOKING'));
		if ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		}
		elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$this->document->setTitle($title);

		// if the menu item does not concern this tour
		if ($menu && ($menu->query['option'] != 'com_travelbook' || $menu->query['view'] != 'tour'))
		{
			$path[] = array('title' => JText::sprintf('COM_TRAVELBOOK_VIEW_BOOKINGFORM_BOOK', $this->date->title), 'link' => '');
			$path[] = array('title' => $this->date->tour_title, 'link' => $this->date->readmore_link);
			$path = array_reverse($path);
			foreach($path as $item)
			{
				$pathway->addItem($item['title'], $item['link']);
			}
		}
		
		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}

	/**
	 * Prepares the document
	 */
	protected function _prepareAutocomplete()
	{
		$doc = JFactory::getDocument();
		JHtml::stylesheet('com_travelbook/autocomplete.css', false, true);

		$doc->addScript('media/com_travelbook/js/Observer.js');
		$doc->addScript('media/com_travelbook/js/Autocompleter.js');
//		$doc->addScript('media/com_travelbook/js/Autocompleter.Local.js');
		
		$script = $this->_renderScript();
		$doc->addScriptDeclaration($script);
	}

	protected function _renderScript()
	{
		$countries = explode(',', $this->params->get('countries'));
		
		if (!($countries[0])) {
			$countries = explode(',', 'Argentina,Australia,Austria,Belgium,Bulgaria,Canada,China,Croatia,Czech Republic'.
										',Denmark,Egypt,Estonia,Finland,France,Germany,Hungary,Iceland,India,Indonesia' .
										',Ireland,Israel,Italy,Japan,Latvia,Liechtenstein,Lithuania,Luxembourg,Malaysia' .
										',Malta,Mexico,Monaco,Netherlands,New Zealand,Norway,Poland,Portugal,Romania,San Marino' .
										',Serbia,Singapore,Slovakia,Slovenia,South Africa,Spain,Sweden,Switzerland,Thailand' .
										',Turkey,Ukraine,United Kingdom,United States,Uruguay,Viet Nam');
		}
		// translate the countries' name
		foreach ($countries as &$country) {
			$country = JText::_($country);
		}
		$countries = "'".implode("','", $countries)."'";
		
		$DOM ="
// <![CDATA[
Autocompleter.Local = new Class({
	Extends : Autocompleter,
	options : {
		minLength : 0,
		delay : 200
	},
	initialize : function(element, tokens, options) {
		this.parent(element, options);
		this.tokens = tokens;
	},
	query : function() {
		this.update(this.filter());
	}
});
document.addEvent('domready', function() {
	// List of countries
	var tokens = [ {$countries} ];

	new Autocompleter.Local('jform_client_country', tokens, {
		'minLength' : 1,
		'overflow' : true,
		'selectMode' : 'type-ahead'
	});
});
//]]>
";

		return $DOM;
	}}
