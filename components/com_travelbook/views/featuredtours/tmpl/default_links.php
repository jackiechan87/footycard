<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die; ?>

<h3><?php echo JText::_('COM_TRAVELBOOK_MORE_TOURS'); ?></h3>
<ol>
<?php foreach ($this->link_items as &$item) : ?>
	<li>
		<a href="<?php echo JRoute::_(TravelbookHelperRoute::getTourRoute($item->slug, $item->catslug)); ?>">
			<?php echo $item->title; ?></a>
	</li>
<?php endforeach; ?>
</ol>