<?php

/**
 * @package		Joomla.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$class = ' class="first"'; 
?>

<ul>
<?php $i = 0; ?>
<?php foreach($this->items as $id => $item) : ?>
	<?php
    $images = json_decode($item->images);
	$i++;
	$class = $i==1 ? ' first' : '';
	$class .= $i==count($this->items) ? ' last' : '';
	$class .= ($i%2)==0 ? ' even' : ' odd';
	$class = ' class="'.trim($class).'"';
	$tourTitle = $this->escape($item->tour_title);
	$dateTitle = $this->escape($item->title);
	$categoryTitle = $this->escape($item->category_title);
	$tourUrl = JRoute::_(TravelbookHelperRoute::getTourRoute($item->TID, $item->tour_catid));
	$bookUrl = JRoute::_(TravelbookHelperRoute::getBookingRoute($item->id, $item->TID));
	?>
	<li id="item-<?php echo $item->id; ?>"<?php echo $class; ?>>
<!-- Tour -->
		<?php if ($this->params->get('show_tour_title', true)) : ?>
			<h2 class="tour-title">
    			<?php if ($this->params->get('link_tour_title', true)) : ?>
    				<a href="<?php echo $tourUrl; ?>"><?php echo $tourTitle; ?></a>
    			<?php else : ?>
    				<?php echo $tourTitle; ?>
    			<?php endif; ?>
			</h2>
		<?php endif; ?>
<!-- Title -->
		<?php if ($this->params->get('show_title', true)) : ?>
			<h3 class="date-title"><?php echo $dateTitle; ?></h3>
		<?php endif; ?>

<!-- Category -->
        <?php if ($this->params->get('show_category')) : ?>
        	<h4 class="category-name"><?php echo $categoryTitle; ?></h4>
        <?php endif; ?>

<!-- Icons -->
        <?php if ($this->params->get('show_email_icon') || $this->params->get('show_ical')) : ?>
        	<ul class="actions">
        		<?php if ($this->params->get('show_email_icon')) : ?>
        			<li class="email-icon">
        			<?php echo JHtml::_('icon.email', $item, $this->params); ?>
        			</li>
        		<?php endif; ?>
        
        		<?php if ($this->params->get('show_ical') == 1) :?>
        		<li class="ical small">
        			<?php $dates = new stdClass(); ?>
        			<?php $dates->departure = $item->departure; ?>
        			<?php echo JHtml::_('icon.icalSmall', $item, array(0 => $dates), $this->params); ?>
        		</li>
        		<?php endif; ?>
        	</ul>
        <?php endif; ?>

<!-- Image -->
		<?php if ($this->params->get('show_image', true) == 1) :?>
            <?php if (isset($images->image_intro) and !empty($images->image_intro)) : ?>
            	<?php $imgfloat = (empty($images->float_intro)) ? $this->params->get('float_intro') : $images->float_intro; ?>
            	<div class="img-intro-<?php echo htmlspecialchars($imgfloat); ?>">
            	<img
            		<?php 
            		    if ($images->image_intro_caption):
            			echo 'class="caption"'.' title="' .htmlspecialchars($images->image_intro_caption) .'"';
            		    endif; 
                    ?>
            		src="<?php echo htmlspecialchars($images->image_intro); ?>" alt="<?php echo htmlspecialchars($images->image_intro_alt); ?>"/>
            	</div>
            <?php endif; ?>
	    <?php endif; ?>

<!-- Description -->
		<?php if ($this->params->get('show_desc', true) == 1) :?>
    		<?php if ($item->introtext) : ?>
    			<div class="date-desc">
    				<?php echo JHtml::_('content.prepare', $item->introtext, '', 'com_travelbook.dates'); ?>
    			</div>
     		<?php endif; ?>
        <?php endif; ?>

		<?php if ($this->params->get('show_image') == 1) :?>
			<div class="clr"></div>
 		<?php endif; ?>

<!-- Details -->
		<ul>
    		<li class="date-departure"><?php echo  JHtml::_('date', $item->departure, JText::_('DATE_FORMAT_TB3')); ?></li>
    		<li class="date-departure"><?php echo  JHtml::_('date', $item->departure, JText::_('DATE_FORMAT_TB5')); ?></li>
    		<li class="date-arrival"><?php echo  JHtml::_('date', $item->arrival, JText::_('DATE_FORMAT_TB3')); ?></li>
    		<li class="date-arrival"><?php echo  JHtml::_('date', $item->arrival, JText::_('DATE_FORMAT_TB5')); ?></li>
    		<li class="date-duration"><?php echo JText::plural('COM_TRAVELBOOK_DATE_DURATION_N_DAYS', $item->duration); ?></li>
    		<li class="date-price"><?php echo JText::sprintf('COM_TRAVELBOOK_RATE_FROM', $this->currency->calculate($item->rate)); ?></li>
		</ul>
		
		<?php if ($this->params->get('show_booking_button', true)) : ?>
			<a href="<?php echo $bookUrl; ?>" title="<?php echo JText::sprintf('COM_TRAVELBOOK_BOOK_DATE', $tourTitle, $dateTitle); ?>">
			    <?php echo JText::_('COM_TRAVELBOOK_BOOK_NOW'); ?>
			</a>
		<?php endif; ?>
	</li>
<?php endforeach; ?>
</ul>