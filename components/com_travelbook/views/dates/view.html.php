<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
jimport('demopage.currency');

/**
 * Travelbook dates view.
 *
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @since 2.0
 */
class TravelbookViewDates extends JViewLegacy
{
    protected $state = null;
    protected $item = null;
    protected $items = null;

    /**
     * Display the view
     *
     * @return	mixed	False on error, null otherwise.
     */
    function display($tpl = null)
    {
        // Initialise variables
        $state = $this->get('State');
        $items = $this->get('Items');
        
        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            JError::raiseWarning(500, implode("\n", $errors));
            return false;
        }

        if ($items === false)
        {
            JError::raiseError(404, JText::_('COM_TRAVELBOOK_ERROR_DATE_NOT_FOUND'));
            return false;
        }

        $tour = $items[0];

        $tour->tourslug = $tour->tour_alias ? ($tour->TID.':'.$tour->tour_alias) : $tour->TID;
		$tour->catslug = $tour->category_alias ? ($tour->catid.':'.$tour->category_alias) : $tour->catid;
		$tour->slug = $tour->tour_alias ? ($tour->TID.':'.$tour->tour_alias) : $tour->TID;
		$tour->catid = $tour->category_alias ? ($tour->catid.':'.$tour->category_alias) : $tour->catid;
		if ($tour->params->get('show_intro', '1') == '1') {
			$tour->text = $tour->introtext.' '.$tour->fulltext;
		}
		elseif ($tour->fulltext) {
			$tour->text = $tour->fulltext;
		}
		else  {
			$tour->text = $tour->introtext;
		}
		
        $params = &$state->params;

        // Currency
        $this->currency = new DPCurrency($params);
        
		$dispatcher	= JDispatcher::getInstance();
        //
		// Process the content plugins.
		//
		JPluginHelper::importPlugin('content');
		$results = $dispatcher->trigger('onContentPrepare', array ('com_travelbook.tour', &$tour, &$params));

		$tour->event = new stdClass();
		$results = $dispatcher->trigger('onContentAfterTitle', array('com_travelbook.tour', &$tour, &$params));
		$tour->event->afterDisplayTitle = trim(implode("\n", $results));

		$results = $dispatcher->trigger('onContentBeforeDisplay', array('com_travelbook.tour', &$tour, &$params));
		$tour->event->beforeDisplayContent = trim(implode("\n", $results));

		$results = $dispatcher->trigger('onContentAfterDisplay', array('com_travelbook.tour', &$tour, &$params));
		$tour->event->afterDisplayContent = trim(implode("\n", $results));
        
		//
		// Process the travelbook plugins.
		//
		JPluginHelper::importPlugin('travelbook');
		$results = $dispatcher->trigger('onTourPrepare', array ('com_travelbook.tour', &$tour, &$params));
		
		$results = $dispatcher->trigger('onTourAfterTitle', array('com_travelbook.tour', &$tour, &$params));
		$tour->event->afterDisplayTourTitle = trim(implode("\n", $results));

		$results = $dispatcher->trigger('onTourBeforeDisplay', array('com_travelbook.tour', &$tour, &$params));
		$tour->event->beforeDisplayTour = trim(implode("\n", $results));

		$results = $dispatcher->trigger('onTourAfterDisplay', array('com_travelbook.tour', &$tour, &$params));
		$tour->event->afterDisplayTour = trim(implode("\n", $results));
		
		$details = array();
		$details[0] = new stdClass();
		$details[0]->title = $tour->title;
		$details[0]->TID = $tour->TID;
		$details[0]->introtext = '<p>{dates}</p>';
		
		$results = $dispatcher->trigger('onDetailPrepare', array ('com_travelbook.tour', &$details, &$params));

    		
    	//Escape strings for HTML output
        $this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));

        $this->assign('maxLevelcat', $params->get('maxLevelcat', -1));
        $this->assignRef('params', $params);
        $this->assignRef('parent', $parent);
        $this->assignRef('items', $items);
        $this->assignRef('tour', $tour);
        $this->assignRef('details', $details);
        
        $this->_prepareDocument();

        parent::display($tpl);
        }

        /**
         * Prepares the document
         */
        protected function _prepareDocument()
        {
			JHtml::stylesheet('com_travelbook/com_travelbook.dates.css', false, true);

			$app = JFactory::getApplication();
            $menus = $app->getMenu();
            $title = null;

            // Because the application sets a default page title,
            // we need to get it from the menu item itself
            $menu = $menus->getActive();
            if ($menu)
            {
                $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
            } else {
                $this->params->def('page_heading', JText::_('COM_TRAVELBOOK_DATES'));
            }
            $title = $this->params->get('page_title', '');
            if (empty($title)) {
                $title = $app->getCfg('sitename');
            }
            elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
                $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
            }
            elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
                $title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
            }
            $this->document->setTitle($title);

            if ($this->params->get('menu-meta_description'))
            {
                $this->document->setDescription($this->params->get('menu-meta_description'));
            }

            if ($this->params->get('menu-meta_keywords'))
            {
                $this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
            }

            if ($this->params->get('robots'))
            {
                $this->document->setMetadata('robots', $this->params->get('robots'));
            }
        }
    }
