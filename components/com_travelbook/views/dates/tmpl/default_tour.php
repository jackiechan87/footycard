<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die; 

// Create shortcuts to some parameters.
$params = $this->params;
$images = json_decode($this->tour->tour_images);
$canEdit = $this->tour->params->get('access-edit');
$user = JFactory::getUser(); 

$this->tourTitle = $this->escape($this->tour->tour_title);
$this->tourUrl = JRoute::_(TravelbookHelperRoute::getTourRoute($this->tour->TID, $this->tour->tour_catid)); ?>

<!-- Title -->
<?php if ($params->get('show_tour_title', true)) : ?>
	<h2 class="tour-title">
		<?php if ($params->get('link_tour_title', true)) : ?>
			<a href="<?php echo $this->tourUrl; ?>"><?php echo $this->tourTitle; ?></a>
		<?php else : ?>
			<?php echo $this->tourTitle; ?>
		<?php endif; ?>
	</h2>
<?php endif; ?>

<!-- Intro -->
<?php  if (!$params->get('show_intro')) :
	echo $this->tour->event->afterDisplayTitle;
	echo $this->tour->event->afterDisplayTourTitle;
endif; ?>

<!-- Plugins -->
<?php echo $this->tour->event->beforeDisplayContent; ?>
<?php echo $this->tour->event->beforeDisplayTour; ?>

<!-- Text -->
<?php if ($this->tour->params->get('access-view')):?>
	<?php if (isset($images->image_fulltext) and !empty($images->image_fulltext)) : ?>
		<?php $imgfloat = (empty($images->float_fulltext)) ? $params->get('float_fulltext') : $images->float_fulltext; ?>
		<div class="img-fulltext-<?php echo htmlspecialchars($imgfloat); ?>">
		<img
			<?php if ($images->image_fulltext_caption):
				echo 'class="caption"'.' title="' .htmlspecialchars($images->image_fulltext_caption) .'"';
			endif; ?>
			src="<?php echo htmlspecialchars($images->image_fulltext); ?>" alt="<?php echo htmlspecialchars($images->image_fulltext_alt); ?>"/>
		</div>
	<?php endif; ?>

	<div class="strong">
	<?php echo $this->tour->introtext; ?>
	</div>
<?php endif; ?>
