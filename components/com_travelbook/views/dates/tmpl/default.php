<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers'); ?>

<div id="travelbook" class="dates-list<?php echo $this->pageclass_sfx;?>">
    <?php if ($this->params->get('show_page_heading', 1)) : ?>
    	<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
    <?php endif; ?>

	<?php if ($this->params->get('show_tour_title', false)) : ?>
		<?php $title = $this->escape($this->items[0]->tour_title); ?>
		<?php $url = JRoute::_(TravelbookHelperRoute::getTourRoute($this->items[0]->TID, $this->items[0]->tour_catid)); ?>
		<h2>
        	<?php if ($this->params->get('link_tour_title', true)) : ?>
        		<a href="<?php echo $url; ?>"><?php echo $title; ?></a>
        	<?php  else : ?>
        		<?php echo $title; ?>
        	<?php endif; ?>
    	</h2>    	
    <?php endif; ?>
    
    <?php if ($this->params->get('show_base_description')) : ?>
        <?php if($this->params->get('dates_description')) : ?>
        	<div class="base-desc">
        	    <?php echo  JHtml::_('content.prepare', $this->params->get('dates_description'), '', 'com_travelbook.dates'); ?>
        	</div>
        <?php endif; ?>
	<?php endif; ?>     

<!-- TOUR -->
    <?php echo $this->loadTemplate('tour'); ?>

<!-- DATES -->    
    <?php echo $this->loadTemplate('details'); ?>
</div>