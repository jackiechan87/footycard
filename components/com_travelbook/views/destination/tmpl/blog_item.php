<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
// JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');

// Create a shortcut for params.
$params = &$this->item->params;
$images = json_decode($this->item->images);
$canEdit = $this->item->params->get('access-edit');
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.framework'); ?>

<?php if ($this->item->state == 0) : ?>
<div class="system-unpublished">
<?php endif; ?>

<!-- Title -->
<?php if ($params->get('show_title')) : ?>
	<h2>
		<?php if ($params->get('link_titles') && $params->get('access-view')) : ?>
			<a href="<?php echo JRoute::_(TravelbookHelperRoute::getTourRoute($this->item->slug, $this->item->catid)); ?>">
			<?php echo $this->escape($this->item->title); ?></a>
		<?php else : ?>
			<?php echo $this->escape($this->item->title); ?>
		<?php endif; ?>
	</h2>
<?php endif; ?>

<!-- Category -->

<!-- Icons -->
<?php if ($params->get('show_print_icon') || $params->get('show_email_icon') || $canEdit || $this->params->get('show_ical')) : ?>
	<ul class="actions">
		<?php if ($params->get('show_print_icon')) : ?>
		<li class="print-icon">
			<?php echo JHtml::_('icon.print_popup', $this->item, $params); ?>
		</li>
		<?php endif; ?>
		<?php if ($params->get('show_email_icon')) : ?>
		<li class="email-icon">
			<?php echo JHtml::_('icon.email', $this->item, $params); ?>
		</li>
		<?php endif; ?>
		<?php if ($canEdit) : ?>
		<li class="edit-icon">
			<?php echo JHtml::_('icon.edit', $this->item, $params); ?>
		</li>
		<?php endif; ?>
	</ul>
<?php endif; ?>

<!-- Intro -->
<?php if (!$params->get('show_intro')) : ?>
	<?php echo $this->item->event->afterDisplayTitle; ?>
	<?php echo $this->item->event->afterDisplayTourTitle; ?>
	<?php endif; ?>

<!-- Plugins -->
<?php echo $this->item->event->beforeDisplayContent; ?>
<?php echo $this->item->event->beforeDisplayTour; ?>

<!-- Image -->
<?php  if (isset($images->image_intro) and !empty($images->image_intro)) : ?>
	<?php $imgfloat = (empty($images->float_intro)) ? $params->get('float_intro') : $images->float_intro; ?>
	<div class="img-intro-<?php echo htmlspecialchars($imgfloat); ?>">
	<img
		<?php if ($images->image_intro_caption):
			echo 'class="caption"'.' title="' .htmlspecialchars($images->image_intro_caption) .'"';
		endif; ?>
		src="<?php echo htmlspecialchars($images->image_intro); ?>" alt="<?php echo htmlspecialchars($images->image_intro_alt); ?>"/>
	</div>
<?php endif; ?>

<!-- IntroText -->
<?php echo $this->item->introtext; ?>

<!-- ReadMore -->
<?php if ($params->get('show_readmore') && $this->item->readmore) :
	if ($params->get('access-view')) :
		$link = JRoute::_(TravelbookHelperRoute::getTourRoute($this->item->slug, $this->item->catid));
	else :
		$menu = JFactory::getApplication()->getMenu();
		$active = $menu->getActive();
		$itemId = $active->id;
		$link1 = JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId);
		$returnURL = JRoute::_(TravelbookHelperRoute::getTourRoute($this->item->slug, $this->item->catid));
		$link = new JURI($link1);
		$link->setVar('return', base64_encode($returnURL));
	endif;
?>
		<p class="readmore">
				<a href="<?php echo $link; ?>">
					<?php if (!$params->get('access-view')) :
						echo JText::_('COM_TRAVELBOOK_REGISTER_TO_READ_MORE');
					elseif ($readmore = $this->item->alternative_readmore) :
						echo $readmore;
						if ($params->get('show_readmore_title', 0) != 0) :
						    echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
						endif;
					elseif ($params->get('show_readmore_title', 0) == 0) :
						echo JText::sprintf('COM_TRAVELBOOK_READ_MORE_TITLE');
					else :
						echo JText::_('COM_TRAVELBOOK_READ_MORE');
						echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
					endif; ?></a>
		</p>
<?php endif; ?>

<!-- Details -->
<?php $useDefList = ($params->get('show_category') || $params->get('show_publish_date') || $params->get('show_hits')); ?>
<?php if ($useDefList) : ?>
<ul class="tour-info">
<?php endif; ?>
<?php if ($params->get('show_hits')) : ?>
	<li class="hits">
	<?php echo JText::sprintf('COM_TRAVELBOOK_TOUR_HITS', $this->item->hits); ?>
	</li>
<?php endif; ?>
<?php if ($params->get('show_publish_date')) : ?>
	<li class="published">
	<?php echo JText::sprintf('COM_TRAVELBOOK_PUBLISHED_DATE_ON', JHtml::_('date', $this->item->publish_up, JText::_('DATE_FORMAT_LC2'))); ?>
	</li>
<?php endif; ?>
<?php if ($params->get('show_category')) : ?>
	<li class="category-name">
		<?php 
		    $title = $this->escape($this->item->category_title);
			$url = '<a href="' . JRoute::_(TravelbookHelperRoute::getCategoryRoute($this->item->catid)) . '">' . $title . '</a>'; 
		?>
		<?php echo $params->get('link_category') ? JText::sprintf('COM_TRAVELBOOK_CATEGORY', $url) : JText::sprintf('COM_TRAVELBOOK_CATEGORY', $title); ?>
	</li>
<?php endif; ?>
<?php if ($useDefList) : ?>
</ul>
<?php endif; ?>

<?php if ($this->item->state == 0) : ?>
</div>
<?php endif; ?>

<div class="item-separator"></div>
<?php echo $this->item->event->afterDisplayContent; ?>
