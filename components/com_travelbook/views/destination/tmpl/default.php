<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers'); ?>

<div class="destination-list<?php echo $this->pageclass_sfx;?>">
<!-- Heading -->
	<?php if ($this->params->get('show_page_heading', 1)) : ?>
		<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
	<?php endif; ?>

<!-- Title -->
	<?php if ($this->params->get('show_destination_title', 1) or $this->params->get('page_subheading')) : ?>
	<h2>
		<?php echo $this->escape($this->params->get('page_subheading')); ?>
		<?php if ($this->params->get('show_destination_title')) : ?>
			<span class="subheading-destination"><?php echo $this->destination->title;?></span>
		<?php endif; ?>
	</h2>
	<?php endif; ?>

<!-- Description -->
	<?php if ($this->params->get('show_description', 1) || $this->params->def('show_description_image', 1)) : ?>
	<div class="destination-desc">
		<?php if ($this->params->get('show_description_image') && $this->destination->getParams()->get('image')) : ?>
			<img src="<?php echo $this->destination->getParams()->get('image'); ?>"/>
		<?php endif; ?>
		<?php if ($this->params->get('show_description') && $this->destination->description) : ?>
			<?php echo JHtml::_('content.prepare', $this->destination->description, '', 'com_travelbook.destination'); ?>
		<?php endif; ?>
		<div class="clr"></div>
	</div>
	<?php endif; ?>

<!-- Tours -->
	<div class="cat-items">
		<?php echo $this->loadTemplate('tours'); ?>
	</div>
</div>
