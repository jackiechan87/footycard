<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * HTML View class for the Travelbook component
 *
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @since 2.0
 */
class TravelbookViewDestination extends JViewLegacy
{
	function display()
	{
		$app = JFactory::getApplication();

		$doc = JFactory::getDocument();
		$params = $app->getParams();
		$feedEmail = (@$app->getCfg('feed_email')) ? $app->getCfg('feed_email') : 'author';
		$siteEmail = $app->getCfg('mailfrom');
		// Get some data from the model
		JRequest::setVar('limit', $app->getCfg('feed_limit'));
		$category = $this->get('Destination');
		$rows = $this->get('Items');

		$doc->link = JRoute::_(TravelbookHelperRoute::getCategoryRoute($category->id));

		foreach ($rows as $row)
		{
			// strip html from feed item title
			$title = $this->escape($row->title);
			$title = html_entity_decode($title, ENT_COMPAT, 'UTF-8');

			// Compute the tour slug
			$row->slug = $row->alias ? ($row->id . ':' . $row->alias) : $row->id;

			// url link to tour
			$link = JRoute::_(TravelbookHelperRoute::getTourRoute($row->slug, $row->catid));

			// strip html from feed item description text
			// TODO: Only pull fulltext if necessary (actually, just get the necessary fields).
			$description = ($params->get('feed_summary', 0) ? $row->introtext/*.$row->fulltext*/ : $row->introtext);
			$author = $row->created_by_alias ? $row->created_by_alias : $row->author;
			@$date = ($row->created ? date('r', strtotime($row->created)) : '');

			// load individual item creator class
			$item = new JFeedItem();
			$item->title = $title;
			$item->link = $link;
			$item->description = $description;
			$item->date = $date;
			$item->category = $row->category_title;

			$item->author = $author;
			if ($feedEmail == 'site') {
				$item->authorEmail = $siteEmail;
			}
			else {
				$item->authorEmail = $row->author_email;
			}

			// loads item info into rss array
			$doc->addItem($item);
		}
	}
}
