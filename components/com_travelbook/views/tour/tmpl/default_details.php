<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die; ?>

<?php if ($this->params->get('presentation_style', 'tabs')!='plain'):?>
    <?php echo JHtml::_($this->params->get('presentation_style', 'tabs').'.start', 'details-slider-'.$this->item->id, array('useCookie'=>1)); ?>
<?php endif; ?>
<?php foreach ($this->details as $detail) : ?>
    <?php if ($this->params->get('presentation_style', 'tabs')!='plain'):?>
        <?php echo JHtml::_($this->params->get('presentation_style', 'tabs').'.panel', $detail->title, JApplication::stringURLSafe($detail->title).'-options'); ?>
    <?php endif; ?>
    <h4><?php echo $detail->title; ?></h4>
    <?php echo $detail->introtext; ?>
<?php endforeach; ?>
<?php if ($this->params->get('presentation_style', 'tabs')!='plain'):?>
    <?php echo JHtml::_($this->params->get('presentation_style', 'tabs').'.end'); ?>
<?php endif; ?>
