<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

// Create shortcuts to some parameters.
$params = $this->item->params;
$images = json_decode($this->item->images);
$canEdit = $this->item->params->get('access-edit');
$user = JFactory::getUser(); ?>

<div id="travelbook" class="item-page<?php echo $this->pageclass_sfx?>">
<div id="item-<?php echo $this->item->id; ?>">
<!-- Heading -->
<?php if ($this->params->get('show_page_heading', 1)) : ?>
<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
<?php endif; ?>

<!-- Titel -->
<?php if ($params->get('show_title')) : ?>
	<h2>
	<?php if ($params->get('link_titles') && !empty($this->item->readmore_link)) : ?>
		<a href="<?php echo $this->item->readmore_link; ?>">
		<?php echo $this->escape($this->item->title); ?></a>
	<?php else : ?>
		<?php echo $this->escape($this->item->title); ?>
	<?php endif; ?>
	</h2>
<?php endif; ?>
<!-- Category -->
<?php if ($params->get('show_category')) : ?>
	<?php 
	    $title = $this->escape($this->item->category_title);
	    $url = '<a href="'.JRoute::_(TravelbookHelperRoute::getCategoryRoute($this->item->catslug)).'">'.$title.'</a>';
	?>
	<h3 class="category-name"><?php echo ($params->get('link_category') and $this->item->catslug) ? $url : $title; ?></h3>
<?php endif; ?>

<!-- Icons -->
<?php if ($canEdit ||  $params->get('show_print_icon') || $params->get('show_email_icon') || $params->get('show_ical')) : ?>
	<ul class="actions">
	<?php if (!$this->print) : ?>
		<?php if ($params->get('show_print_icon')) : ?>
			<li class="print-icon">
			<?php echo JHtml::_('icon.print_popup',  $this->item, $params); ?>
			</li>
		<?php endif; ?>

		<?php if ($params->get('show_email_icon')) : ?>
			<li class="email-icon">
			<?php echo JHtml::_('icon.email',  $this->item, $params); ?>
			</li>
		<?php endif; ?>

		<?php if ($canEdit) : ?>
			<li class="edit-icon">
			<?php echo JHtml::_('icon.edit', $this->item, $params); ?>
			</li>
		<?php endif; ?>

		<?php if ($params->get('show_ical') && $this->item->numitems && count($this->dates)) :?>
			<li class="ical small">
				<?php echo JHtml::_('icon.icalSmall', $this->item, $this->dates, $params); ?>
			</li>
		<?php endif; ?>
	<?php else : ?>
		<li>
		<?php echo JHtml::_('icon.print_screen',  $this->item, $params); ?>
		</li>
	<?php endif; ?>
	</ul>
<?php endif; ?>

<!-- Intro -->
<?php  if (!$params->get('show_intro')) :
	echo $this->item->event->afterDisplayTitle;
	echo $this->item->event->afterDisplayTourTitle;
endif; ?>

<!-- Plugins -->
<?php echo $this->item->event->beforeDisplayContent; ?>
<?php echo $this->item->event->beforeDisplayTour; ?>

<!-- Text -->
<?php if ($params->get('access-view')):?>
<?php if (isset($images->image_fulltext) and !empty($images->image_fulltext)) : ?>
<?php $imgfloat = (empty($images->float_fulltext)) ? $params->get('float_fulltext') : $images->float_fulltext; ?>
<div class="img-fulltext-<?php echo htmlspecialchars($imgfloat); ?>">
<img
	<?php if ($images->image_fulltext_caption):
		echo 'class="caption"'.' title="' .htmlspecialchars($images->image_fulltext_caption) .'"';
	endif; ?>
	src="<?php echo htmlspecialchars($images->image_fulltext); ?>" alt="<?php echo htmlspecialchars($images->image_fulltext_alt); ?>"/>
</div>
<?php endif; ?>

<div class="strong">
<?php echo $this->item->introtext; ?>
</div>
<?php echo $this->item->fulltext; ?>

<!-- Optional teaser intro text for guests. -->
<?php elseif ($params->get('show_noauth') == true and  $user->get('guest') ) : ?>
	<?php echo $this->item->introtext; ?>
<!-- Optional link to let them register to see the whole tour. -->
	<?php if ($params->get('show_readmore') && $this->item->fulltext != null) :
		$link1 = JRoute::_('index.php?option=com_users&view=login');
		$link = new JURI($link1);?>
		<p class="readmore">
		<a href="<?php echo $link; ?>">
		<?php $attribs = json_decode($this->item->attribs);  ?>
		<?php
		if ($attribs->alternative_readmore == null) :
			echo JText::_('COM_TRAVELBOOK_REGISTER_TO_READ_MORE');
		elseif ($readmore = $this->item->alternative_readmore) :
			echo $readmore;
			if ($params->get('show_readmore_title', 0) != 0) :
			    echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
			endif;
		elseif ($params->get('show_readmore_title', 0) == 0) :
			echo JText::sprintf('COM_TRAVELBOOK_READ_MORE_TITLE');
		else :
			echo JText::_('COM_TRAVELBOOK_READ_MORE');
			echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
		endif; ?></a>
		</p>
	<?php endif; ?>
<?php endif; ?>

<!-- Details -->

<?php if ($params->get('show_details', true) && count($this->details)) : ?>
    <?php echo $this->loadTemplate('details'); ?>
<?php endif; ?>

<!-- Details -->
<?php $useDefList = (($params->get('show_publish_date')) || ($params->get('show_hits'))); ?>

<?php if ($useDefList) : ?>
	<ul class="tour-info">
<?php endif; ?>
<!-- Details - Hits -->
<?php if ($params->get('show_hits')) : ?>
	<li class="hits">
	<?php echo JText::sprintf('COM_TRAVELBOOK_TOUR_HITS', $this->item->hits); ?>
	</li>
<?php endif; ?>
<!-- Details - Published Date -->
<?php if ($params->get('show_publish_date')) : ?>
	<li class="published">
	<?php echo JText::sprintf('COM_TRAVELBOOK_PUBLISHED_DATE_ON', JHtml::_('date', $this->item->publish_up, JText::_('DATE_FORMAT_TB1'))); ?>
	</li>
<?php endif; ?>
<?php if ($useDefList) : ?>
	</ul>
<?php endif; ?>

<!-- Pagination -->
<?php
if (!empty($this->item->pagination) AND $this->item->pagination AND $this->item->paginationposition AND $this->item->paginationrelative):
	 echo $this->item->pagination;?>
<?php endif; ?>
</div>
</div>

<!-- Plugin -->
<?php echo $this->item->event->afterDisplayContent; ?>
<?php echo $this->item->event->afterDisplayTour; ?>