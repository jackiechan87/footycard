<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

JLoader::register('TravelbookHelperIcal', JPATH_ADMINISTRATOR.'/components/com_travelbook/helpers/ical.php');

echo TravelbookHelperIcal::toIcal(array($this->item), $this->dates);