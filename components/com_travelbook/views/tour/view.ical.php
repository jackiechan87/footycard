<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * HTML Tour View class for the Travelbook component
 *
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @since		2.0
 */
class TravelbookViewTour extends JViewLegacy
{
	protected $item;
	protected $dates;
	protected $params;
	protected $ical;
	protected $state;
	protected $user;

	function display($tpl = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$userId = $user->get('id');
		$dispatcher	= JDispatcher::getInstance();

		$this->item = $this->get('item');
		$this->ical = JRequest::getBool('ical');
		$this->state = $this->get('state');
		$this->user = $user;
		$this->dates = $this->get('dates');
		
		// Check for errors.
		if (count($errors = $this->get('errors'))) {
			JError::raiseWarning(500, implode("\n", $errors));

			return false;
		}

		// Create a shortcut for $item.
		$item = &$this->item;

		// Add dates.
		$dates = &$this->dates;
		
		// Add router helpers.
		$item->slug = $item->alias ? ($item->id.':'.$item->alias) : $item->id;
		$item->catslug = $item->category_alias ? ($item->catid.':'.$item->category_alias) : $item->catid;
		$item->parent_slug = $item->category_alias ? ($item->parent_id.':'.$item->parent_alias) : $item->parent_id;

		// TODO: Change based on shownoauth
		$item->readmore_link = JRoute::_(TravelbookHelperRoute::getTourRoute($item->slug, $item->catslug));

		// Merge tour params. If this is single-tour view, menu params override tour params
		// Otherwise, tour params override menu item params
		$this->params = $this->state->get('params');
		$active	= $app->getMenu()->getActive();
		$temp = clone ($this->params);

		// Check to see which parameters should take priority
		if ($active)
		{
			$currentLink = $active->link;
			if (strpos($currentLink, 'view=tour') && (strpos($currentLink, '&id='.(string) $item->id)))
			{
				$item->params->merge($temp);
				if (isset($active->query['layout']))
				{
					$this->setLayout($active->query['layout']);
				}
			} else {
				$temp->merge($item->params);
				$item->params = $temp;
			}
		} else {
			$temp->merge($item->params);
			$item->params = $temp;
			if ($layout = $item->params->get('tour_layout'))
			{
				$this->setLayout($layout);
			}
		}

		// Check the view access to the tour (the model has already computed the values).
		if ($item->params->get('access-view') != true && (($item->params->get('show_noauth') != true &&  $user->get('guest') )))
		{
			JError::raiseWarning(403, JText::_('JERROR_ALERTNOAUTHOR'));

			return;
		}

		if ($item->params->get('show_intro', '1')=='1') {
			$item->text = $item->introtext.' '.$item->fulltext;
		}
		elseif ($item->fulltext) {
			$item->text = $item->fulltext;
		}
		else  {
			$item->text = $item->introtext;
		}

		parent::display($tpl);
	}

}