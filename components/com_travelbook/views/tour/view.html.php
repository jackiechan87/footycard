<?php
/**
 * @version		$Id$
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * HTML Tour View class for the Travelbook component
 *
 * @package		Travelbook.Site
 * @subpackage	com_travelbook
 * @since		2.0
 */
class TravelbookViewTour extends JViewLegacy
{
	protected $item;
	protected $dates;
	protected $details;
	protected $params;
	protected $print;
	protected $state;
	protected $user;

	function display($tpl = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$userId = $user->get('id');
		$dispatcher	= JDispatcher::getInstance();

		$this->item = $this->get('item');
		$this->print = JRequest::getBool('print');
		$this->state = $this->get('state');
		$this->user = $user;
		$this->dates = $this->get('dates');
		$this->details = $this->get('details');
		
		// Check for errors.
		if (count($errors = $this->get('errors'))) {
			JError::raiseWarning(500, implode("\n", $errors));

			return false;
		}

		// Create a shortcut for $item.
		$item = &$this->item;

		// Add the dates.
		$dates = &$this->dates;
		// Add the details.
		$details = &$this->details;
		
		// Add router helpers.
		$item->slug = $item->alias ? ($item->id.':'.$item->alias) : $item->id;
		$item->catslug = $item->category_alias ? ($item->catid.':'.$item->category_alias) : $item->catid;
		$item->parent_slug = $item->category_alias ? ($item->parent_id.':'.$item->parent_alias) : $item->parent_id;

		// TODO: Change based on shownoauth
		$item->readmore_link = JRoute::_(TravelbookHelperRoute::getTourRoute($item->slug, $item->catslug));

		// Merge tour params. If this is single-tour view, menu params override tour params
		// Otherwise, tour params override menu item params
		$this->params = $this->state->get('params');
		$active	= $app->getMenu()->getActive();
		$temp = clone ($this->params);

		// Check to see which parameters should take priority
		if ($active) {
			$currentLink = $active->link;
			// If the current view is the active item and an tour view for this tour, then the menu item params take priority
			if (strpos($currentLink, 'view=tour') && (strpos($currentLink, '&id='.(string) $item->id))) {
				// $item->params are the tour params, $temp are the menu item params
				// Merge so that the menu item params take priority
				$item->params->merge($temp);
				// Load layout from active query (in case it is an alternative menu item)
				if (isset($active->query['layout'])) {
					$this->setLayout($active->query['layout']);
				}
			}
			else {
				// Current view is not a single tour, so the tour params take priority here
				// Merge the menu item params with the tour params so that the tour params take priority
				$temp->merge($item->params);
				$item->params = $temp;

				// Check for alternative layouts (since we are not in a single-tour menu item)
				// Single-tour menu item layout takes priority over alt layout for an tour
				if ($layout = $item->params->get('tour_layout')) {
					$this->setLayout($layout);
				}
			}
		}
		else {
			// Merge so that tour params take priority
			$temp->merge($item->params);
			$item->params = $temp;
			// Check for alternative layouts (since we are not in a single-tour menu item)
			// Single-tour menu item layout takes priority over alt layout for an tour
			if ($layout = $item->params->get('tour_layout')) {
				$this->setLayout($layout);
			}
		}

		$offset = $this->state->get('list.offset');

		// Check the view access to the tour (the model has already computed the values).
		if ($item->params->get('access-view') != true && (($item->params->get('show_noauth') != true &&  $user->get('guest') ))) {
			JError::raiseWarning(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return;
		}

		if ($item->params->get('show_intro', '1')=='1') {
			$item->text = $item->introtext.' '.$item->fulltext;
		}
		elseif ($item->fulltext) {
			$item->text = $item->fulltext;
		}
		else  {
			$item->text = $item->introtext;
		}

		//
		// Process the content plugins.
		//
		JPluginHelper::importPlugin('content');
		$results = $dispatcher->trigger('onContentPrepare', array ('com_travelbook.tour', &$item, &$this->params, $offset));

		$item->event = new stdClass();
		$results = $dispatcher->trigger('onContentAfterTitle', array('com_travelbook.tour', &$item, &$this->params, $offset));
		$item->event->afterDisplayTitle = trim(implode("\n", $results));

		$results = $dispatcher->trigger('onContentBeforeDisplay', array('com_travelbook.tour', &$item, &$this->params, $offset));
		$item->event->beforeDisplayContent = trim(implode("\n", $results));

		$results = $dispatcher->trigger('onContentAfterDisplay', array('com_travelbook.tour', &$item, &$this->params, $offset));
		$item->event->afterDisplayContent = trim(implode("\n", $results));

		//
		// Process the travelbook plugins.
		//
		JPluginHelper::importPlugin('travelbook');
		$results = $dispatcher->trigger('onTourPrepare', array ('com_travelbook.tour', &$item, &$this->params, $offset));
		
		if (count($details)) {
    		$results = $dispatcher->trigger('onDetailPrepare', array ('com_travelbook.tour', &$details, &$this->params, $offset));
    		foreach ($details as $detail) {
    			$detail->text = &$detail->introtext;
    			$results = $dispatcher->trigger('onContentPrepare', array ('com_travelbook.tour', &$detail, &$this->params, $offset));
    		}
		}
		
		$results = $dispatcher->trigger('onTourAfterTitle', array('com_travelbook.tour', &$item, &$this->params, $offset));
		$item->event->afterDisplayTourTitle = trim(implode("\n", $results));

		$results = $dispatcher->trigger('onTourBeforeDisplay', array('com_travelbook.tour', &$item, &$this->params, $offset));
		$item->event->beforeDisplayTour = trim(implode("\n", $results));

		$results = $dispatcher->trigger('onTourAfterDisplay', array('com_travelbook.tour', &$item, &$this->params, $offset));
		$item->event->afterDisplayTour = trim(implode("\n", $results));

		// Increment the hit counter of the tour.
		if (!$this->params->get('intro_only') && $offset == 0) {
			$model = $this->getModel();
			$model->hit();
		}

		//Escape strings for HTML output
		$this->pageclass_sfx = htmlspecialchars($this->item->params->get('pageclass_sfx'));

		$this->_prepareDocument();

		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 */
	protected function _prepareDocument()
	{
		JHtml::stylesheet('com_travelbook/com_travelbook.tour.css', false, true);
		
		$app	= JFactory::getApplication();
		$menus	= $app->getMenu();
		$pathway = $app->getPathway();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();
		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_TRAVELBOOK_VIEW_TOUR_TOUR'));
		}

		$title = $this->params->get('page_title', '');

		$id = (int) @$menu->query['id'];

		// if the menu item does not concern this tour
		if ($menu && ($menu->query['option'] != 'com_travelbook' || $menu->query['view'] != 'tour' || $id != $this->item->id))
		{
			// If this is not a single tour menu item, set the page title to the tour title
			if ($this->item->title) {
				$title = $this->item->title;
			}
			$path = array(array('title' => $this->item->title, 'link' => ''));
			$category = JCategories::getInstance('travelbook.tour')->get($this->item->catid);
			while ($category && ($menu->query['option'] != 'com_travelbook' || $menu->query['view'] == 'tour' || $id != $category->id) && $category->id > 1)
			{
				$path[] = array('title' => $category->title, 'link' => TravelbookHelperRoute::getCategoryRoute($category->id));
				$category = $category->getParent();
			}
			$path = array_reverse($path);
			foreach($path as $item)
			{
				$pathway->addItem($item['title'], $item['link']);
			}
		}

		// Check for empty title and add site name if param is set
		if (empty($title)) {
			$title = $app->getCfg('sitename');
		}
		elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		}
		elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		if (empty($title)) {
			$title = $this->item->title;
		}
		$this->document->setTitle($title);

		if ($this->item->metadesc)
		{
			$this->document->setDescription($this->item->metadesc);
		}
		elseif (!$this->item->metadesc && $this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->item->metakey)
		{
			$this->document->setMetadata('keywords', $this->item->metakey);
		}
		elseif (!$this->item->metakey && $this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}

		if ($app->getCfg('MetaAuthor') == '1')
		{
			$this->document->setMetaData('author', $this->item->author);
		}

		$mdata = $this->item->metadata->toArray();
		foreach ($mdata as $k => $v)
		{
			if ($v)
			{
				$this->document->setMetadata($k, $v);
			}
		}

		// If there is a pagebreak heading or title, add it to the page title
		if (!empty($this->item->page_title))
		{
			$this->item->title = $this->item->title . ' - ' . $this->item->page_title;
			$this->document->setTitle($this->item->page_title . ' - ' . JText::sprintf('PLG_CONTENT_PAGEBREAK_PAGE_NUM', $this->state->get('list.offset') + 1));
		}

		if ($this->print)
		{
			$this->document->setMetaData('robots', 'noindex, nofollow');
		}
	}
}
