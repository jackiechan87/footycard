<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_travelbook
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Travelbook Component Controller
 *
 * @package		Joomla.Site
 * @subpackage	com_travelbook
 * @since		1.5
 */
class TravelbookController extends JControllerLegacy
{
	function __construct($config = array())
	{
		// Tour frontpage Editor pagebreak proxying:
		if (JRequest::getCmd('view') === 'tour' && JRequest::getCmd('layout') === 'pagebreak') {
			$config['base_path'] = JPATH_COMPONENT_ADMINISTRATOR;
		}
		// Tour frontpage Editor tour proxying:
		elseif(JRequest::getCmd('view') === 'tours' && JRequest::getCmd('layout') === 'modal') {
			JHtml::_('stylesheet', 'system/adminlist.css', array(), true);
			$config['base_path'] = JPATH_COMPONENT_ADMINISTRATOR;
		}

		parent::__construct($config);
	}

	/**
	 * Method to display a view.
	 *
	 * @param	boolean			If true, the view output will be cached
	 * @param	array			An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		$cachable = true;

		JHtml::_('behavior.caption');

		// Set the default view name and format from the Request.
		// Note we are using a_id to avoid collisions with the router and the return page.
		// Frontend is a bit messier than the backend.
		$id = JRequest::getInt('a_id');
		$vName = JRequest::getCmd('view', 'categories');
		JRequest::setVar('view', $vName);
		$vLayout = $vName == 'bookingform' ? 'edit' : JRequest::getCmd('layout', 'default');
		JRequest::setVar('layout', $vLayout);

		$user = JFactory::getUser();

		if ($user->get('id') ||
			($_SERVER['REQUEST_METHOD'] == 'POST' &&
				(($vName == 'category' && JRequest::getCmd('layout') != 'blog') || $vName == 'archive' ))) {
			$cachable = false;
		}

		$safeurlparams = array('catid'=>'INT', 'id'=>'INT', 'cid'=>'ARRAY', 'year'=>'INT', 'month'=>'INT', 'limit'=>'INT', 'limitstart'=>'INT',
			'showall'=>'INT', 'return'=>'BASE64', 'filter'=>'STRING', 'filter_order'=>'CMD', 'filter_order_Dir'=>'CMD', 'filter-search'=>'STRING', 'print'=>'BOOLEAN', 'ical'=>'BOOLEAN', 'lang'=>'CMD');

		// Check for edit form.
		if ($vName == 'form' && !$this->checkEditId('com_travelbook.edit.tour', $id)) {
			// Somehow the person just went to the form - we don't allow that.
			return JError::raiseError(403, JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
		}

		// Check for edit form.
		if ($vName == 'bookingform' && !$this->checkEditId('com_travelbook.edit.booking', $id)) {
			// Somehow the person just went to the form - we don't allow that.
			return JError::raiseError(403, JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
		}

		// add Stylesheet
		JHtml::stylesheet('com_travelbook/com_travelbook.css', false, true);
		
		parent::display($cachable, $safeurlparams);

		return $this;
	}

	function search()
	{
		$jform = JRequest::getVar('jform', array(), 'post');
	    
		$post['limit'] = JRequest::getInt('limit', null, 'post');
		if ($post['limit'] === null) unset($post['limit']);

		// set Itemid id for links from menu
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$items = $menu->getItems('link', 'index.php?option=com_travelbook&view=search&searchword=');

		if(isset($items[0])) {
			$post['Itemid'] = $items[0]->id;
		} elseif (JRequest::getInt('Itemid') > 0) { //use Itemid from requesting page only if there is no existing menu
			$post['Itemid'] = JRequest::getInt('Itemid');
		}

		$searchtype = JRequest::getWord('searchtype', 'advanced', 'post');
		$post['searchtype'] = $searchtype;

		$post['view'] = JRequest::getWord('view', 'search', 'post');
		
		$post['advanced'] = isset($jform['advanced']) ? $jform['advanced'] : array();
		$post['keyword'] = isset($jform['keyword']) ? $jform['keyword'] : array();
		$post['expert'] = isset($jform['expert']) ? $jform['expert'] : array();
		
		
		unset($post['task']);
		unset($post['submit']);

		$uri = JURI::getInstance();
		$uri->setQuery($post);
		$uri->setVar('option', 'com_travelbook');


		$this->setRedirect(JRoute::_('index.php'.$uri->toString(array('query', 'fragment')), false));
	}
}
