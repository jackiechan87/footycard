<?php
/*------------------------------------------------------------------------
# view.html.php - Manage Trading Component
# ------------------------------------------------------------------------
# author    Khuong Tran
# copyright Copyright (C) 2015. All Rights Reserved
# license   www.chienluocweb.com - Tran Hoai Khuong
# website   chienluocweb.com
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import Joomla view library
jimport('joomla.application.component.view');
/**
 * HTML Trading View class for the Manage Trading Component
 */
class TradingViewtrading extends JView
{
	// Overwriting JView display method
	function display($tpl = null)
	{
		parent::display($tpl);
	}
}
?>