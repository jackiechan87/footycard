<?php
/*------------------------------------------------------------------------
# default.php - Manage Trading Component
# ------------------------------------------------------------------------
# author    Khuong Tran
# copyright Copyright (C) 2015. All Rights Reserved
# license   www.chienluocweb.com - Tran Hoai Khuong
# website   chienluocweb.com
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

?>
<div id="trading-content">
	<p><strong>Trading_code</strong>: <?php echo $this->item->tradingcode; ?></p>
	<p><strong>User_id</strong>: <?php echo $this->item->userid; ?></p>
	<p><strong>Created-date</strong>: <?php echo $this->item->createddate; ?></p>
	<p><strong>Status</strong>: <?php echo $this->item->status; ?></p>
	<p><strong>Accepted-date</strong>: <?php echo $this->item->accepteddate; ?></p>
	<p><strong>Trade-what</strong>: <?php echo $this->item->tradewhat; ?></p>
	<p><strong>Trade-for</strong>: <?php echo $this->item->tradefor; ?></p>
	<p><strong>Custom-field-1</strong>: <?php echo $this->item->customfield1; ?></p>
	<p><strong>Custom-field-2</strong>: <?php echo $this->item->customfield2; ?></p>
	<p><strong>Custom-field-3</strong>: <?php echo $this->item->customfield3; ?></p>
</div> 