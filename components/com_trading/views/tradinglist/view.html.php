<?php
/*------------------------------------------------------------------------
# view.html.php - Manage Trading Component
# ------------------------------------------------------------------------
# author    Khuong Tran
# copyright Copyright (C) 2015. All Rights Reserved
# license   www.chienluocweb.com - Tran Hoai Khuong
# website   chienluocweb.com
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML Trading List View class for the Trading Component
 */
class TradingViewtradinglist extends JView
{
	// Overwriting JView display method
	function display($tpl = null)
	{
		// Assign data to the view
		$this->item = $this->get('Item');

		// Check for errors.
		if (count($errors = $this->get('Errors'))){
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		};

		// Display the view
		parent::display($tpl);
	}
}
?>