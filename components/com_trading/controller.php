<?php
/*------------------------------------------------------------------------
# controller.php - Manage Trading Component
# ------------------------------------------------------------------------
# author    Khuong Tran
# copyright Copyright (C) 2015. All Rights Reserved
# license   www.chienluocweb.com - Tran Hoai Khuong
# website   chienluocweb.com
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');
//jimport('joomla.application.component.model');
JLoader::import('tradinglist', JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_trading' . DS . 'models');

/**
 * Trading Component Controller
 */
class TradingController extends JController
{
    
        public function submitTrade()
        {
            $user = JFactory::getUser();
//            if () 
            if (empty($_SESSION['checkList'])
                    || empty($_SESSION['tradeList'])
                    || empty($_SESSION['checkListQuantity'])
                    || empty($_SESSION['tradeListQuantity'])
                    || empty($user)) {
                
                $view = $this->getView('trading', 'html'); // get the view
                $view->assign('result', false);
                $view->assign('error', array('message' => 'Your cart is empty. Please select some cards to continue.'));
                JFactory::getApplication()->enqueueMessage('Your cart is empty. Please select some cards to continue.', 'error');
                $view->display();
                return;
            }
            $tradingCode = substr(md5($user->username . time()), 0, 10);
            $data = array(
                'userid' => $user->id,
                'tradingcode' => $tradingCode,
                'tradewhat' => json_encode($_SESSION['checkList']),
                'tradefor' => json_encode($_SESSION['tradeList']),
                'customfield1' => json_encode($_SESSION['checkListQuantity']),
                'customfield2' => json_encode($_SESSION['tradeListQuantity']),
                'status' => 'new',
                'createddate' => date('Y-m-d'),
            );
            $model = new TradingModeltradinglist();
            $result = $model->save($data);
            $view = $this->getView('trading', 'html'); // get the view
            $view->assign('result', $result);
            if ($result) {
                $view->assign('data', $data); // set the data to the view
                unset($_SESSION['checkList']);
                unset($_SESSION['tradeList']);
                unset($_SESSION['checkListQuantity']);
                unset($_SESSION['tradeListQuantity']);
                JFactory::getApplication()->enqueueMessage(
                        JText::_( 'Congratulation! You have successfully submitted order.')
                        . '<br/>'
                        .JText::_('Your order code is: ' . $data['tradingcode']), 'message');
            } else {
                $view->assign('error', $model->getError());
            }
            
            $view->display(); // show the view
        }
        
        public function showHistory()
        {
            $user = JFactory::getUser();
            $model = new TradingModeltradinglist();
            $data = $model->getItemsByUserId($user->id);
            
            $view = $this->getView('trading', 'html'); // get the view
            $view->assign('data', $data);
            $view->display('showHistory');
        }
        
        public function resetCart()
        {
            unset($_SESSION['checkList']);
            unset($_SESSION['tradeList']);
            unset($_SESSION['checkListQuantity']);
            unset($_SESSION['tradeListQuantity']);
            $app = JFactory::getApplication();
            $app->redirect(JRoute::_(JURI::root()));
        }
}
?>