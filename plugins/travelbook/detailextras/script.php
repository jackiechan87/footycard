<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	PLG_TRAVELBOOK_PAGENAVIGATION
 *
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Script file of sidebar module
 */
class plgtravelbookdetailextrasInstallerScript
{
	/**
	 * method to run before an install/update/uninstall method
	 *
	 * @return void
	 */
	function preflight($type, $parent) {
		$jversion = new JVersion();

		// Installing component manifest file version
		$this->release = $parent->get( "manifest" )->version;

		// Manifest file minimum Joomla version
		$this->minimum_joomla_release = $parent->get( "manifest" )->attributes()->version;

		// Show the essential information at the install/update back-end

		echo JText::sprintf('PLG_TRAVELBOOK_DETAILEXTRAS_PREFLIGHT_INSTALL');
		echo JText::sprintf('PLG_TRAVELBOOK_DETAILEXTRAS_PREFLIGHT_INSTALL_MANIFEST', $this->release);
		if ($this->getParam('version')) {
			echo JText::sprintf('PLG_TRAVELBOOK_DETAILEXTRAS_PREFLIGHT_CURRENT_MANIFEST', $this->getParam('version'));			
		}
		echo JText::sprintf('PLG_TRAVELBOOK_DETAILEXTRAS_PREFLIGHT_MINIMUM_VERSION', $this->release, $this->minimum_joomla_release);
		echo JText::sprintf('PLG_TRAVELBOOK_DETAILEXTRAS_PREFLIGHT_CURRENT_VERSION', $jversion->getShortVersion());

		// abort if the current Joomla release is older
		if( version_compare( $jversion->getShortVersion(), $this->minimum_joomla_release, 'lt' ) ) {
			Jerror::raiseWarning(null, JText::sprintf('PLG_TRAVELBOOK_DETAILEXTRAS_PREFLIGHT_ERROR_MINIMUM_VERSION', $this->release, $this->minimum_joomla_release));
			return false;
		}

		// abort if the component being installed is not newer than the currently installed version
		if ( $type == 'update' ) {
			$oldRelease = $this->getParam('version');
			if (version_compare($this->release, $oldRelease, 'lt')) {
				Jerror::raiseWarning(null, JText::sprintf('PLG_TRAVELBOOK_DETAILEXTRAS_PREFLIGHT_ERROR_VERSION_SEQUENZ', $oldRelease, $this->release));
				return false;
			}
		}

		// Check dependencies
		
		echo JText::sprintf('PLG_TRAVELBOOK_DETAILEXTRAS_PREFLIGHT', $this->release, JText::_($type));
	}

	/**
	 * method to install the component
	 *
	 * @return void
	 */
	function install($parent) {
		echo '<p>' . JText::sprintf('PLG_TRAVELBOOK_DETAILEXTRAS_INSTALL', $this->release) . '</p>';
		// You can have the backend jump directly to the newly installed component configuration page
		// $parent->getParent()->setRedirectURL('index.php?option=PLG_TRAVELBOOK_PAGENAVIGATION');
	}

	/**
	 * method to update the component
	 *
	 * @return void
	 */
	function update($parent) {
		echo '<p>' . JText::sprintf('PLG_TRAVELBOOK_DETAILEXTRAS_UPDATE', $this->release) . '</p>';
		// You can have the backend jump directly to the newly updated component configuration page
		// $parent->getParent()->setRedirectURL('index.php?option=PLG_TRAVELBOOK_PAGENAVIGATION');
	}

	/**
	 * method to run after an install/update/uninstall method
	 *
	 * @return void
	 */
	function postflight($type, $parent) {

		$params = array();

		// always create or modify these parameters
		//		$params['my_param'] = 'Component version ' . $this->release;

		// define the following parameters only if it is an original install
		if ( strtolower($type) == 'install' ) {
//			$params['my_param'] = '4';
		}

		$this->setParams($params);

		echo '<p>' . JText::sprintf('PLG_TRAVELBOOK_DETAILEXTRAS_POSTFLIGHT', $this->release, $type) . '</p>';
	}

	/**
	 * method to install the component
	 *
	 * @return void
	 */
	function uninstall($parent) {
		
		$this->release = $parent->get( "manifest" )->version;
		
		echo '<p>' . JText::sprintf('PLG_TRAVELBOOK_DETAILEXTRAS_UNINSTALL', $this->release) . '</p>';
	}

	/**
	 * get a parameter
	 *
	 * @return string	the value of the parameter
	 */
	function getParam($name) {
		$db = JFactory::getDbo();
		$db->setQuery('SELECT manifest_cache FROM #__extensions WHERE name = "plg_travelbook_detailextras"');
		$manifest = json_decode($db->loadResult(), true);
		return $manifest[$name];
	}

	/**
	 * sets parameter values in the component's row of the extension table
	 *
	 * @return void
	 */
	function setParams($param_array) {
		if ( count($param_array) > 0 ) {
			// read the existing component value(s)
			$db = JFactory::getDbo();
			$db->setQuery('SELECT params FROM #__extensions WHERE name = "plg_travelbook_detailextras"');
			$params = json_decode($db->loadResult(), true);
			// add the new variable(s) to the existing one(s)
			foreach ($param_array as $name => $value) {
				$params[(string) $name] = (string) $value;
			}
			// store the combined new and existing values back as a JSON string
			$paramsString = json_encode($params);
			$db->setQuery('UPDATE #__extensions SET params = ' .
			$db->quote( $paramsString ) .
				' WHERE name = "plg_travelbook_detailextras"' );
			$db->query();
		}
	}
}