<?php
/**
 * @version		$Id$
 * @package		Travelbook.Plugin
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('demopage.currency');

/**
 * Details Helper Dates
 *
 * @package		Travelbook.Plugin
 * @subpackage	com_travelbook
 * @since		2.0
 */
class DetailextrasHelper
{
	/**
	 * String to hold the extras
	 * @access private
	 */
	var $_extras = null;

	/**
	 * String to hold the tour
	 * @access private
	 */
	var $_tour = null;

	var $_output = null;
	var $_template = null;
	
	
	/**
	 * Class constructor
	 *
	 * @param	array	$options	An array of configuration options.
	 *
	 * @return	GmapsHelperGmaps
	 * @since	1.6
	 */
	public function __construct($params)
	{
		// Initialise
		$this->_params = $params;

		// Set Currency
		$cparams = JComponentHelper::getParams( 'com_travelbook' );
		$this->currency = new DPCurrency($cparams);

		$config = JFactory::getConfig();
		$local = $config->getValue('config.language');
		$local = strpos('en-GB,de-DE', $local) ? $local : 'en-GB';
		$this->_lang = substr($local , 0, 2);
	}

	/**
	 * Method to get the Dates of a Tour.
	 *
	 * @param	string	The tour id.
	 * @return	mixed	Object on success, false on failure.
	 */
	public function getExtras($tour_id)
	{
		// Load the data
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$user	= JFactory::getUser();
		
		if (empty( $this->_extras )) {
		  $query->select('a.*, te.rate AS rate_absolute');
		  
		  $query->from('#__tb_extras AS a');
		  
		  $query->join('LEFT', '#__tb_tours_extras as te ON te.id_2=a.id');
		  
		  $query->where('te.id_1=' . (int) $tour_id);
		  $query->where('a.state=1');
		  $query->where('te.state=1');

		  // Implement View Level Access
		  if (!$user->authorise('core.admin')) {
		    $groups	= implode(',', $user->getAuthorisedViewLevels());
		    $query->where('a.access IN ('.$groups.')');
		  }
		  
		  $query->order('te.type ASC, te.ordering ASC');

		  $db->setQuery( $query );
			$this->_extras = $db->loadObjectList();
		}

		if (!$this->_extras) {
			return false;
		}

		return $this->_extras;
	}

	/**
	 * Method to get the Tour.
	 *
	 * @param	string	The tour id.
	 * @return	mixed	Object on success, false on failure.
	 */
	public function getTour($pk)
	{
		// Load the data
		$db = JFactory::getDBO();

		if (empty( $this->_tour )) {
			$query = 	'SELECT * '.
						'FROM #__tb_tours '.
						'WHERE id ='.(int)$pk.';';

			$db->setQuery( $query );
			$this->_tour = $db->loadObject();
		}

		if (!$this->_tour) {
			return false;
		}

		return $this->_tour;
	}

	public function &renderExtras($tour, $extras)
	{
		$document = JFactory::getDocument();
		$document->addStyleSheet('media/plg_travelbook_detailextras/css/layout.css');

		$this->_template = JPath::find(dirname(__FILE__) . '/tmpl', 'list.php');
		if ($this->_template != false)
		{
			// Never allow a 'this' property
			if (isset($this->this))
			{
				unset($this->this);
			}
			// Start capturing output into a buffer
			ob_start();
	
			// Include the requested template filename in the local scope
			include $this->_template;
	
			// Done with the requested template; get the buffer and clear it.
			$this->_output = ob_get_contents();
			ob_end_clean();
			
			return $this->_output;
		}
		else
		{
			return JError::raiseError(500, JText::sprintf('JLIB_APPLICATION_ERROR_LAYOUTFILE_NOT_FOUND', $file));
		}				
	}
}