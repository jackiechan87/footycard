<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die; ?>

<?php 
JHtml::_('behavior.tooltip');

$title = $this->_params->get('title') ? $this->_params->get('title').'&nbsp;' : '';
$title .= $this->_params->get('addTour', false) ? $tour->title : '';

$categories = JCategories::getInstance('travelbook.calculation', array());
$pricingPlans = $categories->get()->getChildren(false);

$statesTitle = array(0 => 'TBINCLUDED', 1 => 'TBEXCLUDED', 2 => 'TBOPTIONAL', 3 => 'TBFACULTATIV');
$statesDesc = array(0 => 'TBINCLUDED_DESC', 1 => 'TBEXCLUDED_DESC', 2 => 'TBOPTIONAL_DESC', 3 => 'TBFACULTATIV_DESC');
?>

<?php echo $title ? '<div class="tb-extras-title">'.$title.'</div>' : ''; ?>
<div class="reference"><?php echo $this->_params->get('reference') ? JTEXT::_('PLG_TRAVELBOOK_DETAILEXTRAS_TOUR_CODE') . $tour->reference : ''; ?></div>

<?php $type = -1; ?>
<ul style="display: none; ">
<?php foreach ($extras as $extra) : ?>
	<?php if ($extra->type !== $type): ?>
		</ul>
			<h5>
			<?php echo $this->_params->get('show_tooltips', false) ? JHTML::tooltip(JTEXT::_($statesDesc[$extra->type]), JTEXT::_($statesTitle[$extra->type]), 'plg_travelbook_detailextras/tooltip.png', '') : ''; ?>
			<?php echo JTEXT::_($statesTitle[$extra->type]); ?>
			</h5>
		<ul>
	<?php endif; ?>	
	<li>
		<span class="text"><?php echo strip_tags($extra->introtext); ?></span>
		<?php if (in_array($extra->type, array(2))) : ?>
			<span class="rate"><?php echo $this->currency->calculate($extra->rate_absolute); ?></span>
			<?php if ($extra->pricing_plan) : ?>
				<?php $pricingPlanArray = array(); ?>
				<?php foreach (json_decode($extra->pricing_plan) as $pricingPlan) : ?>
					<?php $pricingPlanArray[] = JTEXT::_($categories->get((int)$pricingPlan)->title); ?>
				<?php endforeach; ?>
				<span class="pricing-plan">
				<?php echo implode(', ', $pricingPlanArray); ?>
				</span>
			<?php endif; ?>
		<?php endif; ?>
	</li>
	<?php $type = $extra->type; ?>
	<?php endforeach; ?>
</ul>
