<?php
/**
 * @version		<svn_id>
 * @package		Travelbook.Plugin
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

require_once JPATH_SITE.'/components/com_travelbook/router.php';
require_once JPATH_SITE.'/components/com_travelbook/models/dates.php';

/**
 * Travelbook Search plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	Search.content
 * @since		1.6
 */
class plgTravelbookSearchtour extends JPlugin
{
	/**
	 * @return array An array of search areas
	 */
	function onTravelbookSearchAreas()
	{
		static $areas = array(
			'tour' => 'COM_TRAVELBOOK_SEARCHAREA_TOURS'
			);
			return $areas;
	}

	/**
	 * Tour Search method
	 * The sql must return the following fields that are used in a common display
	 * routine: href, title, section, created, text, browsernav
	 * @param string Target search string
	 * @param string mathcing option, exact|any|all
	 * @param string ordering option, newest|oldest|popular|alpha|category
	 * @param mixed An array if the search it to be restricted to areas, null if search all
	 */
	function onTravelbookKeywordSearch($text, $phrase='', $ordering='')
	{
		$db = JFactory::getDbo();
		$app = JFactory::getApplication();

		require_once JPATH_SITE.'/components/com_travelbook/helpers/route.php';
		require_once JPATH_SITE.'/administrator/components/com_travelbook/helpers/search.php';

		$searchText = $text;

//		if (is_array($areas)) {
//			if (!array_intersect($areas, array_keys($this->onTravelbookSearchAreas()))) {
//				return array();
//			}
//		}

		$sTour = $this->params->get('travelbook_searchtour', 1);
		$sArchived = $this->params->get('travelbook_searcharchived', 1);
		$limit = $this->params->def('search_limit', 50);

		$text = trim($text);
		if ($text == '') {
			return array();
		}
		
		$joins = array();
		$wheres = array();
		switch ($phrase) {
			case 'exact':
				$text = $db->Quote('%'.$db->getEscaped($text, true).'%', false);
				$wheres2 = array();
				$wheres2[] = 'a.title LIKE '.$text;
				$wheres2[] = 'a.introtext LIKE '.$text;
				$wheres2[] = 'a.fulltext LIKE '.$text;
				$wheres2[] = 'a.note LIKE '.$text;
				$wheres2[] = 'a.metakey LIKE '.$text;
				$wheres2[] = 'a.metadesc LIKE '.$text;
				$where	 = '(' . implode(') OR (', $wheres2) . ')';
				break;
			case 'all':
			case 'any':
			default:
				$words = explode(' ', $text);
				$wheres = array();
				foreach ($words as $word) {
					$word = $db->Quote('%'.$db->getEscaped($word, true).'%', false);
					$wheres2 = array();
					$wheres2[] = 'a.title LIKE '.$word;
					$wheres2[] = 'a.subtitle LIKE '.$word;
					$wheres2[] = 'a.introtext LIKE '.$word;
					$wheres2[] = 'a.fulltext LIKE '.$word;
					$wheres2[] = 'a.note LIKE '.$word;
					$wheres2[] = 'a.metakey LIKE '.$word;
					$wheres2[] = 'a.metadesc LIKE '.$word;
					$wheres[] = implode(' OR ', $wheres2);
				}
				$where = '(' . implode(($phrase == 'all' ? ') AND (' : ') OR ('), $wheres) . ')';
				break;
		}

		$morder = '';
		switch ($ordering) {
			case 'popular':
				$order = 'a.hits DESC';
				break;

			case 'alpha':
				$order = 'a.title ASC';
				break;

			case 'category':
				$order = 'c.title ASC, a.title ASC';
				$morder = 'a.title ASC';
				break;

			case 'rate':
			default:
				$order = 'minRate ASC';
				break;
		}

		$rows = array();
		
		// search tours
		if ($sTour && $limit > 0)
		{
			$query = $this->getListQuery($where, $joins, $order, '1');
			
// 		    echo nl2br(str_replace('#__','cvzps_',$query));

			$db->setQuery($query, 0, $limit);
			$list = $db->loadObjectList('id');

			$limit -= count($list);

			if (isset($list))
			{
				foreach($list as $key => $item)
				{
					$list[$key]->href = TravelbookHelperRoute::getTourRoute($item->slug, $item->catslug);
				}
			}
			$rows[] = $list;
		}

		// search archived content
		if ($sArchived && $limit > 0)
		{
			$query = $this->getListQuery($where, $joins, $order, '2');
			
			$db->setQuery($query, 0, $limit);
			$list3 = $db->loadObjectList('id');

			// find an itemid for archived to use if there isn't another one
			$item = $app->getMenu()->getItems('link', 'index.php?option=com_travelbook&view=archive', true);
			$itemid = isset($item) ? '&Itemid='.$item->id : '';

			if (isset($list3))
			{
				foreach($list3 as $key => $item)
				{
					$date = JFactory::getDate($item->created);

					$created_month = $date->format("n");
					$created_year = $date->format("Y");

					$list3[$key]->href = JRoute::_('index.php?option=com_travelbook&view=archive&year='.$created_year.'&month='.$created_month.$itemid);
				}
			$rows[] = $list3;
			}
		}

		$results = array();
		if (count($rows))
		{
			foreach($rows as $row)
			{
				$new_row = array();
				foreach($row AS $key => $tour) {
					if (SearchHelper::checkNoHTML($tour, $searchText, array('text', 'title', 'metadesc', 'metakey', 'note'))) {
						$new_row[$key] = $tour;
					}
				}
				$results = $results + $new_row;
			}
		}

		return $results;
	}

	/**
	 * Tour Advanced Search method
	 * The sql must return the following fields that are used in a common display
	 * routine: href, title, section, created, text, browsernav
	 * @param string Target search string
	 * @param string mathcing option, exact|any|all
	 * @param string ordering option, newest|oldest|popular|alpha|category
	 * @param mixed An array if the search it to be restricted to areas, null if search all
	 */
	function onTravelbookAdvancedSearch($criterias, $ordering='', $relevance=100)
	{
		$db = JFactory::getDbo();
		$app = JFactory::getApplication();

		require_once JPATH_SITE.'/components/com_travelbook/helpers/route.php';
		require_once JPATH_SITE.'/administrator/components/com_travelbook/helpers/search.php';

		$searchCriterias = $criterias;
//		if (is_array($areas)) {
//			if (!array_intersect($areas, array_keys($this->onTravelbookSearchAreas()))) {
//				return array();
//			}
//		}

		$sTour = $this->params->get('travelbook_searchtour', 1);
		$sArchived = $this->params->get('travelbook_searcharchived', 1);
		$limit = $this->params->def('search_limit', 50);

		$joins = array();
		$wheres = array();
		// Destinations
		if ($criterias['destination']) {
			$categories = JCategories::getInstance('Travelbook.destination', array());
			$parent = $categories->get((int)$criterias['destination']);
			$children = $parent->getChildren(true);
			$familyDestination = array();
			$familyDestination[] = $parent->id == 'root' ? 0 : $parent->id;
			foreach ($children as $child) {
				$familyDestination[] = $child->id;
			}
			$joins[] = '#__categories AS dest ON dest.id IN ('.implode(',', $familyDestination).')';
		}
		
		// Activities
		if ($criterias['activity']) {
			$categories = JCategories::getInstance('Travelbook.activity', array());
			$parent = $categories->get((int)$criterias['activity']);
			$children = $parent->getChildren(false);
			$familyActivity = array();
			$familyActivity[] = $parent->id == 'root' ? 0 : $parent->id;
			foreach ($children as $child) {
				$familyActivity[] = $child->id;
			}
			$joins[] = '#__categories AS act ON act.id IN ('.implode(',', $familyActivity).')';
		}
		
		// Categories
		if ($criterias['category']) {
			$categories = JCategories::getInstance('Travelbook.tour', array());
			$parent = $categories->get((int)$criterias['category']);
			$children = $parent->getChildren(true);
			$familyCategory = array();
			$familyCategory[] = $parent->id == 'root' ? 0 : $parent->id;
			foreach ($children as $child) {
				$familyCategory[] = $child->id;
			}
			$wheres[] = 'a.catid IN ('.implode(',', $familyCategory).')';
		}
		
		// departure
		$departure = $criterias['departure'] ? new JDate($criterias['departure']) : new JDate();
		$wheres[] = 'd.departure>='.$db->Quote($departure->format('Y-m-d'));
		// arrival
		$arrival = $criterias['arrival'] ? new JDate($criterias['arrival']) : new JDate('2100-12-31');
		$wheres[] = 'd.arrival<='.$db->Quote($arrival->format('Y-m-d'));
		// duration
		$wheres[] = $criterias['duration-from'] ? 'd.duration>='.(int)$criterias['duration-from'] : 'd.duration>=0';
		$wheres[] = $criterias['duration-to'] ? 'd.duration<='.(int)$criterias['duration-to'] : 'd.duration<=99999';
		// budget
		$wheres[] = $criterias['budget-from'] ? 'd.rack_rate>='.(int)$criterias['budget-from'] : 'd.rack_rate>=0';
		$wheres[] = $criterias['budget-to'] ? 'd.rack_rate<='.(int)$criterias['budget-to'] : 'd.rack_rate<=99999';
				
		$where = '(' . implode(') AND (' , $wheres) . ')';
		
		$morder = '';
		switch ($ordering) {
			case 'popular':
				$order = 'a.hits DESC';
				break;

			case 'alpha':
				$order = 'a.title ASC';
				break;

			case 'category':
				$order = 'c.title ASC, a.title ASC';
				$morder = 'a.title ASC';
				break;

			case 'rate':
			default:
				$order = 'minRate ASC';
				break;
		}

		$rows = array();

		// search tours
		if ($sTour && $limit > 0)
		{
			$query = $this->getListQuery($where, $joins, $order, '1');
//    		echo nl2br(str_replace('#__','cvzps_',$query));
			$db->setQuery($query, 0, $limit);
			$list = $db->loadObjectList('id');
			$this->attachDates($list);
			$this->attachRelevances($list, $criterias);

			$limit -= count($list);

			if (isset($list))
			{
				foreach($list as $key => $item)
				{
					$list[$key]->href = TravelbookHelperRoute::getTourRoute($item->slug, $item->catslug);
				}
			}
			$rows[] = $list;
		}

		// search archived content
		if ($sArchived && $limit > 0)
		{
			$query = $this->getListQuery($where, $joins, $order, '2');

			$db->setQuery($query, 0, $limit);
			$list3 = $db->loadObjectList('id');
			$this->attachRelevances($list3, $criterias);
			
			// find an itemid for archived to use if there isn't another one
			$item = $app->getMenu()->getItems('link', 'index.php?option=com_travelbook&view=archive', true);
			$itemid = isset($item) ? '&Itemid='.$item->id : '';

			if (isset($list3))
			{
				foreach($list3 as $key => $item)
				{
					$date = JFactory::getDate($item->created);

					$created_month = $date->format("n");
					$created_year = $date->format("Y");

					$list3[$key]->href = JRoute::_('index.php?option=com_travelbook&view=archive&year='.$created_year.'&month='.$created_month.$itemid);
				}
				$rows[] = $list3;
			}

		}

		$results = array();
		if (count($rows))
		{
			foreach($rows as $row)
			{
				$new_row = array();
				foreach($row AS $key => $tour) {
					if ($tour->relevance >= (int)($relevance)/100) {
						$new_row[] = $tour;
					}
				}
				$results = array_merge($results, (array) $new_row);
			}
		}

		return $results;
	}

	/**
	 * Tour Expert Search method
	 * The sql must return the following fields that are used in a common display
	 * routine: href, title, section, created, text, browsernav
	 * @param string Target search string (reference)
	 * @param string mathcing option, exact|any|all
	 * @param string ordering option, newest|oldest|popular|alpha|category|relevance
	 * @param mixed An array if the search it to be restricted to areas, null if search all
	 */
	function onTravelbookSearchExpert($text, $phrase='', $ordering='')
	{
		$db = JFactory::getDbo();
		$app = JFactory::getApplication();

		require_once JPATH_SITE.'/components/com_travelbook/helpers/route.php';
		require_once JPATH_SITE.'/administrator/components/com_travelbook/helpers/search.php';

		$searchText = $text;
		if (is_array($areas)) {
			if (!array_intersect($areas, array_keys($this->onTravelbookSearchAreas()))) {
				return array();
			}
		}

		$sTour = $this->params->get('travelbook_searchtour', 1);
		$sArchived = $this->params->get('travelbook_searcharchived', 1);
		$limit = $this->params->def('search_limit', 50);

		$text = trim($text);
		if ($text == '') {
			return array();
		}

		$joins = array();
		$wheres = array();
		switch ($phrase) {
			case 'exact':
				$text = $db->Quote('%'.$db->getEscaped($text, true).'%', false);
				$wheres2 = array();
				$wheres2[] = 'a.reference LIKE '.$text;
				$where	 = '(' . implode(') OR (', $wheres2) . ')';
				break;

			case 'all':
			case 'any':
			default:
				$words = explode(' ', $text);
				$wheres = array();
				foreach ($words as $word) {
					$word = $db->Quote('%'.$db->getEscaped($word, true).'%', false);
					$wheres2 = array();
					$wheres2[] = 'a.reference LIKE '.$word;
					$wheres[] = implode(' OR ', $wheres2);
				}
				$where = '(' . implode(($phrase == 'all' ? ') AND (' : ') OR ('), $wheres) . ')';
				break;
		}

		$morder = '';
		switch ($ordering) {
			case 'popular':
				$order = 'a.hits DESC';
				break;

			case 'alpha':
				$order = 'a.title ASC';
				break;

			case 'category':
				$order = 'c.title ASC, a.title ASC';
				$morder = 'a.title ASC';
				break;

			case 'rate':
			default:
				$order = 'minRate ASC';
				break;
		}

		$rows = array();
		
		// search articles
		if ($sTour && $limit > 0)
		{
			$query = $this->getListQuery($where, $joins, $order, '1');
			
			$db->setQuery($query, 0, $limit);
			$list = $db->loadObjectList('id');

			$limit -= count($list);

			if (isset($list))
			{
				foreach($list as $key => $item)
				{
					$list[$key]->href = TravelbookHelperRoute::getTourRoute($item->slug, $item->catslug);
				}
			}
			$rows[] = $list;
		}

		// search archived content
		if ($sArchived && $limit > 0)
		{
			$query = $this->getListQuery($where, $joins, $order, '2');
			
			$db->setQuery($query, 0, $limit);
			$list3 = $db->loadObjectList('id');

			// find an itemid for archived to use if there isn't another one
			$item = $app->getMenu()->getItems('link', 'index.php?option=com_travelbook&view=archive', true);
			$itemid = isset($item) ? '&Itemid='.$item->id : '';

			if (isset($list3))
			{
				foreach($list3 as $key => $item)
				{
					$date = JFactory::getDate($item->created);

					$created_month = $date->format("n");
					$created_year = $date->format("Y");

					$list3[$key]->href = JRoute::_('index.php?option=com_travelbook&view=archive&year='.$created_year.'&month='.$created_month.$itemid);
				}
			$rows[] = $list3;
			}
		}

		$results = array();
		if (count($rows))
		{
			foreach($rows as $row)
			{
				$new_row = array();
				foreach($row AS $key => $tour) {
					if (SearchHelper::checkNoHTML($tour, $searchText, array('reference'))) {
						$new_row[] = $tour;
					}
				}
				$results = array_merge($results, (array) $new_row);
			}
		}

		return $results;
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	protected function getListQuery($where='TRUE', $joins=array(), $order='a.created DESC', $published='1')
	{
		// Create a new query object.
		$app = JFactory::getApplication();
		
		$db = JFactory::getDbo();

		$user = JFactory::getUser();
		$groups = implode(',', $user->getAuthorisedViewLevels());

		$tag = JFactory::getLanguage()->getTag();
		
		$nullDate = $db->getNullDate();
		$date = JFactory::getDate();
		$now = $date->toMySQL();
		
		$query = $db->getQuery(true);
		$query->clear();

		$query->select('a.id AS id, a.title AS title, a.catid, a.reference AS reference, a.metadesc, a.metakey, a.created AS created, '
					.'CONCAT(a.introtext, a.fulltext) AS text, c.title AS section, '
					.'a.attribs AS attribs, '
					.'a.destination, a.activity, a.style, '
					.'a.note AS note, '
					.'a.featured, '
					.'a.hits, '
					.'a.images, '
					.'1 AS relevance, '
					.'"tour" AS source, '
					.'"2" AS browsernav, '
					.'CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug, '
					.'c.title AS category, '
					.'CASE WHEN CHAR_LENGTH(c.alias) THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as catslug, '
					.'CASE WHEN CHAR_LENGTH(dc.alias) THEN CONCAT_WS(":", dc.id, dc.alias) ELSE dc.id END as datecatslug, '
					.'count(d.id) AS dates, '
					.'min(d.duration) AS minDuration, '
					.'max(d.duration) AS maxDuration, '
					.'min(d.rack_rate) AS minRate, '
					.'max(d.rack_rate) AS maxRate ' );
		$query->from('#__tb_tours AS a');

		$query->innerJoin('#__categories AS c ON c.id = a.catid');
		$query->innerJoin('#__tb_dates AS d ON d.TID = a.id AND d.access IN ('. $groups .')');
		$query->innerJoin('#__categories AS dc ON dc.id = d.catid');
		
//		$query->where('('.$where.')');
		$query->where('d.available > 0 AND '
					.'d.departure > NOW() AND '
					.'a.state='.(int)$published.' AND c.published='.(int)$published.' AND d.state=1 '
					.'AND a.access IN ('.$groups.') AND c.access IN ('.$groups.') AND d.access IN ('.$groups.') '
					.'AND (a.publish_up = '.$db->Quote($nullDate).' OR a.publish_up <= '.$db->Quote($now).') '
					.'AND (a.publish_down = '.$db->Quote($nullDate).' OR a.publish_down >= '.$db->Quote($now).')'
					.'AND (d.publish_up = '.$db->Quote($nullDate).' OR d.publish_up <= '.$db->Quote($now).') '
					.'AND (d.publish_down = '.$db->Quote($nullDate).' OR d.publish_down >= '.$db->Quote($now).')' );

		$query->group('a.id');

		$query->order($order);

		// Filter by language
		if ($app->isSite() && $app->getLanguageFilter()) {
			$query->where('a.language in (' . $db->Quote($tag) . ',' . $db->Quote('*') . ')');
			$query->where('c.language in (' . $db->Quote($tag) . ',' . $db->Quote('*') . ')');
			$query->where('d.language in (' . $db->Quote($tag) . ',' . $db->Quote('*') . ')');
		}
	
//		echo nl2br(str_replace('#__','cvzps_',$query));
		return $query;
	}

	/**
	 * Attach all dates to a tour
	 * @return data to be used later
	 */
	private function attachDates(&$list)
	{
		$dates = New TravelbookModelDates();
		$seasons = $dates->getItems();

		foreach ($seasons as $season ) {
			if ( array_key_exists($season->TID, $list) ) {
				$departure = new JDate($season->departure);
				$arrival = new JDate($season->arrival);

				$list[(int)$season->TID]->departure[] = $departure->toUnix();
				$list[(int)$season->TID]->arrival[] = $arrival->toUnix();
				$list[(int)$season->TID]->rate[] = $season->rate;
				$list[(int)$season->TID]->duration[] = $season->duration;
			}
		}
	}

	/**
	 * Calculate and attach value for relevance to the object
	 * @return data to be used later
	 */
	private function attachRelevances(&$list, $criterias)
	{

		if ( isset( $criterias['departure'] ) ) {
			$departure = new JDate($criterias['departure']);
			$departure = $departure->toUnix();
		}
		
		if ( isset( $criterias['arrival'] ) ) {
			$arrival = new JDate($criterias['arrival']);
			$arrival = $arrival->toUnix();
		}

		if (count($criterias)) {
			foreach ($list AS $element) {
				$element->relevance = 0;
				$attribs = new JRegistry($element->attribs);
				$numberOfCriterias = 0;

				// destination
				if ($criterias['destination']) {
					$numberOfCriterias++;
					$destination = json_decode($element->destination, true);
					if ( in_array($criterias['destination'], $destination) ) {
						$element->relevance++;
					}
				}

				// activity
				if ($criterias['activity']) {
					$numberOfCriterias++;
					$activity = json_decode($element->activity, true);
					if ( in_array($criterias['activity'], $activity) ) {
						$element->relevance++;
					}
				}

				// dates
//				if (isset($criterias['departure'] != '' && $criterias['arrival'] != '') {
				if ($criterias['departure'] && $criterias['arrival']) {
					$numberOfCriterias++;
					$numberOfCriterias++;
					// run through all dates(departure and arrival)
					foreach ($element->departure as $key => $elementDeparture) {
						if ( $departure <= $elementDeparture &&  $arrival >= $element->arrival[$key] ) {
							$element->relevance++;
							$element->relevance++;
							break;
						}
					}
				}

				// category
				if ($criterias['category']) {
					$numberOfCriterias++;
					if ( $criterias['category'] == $element->catid ) {
						$element->relevance++;
					}
				}

				// duration
				if ( ( $criterias['duration-from'] != 0 ) && ( $criterias['duration-to'] != 99999 ) ) {
					$numberOfCriterias++;
					$numberOfCriterias++;
					foreach ($element->duration as $duration) {
						if ( ($criterias['duration-from'] <= $duration) && ($criterias['duration-to'] >= $duration) ) {
							$element->relevance++;
							$element->relevance++;
							break;
						}
					}
				}

				// budget
				if ( ($criterias['budget-from'] != 0) && ($criterias['budget-to'] != 99999) ) {
					$numberOfCriterias++;
					$numberOfCriterias++;
					foreach ($element->rate as $rate) {
						if ( ($criterias['budget-from'] <= $rate) && ($criterias['budget-to'] >= $rate) ) {
							$element->relevance++;
							$element->relevance++;
							break;
						}
					}
				}

				// calc relevance
				if ($numberOfCriterias) {
					$element->relevance = $element->relevance/$numberOfCriterias;
				} else {
					$element->relevance = 1;
				}

			}
		}
	}
}