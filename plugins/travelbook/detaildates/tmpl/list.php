<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die; ?>

<?php 
JHtml::_('behavior.tooltip');

$title = $this->_params->get('title') ? $this->_params->get('title').'&nbsp;' : '';
$title .= $this->_params->get('addTour', false) ? $tour->title : '';
$ratio = 1;
$i = 0;
?>

<?php echo $title ? '<div class="tb-dates-title">'.$title.'</div>' : ''; ?>
<table class="travelbook rates dates">
	<thead>
		<tr>
			<td width="15%"><strong><?php echo JText::_('PLG_TRAVELBOOK_DETAILDATES_TOUR_ID'); ?></strong></td>
			<td width="18%"><strong><?php echo JText::_('PLG_TRAVELBOOK_DETAILDATES_FROM'); ?></strong></td>
			<td width="18%"><strong><?php echo JText::_('PLG_TRAVELBOOK_DETAILDATES_TO'); ?></strong></td>
			<td align="right"><strong>
				<?php echo $this->_params->get('show_tooltips', false) ? JHTML::tooltip(JTEXT::_('PLG_TRAVELBOOK_DETAILDATES_PRICE_DESC'), JTEXT::_('PLG_TRAVELBOOK_DETAILDATES_PRICE'), 'plg_travelbook_detaildates/tooltip.png', '') : ''; ?>
				<?php echo JText::_('PLG_TRAVELBOOK_DETAILDATES_PRICE'); ?>
			</strong></td>
			<td align="right"><strong>
				<?php echo $this->_params->get('show_tooltips', false) ? JHTML::tooltip(JTEXT::_('PLG_TRAVELBOOK_DETAILDATES_SINGLE_SUPPLEMENT_DESC'), JTEXT::_('PLG_TRAVELBOOK_DETAILDATES_SINGLE_SUPPLEMENT'), 'plg_travelbook_detaildates/tooltip.png', '') : ''; ?>
				<?php echo JText::_('PLG_TRAVELBOOK_DETAILDATES_SINGLE_SUPPLEMENT'); ?>
			</strong></td>
			<?php if ($this->_params->get('show_available', true)) : ?>
				<td width="10%" align="center"><strong>
					<?php echo JText::_('PLG_TRAVELBOOK_DETAILDATES_AVAILABLE'); ?>
				</strong></td>
			<?php endif; ?>
			<?php if (true) : ?>
				<td width="14%" align="right"><strong>&nbsp;</strong></td>
			<?php endif; ?>
		</tr>
	</thead>

	<tbody>
		<?php foreach ($dates as $date) : ?>
			<?php $i++; ?>
			<?php $class = ($i%2) ? 'even' : 'odd'; ?>
			<tr class="<?php echo $class; ?>">
				<td class="reference"><?php 
					$reference = '';
					$reference .= ($this->_params->get('reference')=='both' || $this->_params->get('reference')=='tour' ) ? $tour->reference : '';
					$reference .= ($this->_params->get('reference')=='both' &&  $tour->reference && $date->reference) ? $this->_params->get('seperator') : '';
					$reference .= ($this->_params->get('reference')=='both' || $this->_params->get('reference')=='date' ) ? $date->reference : '';
					echo $reference; ?>
				</td>
				<td class="departure"><?php echo JHtml::_('date', $date->departure, JText::_('DATE_FORMAT_TB3')); ?></td>
				<td class="arrival"><?php echo JHtml::_('date', $date->arrival, JText::_('DATE_FORMAT_TB3')); ?></td>
				<td class="rate" align="right"><?php echo $this->currency->calculate($date->rack_rate); ?></td>
				<td class="single" align="right"><?php echo $date->single_rate > 0 ? $this->currency->calculate($date->single_rate) : ''; ?></td>
				<?php if ($this->_params->get('show_available', true)) : ?>
					<?php if ((max($date->available, $date->size) - $date->available) > $date->minimum) 
					{
						$ratio = $date->available > 0 ? min(3, ceil(3*(min($date->available, $date->size))/$date->size)) : 0;						
					} else {
						$ratio = $date->available > 0 ? max(3,ceil(3*(min($date->available, $date->size))/$date->size)) : 0;						
					}
					?>
					<td class="available" align="center"><div class="ampel ampel-<?php echo $ratio; ?>">&nbsp;</div></td>
				<?php endif; ?>
				<?php if ($ratio) : ?>
					<?php $startDate = JFactory::getDate($date->departure); ?>
					<?php $nowDate = JFactory::getDate(); ?>
					<?php $startDate = strtotime($date->departure); ?>
					<?php $nowDate = strtotime(JFactory::getDate()); ?>
					<td class="book" align="right">
						<?php if ($startDate > $nowDate) : ?>
							<?php if ($date->available) : ?>
								<a href="<?php echo JRoute::_(TravelbookHelperRoute::getBookingRoute($date->id, $tour->id)); ?>"><?php echo JText::_('PLG_TRAVELBOOK_DETAILDATES_SINGLE_BOOK_NOW'); ?></a>
							<?php else : ?>
								<span><?php echo JText::_('PLG_TRAVELBOOK_DETAILDATES_SINGLE_SOLD_OUT'); ?></span>
							<?php endif; ?>
						<?php endif; ?>
					</td>
				<?php endif; ?>
			</tr>
		
			<tr class="<?php echo $class; ?>">
			<td class="<?php echo $class; ?>" colspan="7"><em>
			<?php echo $date->note; ?>
			</em></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<?php if ($this->_params->get('show_available', true)) : ?>
	<div class="desc"><?php echo JText::_('PLG_TRAVELBOOK_STATUS_DESCRIPTION'); ?>
	<ul id="status-description">
	<li class="ampel ampel-0"><span class="text"><?php echo JText::_('PLG_TRAVELBOOK_STATUS_FULLY_BOOKED'); ?></span></li>
	<li class="ampel ampel-1"><span class="text"><?php echo JText::_('PLG_TRAVELBOOK_STATUS_PLACES_LEFT'); ?></span></li>
	<li class="ampel ampel-2"><span class="text"><?php echo JText::_('PLG_TRAVELBOOK_STATUS_DEPARTURE_GUARANTEED'); ?></span></li>
	<li class="ampel ampel-3"><span class="text"><?php echo JText::_('PLG_TRAVELBOOK_STATUS_SEATS_AVAILABLE'); ?></span></li>
	</ul>
	</div>
<?php endif; ?>