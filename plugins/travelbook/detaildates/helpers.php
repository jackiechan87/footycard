<?php
/**
 * @version		$Id$
 * @package		Travelbook.Plugin
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('demopage.currency');

/**
 * Details Helper Dates
 *
 * @package		Travelbook.Plugin
 * @subpackage	com_travelbook
 * @since		2.0
 */
class DetaildatesHelper
{
	/**
	 * String to hold the dates
	 * @access private
	 */
	var $_dates = null;

	/**
	 * String to hold the tour
	 * @access private
	 */
	var $_tour = null;

	/**
	 * Class constructor
	 *
	 * @param	array	$options	An array of configuration options.
	 *
	 * @return	GmapsHelperGmaps
	 * @since	1.6
	 */
	public function __construct($params)
	{
		// Initialise
		$this->_params = $params;

		// Set Currency
		$cparams = JComponentHelper::getParams( 'com_travelbook' );
		$this->currency = new DPCurrency($cparams);

		$config = JFactory::getConfig();
		$local = $config->getValue('config.language');
		$local = strpos('en-GB,de-DE', $local) ? $local : 'en-GB';
		$this->_lang = substr($local , 0, 2);
	}

	/**
	 * Method to get the Dates of a Tour.
	 *
	 * @param	string	The tour id.
	 * @return	mixed	Object on success, false on failure.
	 */
	public function getDates($pk)
	{
		// Load the data
		$db = JFactory::getDBO();
		$nowDate = $db->Quote(JFactory::getDate()->toSql());

		if (empty( $this->_dates )) {
			$query = 	'SELECT * '.
						'FROM #__tb_dates '.
						'WHERE (TID=' . (int) $pk . ') AND (state=1) AND (departure>=' . $nowDate . ');';

			$db->setQuery( $query );
			$this->_dates = $db->loadObjectList();
		}

		if (!$this->_dates) {
			return false;
		}

		return $this->_dates;
	}

	/**
	 * Method to get the Tour.
	 *
	 * @param	string	The tour id.
	 * @return	mixed	Object on success, false on failure.
	 */
	public function getTour($pk)
	{
		// Load the data
		$db = JFactory::getDBO();

		if (empty( $this->_tour )) {
			$query = 	'SELECT * '.
						'FROM #__tb_tours '.
						'WHERE id ='.(int)$pk.';';

			$db->setQuery( $query );
			$this->_tour = $db->loadObject();
		}

		if (!$this->_tour) {
			return false;
		}

		return $this->_tour;
	}

	public function &renderDates($tour, $dates)
	{

		$document = JFactory::getDocument();
		$document->addStyleSheet('media/plg_travelbook_detaildates/css/layout.css');

		$this->_template = JPath::find(dirname(__FILE__) . '/tmpl', 'list.php');
		if ($this->_template != false)
		{
			// Never allow a 'this' property
			if (isset($this->this))
			{
				unset($this->this);
			}
			// Start capturing output into a buffer
			ob_start();
	
			// Include the requested template filename in the local scope
			include $this->_template;
	
			// Done with the requested template; get the buffer and clear it.
			$this->_output = ob_get_contents();
			ob_end_clean();
			
			return $this->_output;
		}
		else
		{
			return JError::raiseError(500, JText::sprintf('JLIB_APPLICATION_ERROR_LAYOUTFILE_NOT_FOUND', $file));
		}				
	}
}