<?php
/**
 * @version		$Id$
 * @package		Travelbook.Plugin
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');
require_once JPATH_PLUGINS.'/travelbook/detaildates/helpers.php';

/**
 * Travelbook Search plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	Search.content
 * @since		1.6
 */
class plgTravelbookDetaildates extends JPlugin
{
    /**
     * Constructor
     *
     * @access      protected
     * @param       object  $subject The object to observe
     * @param       array   $config  An array that holds the plugin configuration
     * @since       1.5
     */
    public function __construct(& $subject, $config)
    {
        parent::__construct($subject, $config);
        $this->loadLanguage();
    }

    /**
     * @param	string	The context of the content being passed to the plugin.
     * @param	object	The tour object.  Note $tour->text is also available
     * @param	object	The tour params
     * @param	int		The 'page' number
     *
     * @return	void
     * @since	2.0
     */
    public function onDetailPrepare($context, &$rows, &$params, $page=0)
    {
    	$code = $this->params->get('code', 'dates');
        // expression to search for
        $regex = '/{'.$code.'?}/si';
        
        $tour_id = $rows[0]->TID;
        $dates = new DetaildatesHelper($this->params);
        $tour = $dates->getTour($tour_id);
        
        foreach ($rows as $row) {
            if (JString::strpos($row->introtext, $code) === false) {
                continue;
            }
            // get dates
			if ($datesList = $dates->getDates($tour_id)) {
			    $DOM = $dates->renderDates($tour, $datesList);
			} else {
			    $DOM = '';
			}
            $numberMatches = preg_match_all($regex, $row->introtext, $matches, PREG_OFFSET_CAPTURE | PREG_PATTERN_ORDER);
            $datesCode = $matches[0];
            $tag = $this->params->get('tag', 'p');
            $needle = $this->params->get('clean', true) ? '<'.$tag.'>'.$datesCode[0][0].'</'.$tag.'>' : $datesCode[0][0];
            $row->introtext = substr_replace($row->introtext, $DOM, strpos($row->introtext, $needle), strlen($needle) );
        }

        return true;
    }
}