<?php
/**
 * @version		$Id$
 * @package		Mapwork
 * @subpackage	Plugins
 * @copyright	Copyright (C) 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Social Multi Share Helper
 *
 * @package		Mapwork
 * @subpackage	Plugins
 * @since		1.6
 */
class SocialmultishareHelper
{

    private	$_title = '';
    private	$_aspect = '';
    private	$_fields = '';
    private	$_code = '';

    /**
     * Class constructor
     *
     * @param	array	$options	An array of configuration options.
     *
     * @return	MapworkHelperMapwork
     * @since	1.6
     */
    public function __construct($position)
    {
        $this->_position = $position;

        $plugin = JPluginHelper::getPlugin('travelbook', 'socialmultishare');
        $this->_params = new JRegistry($plugin->params);

        $config = JFactory::getConfig();
        $lang = $config->getValue('config.language');
        $lang = strpos('en-GB,de-DE', $lang) ? $lang : 'en-GB';
        $this->_lang = str_replace('-', '_', $lang);
        $this->_local = substr($lang , 0, 2);

        //        $this->meta();
    }

    /**
     * 
     * @param unknown_type $article
     */
    public function setArticle($article)
    {
        $this->_article = $article;
    }

    /**
     * Method to get the Map-Id.
     *
     * @param	string	The list of fields.
     * @return	mixed	Object on success, false on failure.
     */
    public function setRouter($context)
    {
        switch ($context) {
            case 'com_travelbook.tour':
                $this->_route = 'travelbook';
                break;
            case 'com_eventmanager.event':
                $this->_route = 'eventmanager';
                break;
            default:
                $this->_route = 'travelbook';
                break;
        }
    }


    private function isFacebook()
    {
        if ($_SERVER['HTTP_USER_AGENT'] == 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)') {
            return true;
        } else {
            return false;
        }
    }

    private function isGoogle()
    {
        if ($_SERVER['HTTP_USER_AGENT'] == 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.8) Gecko/20050511 Firefox/1.0.4') {
            return true;
        } else {
            return true;
        }
    }

    public function facebookMeta(&$article)
    {
        $document = JFactory::getDocument();
        $conf = JFactory::getConfig();

        $description = $document->getDescription();

        if ($this->isFacebook()) {
            $meta = '<meta property="fb:appid" content="'.$this->_params->get('og_app_id').'" />';
            $document->addCustomTag($meta);
        }

        $document->addScript("http://connect.facebook.net/$this->_lang/all.js#xfbml=1");

    }

    public function googleMeta(&$article)
    {
        $document = JFactory::getDocument();

        $description = $document->getDescription();

        if ($this->isGoogle()) {
            $meta = '<html itemscope itemtype="http://schema.org/'.$this->_params->get('og_type', 'article').'" />';
            $document->addCustomTag($meta);
            $meta = '<meta itemprop="name" content="'.$article->title.'" />';
            $document->addCustomTag($meta);
            $meta = '<meta itemprop="description" content="'.$article->metadesc.'" />';
            $document->addCustomTag($meta);
            $meta = '<meta itemprop="image" content="'.JURI::base().$this->_params->get('og_image').'" />';
            $document->addCustomTag($meta);
        }

    }

    private function addTwitterScript()
    {
        $document = JFactory::getDocument();

        $document->addScript("http://platform.twitter.com/widgets.js");
    }

    private function addGoogleScript()
    {
        $document = JFactory::getDocument();

        $script =
<<<EOD
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
EOD;
        $document->addScriptDeclaration($script);
    }

    public function renderTwitter()
    {
        $url = $this->getPageUrl($this->_article);

        $DOM = '<a href="http://twitter.com/share" class="twitter-share-button" '.
		       'data-lang="'.$this->_local.'" '.
		       'data-via="'.$this->_params->get('data-via', '').'" '.
		       'data-related="'.$this->_params->get('data-related', '').'" '.
		       'data-url="'.$url.'" '.
		       'data-text="'.$this->_article->title.'" '.
		       'data-count="'.$this->_params->get('data-count', '').'" '.
		       '>Tweet</a>';


        return $DOM;
    }

    public function renderFacebook()
    {
        $colorscheme = $this->_params->get('colorscheme', 'light');
        $font = $this->_params->get('font', 'arial');
        $url = $this->getPageUrl($this->_article);
        $layout = $this->_params->get('layout', 'arial');
        $show_faces = $this->_params->get('show_faces', 'false');
        $send = $this->_params->get('send', 'true');
        $action = $this->_params->get('action', 'like');
        $width = $this->_params->get('width', '');

        $DOM = '<div id="fb-root"></div>';

        if ($this->_params->get('standalone', 1)) {
            $DOM .= '<div class="fb-send" '.
                    'data-href="'.$url.'" '.
                    'data-font="'.$font.'">'.
                    '</div>';
        } else {
            $DOM .= '<div class="fb-like" '.
                    'data-href="'.$url.'" '.
                    'data-send="'.$send.'" '.
                    'data-layout="'.$layout.'" '.
                    'data-width="'.(int)$width.'" '.
                    'data-show-faces="'.$show_faces.'" '.
                    'data-font="'.$font.'">'.
                    '</div>';
        }

        return $DOM;
    }

    public function renderGoogle()
    {
        $dataSize = $this->_params->get('size', 'arial');
        $dataAnnotation = $this->_params->get('annotation', 'standard');

        $DOM ='<div class="g-plusone" data-size="'.$dataSize.'" data-annotation="'.$dataAnnotation.'"></div>';

        return $DOM;
    }

    public function &render($article)
    {

		JHtml::stylesheet('mod_travelbook/plg_travelbook_socialmultishare.css', false, true);
        JHtml::stylesheet('mod_travelbook/custom.css', false, true);

        $this->setArticle($article);

        $render = '<div class="sociallinks head">';
        $render .= '<ul class="sociallinks '.strtolower($this->_position).'">';
        if ($this->_params->get('twitter') ) {

            $this->addTwitterScript();

            $render .= '<li class="twitter">';
            $render .= $this->renderTwitter();
            $render .= '</li>';
        }
        if ($this->_params->get('google') ) {
            $this->addGoogleScript();
            $render .= '<li class="google">';
            $render .= $this->renderGoogle();
            $render .= '</li>';
        }
        if ($this->_params->get('facebook') ) {
            $render .= '<li class="facebook">';
            $render .= $this->renderFacebook();
            $render .= '</li>';
        }
        $render .= '</ul>';
        $render .= '</div>';

        return $render;
    }

    private function getPageUrl($obj)
    {
        if (empty($obj->catslug)){
            $obj_id = $obj->id;
            $obj_catid = $obj->catid;
        } else {
            $obj_id = $obj->slug;
            $obj_catid = $obj->catslug;
        }
        
        $uri = JURI::getInstance();
		$base = $uri->toString(array('scheme', 'host', 'port'));

        switch ($this->_route) {
            case 'travelbook':
                $url = $base.JRoute::_(TravelbookHelperRoute::getTourRoute($obj_id, $obj_catid));
                break;
            case 'eventmanager':
                $url = $base.JRoute::_(EventmanagerHelperRoute::getEventRoute($obj_id, $obj_catid));
                break;
            default:
                $url = $base.JRoute::_(ContentHelperRoute::getArticleRoute($obj_id, $obj_catid));
                break;
        }

        return $url;
    }
}