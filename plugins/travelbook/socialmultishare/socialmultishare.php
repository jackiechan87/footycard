<?php
/**
 * @version		$Id$
 * @package		Social Multi Share
 * @subpackage	Plugins
 * @copyright	Copyright (C) 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');
require_once dirname(__FILE__).'/helpers.php';

/**
 * Social Multi Share Content Plugin
 *
 * @package		Gmap
 * @subpackage	Plugins
 * @since		1.6
 */
class plgTravelbookSocialMultiShare extends JPlugin
{

    protected $_position = 'afterDisplay';

    /**
     * Constructor
     *
     * @access      protected
     * @param       object  $subject The object to observe
     * @param       array   $config  An array that holds the plugin configuration
     * @since       1.5
     */
    public function __construct(& $subject, $config)
    {
        parent::__construct($subject, $config);
        $this->loadLanguage();

        $this->_position = $this->params->get('position', 'afterDisplay' );

        if ($this->_position) {
            $this->_helper = new socialmultishareHelper($this->_position);
        }

    }

    /**
     * Gmap after display content method
     *
     * Method is called by the view and the results are imploded and displayed in a placeholder
     *
     * @param	string		The context for the content passed to the plugin.
     * @param	object		The content object.  Note $article->text is also available
     * @param	object		The content params
     * @param	int			The 'page' number
     * @return	string
     * @since	1.6
     */
    public function onTourAfterDisplay($context, &$row, &$params, $page=0)
    {
        $app = JFactory::getApplication();

        if ($this->_position == 'afterDisplay' || $this->_position == 'both') {
            $this->_helper->setRouter($context);
            $this->_helper->facebookMeta($row);
            $this->_helper->googleMeta($row);
            return $this->_helper->render($row);
        } else {
            return '';
        }
    }

    /**
     * Gmap before display content method
     *
     * Method is called by the view and the results are imploded and displayed in a placeholder
     *
     * @param	string		The context for the content passed to the plugin.
     * @param	object		The content object.  Note $article->text is also available
     * @param	object		The content params
     * @param	int			The 'page' number
     * @return	string
     * @since	1.6
     */
    public function onTourBeforeDisplay($context, &$row, &$params, $page=0)
    {
        $app = JFactory::getApplication();

        if ($this->_position == 'beforeDisplay' || $this->_position == 'both') {
            $this->_helper->setRouter($context);
            $this->_helper->facebookMeta($row);
            $this->_helper->googleMeta($row);
            return $this->_helper->render($row);
        } else {
            return '';
        }
    }

    /**
     * Gmap before display content method
     *
     * Method is called by the view and the results are imploded and displayed in a placeholder
     *
     * @param	string		The context for the content passed to the plugin.
     * @param	object		The content object.  Note $article->text is also available
     * @param	object		The content params
     * @param	int			The 'page' number
     * @return	string
     * @since	1.6
     */
    public function onTourAfterTitle($context, &$row, &$params, $page=0)
    {
        $app = JFactory::getApplication();
        return '';
    }


    /**
     * Gmap prepare content method
     *
     * Method is called by the view
     *
     * @param	string	The context of the content being passed to the plugin.
     * @param	object	The content object.  Note $article->text is also available
     * @param	object	The content params
     * @param	int		The 'page' number
     * @since	1.6
     */
    public function onTourPrepare($context, &$article, &$params, $page = 0)
    {
        $app = JFactory::getApplication();
        return true;
    }
}