<?php
/**
 * @version		$Id$
 * @package		Travelbook.Plugin
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');
require_once JPATH_PLUGINS.'/travelbook/emails/helpers.php';

/**
 * Travelbook Search plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	Search.content
 * @since		1.6
 */
class plgTravelbookEmails extends JPlugin
{
    /**
     * Constructor
     *
     * @access      protected
     * @param       object  $subject The object to observe
     * @param       array   $config  An array that holds the plugin configuration
     * @since       1.5
     */
    public function __construct(& $subject, $config)
    {
        parent::__construct($subject, $config);
        $this->loadLanguage();
    }

    /**
     * @param	string	The context of the content being passed to the plugin.
     * @param	object	The tour object.  Note $tour->text is also available
     * @param	object	The tour params
     * @param	int		The 'page' number
     *
     * @return	void
     * @since	2.0
     */
    public function onSubmitEmail($context, &$email, &$data)
    {
        $mail = new EmailsHelper($this->params);
        $mail->setData($data);
                
        $mail->setTour($data['DID']);
        $BID = JRequest::getVar('BID', 0, 'get', 'int');
        $mail->setBooking($BID);
        
        $mail->setExtras();
        
        $email->mailbody_plain = $mail->render($email->mailbody_plain);
        
		$email->mailbody_html = $mail->render($email->mailbody_html);
		$email->mailbody_html = $this->emogrify($email->mailbody_html, $email->css);
		
        return true;
    }
    
    /**
     * @param	string	The context of the content being passed to the plugin.
     * @param	object	The tour object.  Note $tour->text is also available
     * @param	object	The tour params
     * @param	int		The 'page' number
     *
     * @return	void
     * @since	2.0
     */
    public function emogrify($html, $css)
    {
		require_once dirname(__FILE__).'/emogrifier.php';
		$emogrifier = new Emogrifier($html, $css);
		
		$result = $emogrifier->emogrify();

		return $result;
    }
}