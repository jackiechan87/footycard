<?php
/**
 * @version		$Id$
 * @package		Travelbook.Plugin
 * @subpackage	com_travelbook
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('demopage.currency');

/**
 * Details Helper Dates
 *
 * @package		Travelbook.Plugin
 * @subpackage	com_travelbook
 * @since		2.0
 */
class EmailsHelper
{
	/**
	 * String to hold the tour
	 * @access private
	 */
	var $_tour = null;
	var $_form = array();
	var $_client = array();
	var $_guests = array();
	
	/**
	 * Class constructor
	 *
	 * @param	array	$options	An array of configuration options.
	 *
	 * @return	GmapsHelperGmaps
	 * @since	1.6
	 */
	public function __construct($params)
	{
		// Initialise
		$this->_params = $params;

		// Set Currency
		$cparams = JComponentHelper::getParams( 'com_travelbook' );
		$this->currency = new DPCurrency($cparams);

		$config = JFactory::getConfig();
		$local = $config->getValue('config.language');
		$local = strpos('en-GB,de-DE', $local) ? $local : 'en-GB';
		$this->_lang = substr($local , 0, 2);
	}

	/**
	 * Method to set the data of the booking.
	 *
	 * @param	object	Booking data.
	 */
	public function setData($data)
	{
		
		$this->_form['pax'] = $data['pax'];
		$this->_form['single'] = $data['single'];
		$this->_form['double'] = $data['double'];
		$this->_form['total'] = $this->currency->calculate($data['total']/100);

		$this->_client = $data['client'];
		$this->_guests = $data['guests'];
		$this->_booked_extras = array_key_exists('extras', $data) ? $data['extras'] : array();
		
	}
	/**
	 * Method to get the Dates of a Tour.
	 *
	 * @param	string	The tour id.
	 * @return	mixed	Object on success, false on failure.
	 */
	public function setExtras()
	{
		// Load the data
		$db = JFactory::getDBO();
		$nowDate = $db->Quote(JFactory::getDate()->toSql());

		if (empty( $this->_extras )) {
			$query = 	'SELECT a.* ' .
						'FROM #__tb_extras AS a ' .
						'LEFT JOIN #__tb_tours_extras as te ON te.id_2=a.id ' .
						'WHERE a.state=1 AND te.state=1 AND te.id_1=' . (int) $this->_tour['id'] . '  AND (a.type IN (0,1,3) OR a.id IN ('. implode(',', $this->_booked_extras).')) ' .
						'ORDER BY a.type, a.catid ASC, ordering ASC';

			$db->setQuery( $query );
			$this->_extras = $db->loadObjectList();
		}

		if (!$this->_extras) {
			return false;
		}

		return true;
	}

	/**
	 * Method to get the Tour.
	 *
	 * @param	string	The tour id.
	 * @return	mixed	Object on success, false on failure.
	 */
	public function setTour($pk)
	{
		// Load the data
		$db = JFactory::getDBO();

		if (empty( $this->_tour )) {
			$query = 	'SELECT t.id, t.title AS title, t.reference AS tour_reference, c.title AS category, d.reference AS date_reference, d.departure, d.duration, d.arrival, d.rack_rate, d.single_rate, d.child_rate ' .
						'FROM #__tb_dates AS d ' .
						'LEFT JOIN #__tb_tours AS t ON t.id = d.TID ' .
						'LEFT JOIN #__categories AS c ON c.id = t.catid ' .
						'WHERE d.id='.(int)$pk.';';

			$db->setQuery( $query );
			$this->_tour = $db->loadAssoc();
		}

		if (!$this->_tour) {
			return false;
		}

		return true;
	}

	/**
	 * Method to get the Tour.
	 *
	 * @param	string	The tour id.
	 * @return	mixed	Object on success, false on failure.
	 */
	public function setBooking($pk)
	{
		// Load the data
		$db = JFactory::getDBO();

		if (empty( $this->_booking )) {
			$query = 	'SELECT b.title AS reference, b.created ' .
						'FROM #__tb_bookings AS b ' .
						'WHERE b.id='.(int)$pk.';';

			$db->setQuery( $query );
			$this->_booking = $db->loadAssoc();
		}

		if (!$this->_booking) {
			return false;
		}

		return true;
	}

	public function &render($body)
	{
        $regex = '/{.*?}/si';
		
		$numberMatches = preg_match_all($regex, $body, $matches, PREG_OFFSET_CAPTURE | PREG_PATTERN_ORDER);
		$occurrences = $matches[0];

		foreach ($occurrences as $occurrence) {
				$needle = $this->getNeedle($occurrence[0]);
				$body = substr_replace($body, $needle, strpos($body, $occurrence[0]), strlen($occurrence[0]));
		}

		return $body;
	}
	
	private function getNeedle($pattern)
	{
		$list = explode('.', trim($pattern, '{}'));
		
		if (count($list) == 2)
		{
			$target = '_' . $list[0];
			$selection = $this->$target;
			$attribute = $selection[$list[1]];
			if (isset($attribute) && is_array($attribute)) {
				$result = implode(', ', $attribute);
			}
			elseif (isset($attribute) && is_string($attribute)) 
			{
				$result = $attribute;
			}
			else {
				$result = '';	
			}
		} 
		elseif ((count($list) == 1) && $list[0] == 'guests' && isset($this->_guests))
		{
			$result = $this->renderGuests();
		}
		elseif ((count($list) == 1) && $list[0] == 'extras' && isset($this->_extras))
		{
			$result = $this->renderExtras();
		}
		else
		{
			$result = '';
		}
		
		return $result;
	}

	private function renderGuests()
	{
		if ($this->_client['mode'] == 'HTML') {
			$result = '<ul>';
			foreach ($this->_guests['selected'] as $key => $selected) {
				$result .= $selected ? '<li>' . ($this->_guests['address'][$key] ? JText::_('COM_TRAVELBOOK_MRS') : JText::_('COM_TRAVELBOOK_MR')) : '';
				$result .= $selected ? ' ' . $this->_guests['lastname'][$key] . ', ' . $this->_guests['firstname'][$key] . ', ' . $this->_guests['date_of_birth'][$key] . '</li>' : '';
			}
			$result .= '</ul>';
		}
		else {
			$result = '';
			foreach ($this->_guests['selected'] as $key => $selected) {
				$result .= $selected ? ($this->_guests['address'][$key] ? JText::_('COM_TRAVELBOOK_MRS') : JText::_('COM_TRAVELBOOK_MR')) : '';
				$result .= $selected ? ' ' . $this->_guests['lastname'][$key] . ', ' . $this->_guests['firstname'][$key] . ', ' . $this->_guests['date_of_birth'][$key] . PHP_EOL : '';
			}
		}

		return $result;
	}
	
	private function renderExtras()
	{
		$statesTitle = array(0 => 'TBINCLUDED', 1 => 'TBEXCLUDED', 2 => 'TBOPTIONAL', 3 => 'TBFACULTATIV');
		
		$html = $this->_client['mode'] == 'HTML';
		$result = $html ? '<ul>' : '';
		foreach ($this->_extras as $extra) {
			$title = $html ? '<span class="title">' . $extra->title . '</span>&nbsp;' : $extra->title . ' ';
			$pricinPlan = $html ? '<span class="pricing-plan">' . $this->getPricingPlan($extra) . '</span>&nbsp;' : $this->getPricingPlan($extra) . ' ';
			$rate = $html ? '<span class="rate">' . $this->currency->calculate($extra->rate) . '</span>&nbsp;' : $extra->rate . ' ';
			$rate = $extra->type == '2' ? $rate : JText::_($statesTitle[$extra->type]);
			$row = $html ? '<li>' . $title . $pricinPlan . $rate . '</li>' : $title . $pricinPlan . $rate . PHP_EOL;
			$result .=  in_array($extra->type, array(0,2,3)) ? $row : '';
		}
		$result .= $html ? '</ul>' : '';

		return $result;
	}

	private function getPricingPlan($extra)
	{
		$categories = JCategories::getInstance('travelbook.calculation', array());
		
		if ($extra->pricing_plan && $extra->type == '2')
		{
			$pricingPlanArray = array();
			foreach (json_decode($extra->pricing_plan) as $pricingPlan)
			{
				$pricingPlanArray[] = JTEXT::_($categories->get((int)$pricingPlan)->title);
			}
			$output = implode(', ', $pricingPlanArray);
		}
		
		return $output;
	}
}