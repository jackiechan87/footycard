<?php
/**
 * @version		$Id$
 * @package		Tourmanager.Administrator
 * @subpackage	com_tourmanager
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Tourmanager Component Currency
 *
 * @package		com_tripmanager
 * @subpackage	site
 */
class DPCurrency
{
	function __construct(JRegistry $params=null) {

		if ($params && is_a($params, 'JRegistry')) {
			$this->_params = $params;
		}
	}

	public function calculate( $value=0, $mailMode=1 )
	{
		$value = number_format ($value, $this->_params->get('decimals',0), $this->_params->get('dec_point',','), $this->_params->get('thousands_sep','.'));

		switch ( $this->_params->get('cur_position','+1') ){
			case '-1':
				return $this->getCurrency($mailMode).' '.$value;
				break;
			case '0':
				return $value;
				break;
			case '+1':
				return $value.' '.$this->getCurrency($mailMode);
				break;
			default:
				return $this->getCurrency($mailMode).' '.$value;
		}
	}

	public function getCurrency($type=1)
	{

		switch ( $this->_params->get('currency','&euro;') ) {
			case 'EUR':
				$currency = $type ? '&euro;' : 'Euro';
				break;
			case 'CHF':
				$currency = $type ? 'CHF' : 'CHF';
				break;
			case 'GBP':
				$currency = $type ? '&pound;' : 'Pound';
				break;
			case 'USD':
				$currency = $type ? '$' : 'Dollar';
				break;
			case 'YEN':
				$currency = $type ? '&yen;' : 'Yen';
				break;
			case 'CUR':
				$currency = $type ? '&curren;' : 'Curren';
				break;
			case 'FLO':
				$currency = $type ? '&fnof;' : 'Fnof';
				break;
			case 'X':
				$currency = $this->_params->get('xcurrency');
				break;
			default:
				$currency = $type ? '&euro;' : 'Euro';
		}
		
		return $currency;
	}
	
	public function getValue( $value = 0 )
	{
		$value = number_format ($value, $this->_params->get('decimals',0), $this->_params->get('dec_point',','), $this->_params->get('thousands_sep','.'));

		return $value;
	}
}