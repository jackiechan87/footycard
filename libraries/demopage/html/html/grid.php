<?php
/**
 * @package     Joomla.Platform
 * @subpackage  HTML
 *
 * @copyright   Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('JPATH_PLATFORM') or die;

/**
 * Utility class for creating HTML Grids
 *
 * @package     Joomla.Platform
 * @subpackage  HTML
 * @since       11.1
 */
abstract class DPHtmlGrid
{
	/**
	 *
	 * @param   string   $title          The link title
	 * @param   string   $order          The order field for the column
	 * @param   string   $direction      The current direction
	 * @param   string   $selected       The selected ordering
	 * @param   string   $task           An optional task override
	 * @param   string   $new_direction  An optional direction for the new column
	 *
	 * @return  string
	 *
	 * @since    11.1
	 */
	public static function sortLinked($title, $order, $direction = 'asc', $selected = 0, $task=NULL, $new_direction='asc')
	{
		$direction	= strtolower($direction);
		$images		= array('sort_asc.png', 'sort_desc.png');
		$index		= intval($direction == 'desc');

		if ($order != $selected) {
			$direction = $new_direction;
		} else {
			$direction	= ($direction == 'desc') ? 'asc' : 'desc';
		}

		$html = '<a href="javascript:linkedTableOrdering(\''.$order.'\',\''.$direction.'\',\''.$task.'\');" title="'.JText::_('JGLOBAL_CLICK_TO_SORT_THIS_COLUMN').'">';
		$html .= JText::_($title);

		if ($order == $selected) {
			$html .= JHtml::_('image','system/'.$images[$index], '', NULL, true);
		}

		$html .= '</a>';

		return $html;
	}

	/**
	 *
	 * @param   string   $title          The link title
	 * @param   string   $order          The order field for the column
	 * @param   string   $direction      The current direction
	 * @param   string   $selected       The selected ordering
	 * @param   string   $task           An optional task override
	 * @param   string   $new_direction  An optional direction for the new column
	 *
	 * @return  string
	 *
	 * @since    11.1
	 */
	public static function sortUnlinked($title, $order, $direction = 'asc', $selected = 0, $task=NULL, $new_direction='asc')
	{
		$direction	= strtolower($direction);
		$images		= array('sort_asc.png', 'sort_desc.png');
		$index		= intval($direction == 'desc');

		if ($order != $selected) {
			$direction = $new_direction;
		} else {
			$direction	= ($direction == 'desc') ? 'asc' : 'desc';
		}

		$html = '<a href="javascript:unlinkedTableOrdering(\''.$order.'\',\''.$direction.'\',\''.$task.'\');" title="'.JText::_('JGLOBAL_CLICK_TO_SORT_THIS_COLUMN').'">';
		$html .= JText::_($title);

		if ($order == $selected) {
			$html .= JHtml::_('image','system/'.$images[$index], '', NULL, true);
		}

		$html .= '</a>';

		return $html;
	}
}