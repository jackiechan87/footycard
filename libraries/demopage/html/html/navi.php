<?php
/**
 * @package     Joomla.Platform
 * @subpackage  HTML
 *
 * @copyright   Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('JPATH_PLATFORM') or die;

/**
 * Utility class for DPNavi elements.
 *
 * @package     Joomla.Platform
 * @subpackage  HTML
 * @version		1.6
 */
abstract class DPHtmlNavi
{
	/**
	 * Creates a panes and creates the JavaScript object for it.
	 *
	 * @param   string  The pane identifier
	 * @param   array   An array of option.
	 *
	 * @return  string
	 *
	 * @since   11.1
	 */
	public static function start($group='tm-tabs', $params=array())
	{
		DPHtmlNavi::_loadBehavior($group, $params);

		return '<dl class="tabs" id="'.$group.'"><dt style="display:none;"></dt><dd style="display:none;">';
	}

	/**
	 * Close the current pane
	 *
	 * @return  string  HTML to close the pane
	 *
	 * @since   11.1
	 */
	public static function end($group='tm-tabs')
	{
		$tabs = '';
//		$tabs = '<ul id="'.$group.'-navi" class="navi">';
//		$tabs .= '<li class="navi navi-left" style="display:none;"><span><h3><a href="javascript:void(0);">'.JText::_('BM_PREVIOUS').'</a></h3></span></li>';
//		$tabs .= '<li class="navi navi-right"><span><h3><a href="javascript:void(0);">'.JText::_('BM_NEXT').'</a></h3></span></li>';
//		$tabs .= '</ul>';
		
		return '</dd></dl>'.$tabs;
	}

	/**
	 * Begins the display of a new panel.
	 *
	 * @param   string  Text to display.
	 * @param   string  Identifier of the panel.
	 *
	 * @return  string  HTML to start a new panel
	 *
	 * @since   11.1
	 */
	public static function panel($text, $id)
	{
		$tabs ='';
		$tabs .= '</dd>';
		$tabs .= '<ul class=\'navi\'>';
		$tabs .= '<li class=\'navi navi-left\'>';
		$tabs .= '<span>';
		$tabs .= '<h3>';
		$tabs .= '<a href="javascript:void(0);">'.JText::_('BM_PREVIOUS').'</a>';
		$tabs .= '</h3>';
		$tabs .= '</span>';
		$tabs .= '</li>';
		$tabs .= '<li class=\'navi navi-right\'>';
		$tabs .= '<span>';
		$tabs .= '<h3>';
		$tabs .= '<a href="javascript:void(0);">'.JText::_('BM_NEXT').'</a>';
		$tabs .= '</h3>';
		$tabs .= '</span>';
		$tabs .= '</li>';
		$tabs .= '</ul>';
		$tabs .= '<dt class="tabs '.$id.'"><span><h3><a href="javascript:void(0);">'.$text.'</a></h3></span></dt><dd class="tabs">';
		
		return $tabs;
	}

	/**
	 * Load the JavaScript behavior.
	 *
	 * @param   string  The pane identifier.
	 * @param   array  Array of options.
	 *
	 * @return  void
	 *
	 * @since   11.1
	 */
	protected static function _loadBehavior($group, $params = array())
	{
		static $loaded = array();

		if (!array_key_exists($group, $loaded))
		{
			// Include MooTools framework
			JHtml::_('behavior.framework', true);

			$options = '{';
			$opt['onActive']			= (isset($params['onActive'])) ? $params['onActive'] : null ;
			$opt['onBackground']		= (isset($params['onBackground'])) ? $params['onBackground'] : null ;
			$opt['display']				= (isset($params['startOffset'])) ? (int)$params['startOffset'] : null ;
			$opt['useStorage']			= (isset($params['useCookie']) && $params['useCookie']) ? 'true' : null ;
			$opt['navi']				= "'ul#".$group."-navi.navi'";
			$opt['titleSelector']		= "'dt.tabs'";
			$opt['descriptionSelector']	= "'dd.tabs'";
			foreach ($opt as $k => $v)
			{
				if ($v) {
					$options .= $k.': '.$v.',';
				}
			}
			if (substr($options, -1) == ',') {
				$options = substr($options, 0, -1);
			}
			$options .= '}';

			$js = '	window.addEvent(\'domready\', function(){
						$$(\'dl#'.$group.'.tabs\').each(function(tabs){
							new DPTabs(tabs, '.$options.');
						});
					});';

			$document = JFactory::getDocument();
			$document->addScriptDeclaration($js);
			JHtml::_('script', 'com_tourmanager/assets/tabs.js', false, true);
			
			$loaded[$group] = true;
		}
	}
}
