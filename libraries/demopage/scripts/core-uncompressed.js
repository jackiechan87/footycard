/**
 * @version		$Id: core-uncompressed.js 21570 2011-06-19 13:47:57Z chdemko $
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// Only define the Joomla namespace if not defined.
if (typeof(Joomla) === 'undefined') {
	var Joomla = {};
}

/**
 * USED IN: libraries/tourmanager/html/html/grid.php
 */
function linkedTableOrdering(order, dir, task) {
	var form = document.linkedForm;

	form.filter_order.value = order;
	form.filter_order_Dir.value = dir;
	Joomla.submitform(task, document.getElementById('linkedForm'));
}

function unlinkedTableOrdering(order, dir, task) {
	var form = document.unlinkedForm;

	form.filter_order.value = order;
	form.filter_order_Dir.value = dir;
	Joomla.submitform(task, document.getElementById('unlinkedForm'));
}

function linkedListItemTask(id, task) {
	var f = document.linkedForm;
	var cb = f[id];
	if (cb) {
		for (var i = 0; true; i++) {
			var cbx = f['cb'+i];
			if (!cbx)
				break;
			cbx.checked = false;
		} // for
		cb.checked = true;
		f.boxchecked.value = 1;
		submitbutton(task);
	}
	return false;
}
