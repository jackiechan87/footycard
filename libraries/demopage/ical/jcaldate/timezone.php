<?php
/**
 * @version		$Id: $
 * @package		JCalPro
 * @subpackage	com_jcalpro

**********************************************
JCal Pro
Copyright (c) 2006-2011 Anything-Digital.com
**********************************************
JCalPro is a native Joomla! calendar component for Joomla!

JCal Pro was once a fork of the existing Extcalendar component for Joomla!
(com_extcal_0_9_2_RC4.zip from mamboguru.com).
Extcal (http://sourceforge.net/projects/extcal) was renamed
and adapted to become a Mambo/Joomla! component by
Matthew Friedman, and further modified by David McKinnis
(mamboguru.com) to repair some security holes.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This header must not be removed. Additional contributions/changes
may be added to this header as long as no information is deleted.
**********************************************
Get the latest version of JCal Pro at:
http://anything-digital.com/
**********************************************

 */

defined('_JEXEC') or die;

jimport('jcaldate.exceptions.timezone');

/**
 * JCalPro DateTimeZone class
 * 
 */
class JCalTimeZone extends DateTimeZone
{
	/**
	 * An array of offsets and time zone strings representing the available
	 * options from Joomla! CMS 1.5 and below.
	 * 
	 * This list was originally borrowed from the 11.1 JDate class
	 * However, we are not extending JDate so we moved it here.
	 * 
	 * Thus far the only time this has been used is when dealing with a 
	 * manually-adjusted configuration, so this should never come up in practice
	 * 
	 * Also of importance - some of these zones have been altered to better ones,
	 * like "UTC" in place of "Europe/London"
	 * 
	 * Finally, if anyone actually ever needs UTC-12, you are out of luck!
	 * 
	 */
	protected static $offsets = array(
		'-12'   => 'Etc/GMT-12'
	,	'-11'   => 'Pacific/Midway'
	,	'-10'   => 'Pacific/Honolulu'
	,	'-9.5'  => 'Pacific/Marquesas'
	,	'-9'    => 'US/Alaska'
	,	'-8'    => 'US/Pacific'
	,	'-7'    => 'US/Mountain'
	,	'-6'    => 'US/Central'
	,	'-5'    => 'US/Eastern'
	,	'-4.5'  => 'America/Caracas'
	,	'-4'    => 'America/Barbados'
	, '-3.5'  => 'Canada/Newfoundland'
	,	'-3'    => 'America/Buenos_Aires'
	,	'-2'    => 'Atlantic/South_Georgia'
	,	'-1'    => 'Atlantic/Azores'
	,	'0'     => 'UTC'
	,	'1'     => 'Europe/Amsterdam'
	,	'2'     => 'Europe/Istanbul'
	,	'3'     => 'Asia/Riyadh'
	,	'3.5'   => 'Asia/Tehran'
	,	'4'     => 'Asia/Muscat'
	,	'4.5'   => 'Asia/Kabul'
	,	'5'     => 'Asia/Karachi'
	,	'5.5'   => 'Asia/Calcutta'
	,	'5.75'  => 'Asia/Katmandu'
	,	'6'     => 'Asia/Dhaka'
	,	'6.5'   => 'Indian/Cocos'
	,	'7'     => 'Asia/Bangkok'
	,	'8'     => 'Australia/Perth'
	,	'8.75'  => 'Australia/West'
	,	'9'     => 'Asia/Tokyo'
	,	'9.5'   => 'Australia/Adelaide'
	,	'10'    => 'Australia/Brisbane'
	,	'10.5'  => 'Australia/Lord_Howe'
	,	'11'    => 'Pacific/Kosrae'
	,	'11.5'  => 'Pacific/Norfolk'
	,	'12'    => 'Pacific/Auckland'
	,	'12.75' => 'Pacific/Chatham'
	,	'13'    => 'Pacific/Tongatapu'
	,	'14'    => 'Pacific/Kiritimati'
	);
	
	public function __construct($timezone) {
		// try to prevent Exceptions if possible
		if ($timezone instanceof DateTimeZone) {
			$timezone = $timezone->getName();
		}
		else if (is_numeric($timezone) && array_key_exists("$timezone", self::$offsets)) {
			$timezone = self::$offsets[$timezone];
		}
		else if (empty($timezone)) {
			$timezone = 'UTC';
		}
		
		try {
			parent::__construct((string) $timezone);
		}
		catch (Exception $e) {
			throw new JCalTimeZoneException($e->getMessage());
		}
	}

	/**
	 * static method to create a new instance in UTC
	 * 
	 * @return JCalTimeZone object
	 */
	static public function utc() {
		static $utc;
		if (is_null($utc)) {
			$utc = new JCalTimeZone('UTC');
		}
		return $utc;
	}
	
	/**
	 * static method to create a new instance in the Joomla! timezone
	 * 
	 * @return JCalTimeZone object
	 */
	static public function joomla() {
		static $joomla;
		if (is_null($joomla)) {
			$joomla = new JCalTimeZone(JFactory::getConfig()->getValue('config.offset'));
		}
		return $joomla;
	}
	
	/**
	 * static method to create a new instance in the user's timezone
	 * 
	 * @return JCalTimeZone object
	 */
	static public function user() {
		static $user;
		if (is_null($user)) {
			$user = new JCalTimeZone(JFactory::getUser()->getParam('timezone', (string) self::joomla()));
		}
		return $user;
	}
	
	/**
	 * converts to the full timezone
	 * 
	 * @return string
	 */
	public function __toString() {
		return $this->getName();
	}
}
