<?php
/**
 * @version		$Id: icon.php 22646 2012-01-06 18:06:19Z github_bot $
 * @package		ical library
 * @subpackage	lib_ical
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access

defined('_JEXEC') or die;

require_once 'iCalcreator.class.php';