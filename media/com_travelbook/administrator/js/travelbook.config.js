window.addEvent('domready', function() {
	Date.defineParser('%d.%m.%Y');
	var oPattern = Date.parsePatterns[Date.parsePatterns.length-1];
	$$('input.calc-date').each(function(item, index) {
		item.addEvent('blur', function() {
			bits = oPattern.re.exec($('jform_departure').get('value'));
			departure = (bits) ? (oPattern.handler(bits)) : false;
			duration = $('jform_duration').get('value').toInt()-1;
			arrival = departure.increment('day', duration).format('%d.%m.%Y');
			$('jform_arrival').set('value', arrival);
		});
	});
});
