window.addEvent('domready', function() {

	calcExtras();
	
	$$('.plus').addEvent('click', function() {
		addPax(1);

		parts = this.id.split('-');
		document.id('guest-' + parts[1]).toggle();
		document.id('summary_guest_' + (1+parts[1].toInt())).toggle();
		
		this.toggle();

		document.id('jform_guests_selected' + parts[1]).value = '1';
		Array.each($$('#guest-' + parts[1] + ' input'), function(el) {
			el.set('disabled', false); 
		});
		
		calcPax(1);
	});
	
	$$('.minus').addEvent('click', function() {
		addPax(-1);

		parts = this.id.split('-');
		document.id('guest-' + parts[1]).toggle();
		document.id('summary_guest_' + (1+parts[1].toInt())).toggle();
		
		document.id('plus-' + parts[1]).toggle();

		// Let's unselect this guest so we do not store it in the booking controller.
		document.id('jform_guests_selected' + parts[1]).value = '0';
		// Let's disable all input fields of this guest so it will not be validated.
		Array.each($$('#guest-' + parts[1] + ' input'), function(el) {
			el.set('disabled', true); 
		});
		
		calcPax(-1);
	});

	// Fire when a extras' checkbox is clicked
	$$('#bookingForm input.cb').addEvent('click', function() {
		calcExtras();
	});

	document.id('jform_utilities_inherit_lastname').addEvent('click', function() {
		lastname = document.id('jform_client_lastname').value;
		if (this.checked) {
			Array.each($$('input.lastname'), function(input) {
				input.value = lastname;
			});
			Array.each($$('span.lastname.html'), function(span) {
				span.set('html', lastname);
			});
		} else {
			Array.each($$('input.lastname'), function(input, index) {
				if (index == 0 && document.id('jform_utilities_hitchhike0').checked) {
					input.value = lastname;
				} else {
					input.value = '';
				}
			});
			Array.each($$('span.lastname.html'), function(span, index) {
				if (index == 0 && document.id('jform_utilities_hitchhike0').checked) {
					span.set('html', lastname);
				} else {
					span.set('html', '');
				}
			});
		}
	});

	$$('.hitchhike').addEvent('click', function(elem) {
		if (document.id(elem.target).value == "1") {
			document.id('jform_guests_address_0-0').checked = document.id('jform_client_address0').checked;
			document.id('jform_guests_address_0-1').checked = document.id('jform_client_address1').checked;
			document.id('jform_guests_firstname_0').value = document.id('jform_client_firstname').value;
			document.id('jform_guests_lastname_0').value = document.id('jform_client_lastname').value;

			if (document.id('jform_client_address0').checked) {
				document.id('jform_guests_address_0_summary').set('html', title[0]);
			} else {
				document.id('jform_guests_address_0_summary').set('html', title[1]);				
			}
			
			document.id('jform_guests_firstname_0_summary').set('html', document.id('jform_client_firstname').value);
			document.id('jform_guests_lastname_0_summary').set('html', document.id('jform_client_lastname').value);
		} 
		if (document.id(elem.target).value == "0") {
			document.id('jform_guests_address_0-0').checked = true;
			document.id('jform_guests_address_0-1').checked = false;
			document.id('jform_guests_firstname_0').value = '';

			document.id('jform_guests_address_0_summary').set('html', title[0]);
			
			if (document.id('jform_utilities_inherit_lastname').checked) {
				document.id('jform_guests_lastname_0').value = document.id('jform_client_lastname').value;				
				document.id('jform_guests_lastname_0_summary').value = document.id('jform_client_lastname').value;				
			} else {
				document.id('jform_guests_lastname_0').value = '';								
				document.id('jform_guests_lastname_0_summary').value = '';								
			}
		}
	});

	$$('.blur').addEvent('blur', function() {
		if (document.id('jform_utilities_inherit_lastname').checked) {
			lastname = document.id('jform_client_lastname').value;
			Array.each($$('input.lastname'), function(input) {
				input.value = lastname;
			});
			Array.each($$('span.lastname.html'), function(span) {
				span.set('html', lastname);
			});
		}
		if (document.id('jform_utilities_hitchhike0').checked) {
			document.id('jform_guests_address_0-0').checked = document.id('jform_client_address0').checked;
			document.id('jform_guests_address_0-1').checked = document.id('jform_client_address1').checked;
			document.id('jform_guests_firstname_0').value = document.id('jform_client_firstname').value;
			document.id('jform_guests_lastname_0').value = document.id('jform_client_lastname').value;

			if (document.id('jform_client_address0').checked) {
				document.id('jform_guests_address_0_summary').set('html', title[0]);
			} else {
				document.id('jform_guests_address_0_summary').set('html', title[1]);				
			}

			document.id('jform_guests_firstname_0_summary').set('html', document.id('jform_client_firstname').value);
			document.id('jform_guests_lastname_0_summary').set('html', document.id('jform_client_lastname').value);
		}
	});

	$$('.click').addEvent('click', function() {
		if (document.id('jform_utilities_hitchhike0').checked) {
			document.id('jform_guests_address_0-0').checked = document.id('jform_client_address0').checked;
			document.id('jform_guests_address_0-1').checked = document.id('jform_client_address1').checked;
			document.id('jform_guests_firstname_0').value = document.id('jform_client_firstname').value;
			document.id('jform_guests_lastname_0').value = document.id('jform_client_lastname').value;

			if (document.id('jform_client_address0').checked) {
				document.id('jform_guests_address_0_summary').set('html', title[0]);
			} else {
				document.id('jform_guests_address_0_summary').set('html', title[1]);				
			}

			document.id('jform_guests_firstname_0_summary').set('html', document.id('jform_client_firstname').value);
			document.id('jform_guests_lastname_0_summary').set('html', document.id('jform_client_lastname').value);
		}
	});
	
	$$('.guests').addEvent('blur', function(elem) {
		parts = elem.target.id.split('-');
		document.id(parts[0] + '_summary').set('html', document.id(elem.target.id).value);
	});

	$$('input.titles').addEvent('click', function(elem) {
		if (document.id(elem.target).value == "0" || document.id(elem.target).value == "1") {
			parts = elem.target.id.split('-');
			document.id(parts[0] + '_summary').set('html', title[document.id(elem.target.id).value]);
		}
	});
});

function addPax(sign) {
	numberGuestsDefault = numberGuestsDefault.toInt()+(sign*1);
}

function calcPax (sign) {
	Array.each($$('.extraFactor.per-person'), function(el) {
		el.set('html', el.get('html').toInt()+(sign*1)); 
	});
	
    formPax = document.id('form-pax');
    if (formPax) {
    	formPax.set('value', formPax.get('value').toInt()+(sign*1));
    }
    
	calcRoom();
}

function calcRoom () {
	Array.each($$('.extraFactor.per-room'), function(el) {
		el.set('html', (Math.ceil(numberGuestsDefault/2)).toInt()); 
	});

	subtotalTour = numberGuestsDefault*rackRate + (numberGuestsDefault%2)*singleRate;
	document.id('tour-rate').getFirst('.value').set('html', subtotalTour.toInt());
	document.id('summary-tour-rate').getFirst('.value').set('html', subtotalTour.toInt());
	
	occupancySingleDisplay = (numberGuestsDefault%2) ? 'block' : 'none';
	document.id('summary-occupancy-single').setStyle('display', occupancySingleDisplay);
	document.id('summary-occupancy-double-pax').set('html', (Math.floor(numberGuestsDefault/2)).toInt());
	document.id('summary-occupancy-single-pax').set('html', (numberGuestsDefault%2));
	document.id('summary-occupancy-single-pax-total').set('html', (numberGuestsDefault%2));

    formSingle = document.id('form-single');
    if (formSingle) {
    	formSingle.set('value', (numberGuestsDefault%2));
    }
    formDouble = document.id('form-double');
    if (formDouble) {
    	formDouble.set('value', (Math.floor(numberGuestsDefault/2)).toInt());
    }
	
	calcExtras();
}

function calcExtras() {
	extrasRate = 0;
	Array.each($$('.calc-extras'), function(el) {
		cb = el.getPrevious('input.cb');
		extraRate = 0;
		if (cb.checked) {
			total = el.getChildren('.extraTotal');
			factors = el.getChildren('.extraFactor');
			factor = 1;
			Array.each(factors, function(f) {
				factor *= f.get('html').toInt();
			});
			extraRate += factor*total[0].get('html').toInt();
			extrasRate += factor*total[0].get('html').toInt();
		}
		document.id(cb.id+'-rate').getFirst('.value').set('html', extraRate);
	});
	
	extraRate = document.id('extras-rate');
    if (extraRate) {
    	extraRate.getFirst('.value').set('html', extrasRate);
    }  
	extrasSubtotal = document.id('extras-subtotal');
    if (extrasSubtotal) {
    	extrasSubtotal.getFirst('.value').set('html', extrasRate);
    }  
	summaryExtrasSubtotal = document.id('summary-extras-subtotal');
    if (summaryExtrasSubtotal) {
    	summaryExtrasSubtotal.getFirst('.value').set('html', extrasRate);
    }  

	subtotalTour = document.id('tour-rate').getFirst('.value').get('html').toInt();
	
	totalRate = document.id('total-rate');
    if (totalRate) {
    	totalRate.getFirst('.value').set('html', extrasRate + subtotalTour);
    }  

    
    formTotalRate = document.id('form-total-rate');
    if (formTotalRate) {
    	formTotalRate.set('value', extrasRate + subtotalTour);
    }
    
    summaryTotalRate = document.id('summary-total-rate');
    if (summaryTotalRate) {
    	summaryTotalRate.getFirst('.value').set('html', extrasRate + subtotalTour);
    }  
	
	calcExtrasSummary();
	renderCurrencies();

	Array.each($$('.guests'), function(elem) {
		parts = elem.id.split('-');
		document.id(parts[0] + '_summary').set('html', document.id(elem.id).value);
	});

	Array.each($$('input.titles'), function(elem) {
		if (elem.checked) {
			parts = elem.id.split('-');
			document.id(parts[0] + '_summary').set('html', title[document.id(elem.id).value]);
		}
	});
}

function calcExtrasSummary() {
	Array.each($$('.calc-extras'), function(el) {
		cb = el.getPrevious('input.cb');
		if (cb.checked) {
			document.id(cb.id+'-summary').setStyle('display', 'block');
		}
		else 
		{
			document.id(cb.id+'-summary').setStyle('display', 'none');
		}
	});
}
