JFormValidator.prototype.partIsValid = function(page) {

	pageInputs = $$('#' + page + ' input');
	valid = true;

	// Validate form fields
	for (i = pageInputs.length - 1; i > 1; i--) {
		if (this.validate(pageInputs[i]) == false) {
			pageInputs[i].focus();
			valid = false;
		}
	}

	return valid;
};

function myValidate(subPage, nextForm, message) {

	if (document.formvalidator.partIsValid(subPage)) {
		collapseElem(subPage);
		expandElem(nextForm);
		return true;
	} else {
		alert(message);
	}
	return false;
}
