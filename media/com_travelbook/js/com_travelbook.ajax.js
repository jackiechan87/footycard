window.addEvent('domready', function() {
	Ajax = Request.extend({
		initialize : function(options) {
			this.parent(options);
			this.ready = false;
			this.output = '';
		},
		success : function(text, xml) {
			this.ready = true;
			this.parent(text, xml);
		},
		onSuccess : function() {
			this.ready = true;
			this.parent();
		}
	});
});

var AJAX_QUEUE = [];

function renderCurrencies() {

	dom = '{';
	Array.each($$('.render.ajax'), function(item, index) {
		value = item.getFirst('.value');
		dom = dom + '"' + item.id + '":' + value.get('html') + ',';
	});
	dom = dom + '}'.replace(',}','}');;
	
	dom = JSON.decode(dom); 
	//"values" is the interface for the booking.json controller
	data = new Object({values: dom});

	var a = new Ajax({
		url : 'index.php?option=com_travelbook&task=booking.render&format=json&tmpl=component',
		method : 'get',
		data : data, 
		onRequest: function(){
			document.id('ajax-failure').set('html', '');
		}, 
		onFailure : function(xhr) {
			document.id('ajax-failure').set('html', xhr.status);
		},
		onSuccess : function(responseText, responseXML) {
			this.output = JSON.decode(responseText, true);
			if (this.output != null) {
				Object.each(this.output, function(value, key) {
					document.id(key).getFirst('.amount').set('html', value);
				});
			}
		},
		onComplete : function() {
		},
		onTimeout : function() {
		}
	});
	AJAX_QUEUE.push(a);
	a.send();
}