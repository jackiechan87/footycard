window.addEvent('domready', function() {

	Array.each($$('.booking-step'), function(container, index) {
		collapseElem(container);
	});
	expandFirst();

});

function collapseElem(obj) {
	document.id(obj).setStyle('display', 'none');
}

function expandElem(obj) {
	document.id(obj).setStyle('display', 'block');
}

function expandFirst() {
	currPageId = ('mainForm_1');
	expandElem(currPageId);
}