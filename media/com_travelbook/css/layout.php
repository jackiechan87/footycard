<?php
header("Content-type: text/css; charset: UTF-8");
define('_JEXEC', 1);
// No direct access.
defined('_JEXEC') or die;
// Set the directory separator
define( 'DS', DIRECTORY_SEPARATOR );
define('JPATH_BASE', dirname(__FILE__) . '/../../..' );
require_once(JPATH_BASE . '/includes/defines.php');
require_once(JPATH_BASE . '/includes/framework.php');
jimport('joomla.database.database');
jimport('joomla.database.table');
$app = JFactory::getApplication('site');
$app->initialise();
// Load component parameters
$params= $app->getParams('com_travelbook');
// Load menu parameters (priority by menu parameters)
if ($itemid = JRequest::getInt('Itemid'))
{
	$menu = JFactory::getApplication()->getMenu();
	$active = $menu->getItem($itemid);
	$params = $menu->getParams( $active->id );
}
// Use a parameter
$color= $params->get('ext_color', '#CC0000');
?>
legend { 
color: <?php echo $color ;?>;
}
